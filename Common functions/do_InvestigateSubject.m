% This function is used to select which subject, data type, testing trial,
% and testing day to analyze. 
function out = do_InvestigateSubject(subNumber, TestingDay, trials, measure)
switch measure % Select which measure to analyze
    case {'torque'}
        plotgraph = 'yes';
       [NetTorqueAvg] = do_TorqueSubjectIndAnalysis(subNumber, TestingDay,trials,plotgraph);
       out = [NetTorqueAvg]

    case{'respiratory'}
       [VO2_gross, VO2_net,order] = do_respirometrycomparison(subNumber, TestingDay,1);
       out  = [VO2_gross, VO2_net,order]
  
    case{'EMG'}
       [graph] = do_EMGSubjectIndAnalysis(subNumber, TestingDay, trials);
       out = graph
end               
end