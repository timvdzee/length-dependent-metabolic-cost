clear all; close all; clc
% this script is used to determine each subject's EMG MVC data by first
% cutting the data and then averaging it.
% this script outputs two .mat files called 'MVCDay1EMGData.mat' and 'MVCDay2EMGData.mat', which
% contains each subject's MVC EMG data and their respective cut intervals
% where the MVC occurs for each day.
%% Day 1 MVC EMG Analysis Parameters
% subject names (according to protocol)
subnames = {'S1','Koen','Billy','Rudi','Jordan','Emily','Brian','Austin','Nick','Eliz','Gaby'};

%Downsample factor
n = 10;

% Create not a number filled variable for each MVC EMG average 
N = length(subnames);
AllMVCRVLEMGAvg = nan(N,6);
AllMVCRVMEMGAvg = nan(N,6);
AllMVCRRFEMGAvg = nan(N,6);
AllMVCLVMEMGAvg = nan(N,6);
AllMVCRBEMGAvg = nan(N,6);

% load previous intervals
Ecut = nan(11,2,6);
load('AllMVCEMGData.mat','Ecut');
Ecut = round(Ecut);

% want to determine new intervals??
redo = 0;
show = 1;
%% Loop through each subject and select cut intervals for day 1
for i = 1:N
    
    if i == 1 % Subject 1 doesn't have EMG
            continue
    else 
                data = csvread([subnames{i},'_AllMVC.csv']);% Call upon which data file to analyze
                if i == 2 % Subject 2 only has 3 muscles EMG data (right leg VL,VM,RRF)
                EMGraw = data(:,4:6);
                else
                EMGraw = data(:,4:8);% All other subjects have all 5 measured muscles
                end       
    end
            t = data(:,1);% Time data
            Traw = data(:,2);% Torque data
            Tt = data(:,3);% Target data
            fs = 1 / mean(diff(t));% Sample frequency

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,5 / (fs*.5),'low');

            if i == 2
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            else
            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
            FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
            end

            % Downsample EMG data
            EMG_RVM = downsample(FiltEMG_RVM,n);
            EMG_RRF = downsample(FiltEMG_RRF,n);
            if i>2
                EMG_RVL = downsample(FiltEMG_RVL,n);
                EMG_LVM = downsample(FiltEMG_LVM,n);
                EMG_RB = downsample(FiltEMG_RB,n);
            end
            
            if redo % Plot figure for each subject
                figure(); 
                plot(EMG_RVM,'r'); hold on 
                plot(EMG_RRF,'g'); hold on
                if i>2
                    plot(EMG_RVL,'k'); hold on
                    plot(EMG_LVM,'y'); hold on 
                    plot(EMG_RB,'b'); hold on 
                end
                Z = subnames{i};
                title('MVC EMG',sprintf('%s',Z));
                for k = 1:6
                [Ecut(i,:,k),~] = ginput(2); 
                end
            end
            
            %Determine Max MVC EMG
            for j = 1:6
            Ecut1 = Ecut(i,:,j)/10; % Get cut intervals
            RVMEMGcut = EMG_RVM(Ecut1(1,1):Ecut1(1,2),:);% Cut data
            [AllMVCRVMEMGAvg(i,j),loc2] = max(RVMEMGcut);% Pull out max MVC value
            RRFEMGcut = EMG_RRF(Ecut1(1,1):Ecut1(1,2),:);% Cut data
            [AllMVCRRFEMGAvg(i,j),loc3] = max(RRFEMGcut);% Pull out max MVC value
            
            if i>2
                RVLEMGcut = EMG_RVL(Ecut1(1,1):Ecut1(1,2),:);% Cut data
                [AllMVCRVLEMGAvg(i,j),loc1] = max(RVLEMGcut);% Pull out max MVC value
                LVMEMGcut = EMG_LVM(Ecut1(1,1):Ecut1(1,2),:);% Cut data
                [AllMVCLVMEMGAvg(i,j),loc4] = max(LVMEMGcut);% Pull out max MVC value
                RBEMGcut = EMG_RB(Ecut1(1,1):Ecut1(1,2),:);% Cut data
                [AllMVCRBEMGAvg(i,j),loc5] = max(RBEMGcut);% Pull out max MVC value
            end
            if show %Plot EMG and Cut            
            figure(i); 
            plot(EMG_RVM,'r'); hold on 
            plot(loc2+Ecut1(1,1), AllMVCRVMEMGAvg(i,j),'ro'); hold on
            plot(EMG_RRF,'g'); hold on 
            plot(loc3+Ecut1(1,1), AllMVCRRFEMGAvg(i,j),'go'); hold on
            if i>2
                plot(EMG_RVL,'k'); hold on
                plot(loc1+Ecut1(1,1), AllMVCRVLEMGAvg(i,j),'ko'); hold on
                plot(EMG_LVM,'y'); hold on 
                plot(loc4+Ecut1(1,1), AllMVCLVMEMGAvg(i,j),'yo'); hold on
                plot(EMG_RB,'b'); hold on 
                plot(loc5+Ecut1(1,1), AllMVCRBEMGAvg(i,j),'bo'); hold on
            end
            Z = subnames{i};
            legend('Vastus Medialis','Vastus Medialis Max','Rectus Femoris','Rectus Femoris Max','Vastus Lateralis','Vastus Lateralis Max');
            xlabel('Sample Number');ylabel('EMG Signal (mV)');
            title('Day 1 MVC EMG Cut Interval Comparison',sprintf('%s',Z));
            end
            end 
end

if redo % Save data
save('AllMVCEMGData','subnames','AllMVCRVLEMGAvg','AllMVCRVMEMGAvg','AllMVCRRFEMGAvg','AllMVCLVMEMGAvg','AllMVCRBEMGAvg','Ecut')
end