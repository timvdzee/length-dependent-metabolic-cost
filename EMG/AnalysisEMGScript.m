clear all; close all; clc
% this script is used to analyze each subject's raw EMG data by filtering, downsampling, and cutting the data.   
% this script outputs two .mat files called 'EMGSummaryAnalysisDay1.mat' and 'EMGSummaryAnalysisDay2.mat', which
% contains the subject name order and each subject's analyzed EMG data for
% the 5 muscles measured.

%% Day 1 EMG Analysis

% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);

% load MVC
load('MVCDay1EMGData.mat')

% some parameters
showfig = true;% Show figures
angles = [10:10:60];% Day 1 angles
Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; %Day 1 filenames order
n = 10; %Downsample factor

% Create not a number filled variable for each EMG average 
Day1RVLEMGAvg = nan(N,length(Day1filenames));
Day1RVMEMGAvg = nan(N,length(Day1filenames));
Day1RRFEMGAvg = nan(N,length(Day1filenames));
Day1LVMEMGAvg = nan(N,length(Day1filenames));
Day1RBEMGAvg = nan(N,length(Day1filenames));

%Resave variables
resave = true

%% Day 1
% loop over subjects
load('TorqueProcDay1.mat') % Load cut interval data for day 1
for i = 1:N
    if i == 2 % Koen has weird EMG data formatting

        % extract variables
        data = csvread('Koen_June21_submax.csv');
        t = data(:,1);% Time
        Traw = data(:,2);% Torque data
        Tt = data(:,3);% Target data
        EMGraw = data(:,4:6);% EMG data
        fs = 1 / mean(diff(t));% Get sample frequency

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);

        %Cut EMG data
        Ecut10 = Exercisecutint{i,6};% Get cut exercise interval
        RVLEMGcut = EMG_RVL(Ecut10(1):Ecut10(2),:);% Cut data
        KoenDay1RVLMVCEMG(1,1) = mean(RVLEMGcut);% Determine mean of cut section
        RVMEMGcut = EMG_RVM(Ecut10(1):Ecut10(2),:);
        KoenDay1RVMMVCEMG(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut10(1):Ecut10(2),:);
        KoenDay1RRFMVCEMG(1,1) = mean(RRFEMGcut); 
        
        Gcut10 = Gravitycutint{i,6};% Get cut interval for gravity measurment
        RVLEMGcut = EMG_RVL(Gcut10(1):Gcut10(2),:);% Cut data
        KoenDay1RVLMVCEMGZero(1,1) = mean(RVLEMGcut);% Determine mean of gravity cut section
        RVMEMGcut = EMG_RVM(Gcut10(1):Gcut10(2),:);
        KoenDay1RVMMVCEMGZero(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut10(1):Gcut10(2),:);
        KoenDay1RRFMVCEMGZero(1,1) = mean(RRFEMGcut);  
        
        Ecut20 = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut20(1):Ecut20(2),:);
        KoenDay1RVLMVCEMG(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut20(1):Ecut20(2),:);
        KoenDay1RVMMVCEMG(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut20(1):Ecut20(2),:);
        KoenDay1RRFMVCEMG(1,2) = mean(RRFEMGcut);
        
        Gcut20 = Gravitycutint{i,2};
        RVLEMGcut = EMG_RVL(Gcut20(1):Gcut20(2),:);
        KoenDay1RVLMVCEMGZero(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut20(1):Gcut20(2),:);
        KoenDay1RVMMVCEMGZero(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut20(1):Gcut20(2),:);
        KoenDay1RRFMVCEMGZero(1,2) = mean(RRFEMGcut);

        Ecut30 = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut30(1):Ecut30(2),:);
        KoenDay1RVLMVCEMG(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut30(1):Ecut30(2),:);
        KoenDay1RVMMVCEMG(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut30(1):Ecut30(2),:);
        KoenDay1RRFMVCEMG(1,3) = mean(RRFEMGcut);
        
        Gcut30 = Gravitycutint{i,5};
        RVLEMGcut = EMG_RVL(Gcut30(1):Gcut30(2),:);
        KoenDay1RVLMVCEMGZero(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut30(1):Gcut30(2),:);
        KoenDay1RVMMVCEMGZero(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut30(1):Gcut30(2),:);
        KoenDay1RRFMVCEMGZero(1,3) = mean(RRFEMGcut);

        Ecut40 = Exercisecutint{i,4};
        RVLEMGcut = EMG_RVL(Ecut40(1):Ecut40(2),:);
        KoenDay1RVLMVCEMG(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut40(1):Ecut40(2),:);
        KoenDay1RVMMVCEMG(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut40(1):Ecut40(2),:);
        KoenDay1RRFMVCEMG(1,4) = mean(RRFEMGcut);
        
        Gcut40 = Gravitycutint{i,4};
        RVLEMGcut = EMG_RVL(Gcut40(1):Gcut40(2),:);
        KoenDay1RVLMVCEMGZero(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut40(1):Gcut40(2),:);
        KoenDay1RVMMVCEMGZero(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut40(1):Gcut40(2),:);
        KoenDay1RRFMVCEMGZero(1,4) = mean(RRFEMGcut);

        Ecut50 = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut50(1):Ecut50(2),:);
        KoenDay1RVLMVCEMG(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut50(1):Ecut50(2),:);
        KoenDay1RVMMVCEMG(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut50(1):Ecut50(2),:);
        KoenDay1RRFMVCEMG(1,5) = mean(RRFEMGcut);
        
        Gcut50 = Gravitycutint{i,1};
        RVLEMGcut = EMG_RVL(Gcut50(1):Gcut50(2),:);
        KoenDay1RVLMVCEMGZero(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut50(1):Gcut50(2),:);
        KoenDay1RVMMVCEMGZero(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut50(1):Gcut50(2),:);
        KoenDay1RRFMVCEMGZero(1,5) = mean(RRFEMGcut);

        Ecut60 = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut60(1):Ecut60(2),:);
        KoenDay1RVLMVCEMG(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut60(1):Ecut60(2),:);
        KoenDay1RVMMVCEMG(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut60(1):Ecut60(2),:);
        KoenDay1RRFMVCEMG(1,6) = mean(RRFEMGcut);
        
        Gcut60 = Gravitycutint{i,3};
        RVLEMGcut = EMG_RVL(Gcut60(1):Gcut60(2),:);
        KoenDay1RVLMVCEMGZero(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut60(1):Gcut60(2),:);
        KoenDay1RVMMVCEMGZero(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut60(1):Gcut60(2),:);
        KoenDay1RRFMVCEMGZero(1,6) = mean(RRFEMGcut);

        % Subtract gravity mean from exercise mean to zero and then normalize by MVC data
        Day1RVLEMGAvg(i,:) = (KoenDay1RVLMVCEMG-KoenDay1RVLMVCEMGZero)/MVCDay1RVLEMGAvg(i,1)*100;
        Day1RVMEMGAvg(i,:) = (KoenDay1RVMMVCEMG-KoenDay1RVMMVCEMGZero)/MVCDay1RVMEMGAvg(i,1)*100;
        Day1RRFEMGAvg(i,:) = (KoenDay1RRFMVCEMG-KoenDay1RRFMVCEMGZero)/MVCDay1RRFEMGAvg(i,1)*100;
     
    else %All other subjects have the same data formatting

        % loop over trials
        for k = 1:length(Day1filenames)
            data = csvread([subnames{i},Day1filenames{k}]);% Pull out subject specific data

            % extract variables
            t = data(:,1);% Time data
            Traw = data(:,2);% Torque data
            Tt = data(:,3);% Target data
            if i == 1 % Subject 1 only has 3 muscles (Right leg VL,VM,RRF)
            EMGraw = data(:,4:6);    
            else
            EMGraw = data(:,4:8);% All others subjects have all 5 muscles
            end
            fs = 1 / mean(diff(t));% Get sample frequency

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,5 / (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            if i ~= 1
            FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
            FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
            end
            
            % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);
            EMG_RVM = downsample(FiltEMG_RVM,n);
            EMG_RRF = downsample(FiltEMG_RRF,n);
            if i ~= 1
            EMG_LVM = downsample(FiltEMG_LVM,n);
            EMG_RB = downsample(FiltEMG_RB,n);
            end

            %Cut and Plot EMG data
            Ecut = Exercisecutint{i,k}*200;% Get subject's exercise cut intervals
            Gcut = Gravitycutint{i,k}*200;% Get subject's gravity cut intervals
            RVLEMGcut = EMG_RVL(Gcut(1):Gcut(2),:);% Cut data 
            Day1RVLEMGAvgZero(i,k) = mean(RVLEMGcut);% Get mean of cut data for gravity measurment
            
            RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);% Cut data 
            Day1RVLEMGAvg(i,k) = (mean(RVLEMGcut)-Day1RVLEMGAvgZero(i,k))/MVCDay1RVLEMGAvg(i,1)*100;% Get mean of cut data 

            RVMEMGcut = EMG_RVM(Gcut(1):Gcut(2),:);
            Day1RVMEMGAvgZero(i,k) = mean(RVMEMGcut);
            
            RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
            Day1RVMEMGAvg(i,k) = (mean(RVMEMGcut)-Day1RVMEMGAvgZero(i,k))/MVCDay1RVMEMGAvg(i,1)*100;
           
            RRFEMGcut = EMG_RRF(Gcut(1):Gcut(2),:);
            Day1RRFEMGAvgZero(i,k) = mean(RRFEMGcut);
            
            RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
            Day1RRFEMGAvg(i,k) = (mean(RRFEMGcut)-Day1RRFEMGAvgZero(i,k))/MVCDay1RRFEMGAvg(i,1)*100;
            
            if i ~= 1
            LVMEMGcut = EMG_LVM(Gcut(1):Gcut(2),:);
            Day1LVMEMGAvgZero(i,k) = mean(LVMEMGcut);
            LVMEMGcut = EMG_LVM(Ecut(1):Ecut(2),:);
            Day1LVMEMGAvg(i,k) = (mean(LVMEMGcut)-Day1LVMEMGAvgZero(i,k))/MVCDay1LVMEMGAvg(i,1)*100;
            RBEMGcut = EMG_RB(Ecut(1):Ecut(2),:);
            Day1RBEMGAvgZero(i,k) = mean(RBEMGcut);
            RBEMGcut = EMG_RB(Ecut(1):Ecut(2),:);
            Day1RBEMGAvg(i,k) = (mean(RBEMGcut)-mean(RBEMGcut))/MVCDay1RBEMGAvg(i,1)*100;
            end

            %Show comparison of cut section to uncut section for Right Vastus Lateralis
            if showfig
                figure;
                plot(EMG_RVL); hold on
                plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            end

end
    end
end 
if resave % Save data
save('EMGSummaryAnalysisDay1','subnames','angles','Day1RVLEMGAvg','Day1RVMEMGAvg','Day1RRFEMGAvg','Day1LVMEMGAvg','Day1RBEMGAvg');
end
%% Day 2 Analysis
load('TorqueProcDay2.mat') % load cut intervals for day 2
% Some parameters
frequencies = [0,1,2,0.5,1.5,2.5,0];% Frequency file order for day 2
Day2filenames = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv','_60deg_0Hz.csv'}; % Day 2 file order
N = length(subnames);

% Create not a number filled variable for each EMG average 
Day2RVLEMGAvg = nan(N,length(Day2filenames));
Day2RVMEMGAvg = nan(N,length(Day2filenames));
Day2RRFEMGAvg = nan(N,length(Day2filenames));
Day2LVMEMGAvg = nan(N,length(Day2filenames));
Day2RBEMGAvg = nan(N,length(Day2filenames));

% Load Day 2 MVC
load('MVCDay2EMGData.mat')

% Loop Over Subjects
for i = 1:N
    if i == 2 % Koen has weird EMG data formatting

        % extract variables
        %load('KoenProcDay2.mat')
        data = csvread('Koen_June21_submax_variable_freq.csv');
        t = data(:,1);
        Traw = data(:,2);
        Tt = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        
        %Cut EMG data
        Ecut0 = Exercisecutint{i,4};
        RVLEMGcut = EMG_RVL(Ecut0(1):Ecut0(2),:);
        KoenDay2RVLMVCEMG(1,1) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut0(1):Ecut0(2),:);
        KoenDay2RVMMVCEMG(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut0(1):Ecut0(2),:);
        KoenDay2RRFMVCEMG(1,1) = mean(RRFEMGcut);
        
        Gcut0 = Gravitycutint{i,4};
        RVLEMGcut = EMG_RVL(Gcut0(1):Gcut0(2),:);
        KoenDay2RVLMVCEMGZero(1,1) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut0(1):Gcut0(2),:);
        KoenDay2RVMMVCEMGZero(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut0(1):Gcut0(2),:);
        KoenDay2RRFMVCEMGZero(1,1) = mean(RRFEMGcut);
        
        Ecut1 = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut1(1):Ecut1(2),:);
        KoenDay2RVLMVCEMG(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut1(1):Ecut1(2),:);
        KoenDay2RVMMVCEMG(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut1(1):Ecut1(2),:);
        KoenDay2RRFMVCEMG(1,2) = mean(RRFEMGcut);
        
        Gcut1 = Gravitycutint{i,1};
        RVLEMGcut = EMG_RVL(Gcut1(1):Gcut1(2),:);
        KoenDay2RVLMVCEMGZero(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut1(1):Gcut1(2),:);
        KoenDay2RVMMVCEMGZero(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut1(1):Gcut1(2),:);
        KoenDay2RRFMVCEMGZero(1,2) = mean(RRFEMGcut);
        
        Ecut2 = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut2(1):Ecut2(2),:);
        KoenDay2RVLMVCEMG(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut2(1):Ecut2(2),:);
        KoenDay2RVMMVCEMG(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut2(1):Ecut2(2),:);
        KoenDay2RRFMVCEMG(1,3) = mean(RRFEMGcut);
        
        Gcut2 = Gravitycutint{i,2};
        RVLEMGcut = EMG_RVL(Gcut2(1):Gcut2(2),:);
        KoenDay2RVLMVCEMGZero(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut2(1):Gcut2(2),:);
        KoenDay2RVMMVCEMGZero(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut2(1):Gcut2(2),:);
        KoenDay2RRFMVCEMGZero(1,3) = mean(RRFEMGcut);

        Ecut05 = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut05(1):Ecut05(2),:);
        KoenDay2RVLMVCEMG(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut05(1):Ecut05(2),:);
        KoenDay2RVMMVCEMG(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut05(1):Ecut05(2),:);
        KoenDay2RRFMVCEMG(1,4) = mean(RRFEMGcut);
        
        Gcut05 = Gravitycutint{i,3};
        RVLEMGcut = EMG_RVL(Gcut05(1):Gcut05(2),:);
        KoenDay2RVLMVCEMGZero(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut05(1):Gcut05(2),:);
        KoenDay2RVMMVCEMGZero(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut05(1):Gcut05(2),:);
        KoenDay2RRFMVCEMGZero(1,4) = mean(RRFEMGcut);

        Ecut15 = Exercisecutint{i,6};
        RVLEMGcut = EMG_RVL(Ecut15(1):Ecut15(2),:);
        KoenDay2RVLMVCEMG(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut15(1):Ecut15(2),:);
        KoenDay2RVMMVCEMG(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut15(1):Ecut15(2),:);
        KoenDay2RRFMVCEMG(1,5) = mean(RRFEMGcut);
        
        Gcut15 = Gravitycutint{i,6};
        RVLEMGcut = EMG_RVL(Gcut15(1):Gcut15(2),:);
        KoenDay2RVLMVCEMGZero(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut15(1):Gcut15(2),:);
        KoenDay2RVMMVCEMGZero(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut15(1):Gcut15(2),:);
        KoenDay2RRFMVCEMGZero(1,5) = mean(RRFEMGcut);

        Ecut25 = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut25(1):Ecut25(2),:);
        KoenDay2RVLMVCEMG(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut25(1):Ecut25(2),:);
        KoenDay2RVMMVCEMG(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut25(1):Ecut25(2),:);
        KoenDay2RRFMVCEMG(1,6) = mean(RRFEMGcut);
        
        Gcut25 = Gravitycutint{i,5};
        RVLEMGcut = EMG_RVL(Gcut25(1):Gcut25(2),:);
        KoenDay2RVLMVCEMGZero(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut25(1):Gcut25(2),:);
        KoenDay2RVMMVCEMGZero(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut25(1):Gcut25(2),:);
        KoenDay2RRFMVCEMGZero(1,6) = mean(RRFEMGcut);
        
        Ecut60 = Exercisecutint{i,7};
        RVLEMGcut = EMG_RVL(Ecut60(1):Ecut60(2),:);
        KoenDay2RVLMVCEMG(1,7) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut60(1):Ecut60(2),:);
        KoenDay2RVMMVCEMG(1,7) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut60(1):Ecut60(2),:);
        KoenDay2RRFMVCEMG(1,7) = mean(RRFEMGcut);
        
        Gcut60 = Gravitycutint{i,7};
        RVLEMGcut = EMG_RVL(Gcut60(1):Gcut60(2),:);
        KoenDay2RVLMVCEMGZero(1,7) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut60(1):Gcut60(2),:);
        KoenDay2RVMMVCEMGZero(1,7) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut60(1):Gcut60(2),:);
        KoenDay2RRFMVCEMGZero(1,7) = mean(RRFEMGcut);
        
        Day2RVLEMGAvg(i,:) = (KoenDay2RVLMVCEMG-KoenDay2RVLMVCEMGZero)/MVCDay2RVLEMGAvg(i,1)*100;
        Day2RVMEMGAvg(i,:) = (KoenDay2RVMMVCEMG-KoenDay2RVMMVCEMGZero)/MVCDay2RVMEMGAvg(i,1)*100;
        Day2RRFEMGAvg(i,:) = (KoenDay2RRFMVCEMG-KoenDay2RRFMVCEMGZero)/MVCDay2RRFEMGAvg(i,1)*100;
                    
    elseif i == 3 % Billy has weird EMG data formatting
             
        % extract variables
        %load('BillyProcDay2.mat')
        data = csvread('Billy_June27_variable_frequency.csv');
        t = data(:,1);
        Traw = data(:,2);
        Tt = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        
        %Cut EMG data
        Ecut0 = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut0(1):Ecut0(2),:);
        BillyDay2RVLMVCEMG(1,1) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut0(1):Ecut0(2),:);
        BillyDay2RVMMVCEMG(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut0(1):Ecut0(2),:);
        BillyDay2RRFMVCEMG(1,1) = mean(RRFEMGcut);
        
        Gcut0 = Gravitycutint{i,1};
        RVLEMGcut = EMG_RVL(Gcut0(1):Gcut0(2),:);
        BillyDay2RVLMVCEMGZero(1,1) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut0(1):Gcut0(2),:);
        BillyDay2RVMMVCEMGZero(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut0(1):Gcut0(2),:);
        BillyDay2RRFMVCEMGZero(1,1) = mean(RRFEMGcut);

        Ecut1 = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut1(1):Ecut1(2),:);
        BillyDay2RVLMVCEMG(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut1(1):Ecut1(2),:);
        BillyDay2RVMMVCEMG(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut1(1):Ecut1(2),:);
        BillyDay2RRFMVCEMG(1,2) = mean(RRFEMGcut);
        
        Gcut1 = Gravitycutint{i,3};
        RVLEMGcut = EMG_RVL(Gcut1(1):Gcut1(2),:);
        BillyDay2RVLMVCEMGZero(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut1(1):Gcut1(2),:);
        BillyDay2RVMMVCEMGZero(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut1(1):Gcut1(2),:);
        BillyDay2RRFMVCEMGZero(1,2) = mean(RRFEMGcut);

        Ecut2 = Exercisecutint{i,7};
        RVLEMGcut = EMG_RVL(Ecut2(1):Ecut2(2),:);
        BillyDay2RVLMVCEMG(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut2(1):Ecut2(2),:);
        BillyDay2RVMMVCEMG(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut2(1):Ecut2(2),:);
        BillyDay2RRFMVCEMG(1,3) = mean(RRFEMGcut);
        
        Gcut2 = Gravitycutint{i,7};
        RVLEMGcut = EMG_RVL(Gcut2(1):Gcut2(2),:);
        BillyDay2RVLMVCEMGZero(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut2(1):Gcut2(2),:);
        BillyDay2RVMMVCEMGZero(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut2(1):Gcut2(2),:);
        BillyDay2RRFMVCEMGZero(1,3) = mean(RRFEMGcut);

        Ecut05 = Exercisecutint{i,6};
        RVLEMGcut = EMG_RVL(Ecut05(1):Ecut05(2),:);
        BillyDay2RVLMVCEMG(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut05(1):Ecut05(2),:);
        BillyDay2RVMMVCEMG(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut05(1):Ecut05(2),:);
        BillyDay2RRFMVCEMG(1,4) = mean(RRFEMGcut);
        
        Gcut05 = Gravitycutint{i,6};
        RVLEMGcut = EMG_RVL(Gcut05(1):Gcut05(2),:);
        BillyDay2RVLMVCEMGZero(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut05(1):Gcut05(2),:);
        BillyDay2RVMMVCEMGZero(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut05(1):Gcut05(2),:);
        BillyDay2RRFMVCEMGZero(1,4) = mean(RRFEMGcut);

        Ecut15 = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut15(1):Ecut15(2),:);
        BillyDay2RVLMVCEMG(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut15(1):Ecut15(2),:);
        BillyDay2RVMMVCEMG(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut15(1):Ecut15(2),:);
        BillyDay2RRFMVCEMG(1,5) = mean(RRFEMGcut);
        
        Gcut15 = Gravitycutint{i,2};
        RVLEMGcut = EMG_RVL(Gcut15(1):Gcut15(2),:);
        BillyDay2RVLMVCEMGZero(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut15(1):Gcut15(2),:);
        BillyDay2RVMMVCEMGZero(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut15(1):Gcut15(2),:);
        BillyDay2RRFMVCEMGZero(1,5) = mean(RRFEMGcut);

        Ecut25 = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut25(1):Ecut25(2),:);
        BillyDay2RVLMVCEMG(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut25(1):Ecut25(2),:);
        BillyDay2RVMMVCEMG(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut25(1):Ecut25(2),:);
        BillyDay2RRFMVCEMG(1,6) = mean(RRFEMGcut);
        
        Gcut25 = Gravitycutint{i,5};
        RVLEMGcut = EMG_RVL(Gcut25(1):Gcut25(2),:);
        BillyDay2RVLMVCEMGZero(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut25(1):Gcut25(2),:);
        BillyDay2RVMMVCEMGZero(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut25(1):Gcut25(2),:);
        BillyDay2RRFMVCEMGZero(1,6) = mean(RRFEMGcut);
        
        Ecut60 = Exercisecutint{i,4};
        RVLEMGcut = EMG_RVL(Ecut60(1):Ecut60(2),:);
        BillyDay2RVLMVCEMG(1,7) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut60(1):Ecut60(2),:);
        BillyDay2RVMMVCEMG(1,7) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut60(1):Ecut60(2),:);
        BillyDay2RRFMVCEMG(1,7) = mean(RRFEMGcut);
        
        Gcut60 = Gravitycutint{i,4};
        RVLEMGcut = EMG_RVL(Gcut60(1):Gcut60(2),:);
        BillyDay2RVLMVCEMGZero(1,7) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Gcut60(1):Gcut60(2),:);
        BillyDay2RVMMVCEMGZero(1,7) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Gcut60(1):Gcut60(2),:);
        BillyDay2RRFMVCEMGZero(1,7) = mean(RRFEMGcut);
        
        Day2RVLEMGAvg(i,:) = (BillyDay2RVLMVCEMG-BillyDay2RVLMVCEMGZero)/MVCDay2RVLEMGAvg(i,1)*100;
        Day2RVMEMGAvg(i,:) = (BillyDay2RVMMVCEMG-BillyDay2RVMMVCEMGZero)/MVCDay2RVMEMGAvg(i,1)*100;
        Day2RRFEMGAvg(i,:) = (BillyDay2RRFMVCEMG-BillyDay2RRFMVCEMGZero)/MVCDay2RRFEMGAvg(i,1)*100;
            
    else %All other subjects have the same data formatting
        %load ([subnames{i},'ProcDay2.mat']);
                          
               % Loop Over Trials
               for k = 6; % 1:length(Day2filenames)
                    data = csvread([subnames{i},Day2filenames{k}]);
                    % extract variables
                    t = data(:,1);
                    Traw = data(:,2);
                    Tt = data(:,3);
                    if i == 1
                    EMGraw = data(:,4:6);    
                    else
                    EMGraw = data(:,4:8);
                    end
                    fs = 1 / mean(diff(t));

                    %  band-pass filter EMG
                    fc1 = [30 500];
                    [b,a] = butter(2,fc1 / (fs*.5));
                    [b2,a2] = butter(2,10 / (fs*.5),'low');

                    FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
                    FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
                    FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
                    if i~=1
                    FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
                    FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
                    end

                    % Downsample EMG data
                    EMG_RVL = downsample(FiltEMG_RVL,n);
                    EMG_RVM = downsample(FiltEMG_RVM,n);
                    EMG_RRF = downsample(FiltEMG_RRF,n);
                    if i~=1
                    EMG_LVM = downsample(FiltEMG_LVM,n);
                    EMG_RB = downsample(FiltEMG_RB,n);
                    end

                    % Cut and plot EMG data 
                    Ecut = Exercisecutint{i,k}*200;
                    Gcut = Gravitycutint{i,k}*200;
                    RVLEMGcut = EMG_RVL(Gcut(1):Gcut(2),:);
                    Day2RVLEMGAvgZero(i,k) = mean(RVLEMGcut);
                    
                    RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
                    Day2RVLEMGAvg(i,k) = (mean(RVLEMGcut)-Day2RVLEMGAvgZero(i,k))/MVCDay2RVLEMGAvg(i,1)*100;

                    RVMEMGcut = EMG_RVM(Gcut(1):Gcut(2),:);
                    Day2RVMEMGAvgZero(i,k) = mean(RVMEMGcut);

                    RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
                    Day2RVMEMGAvg(i,k) = (mean(RVMEMGcut)-Day2RVMEMGAvgZero(i,k))/MVCDay2RVMEMGAvg(i,1)*100;

                    RRFEMGcut = EMG_RRF(Gcut(1):Gcut(2),:);
                    Day2RRFEMGAvgZero(i,k) = mean(RRFEMGcut);
                    
                    RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
                    Day2RRFEMGAvg(i,k) = (mean(RRFEMGcut)-Day2RRFEMGAvgZero(i,k))/MVCDay2RRFEMGAvg(i,1)*100;

                    if i~=1
                    LVMEMGcut = EMG_LVM(Gcut(1):Gcut(2),:);
                    Day2LVMEMGAvgZero(i,k) = mean(LVMEMGcut);
                    
                    LVMEMGcut = EMG_LVM(Ecut(1):Ecut(2),:);
                    Day2LVMEMGAvg(i,k) = (mean(LVMEMGcut)-Day2LVMEMGAvgZero(i,k))/MVCDay2LVMEMGAvg(i,1)*100;
                    
                    RBEMGcut = EMG_RB(Gcut(1):Gcut(2),:);
                    Day2RBEMGAvgZero(i,k) = mean(RBEMGcut);
                    
                    RBEMGcut = EMG_RB(Ecut(1):Ecut(2),:);
                    Day2RBEMGAvg(i,k) = (mean(RBEMGcut)-Day2RBEMGAvgZero(i,k))/MVCDay2RBEMGAvg(i,1)*100;
                    end 
                    
                    %Show comparison of cut section to uncut section for Right Vastus Lateralis
                   if showfig
                    figure;
                    plot(EMG_RVL); hold on
                    plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
                   end

end
    end
end 
if resave % Save data
 save('EMGSummaryAnalysisDay2','subnames','frequencies','Day2RVLEMGAvg','Day2RVMEMGAvg','Day2RRFEMGAvg','Day2LVMEMGAvg','Day2RBEMGAvg');
end 
