clear all; close all; clc
% this script is used to determine each subject's EMG MVC data by first
% cutting the data and then averaging it.
% this script outputs two .mat files called 'MVCDay1EMGData.mat' and 'MVCDay2EMGData.mat', which
% contains each subject's MVC EMG data and their respective cut intervals
% where the MVC occurs for each day.
%% Day 1 MVC EMG Analysis Parameters
% subject names (according to protocol)
subnames = {'S1','Koen','Billy','Rudi','Jordan','S6','Brian','Austin','Nick','Eliz','Gaby'};

%Downsample factor
n = 10;

% Create not a number filled variable for each MVC EMG average 
N = length(subnames);
MVCDay1RVLEMGMax = nan(N,1);
MVCDay1RVMEMGMax = nan(N,1);
MVCDay1RRFEMGMax = nan(N,1);
MVCDay1LVMEMGMax = nan(N,1);
MVCDay1RBEMGMax = nan(N,1);

% load previous intervals
load('MVCDay1EMGData.mat','Ecut1','Gcut1');
Ecut1 = round(Ecut1);
Gcut1 = round(Gcut1);

% want to determine new intervals??
redo = 0;
show = 1;
%% Loop through each subject and select cut intervals for day 1
for i = 1:N
    
            if i == 1 % Subject 1 only has 3 muscles (Right leg VL,VM,RRF)
                data = csvread('S1_20deg_MVC.csv');% Read subject's CSV file
                EMGraw = data(:,4:6); % Raw EMG data
            elseif i == 2 % Subject 2 only has 3 muscles (Right leg VL,VM,RRF)
                data = csvread('Koen_June21_MVC.csv');
                EMGraw = data(:,4:6);  % Raw EMG data
            elseif (i == 6)||(i==10)||(i==11) % Subjects 6,10,and 11 have different MVC trial used         
                data = csvread([subnames{i},'_10deg_MVC.csv']);
                EMGraw = data(:,4:8); % Raw EMG data
            else % All other subjects use all 5 EMG muscles and filenames
                data = csvread([subnames{i},'_AllMVC.csv']);
                EMGraw = data(:,4:8); % Raw EMG data
                
            end
            t = data(:,1);% Time data
            Traw = data(:,2);% Raw torque data
            Tt = data(:,3);% Target data
            fs = 1 / mean(diff(t));

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,5 / (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            if (i~=1)&&(i~=2)
                FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
                FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
            end
            % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);
            EMG_RVM = downsample(FiltEMG_RVM,n);
            EMG_RRF = downsample(FiltEMG_RRF,n);
            if (i~=1)&&(i~=2)
                EMG_LVM = downsample(FiltEMG_LVM,n);
                EMG_RB = downsample(FiltEMG_RB,n);
            end
            
            
            if redo
                % Plot figure
                figure(); 
                plot(EMG_RVL,'k'); hold on
                plot(EMG_RVM,'r'); hold on 
                plot(EMG_RRF,'g'); hold on
                if (i~=1)&&(i~=2)
                    plot(EMG_LVM,'y'); hold on 
                    plot(EMG_RB,'b'); hold on 
                end
                Z = subnames{i};
                title('MVC EMG',sprintf('%s',Z));
                [Ecut1(i,:),~] = ginput(2);
                [Gcut1(i,:),~] = ginput(2);
            end

            %Determine Max MVC EMG
            RVLEMGcut = EMG_RVL(Ecut1(i,1):Ecut1(i,2),:);% Cut exercise interval data
            [MVCDay1RVLEMGMax(i,:),loc1] = max(RVLEMGcut);% Pull out max MVC EMG signal
            RVMEMGcut = EMG_RVM(Ecut1(i,1):Ecut1(i,2),:);
            [MVCDay1RVMEMGMax(i,:),loc2] = max(RVMEMGcut);
            RRFEMGcut = EMG_RRF(Ecut1(i,1):Ecut1(i,2),:);
            [MVCDay1RRFEMGMax(i,:),loc3] = max(RRFEMGcut);
            
            RVLEMGcut = EMG_RVL(Gcut1(i,1):Gcut1(i,2),:);% Cut gravity intervals
            [MVCDay1RVLEMGZero(i,:)] = mean(RVLEMGcut);% Get mean of gravity signal
            RVMEMGcut = EMG_RVM(Gcut1(i,1):Gcut1(i,2),:);
            [MVCDay1RVMEMGZero(i,:)] = mean(RVMEMGcut);
            RRFEMGcut = EMG_RRF(Gcut1(i,1):Gcut1(i,2),:);
            [MVCDay1RRFEMGZero(i,:)] = mean(RRFEMGcut);

            % Zero EMG data by subtracting gravity mesurment
            MVCDay1RVLEMGAvg(i,:) = MVCDay1RVLEMGMax(i,:) - MVCDay1RVLEMGZero(i,:);
            MVCDay1RVMEMGAvg(i,:) = MVCDay1RVMEMGMax(i,:) - MVCDay1RVMEMGZero(i,:);
            MVCDay1RRFEMGAvg(i,:) = MVCDay1RRFEMGMax(i,:) - MVCDay1RRFEMGZero(i,:);
            
            if (i~=1)&&(i~=2)
                LVMEMGcut = EMG_LVM(Ecut1(i,1):Ecut1(i,2),:);
                [MVCDay1LVMEMGMax(i,:),loc4] = max(LVMEMGcut);
                RBEMGcut = EMG_RB(Ecut1(i,1):Ecut1(i,2),:);
                [MVCDay1RBEMGMax(i,:),loc5] = max(RBEMGcut); 
                
                LVMEMGcut = EMG_LVM(Ecut1(i,1):Ecut1(i,2),:);
                [MVCDay1LVMEMGZero(i,:)] = mean(LVMEMGcut);
                RBEMGcut = EMG_RB(Ecut1(i,1):Ecut1(i,2),:);
                [MVCDay1RBEMGZero(i,:)] = mean(RBEMGcut); 
                
                MVCDay1LVMEMGAvg(i,:) = MVCDay1LVMEMGMax(i,:) - MVCDay1LVMEMGZero(i,:);
                MVCDay1RBEMGAvg(i,:) = MVCDay1RBEMGMax(i,:) - MVCDay1RBEMGZero(i,:);
            end

           if show %Plot EMG and Cut            
            figure(); 
            plot(EMG_RVL,'k'); hold on
            plot(EMG_RVM,'r'); hold on 
            plot(EMG_RRF,'g'); hold on
            plot(loc1+Ecut1(i,1), MVCDay1RVLEMGMax(i,:),'ko'); hold on
            plot(loc2+Ecut1(i,1), MVCDay1RVMEMGMax(i,:),'ro'); hold on
            plot(loc3+Ecut1(i,1), MVCDay1RRFEMGMax(i,:),'go'); hold on
            if (i~=1)&&(i~=2)
                plot(EMG_LVM,'y'); hold on 
                plot(EMG_RB,'b'); hold on 
                plot(loc4+Ecut1(i,1), MVCDay1LVMEMGMax(i,:),'yo'); hold on
                plot(loc5+Ecut1(i,1), MVCDay1RBEMGMax(i,:),'bo'); hold on
            end
            Z = subnames{i};
            legend('Vastus Lateralis','Vastus Medialis','Rectus Femoris');
            xlabel('Sample Number');ylabel('EMG Signal (mV)');
            title('Day 1 MVC EMG Cut Interval Comparison',sprintf('%s',Z));
           end
end          

if redo % Save data
save('MVCDay1EMGData','subnames','MVCDay1RVLEMGAvg','MVCDay1RVMEMGAvg','MVCDay1RRFEMGAvg','MVCDay1LVMEMGAvg','MVCDay1RBEMGAvg','Ecut1','Gcut1')
end
%% Day 2 MVC EMG Analysis
% Create not a number filled variable for each MVC EMG average 
MVCDay2RVLEMGMax = nan(N,1);
MVCDay2RVMEMGMax = nan(N,1);
MVCDay2RRFEMGMax = nan(N,1);
MVCDay2LVMEMGMax = nan(N,1);
MVCDay2RBEMGMax = nan(N,1);

% load previous intervals
load('MVCDay2EMGData.mat','Ecut2','Gcut2');
Ecut2 = round(Ecut2);
Gcut2 = round(Gcut2);
%% Loop through each subject and select cut intervals for day 1
for i = 1:N

            if i == 1 % Subject 1 only has 3 muscles (Right leg VL,VM,RRF)
                data = csvread('S1_20deg_MVC(2).csv'); % Read CSV file
                EMGraw = data(:,4:6); % Raw EMG data
            elseif i == 2 % Subject 2 only has 3 muscles (Right leg VL,VM,RRF)
                data = csvread('Koen_June21_MVC.csv');
                EMGraw = data(:,4:6);
            elseif i == 3 % Subject 3 only has 3 muscles (Right leg VL,VM,RRF)
                data = csvread('Billy_June26.csv');
                EMGraw = data(:,4:6);
            elseif (i==10)||(i==11) % Subjects 6,10,and 11 have different MVC trial used                 
                data = csvread([subnames{i},'_AllMVC.csv']);
                EMGraw = data(:,4:8);
            else  % All other subjects use all 5 EMG muscles and filenames
                data = csvread([subnames{i},'_20deg_MVC.csv']);
                EMGraw = data(:,4:8);   
            end
            
            t = data(:,1);
            Traw = data(:,2);
            Tt = data(:,3);
            fs = 1 / mean(diff(t));% Get sample frequency

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,5 / (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            if (i~=2)&&(i~=3)&&(i~=12)
                FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
                FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
            end
            % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);
            EMG_RVM = downsample(FiltEMG_RVM,n);
            EMG_RRF = downsample(FiltEMG_RRF,n);
            if (i~=1)&&(i~=2)&&(i~=3)
                EMG_LVM = downsample(FiltEMG_LVM,n);
                EMG_RB = downsample(FiltEMG_RB,n);
            end
            
            if redo
                figure(); 
                plot(EMG_RVL,'k'); hold on
                plot(EMG_RVM,'r'); hold on 
                plot(EMG_RRF,'g'); hold on
                if (i~=1)&&(i~=2)&&(i~=3)
                    plot(EMG_LVM,'y'); hold on 
                    plot(EMG_RB,'b'); hold on 
                end
                Z = subnames{i};
                title('MVC EMG',sprintf('%s',Z));
                [Ecut2(i,:),~] = ginput(2); 
                [Gcut2(i,:),~] = ginput(2); 
            end
            
            RVLEMGcut = EMG_RVL(Ecut2(i,1):Ecut2(i,2),:);% Cut exercise data
            [MVCDay2RVLEMGMax(i,:),loc1] = max(RVLEMGcut); % Pull out max MVC EMG signal
            RVMEMGcut = EMG_RVM(Ecut2(i,1):Ecut2(i,2),:);
            [MVCDay2RVMEMGMax(i,:),loc2] = max(RVMEMGcut);
            RRFEMGcut = EMG_RRF(Ecut2(i,1):Ecut2(i,2),:);
            [MVCDay2RRFEMGMax(i,:),loc3] = max(RRFEMGcut);
            
            RVLEMGcut = EMG_RVL(Gcut2(i,1):Gcut2(i,2),:);% Cut gravity data
            [MVCDay2RVLEMGZero(i,:)] = mean(RVLEMGcut);% Get mean of gravity cut data
            RVMEMGcut = EMG_RVM(Gcut2(i,1):Gcut2(i,2),:);
            [MVCDay2RVMEMGZero(i,:)] = mean(RVMEMGcut);
            RRFEMGcut = EMG_RRF(Gcut2(i,1):Gcut2(i,2),:);
            [MVCDay2RRFEMGZero(i,:)] = mean(RRFEMGcut);
            
            % Zero EMG data by subtracting gravity mesurment
            MVCDay2RVLEMGAvg(i,:) = MVCDay2RVLEMGMax(i,:) - MVCDay2RVLEMGZero(i,:);
            MVCDay2RVMEMGAvg(i,:) = MVCDay2RVMEMGMax(i,:) - MVCDay2RVMEMGZero(i,:);
            MVCDay2RRFEMGAvg(i,:) = MVCDay2RRFEMGMax(i,:) - MVCDay2RRFEMGZero(i,:);
            
            if (i~=1)&&(i~=2)&&(i~=3)
                LVMEMGcut = EMG_LVM(Ecut2(i,1):Ecut2(i,2),:);
                [MVCDay2LVMEMGMax(i,:),loc4] = max(LVMEMGcut);
                RBEMGcut = EMG_RB(Ecut2(i,1):Ecut2(i,2),:);
                [MVCDay2RBEMGMax(i,:),loc5] = max(RBEMGcut); 
                
                LVMEMGcut = EMG_LVM(Ecut2(i,1):Ecut2(i,2),:);
                [MVCDay2LVMEMGZero(i,:)] = mean(LVMEMGcut);
                RBEMGcut = EMG_RB(Ecut2(i,1):Ecut2(i,2),:);
                [MVCDay2RBEMGZero(i,:)] = mean(RBEMGcut); 
                
                MVCDay2LVMEMGAvg(i,:) = MVCDay2LVMEMGMax(i,:) - MVCDay2LVMEMGZero(i,:);
                MVCDay2RBEMGAvg(i,:) = MVCDay2RBEMGMax(i,:) - MVCDay2RBEMGZero(i,:);
            end
          
           if show %Plot EMG and Cut            
            figure(); 
            plot(EMG_RVL,'k'); hold on
            plot(EMG_RVM,'r'); hold on 
            plot(EMG_RRF,'g'); hold on
            plot(loc1+Ecut2(i,1), MVCDay2RVLEMGMax(i,:),'ko'); hold on
            plot(loc2+Ecut2(i,1), MVCDay2RVMEMGMax(i,:),'ro'); hold on
            plot(loc3+Ecut2(i,1), MVCDay2RRFEMGMax(i,:),'go'); hold on
            if (i~=1)&&(i~=2)&&(i~=3)
                plot(EMG_LVM,'y'); hold on 
                plot(EMG_RB,'b'); hold on 
                plot(loc4+Ecut2(i,1), MVCDay2LVMEMGMax(i,:),'yo'); hold on
                plot(loc5+Ecut2(i,1), MVCDay2RBEMGMax(i,:),'bo'); hold on
            end
            legend('Vastus Lateralis','Vastus Medialis','Rectus Femoris');
            xlabel('Sample Number');ylabel('EMG Signal (mV)');
            Z = subnames{i};
                title('Day 2 MVC EMG Cut Interval Comparison',sprintf('%s',Z));
           end
end          
if redo % Save data
    save('MVCDay2EMGData','subnames','MVCDay2RVLEMGAvg','MVCDay2RVMEMGAvg','MVCDay2RRFEMGAvg','MVCDay2LVMEMGAvg','MVCDay2RBEMGAvg','Ecut2','Gcut2')
end