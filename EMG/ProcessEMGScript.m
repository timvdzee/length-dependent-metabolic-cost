clear all; close all; clc
% purpose: save downsampled & filtered EMG files
mainfolder = 'C:\Users\timvd';

% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);

% load MVC
% load('MVCDay1EMGData.mat')

% some parameters
showfig = false;% Show figures
angles = [10:10:60];% Day 1 angles
frequencies = [0,1,2,0.5,1.5,2.5,0];% Frequency file order for day 2

      
% Koen's order is [50, 20, 60, 40, 30, 10] and [1 2 0.5 0 2.5 1.5 60]
% Billy has [0 1.5 1 60 2.5 0.5 2]
Billy_order = [1, 5, 2, 7, 6, 4, 3];
Koen_order = [2, 3, 4, 1, 6, 5, 7];
    
Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; %Day 1 filenames order
Day2filenames = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv','_60deg_0Hz.csv'}; % Day 2 file order

freqs = [0 1 2 0.5 1.5 2.5 0];

n = 20; %Downsample factor (2000 Hz -> 100 Hz)
fs = 2000; % original sample frequency
fs_new = fs / n;

% Create not a number filled variable for each EMG average 
Day1RVLEMGAvg = nan(N,length(Day1filenames));
Day1RVMEMGAvg = nan(N,length(Day1filenames));
Day1RRFEMGAvg = nan(N,length(Day1filenames));
Day1LVMEMGAvg = nan(N,length(Day1filenames));
Day1RBEMGAvg = nan(N,length(Day1filenames));

%Resave variables
resave = false;

% filter parameters
fc1 = [30 500];
fc2 = 5;
[b,a] = butter(2,fc1 / (fs*.5));
[b2,a2] = butter(2,fc2 / (fs*.5),'low');

% channels:
% 1: Time
% 2: Torque
% 3: Target
% 4: RVL
% 5: RVM
% 6: RRF

%% Day 1
% loop over subjects
% load('TorqueProcDay1.mat') % Load cut interval data for day 1

for d = 2 % DAY 1 AND DAY 2
    cd([mainfolder,'\OneDrive - University of Calgary\ultrasound\Billy pilot\Data\Variable Frequency and Knee Angle Experiment\Torque and EMG\Torque\Analyzed'])
    load(['TorqueProcDay', num2str(d),'.mat'],'Exercisecutint','Gravitycutint') % Load cut interval data for day
    
    if d == 1, Dayfilenames = Day1filenames;
    else, Dayfilenames = Day2filenames;
    end
    
    for p = 4:N
        
        % trials
        for j = 1:length(Dayfilenames)  
            disp(['Trial: ', num2str(j)])
            corf = 10/n; % based on downsample of 10
            
        % Koen has weird EMG data formatting
        if d == 1 && p == 2,                filename = 'Koen_June21_submax.csv'; flag = 1;
        elseif d == 2 && p == 2,            filename = 'Koen_June21_submax_variable_freq.csv';  flag = 1;
                                            Dayfilenames{j} = Day2filenames{Koen_order(j)};
        elseif d == 2 && p == 3,            filename = 'Billy_June27_variable_frequency.csv';  flag = 1;  
                                            Dayfilenames{j} = Day2filenames{Billy_order(j)};
        else,                               filename = [subnames{p},Dayfilenames{j}];  flag = 0;
                                            corf = fs_new; % time -> samples
        end

        if (~flag || j == 1)
        disp(['Loading: ', filename]);
        rdata = csvread(filename);
        disp('Done')
        end
        
        % cut exercise and gravity
        % Exercisecutint is based on downsampled torque (fs = 200 Hz) 
        Ecut = round(Exercisecutint{p,j} * corf); 
        Gcut = round(Gravitycutint{p,j} * corf);

        cdata = nan(Ecut(2)-Ecut(1)+1, 6);
        gdata = nan(Gcut(2)-Gcut(1)+1, 6);
        
        % make a copy
        data = rdata;
        
        % variable frequency filtering
%         fc2 = freqs(j) + 5;
%         [b2,a2] = butter(2,fc2 / (fs*.5));
        
        for i = 1:6
            if i > 3 % only for EMG
                data(:,i) = abs(hilbert(filtfilt(b,a, data(:,i))));
            end
            
            if i > 1 % not for time
                data(:,i) = filtfilt(b2,a2,data(:,i));
            end
            
            
            ddata = downsample(data(:,i),n);
            
            if Ecut(2) > length(ddata)
                disp('Warning Ecut > length(ddata)');
                Ecut(2) = length(ddata);
            end
            
            cdata(:,i) = ddata(Ecut(1):Ecut(2));
            gdata(:,i) = ddata(Gcut(1):Gcut(2));
        end
                 
        cd([mainfolder,'\OneDrive - University of Calgary\ultrasound\Billy pilot\Data\Variable Frequency and Knee Angle Experiment\Torque and EMG\EMG\Downsampled'])
        save(['P',num2str(p),'_Day',num2str(d),Dayfilenames{j}(1:end-4),'_fc=5.mat'],'cdata','gdata', 'fc2','fs_new')
        end
    end
end

