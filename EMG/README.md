# Length-dependent-metabolic-cost: EMG Folder

The following readme is used as a guide through the EMG folder which includes the relevant scripts and functions to analyze and process the raw EMG data. Below is a flow chart to visualize this process.

![EMGFlowChart](EMGFlowChart.jpg)  

## Analysis of main EMG data
Raw data starts out as a .csv file that contains the recorded raw time, torque, waveform target, and EMG data. EMG data is taken from channels 4 to 6 or 4 to 8 depending on the subject. Muscles where EMG data was taken included:

* Right Vastus Lateralis - Channel 4
* Right Vastus Medialis - Channel 5
* Right Rectus Femoris - Channel 6
* Left Vastus Medialis - Channel 7
* Right Bicep - Channel 8
The 'AnalysisEMGScript.m' script analyzes the raw EMG data. The raw data is first band-pass filtered (cut-off frequency 𝑓 = 30-500 Hz, Butterworth) and low-pass filtered to obtain the envelop (𝑓 = 5 Hz, Butterworth) (Hof, 1984). Next the data is downsampled by a factor of 10 and then cut using the previous obtained exercise and gravity intervals from the torque analysis. The cut EMG data is next normalized by each subject's respective MVC EMG signal for both days. The process by which the MVC EMG data is obtained is explained below. Once the raw data is filtered, downsampled, cut, and normalized it is saved as the variables 'EMGSummaryAnalysisDay1.mat' and 'EMGSummaryAnalysisDay2.mat'. This analyzed data is used in one of two ways:

* The analyzed EMG data is visualized using the script 'VisualizeEMGScript.m'. This allows further examination of the EMG by visualizing each subject's individual muscle's EMG data and as an average of their right leg's 3 recorded muscles.  
* The analyzed EMG data is summarized and visualized using the function 'EMGTorqueRespiratorySummary.m'. 

## Analysis of EMG MVC data
The raw MVC data is analyzed in two ways: 

* The first method uses the script 'EMGMVCAnalysis.m' which analyzes the MVC EMG data that will be used to normalize each subjects respective torque data from the main trials. The MVC data is analyzed by being filtered and downsampled the same way as above but the MVC cut intervals are determined manually. From here the max EMG signal is determined and saved for each subject. The data is saved as two Matlab variables, 'MVCDay1EMGData.mat' and 'MVCDay2EMGData.mat'. These variables are then used to normalize the main EMG data as outlined above. 
* The second method uses the script 'AllMVCEMGAnalysis.m' script to analyze each subject's MVC trial in which they performed MVCs against different angles. This MVC data is analyzed in the same way as the first MVC data analysis method and saved as 'EMGMVCAnalysis.mat'.