clear all; close all; clc

% this script visualizes the analysed EMG data by plotting each
% subjects' EMG results for each of the 5 muscles measured on each day. As well one summary
% graph for each day that plots all the muscles on one graph is plotted. 

%% Day 1 Parameters
load('EMGSummaryAnalysisDay1.mat'); %Load Day 1 Variables
angles = [10:10:60];% Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];% Symbols for plotting
N = 11;

%% Figure 1: Right vastus lateralis constant frequency
figure();
errorbar(angles,mean(Day1RVLEMGAvg,'omitnan'),std(Day1RVLEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
    plot(angles,Day1RVLEMGAvg(i,:),symbols(i),'markersize',10); hold on
end

legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('Right Vastus Lateralis EMG Summary Variable Angle, Constant Frequency');

%% Figure 2: Right Vastus Medialis constant frequency 
figure();
errorbar(angles,mean(Day1RVMEMGAvg,'omitnan'),std(Day1RVMEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
plot(angles,Day1RVMEMGAvg(i,:),symbols(i),'markersize',10); hold on
end
legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('Right Vastus Medialis EMG Summary Variable Angle, Constant Frequency');

%% Figure 3: Right Rectus Femoris constant frequency  
figure();
errorbar(angles,mean(Day1RRFEMGAvg,'omitnan'),std(Day1RRFEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
plot(angles,Day1RRFEMGAvg(i,:),symbols(i),'markersize',10); hold on
end

legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('Right Rectus Femoris EMG Summary Variable Angle, Constant Frequency');

%% Figure 4:Left Vastus Medialis constant frequency  
figure();
errorbar(angles,mean(Day1LVMEMGAvg,'omitnan'),std(Day1LVMEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
    if 1 <= i && i<=2 %Tim and Koen missing EMG data
        continue
    else
plot(angles,Day1LVMEMGAvg(i,:),symbols(i),'markersize',10); hold on
    end
end

legend('Mean EMG','Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby','12')
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('Left Vastus Medialis EMG Summary Variable Angle, Constant Frequency');

%% Figure 5:Right Biceps constant frequency 
figure();
errorbar(angles,mean(Day1RBEMGAvg,'omitnan'),std(Day1RBEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
    if (i==1) || (i==2) %Tim, and Koen missing EMG data
        continue
    else
plot(angles,Day1RBEMGAvg(i,:),symbols(i),'markersize',10); hold on
    end
end

legend('Mean EMG','Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('Right Biceps EMG Summary Variable Angle, Constant Frequency');

%% Figure 6: All Muscles Constant Frequency
figure();
errorbar(angles,mean(Day1RVLEMGAvg,'omitnan'),std(Day1RVLEMGAvg,'omitnan'),'k.','markersize',20); hold on
errorbar(angles,mean(Day1RVMEMGAvg,'omitnan'),std(Day1RVMEMGAvg,'omitnan'),'r.','markersize',20); hold on
errorbar(angles,mean(Day1RRFEMGAvg,'omitnan'),std(Day1RRFEMGAvg,'omitnan'),'b.','markersize',20); hold on
errorbar(angles,mean(Day1LVMEMGAvg,'omitnan'),std(Day1LVMEMGAvg,'omitnan'),'g.','markersize',20); hold on
errorbar(angles,mean(Day1RBEMGAvg,'omitnan'),std(Day1RBEMGAvg,'omitnan'),'m.','markersize',20); hold on

legend( 'Right Vastus Lateralis','Right Vastus Medialis','Right Rectus Femoris', 'Left Vastus Medialis','Right Biceps') 
xlabel('Angle (degrees)');
ylabel('Average EMG (% of MVC)');
title('EMG Summary Variable Angle, Constant Frequency');

%% Day 2 Parameters
load('EMGSummaryAnalysisDay2.mat'); %load day 2 variables
frequencies = [0,1,2,0.5,1.5,2.5,0];

%% Figure 7: Right Vastus Lateralis Constant Angle
figure();
errorbar(frequencies,mean(Day2RVLEMGAvg,'omitnan'),std(Day2RVLEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
plot(frequencies,Day2RVLEMGAvg(i,:),symbols(i),'markersize',10); hold on
end

legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('Right Vastus Lateralis EMG Summary Constant Angle, Variable Frequency');

%% Figure 8: Right Vastus Medialis Constant Angle
figure();
errorbar(frequencies,mean(Day2RVMEMGAvg,'omitnan'),std(Day2RVMEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
plot(frequencies,Day2RVMEMGAvg(i,:),symbols(i),'markersize',10); hold on
end

legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('Right Vastus Medialis EMG Summary Constant Angle, Variable Frequency');

%% Figure 9: Right Rectus Femoris Constant Angle 
figure();
errorbar(frequencies,mean(Day2RRFEMGAvg,'omitnan'),std(Day2RRFEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
plot(frequencies,Day2RRFEMGAvg(i,:),symbols(i),'markersize',10); hold on
end

legend('Mean EMG','S1','Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('Right Rectus Femoris EMG Summary Constant Angle, Variable Frequency');

%% Figure 10: Left Vastus Medialis Constant Angle
figure();
errorbar(frequencies,mean(Day2LVMEMGAvg,'omitnan'),std(Day2LVMEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
    if (i==1) || (i==2)|| (i==3) %Tim, Koen, and Billy missing EMG data
        continue
    else
plot(frequencies,Day2LVMEMGAvg(i,:),symbols(i),'markersize',10); hold on
    end
end

legend('Mean EMG','Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('Left Vastus Medialis EMG Summary Constant Angle, Variable Frequency');

%% Figure 11: Right Biceps Constant Angle
figure();
errorbar(frequencies,mean(Day2RBEMGAvg,'omitnan'),std(Day2RBEMGAvg,'omitnan'),'k.','markersize',40); hold on
for i = 1:N
    if (i==1) || (i==2)|| (i==3) %Tim, Koen, and Billy missing EMG data
        continue
    else
plot(frequencies,Day2RBEMGAvg(i,:),symbols(i),'markersize',10); hold on
    end
end

legend('Mean EMG', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby')
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('Right Biceps EMG Summary Constant Angle, Variable Frequency');

%% Figure 12: All Muscles Constant Angle
figure();
errorbar(frequencies,mean(Day2RVLEMGAvg,'omitnan'),std(Day2RVLEMGAvg,'omitnan'),'k.','markersize',20); hold on
errorbar(frequencies,mean(Day2RVMEMGAvg,'omitnan'),std(Day2RVMEMGAvg,'omitnan'),'r.','markersize',20); hold on
errorbar(frequencies,mean(Day2RRFEMGAvg,'omitnan'),std(Day2RRFEMGAvg,'omitnan'),'b.','markersize',20); hold on
errorbar(frequencies,mean(Day2LVMEMGAvg,'omitnan'),std(Day2LVMEMGAvg,'omitnan'),'g.','markersize',20); hold on
errorbar(frequencies,mean(Day2RBEMGAvg,'omitnan'),std(Day2RBEMGAvg,'omitnan'),'m.','markersize',20); hold on

legend('Right Vastus Lateralis','Right Vastus Medialis', 'Right Rectus Femoris', 'Left Vastus Medialis','Right Biceps') 
xlabel('Frequency (Hz)');
ylabel('Average EMG (% of MVC)');
title('EMG Summary Constant Angle, Variable Frequency');








