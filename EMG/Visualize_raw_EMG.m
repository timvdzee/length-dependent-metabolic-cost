clear all; close all; clc
mainfolder = 'C:\Users\tim.vanderzee\OneDrive - University of Calgary\ultrasound\Billy pilot\Data\Variable Frequency and Knee Angle Experiment\Brian\Brian Day 2\Cybex';

freqs = {'05Hz', '1Hz', '15Hz', '2Hz(2)', '25Hz'};

% subject = 'Jordan';
subject = 'Brian';
% subject = 'Austin';


%% MVC
MVCdata = csvread([subject,'_20deg_MVC.csv']);
MVC = max(MVCdata(:,4:6));

%% Load data
d(1:5) = struct();
for i = 1:5
    disp(['Loading: ', subject,'_20deg_',num2str(freqs{i}),'.csv'])
    d(i).data = csvread([subject,'_20deg_',num2str(freqs{i}),'.csv']);
end

%% subtract mean and normalize MVC
clear r
r(1:5) = struct();

for i = 1:5
    r(i).data = (d(i).data(:,4:6) - repmat(mean(d(i).data(:,4:6)),length(d(i).data),1)) ./ repmat(MVC, length(d(i).data),1);
end

%% whole time-series
freqs = .5:.5:2.5;
for i = 1:5
    figure(1)
    subplot(5,1,i);
    t = d(i).data(:,1);
    
    plot(t, r(i).data(:,1))

    xlim([0 max(t)]); ylim([-.5 .5]); 
    
    xlabel('Time (s)'); ylabel('EMG');
    title([num2str(freqs(i)),' Hz'])
end

%% zoomed in somewhere
freqs = .5:.5:2.5;
tstar = 260;
for i = 1:5
    figure(1)
    subplot(5,1,i);

    xlim([tstar tstar + 10]);
end

%% filter
clear f
f(1:5) = struct();

fs = 2000;


% filter parameters
fc1 = [30 500];
fc2 = 10;
[b,a] = butter(2,fc1 / (fs*.5));
[b2,a2] = butter(2,fc2 / (fs*.5),'low');

for i = 1:5
    for j = 1:3
        f(i).data(:,j) =  filtfilt(b2,a2, abs(hilbert(filtfilt(b,a, r(i).data(:,j)))));
    end
end

%% whole time-series
close all
freqs = .5:.5:2.5;
for i = 1:5
    figure(1)
    subplot(5,1,i);
    t = d(i).data(:,1);
    
    plot(t, r(i).data(:,1)); hold on
    plot(t, movmean(abs(r(i).data(:,1)),50),'linewidth',2); hold on
    plot(t, f(i).data(:,1),'linewidth',2)

    xlim([0 max(t)]); ylim([0 .5]); 
    
    xlabel('Time (s)'); ylabel('EMG');
    title([num2str(freqs(i)),' Hz'])
end

%% zoomed in somewhere
freqs = .5:.5:2.5;
tstar = 260;
for i = 1:5
    figure(1)
    subplot(5,1,i);

    xlim([tstar tstar + 10]);
    
    ylim([0 .3])
end