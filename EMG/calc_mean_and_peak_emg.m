clear all; close all; clc
% script calculate mean and peak EMG from filtered data

% codefolder = 'C:\Users\timvd\Documents\length-dependent-metabolic-cost';
codefolder = 'C:\Users\tim.vanderzee\Documents\Length-dependent-metabolic-cost';

Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; % Day 1 filenames order
Day2filenames = {'_20deg_0Hz.csv','_20deg_05Hz.csv','_20deg_1Hz.csv','_20deg_15Hz.csv','_20deg_2Hz(2).csv','_20deg_25Hz.csv','_60deg_0Hz.csv'}; % Day 2 file order

fs = 100;
dt = 1/fs;
fc = 10;

% load MVC
cd('C:\Users\timvd\Documents\length-dependent-metabolic-cost\EMG\summary-data')
load('MVCDay2EMGData.mat')

MVC = [MVCDay2RVLEMGAvg MVCDay2RVMEMGAvg MVCDay2RRFEMGAvg]';


%%
if ishandle(2), close(2); end
figure(2)
        
color = get(gca,'colororder');

N = 200;
memgrel_ts = nan(N,7,10,3);
tshift = nan(5000, 7,10,3);

%% normalize for MVC and plot
cd([codefolder,'\EMG\summary-data'])
load('MVCDay2EMGData.mat')

MVC = [MVCDay2RVLEMGAvg MVCDay2RVMEMGAvg MVCDay2RRFEMGAvg]';

%%
ps = [1:8, 10:11];
emgrel_ts = nan(N, 7, 10, 3, 5000);
  
freqs = [0:.5:2.5, 0];

% participants
for p = 1:11
    
    % days
for d = 2
    
    if d == 1 
        Dayfilenames = Day1filenames;
        MVC = [MVCDay1RVLEMGAvg MVCDay1RVMEMGAvg MVCDay1RRFEMGAvg]';
    else 
        Dayfilenames = Day2filenames;
        MVC = [MVCDay2RVLEMGAvg MVCDay2RVMEMGAvg MVCDay2RRFEMGAvg]';
    end
    
    for i = 1:length(Dayfilenames)  

        disp(['Loading:  P',num2str(ps(p)),'_Day',num2str(d),Dayfilenames{i}(1:end-4),'_fc=',num2str(fc),'.mat'])
        load(['P',num2str(ps(p)),'_Day',num2str(d),Dayfilenames{i}(1:end-4),'_fc=',num2str(fc),'.mat'])

               
        % baseline subtraction (still need to do MVC correction)
        emgrel = (cdata(:,4:end) - mean(gdata(:,4:end))) ./ MVC(:,p)';
        torque = -cdata(:,2)  + mean(gdata(:,2)); % verify minus sign at some point
        
        % remove NaNs (for filtering)
        isf = isfinite(emgrel(:,1)); % assume 1st column is representative
        emgrel = emgrel(isf,:) ./ repmat(MVC(:,ps(p))', length(emgrel),1); % normalize for MVC

        % second low-pass filtering
         [b,a] = butter(2, 6 ./ (.5*fs));
        emgrelfilt = filtfilt(b,a,emgrel); % torque
        
        % fourier
        for j = 1:3
            if freqs(i) > 0
                [f,P] = fourier(emgrel(:,j)-mean(emgrel(:,j)), fs);
            else
              [f,P] = fourier(emgrel(:,j), fs);
            end
            dfreq(i,p,j) = f(P==max(P));
            
            id = (f < (dfreq(i,p,j)+.1)) & (f > (dfreq(i,p,j)-.1));
            p_int = trapz(f(id), P(id));
            max_power(i,p,j) = P(P==max(P));
            
%             max_power(i,p,j) = p_int;
        end
        
        % low-pass signal that's used as a clock
        [b,a] = butter(2, 3 ./ (.5*fs));
%         x_lowpass = filtfilt(b,a,sum(emgrel,2)); % EMG
        x_lowpass = filtfilt(b,a,torque); % torque
        x_lowpass_centered = x_lowpass - mean(x_lowpass);
        
        % zero-crossings function
        zci = @(v) find(diff(sign(v))>0);

        % find zero crossings
        zc = zci(x_lowpass_centered);
        id = nan(length(zc)-1,1);
        
        % find minimum between zero crossings
        for z = 1:length(zc)-1
            [~,minloc] = min(x_lowpass_centered(zc(z):zc(z+1)));
            id(z) = minloc + zc(z);
        end
        
        peakemgs = nan(length(id)-1,1);
        locs = nan(length(id)-1,1);
      
        
        % plot(zc, emgrel_lowpass_cent(zc,j),'o');
        for j = 1:size(emgrel, 2)
            
            % loop over cycles
            for z = 1:length(id)-1
                if z <= 5000
                    
%                     [peakemgs(z), locs(z)] = max(emgrel(id(z):id(z+1),j));
                     [peakemgs(z), locs(z)] = max(emgrelfilt(id(z):id(z+1),j));
                    
                    % 'time-series'
                    emgrel_ts(:,i,p,j,z) = interp1(id(z):id(z+1), emgrel(id(z):id(z+1),j), linspace(id(z),id(z+1), N));
                else
                    disp('exceded max cycles')
                end
            end
        
            %
            peaklocs = 1./locs(:) + id(1:end-1,1);
        
            % average over cycles
            memgrel_ts(:,i,p,j) = mean(emgrel_ts(:,i,p,j,:),5,'omitnan'); 

            peakemg(i,p,j) = mean(peakemgs);
            meanemg(i,p,j) = mean(emgrel(:,j));
            freq_check(i,p,j) = 1/(mean(diff(peaklocs)) * dt);
            
            if freqs(i) > 0
                tshift(1:length(locs),i,p,j) = .5/freqs(i) - locs * dt;
            end
        end

    end
end
end

return

%% time series for each subject
close all

freqs = [0:.5:2.5, 0];
muscles = {'VL','VM','RF'};

for p = 1:10
figure(p)   
    for j = 1:3

        for i = 1:7
            subplot(1,3,j)
            
            T = 1/freqs(i);
            T(T==inf) = 2;

            plot(linspace(0,T,N), memgrel_ts(:,i,p,j)); hold on
            ylim([0 max(memgrel_ts(:,:,p,:),[],'all')]); xlim([0 T])
            xlabel('Time (s)'); ylabel('EMG')
        end
        title(muscles{j})
    end

end

%% time series for subject average
close all

figure(1)
freqs = [0:.5:2.5, 0];


for j = 1:3

    for i = 1:7
        subplot(1,3,j)

        T = 1/freqs(i);
        T(T==inf) = 2;

        plot(linspace(0,T,N), mean(memgrel_ts(:,i,:,j),3),'linewidth',1,'color',color(i,:)); hold on
%         plot(linspace(0,T,N), mean(memgrel_ts(:,i,:,j),3) - std(memgrel_ts(:,i,:,j),1,3),'color',color(i,:)); hold on
%         plot(linspace(0,T,N), mean(memgrel_ts(:,i,:,j),3) + std(memgrel_ts(:,i,:,j),1,3),'color',color(i,:)); hold on
        
%         ylim([0 max(mean(memgrel_ts,3,'all'))]); 
        xlim([0 T])
        xlabel('Time (s)'); ylabel('EMG')
    end
    title(muscles{j})
end

%% compare average traces vs. sinusoids
close all

mtshift = mean(tshift,'all','omitnan');

% chosen peak measure
% P = meanemg * 2;
% P = peakemg;
P = squeeze(max(memgrel_ts));
% P = 2 * max_power;

for j = 1:3
    figure('name',muscles{j})
    for i = 2:6
        subplot(1,5,i-1)
        
        tlin = linspace(0,1/freqs(i), N);
        E = mean(P(i,:,j))/2 - mean(P(i,:,j))/2 * cos(2*pi*freqs(i)*(tlin+mtshift));
    
        plot(tlin, mean(memgrel_ts(:,i,:,j),3),'linewidth',1,'color',color(i,:)); hold on
        plot(tlin, E,'k--')
        ylim([0 .25])
        
        title([num2str(freqs(i)), ' Hz']); xlabel('Time (s)'); ylabel('EMG')
    end
end

%% plot specific trial
p = 3;
j = 3;
i = 1:1000;

if ishandle(N), close(N); end; figure(N);


for t = 2:6
    
    tlin = linspace(0,1/freqs(t), N);
    E = P(t,p,j)/2 - P(t,p,j)/2 * cos(2*pi*freqs(t)*(tlin+mtshift));

    subplot(1,5,t-1)
    plot(tlin, squeeze(emgrel_ts(:,t,p,j,i)), 'color', [1 0 0 .01]); hold on
    
    plot(tlin, memgrel_ts(:,t,p,j),'r-','linewidth',3)
    plot(tlin, E,'k-','linewidth',3);

    ylim([0 2*max(P(:,p,j))])
   
    yline(peakemg(t,p,j),'r-')
    
end


%% for each muscle
sfreqs = .5:.5:2.5;
i = 2:6;

peakemg2 = squeeze(max(memgrel_ts,[],1));

if ishandle(101), close(101); end; figure(101);

titles = {'VL','VM','RF'};
for j = 1:3

    
    subplot(1,3,j); 
    
    errorbar(sfreqs(:), mean(2*meanemg(i,:,j),2), std(2*meanemg(i,:,j),1,2),'o-'); hold on
    errorbar(sfreqs(:), mean(peakemg(i,:,j),2), std(peakemg(i,:,j),1,2),'o-'); hold on
    errorbar(sfreqs(:), mean(peakemg2(i,:,j),2), std(peakemg2(i,:,j),1,2),'o-')
    errorbar(sfreqs(:), mean(2 * max_power(i,:,j),2), std(2 * max_power(i,:,j),1,2),'o-')
    
    xlabel('Frequency (Hz)'); ylabel('EMG'); grid on; box off
    title(titles{j})
end

legend('2*mean','raw peak','interp peak','fourier'); legend boxoff

%% Hill model prediction
i = 2:6;
mP = mean(P(i,:,:),3);

vmax_exp = 0.02 * sfreqs; % m/s
% vmax = 1000 / 180 * pi * 0.045; % m/s

lceopt = 0.09;

% van Soest
vmax = 12 * lceopt; % m/s

% wakeling
vfast = 10;
vslow = 5;

f = .5; % fractional occupancy fast twitch
a = .2; % overall muscle activation

cslow = .18; 
cfast = .29;

vmax = (vslow + a * (vfast - vslow)) * lceopt; % m/s
cW = cslow + f * (cfast - cslow);
% vmax_exp = vmax_exp / vmax

% Hill-equation
F2 = @(c, v) (c(2)*(1+c(1))) ./ (v + c(2)) - c(1);
F2 = @(c, v) (c(1) + 1)./ (v/(c(1)*c(2)) + 1) - c(1);
v = linspace(0,vmax,N);

cs = linspace(.1,.5,10);

colors = [linspace(0,1,length(cs))' zeros(length(cs),1) linspace(1,0,length(cs))'];

if ishandle(20), close(20); end

Hpred = nan(length(cs), length(sfreqs));
for k = 1:length(cs)
    
c = [cs(k) vmax];

figure(20)
subplot(121); plot(v, F2(c, v),'color',colors(k,:)); hold on
subplot(122); plot(v, 1./F2(c, v),'color',colors(k,:)); hold on

xlim([0 max(vmax_exp)])

for f = 1:length(sfreqs)
    subplot(121); xline(vmax_exp(f),'--')
    subplot(122); xline(vmax_exp(f),'--')
end

Hpred(k,:) = 1./ (F2(c, vmax_exp) ./ F2(c, vmax_exp(1)));

end

% Yeadon
% r = .045; % [m]
% c1 = [15.4/13.4 13.4*r]; % subject 1, 7 parameter function
% c2 = [8.7/26.8  26.8*r];  % subject 2, 7 parameter function

vW = linspace(0,vmax,N);

subplot(121);
plot(vW, F2([cW vmax],vW),'linewidth',2,'color',color(3,:))
xlabel('Velocity (m/s)'); ylabel('Force (rel. to isometric)'); title('Force velocity')
ylim([0 1])

subplot(122);
plot(vW, 1./F2([cW vmax],vW),'linewidth',2,'color',color(3,:))
xlabel('Velocity (m/s)'); ylabel('Activation (rel. to isometric)'); title('Required peak activation')

%% compare to data

if ishandle(N), close(N); end; figure(N);

% for i = 1:length(cs)
% plot(freqs, Hpred(i,:) * mean(mP(1,:)),'--','color',colors(i,:)); hold on
% end

plot(sfreqs,mean(mP(1,:)) ./ (F2([cW vmax], vmax_exp) ./ F2([cW vmax], vmax_exp(1))),'-','color',color(3,:),'linewidth',2); hold on


% force-rate hypothesis
pfreqs = 0:.1:3;
activationdynamics = tf(1, [.1 1]); % low-pass filter 1/(T*s+1) 
[mag,~] = bode(1/activationdynamics, pfreqs*2*pi);
LP_effect = mag(:)';

plot(pfreqs, mean(mP(1,:),2) * LP_effect ./ (LP_effect(pfreqs == .5)),'color',color(2,:),'linewidth',2)

errorbar(sfreqs(:), mean(mP,2), std(mP,1,2),'o','color',color(5,:), 'markerfacecolor',color(5,:),'markersize',10); hold on
xlabel('Frequency (Hz)'); ylabel('EMG'); title('Peak EMG'); grid on; box off
xlim([0 3])

legend('force-velocity','pure force-rate','EMG data','location','best'); box off
