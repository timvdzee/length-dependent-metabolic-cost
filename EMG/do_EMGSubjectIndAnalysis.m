% This function is used to analyze the EMG data. Subject number, testing
% trials, and testing days can all be controlled to look at all trials or
% only individual trials. Data is filtered, downsampled, cut, averaged. The
% resulting figure visualizes tetsing EMG data for each subject.
function [graph] = do_EMGSubjectIndAnalysis(subNumber, TestingDay, trials);
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
subname = subnames{subNumber};
i = subNumber;
angles = [10:10:60];% Day 1 angles
Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; %Day 1
frequencies = [0, 0.5, 1, 1.5, 2, 2.5, 60];% Day 2 frequencies
dispfreq = {'0', '0.5', '1', '1.5', '2', '2.5','60/0'};% Frequencies for displaying 
Day2filenames = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv','_60deg_0Hz.csv'};
n = 10; %Downsample factor
if trials == 'all'
    if TestingDay == 1
    trials = [10:10:60];
    elseif TestingDay == 2 
        trials = [0, 0.5, 1, 1.5, 2, 2.5,60];
    end
end


if TestingDay == 1
    load('TorqueProcDay1.mat');% Load day 1 cut intervals
    
    if subNumber == 2
        
        % extract variables
        data = csvread('Koen_June21_submax.csv');% load CSV file
        t = data(:,1);% Time data
        Traw = data(:,2);%Torque data
        Tt = data(:,3);%Target data
        EMGraw = data(:,4:6);% EMG data raw
        fs = 1 / mean(diff(t));% Get sample frequency

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
 
        for j = 1:length(trials) % Loop through trials we want analyzed   
        for k = 1:length(angles) % Loop through angles
            if angles(k) == trials(j) % Only analyze data if trials we want analyzed match angles 
        %Cut EMG data
        if k == 1
        Ecut = Exercisecutint{i,6};% Get cut intervals for exercise
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);% Cut data
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);% Get mean of execise intervals
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);  
        
        elseif k == 2
        Ecut = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);

        elseif k == 3
        Ecut = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);

        elseif k == 4
        Ecut = Exercisecutint{i,4};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);

        elseif k == 5
        Ecut = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);

        elseif k == 6
        Ecut = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay1RVLMVCEMG = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay1RVMMVCEMG = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay1RRFMVCEMG = mean(RRFEMGcut);
        end  
        
        % Plot data
            H = 100+j;
            if ishandle(H), close(H); end
                
            figure(H);
            sgtitle((sprintf('Subject %d: %d Degrees-2Hz',subNumber,angles(k))));
            
            subplot(131);plot(EMG_RVL); hold on
            plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            title('Right Vastus Lateralis');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            legend('Raw EMG','Cut EMG');

            subplot(132)
            plot(EMG_RVM); hold on
            plot(Ecut(1):Ecut(2), RVMEMGcut,'--');
            title('Right Vastus Medialis');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');

            subplot(133)
            plot(EMG_RRF); hold on
            plot(Ecut(1):Ecut(2), RRFEMGcut,'--');
            title('Right Rectus Femoris');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            
            else
            continue
        end
        end
        end
        graph = 'displayed'
    else

        % loop over trials
        for j = 1:length(trials) % Loop through trials we want analyzed   
        for k = 1:length(Day1filenames) % Loop through files
            if angles(k) == trials(j) % Only analyze trials we want 
            data = csvread([subname,Day1filenames{k}]);% Load subjects CSV files

            % extract variables
            t = data(:,1);
            Traw = data(:,2);
            Tt = data(:,3);
            if subNumber == 1
            EMGraw = data(:,4:6);  % Subject 1 only has 3 muscles (Right leg VL,VM,RRF) 
            else
            EMGraw = data(:,4:8); % All others subjects have all 5 muscles
            end
            fs = 1 / mean(diff(t)); % Get sample frequency

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,5 / (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            if subNumber ~= 1
            FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
            FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
            end
            
            % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);
            EMG_RVM = downsample(FiltEMG_RVM,n);
            EMG_RRF = downsample(FiltEMG_RRF,n);
            if subNumber ~= 1
            EMG_LVM = downsample(FiltEMG_LVM,n);
            EMG_RB = downsample(FiltEMG_RB,n);
            end

            %Cut and Plot EMG data
            Ecut = Exercisecutint{i,k}*200;
            RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);

            RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
           
            RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
            if subNumber ~= 1
            LVMEMGcut = EMG_LVM(Ecut(1):Ecut(2),:);
            RBEMGcut = EMG_RB(Ecut(1):Ecut(2),:);
            end
            
            % Plot data
            H = 100+j;
            if ishandle(H), close(H); end
                
            figure(H);
            sgtitle((sprintf('Subject %d: %d Degrees-2Hz',subNumber,angles(k))));
            
            subplot(131);plot(EMG_RVL); hold on
            plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            title('Right Vastus Lateralis');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            legend('Raw EMG','Cut EMG');

            subplot(132)
            plot(EMG_RVM); hold on
            plot(Ecut(1):Ecut(2), RVMEMGcut,'--');
            title('Right Vastus Medialis');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');

            subplot(133)
            plot(EMG_RRF); hold on
            plot(Ecut(1):Ecut(2), RRFEMGcut,'--');
            title('Right Rectus Femoris');ylim([-.01 0.5]);
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            else
                continue
            end
            
        end
        end
    end
graph = 'displayed'

elseif TestingDay == 2
    load('TorqueProcDay2.mat'); % Load day 2 cut intervals
    if subNumber == 2
        % extract variables
        data = csvread('Koen_June21_submax_variable_freq.csv'); % Load CSV file
        t = data(:,1);
        Traw = data(:,2);
        Tt = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        
        %Cut EMG data
      for j = 1:length(trials) % Loop through trials we want analyzed   
        for k = 1:length(frequencies) % Loop through frequencies
            if frequencies(k) == trials(j) % Only analyze data if it matches trials we want analyzed
        if k == 1
        Ecut = Exercisecutint{i,4}; % Get cut intervals
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:); % Cut data
        KoenDay2RVLMVCEMG(1,1) = mean(RVLEMGcut);% Get mean of cut section
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,1) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,1) = mean(RRFEMGcut);
        
        elseif k == 2
        Ecut = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,2) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,2) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,2) = mean(RRFEMGcut);
        
        elseif k == 3
        Ecut = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,3) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,3) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,3) = mean(RRFEMGcut);

        elseif k == 4
        Ecut = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,4) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,4) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,4) = mean(RRFEMGcut);

        elseif k == 5
        Ecut = Exercisecutint{i,6};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,5) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,5) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,5) = mean(RRFEMGcut);

        elseif k == 6
        Ecut = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,6) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,6) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,6) = mean(RRFEMGcut);
        
        elseif k == 7
        Ecut = Exercisecutint{i,7};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        KoenDay2RVLMVCEMG(1,7) = mean(RVLEMGcut);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        KoenDay2RVMMVCEMG(1,7) = mean(RVMEMGcut);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        KoenDay2RRFMVCEMG(1,7) = mean(RRFEMGcut);
        end
        
        % Plot data
            H = 100+j;
            if ishandle(H), close(H); end
                
            figure(H);
            if k==7
            sgtitle((sprintf('Subject %d: 0 Hz-60 Degrees',subNumber)));
            else
            sgtitle((sprintf('Subject %d: %s Hz-20 Degrees',subNumber,dispfreq{k})));
            end
            
            subplot(131);plot(EMG_RVL); hold on
            plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            title('Right Vastus Lateralis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            legend('Raw EMG','Cut EMG');

            subplot(132)
            plot(EMG_RVM); hold on
            plot(Ecut(1):Ecut(2), RVMEMGcut,'--');
            title('Right Vastus Medialis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');

            subplot(133)
            plot(EMG_RRF); hold on
            plot(Ecut(1):Ecut(2), RRFEMGcut,'--');
            title('Right Rectus Femoris');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            else
                continue
            end
        end
      end
           
graph = 'displayed';

elseif subNumber == 3
    % extract variables
        data = csvread('Billy_June27_variable_frequency.csv'); % Read CSV file
        t = data(:,1);
        Traw = data(:,2);
        Tt = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,5 / (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        
        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        
        %Cut EMG data
        for j = 1:length(trials)    % Loop through trials we want analyzed
        for k = 1:length(frequencies) % Loop through frequencies
            if frequencies(k) == trials(j) % Only analyze data if it matches trials we want analyzed
            
        if k == 1
        Ecut = Exercisecutint{i,1};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        
        elseif k == 2
        Ecut = Exercisecutint{i,3};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        
        elseif k == 3
        Ecut = Exercisecutint{i,7};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);

        elseif k == 4
        Ecut = Exercisecutint{i,6};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);

        elseif k == 5
        Ecut = Exercisecutint{i,2};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);

        elseif k == 6
        Ecut = Exercisecutint{i,5};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        
        elseif k == 7
        Ecut = Exercisecutint{i,4};
        RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);
        RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);
        RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);
        end
            
        % Plot data
            H = 100+j;
            if ishandle(H), close(H); end
            
            figure(H);
            if k==7
            sgtitle((sprintf('Subject %d: 0 Hz-60 Degrees',subNumber)));
            else
            sgtitle((sprintf('Subject %d: %s Hz-20 Degrees',subNumber,dispfreq{k})));
            end
            
            subplot(131);plot(EMG_RVL); hold on
            plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            title('Right Vastus Lateralis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            legend('Raw EMG','Cut EMG');

            subplot(132)
            plot(EMG_RVM); hold on
            plot(Ecut(1):Ecut(2), RVMEMGcut,'--');
            title('Right Vastus Medialis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');

            subplot(133)
            plot(EMG_RRF); hold on
            plot(Ecut(1):Ecut(2), RRFEMGcut,'--');
            title('Right Rectus Femoris');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            else
                continue
            end
        end
        end
        graph = 'displayed'
    else 
                          
               % Loop Over Trials
         for j = 1:length(trials)  % Loop through trials we want analyzed
         for k = 1:length(Day2filenames) % Loop through day 2 files
            if frequencies(k) == trials(j) % Only analyze data if it matches trials we want analyzed
                    data = csvread([subname,Day2filenames{k}]);
                    % extract variables
                    t = data(:,1);
                    Traw = data(:,2);
                    Tt = data(:,3);
                    if subNumber == 1
                    EMGraw = data(:,4:6); 
                    else
                    EMGraw = data(:,4:8);
                    end
                    fs = 1 / mean(diff(t));

                    %  band-pass filter EMG
                    fc1 = [30 500];
                    [b,a] = butter(2,fc1 / (fs*.5));
                    [b2,a2] = butter(2,5 / (fs*.5),'low');

                    FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
                    FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
                    FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
                    if subNumber ~= 1
                    FiltEMG_LVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,4)))));
                    FiltEMG_RB = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,5)))));
                    end

                    % Downsample EMG data
                    EMG_RVL = downsample(FiltEMG_RVL,n);
                    EMG_RVM = downsample(FiltEMG_RVM,n);
                    EMG_RRF = downsample(FiltEMG_RRF,n);
                    if subNumber ~= 1
                    EMG_LVM = downsample(FiltEMG_LVM,n);
                    EMG_RB = downsample(FiltEMG_RB,n);
                    end

                    % Cut and plot EMG data 
                    Ecut = Exercisecutint{i,k}*200; % Get cut intervals
                    RVLEMGcut = EMG_RVL(Ecut(1):Ecut(2),:);% Cut data

                    RVMEMGcut = EMG_RVM(Ecut(1):Ecut(2),:);

                    RRFEMGcut = EMG_RRF(Ecut(1):Ecut(2),:);

                    if subNumber ~= 1
                    LVMEMGcut = EMG_LVM(Ecut(1):Ecut(2),:);
                    RBEMGcut = EMG_RB(Ecut(1):Ecut(2),:);
                    end
                    
                    % Plot figure
            H = 100+j;
            if ishandle(H), close(H); end
                
            figure(H);
            if k==7
            sgtitle((sprintf('Subject %d: 0 Hz-60 Degrees',subNumber)));
            else
            sgtitle((sprintf('Subject %d: %s Hz-20 Degrees',subNumber,dispfreq{k})));
            end
            
            subplot(131);plot(EMG_RVL); hold on
            plot(Ecut(1):Ecut(2), RVLEMGcut,'--');
            title('Right Vastus Lateralis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            legend('Raw EMG','Cut EMG');

            subplot(132)
            plot(EMG_RVM); hold on
            plot(Ecut(1):Ecut(2), RVMEMGcut,'--');
            title('Right Vastus Medialis');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');

            subplot(133)
            plot(EMG_RRF); hold on
            plot(Ecut(1):Ecut(2), RRFEMGcut,'--');
            title('Right Rectus Femoris');
            if (k == 1)||(k==7);ylim([-.01 0.2]);
            else ylim([-.01 0.5]);
            end
            xlabel('sample rate');ylabel('EMG Signal (mV)');
            else
                continue
            end
         end
         end
    
graph = 'displayed';
end
end
end

