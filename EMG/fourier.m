function[f,p] = fourier(x, fs)

if rem(length(x),2) > 0
    x = x(1:end-1);
end

    nfft = length(x);
    res = fft(x,nfft)/ nfft; % normalizing the fft
    f = fs/2*linspace(0,1,nfft/2+1); % choosing correct frequency axes
    res = res(1:nfft/2+1);
    p = 2 * abs(res);
%     p = sqrt(abs(res));
end