clear all; close all; clc
% This script analyzes each of the different butterworth filter cutoffs
% used to see which best fits the data by visualizing the data's trends
%% 
cutoffs = {'2','5','6','8','11','15','20','30','50'};
angles = [10:10:60];symbols = ['o','*','s','d','p','^','v','x','h','<','>','+'];
% Day 1
for i = 1:length(cutoffs)
    load([cutoffs{i},'_PeakEMGAnalysisDay1.mat']);
    for j = 1:12
        if j == 1
            MeanDay1RVLEMGPeak{i}(j,:)=nan(1,6);
        else
            MeanDay1RVLEMGPeak{i}(j,:)=mean(Day1RVLEMGPeak{j},2,'omitnan');
        end
    end
end

frequencies = [0,1,2,0.5,1.5,2.5];
[freq,I]=sort(frequencies);
% Day 2
for i = 1:length(cutoffs)
    load([cutoffs{i},'_PeakEMGAnalysisDay2.mat']);
    for j = 1:12
        if j == 1
            MeanDay2RVLEMGPeak{i}(j,:)=nan(1,6);
        else
            unsortedPeakDay2=mean(Day2RVLEMGPeak{j}(1:6,:),2,'omitnan');
            MeanDay2RVLEMGPeak{i}(j,:) = unsortedPeakDay2(I);
        end
    end
end

%% Plot Summary figures
figure();color =  [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:length(cutoffs)
    plot(angles,mean(MeanDay1RVLEMGPeak{i},'omitnan'),symbols(i),'markersize',15,'color', color(i,:),'LineStyle','-');hold on
end
xlabel('Knee angle (deg)');ylabel('Peak EMG (%MVC)');
%legend('2','5','6','8','11','15','20','30','50');
title('Peak EMG Signal vs Knee Angle');

figure();color =  [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:length(cutoffs)
    plot(freq,mean(MeanDay2RVLEMGPeak{i},'omitnan'),symbols(i),'markersize',15,'color', color(i,:),'LineStyle','-'); hold on
end
xlabel('Waveform frequency (Hz)');ylabel('Peak EMG (%MVC)');
%legend('2','5','6','8','11','15','20','30','50');
title('Peak EMG Signal vs Waveform Frequency');
