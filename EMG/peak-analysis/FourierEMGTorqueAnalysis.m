clear all; close all; clc
% This script performs a fourier analysis on both EMG and torque data. Then
% the results are visualized looking at peak power using a welch analysis
% in addition to the frequency where peak power was found comapred to
% expected waveform frequency
%% Load data
load('50_RVL_PeakEMGProcessedDay2.mat'); % Load EMG data for day 2
load('TorqueProcDay2.mat'); % Load torque data for day 2

N_per_volt = 69.71; % Conversion V to Nm 
frequenciestorque = [0, 0.5, 1, 1.5, 2, 2.5];% Frequency order 
dispfreq = {'0', '0.5', '1', '1.5', '2', '2.5'};
WaveformAmp = ([0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163]*0.5)*N_per_volt; % Subjects waveform ampltude
% Subjects day 2 trial order
day2order = [1,4,2,5,3,6;4,3,1,6,2,5;1,6,3,2,7,5;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6];
%% Cut torque data
for i = 1:11
    for k = 1:length(frequenciestorque)
        if (i == 2)||(i == 3)
            o = day2order(i, k);% Get order of trials for each subject
            TE = torque{i,o};% Pull out specific trial
            Ecut = Exercisecutint{i,o};% Get cut interval for that trial
            Ecut = round(Ecut);
            Ecutdiff = round(Ecut(2)-Ecut(1)); % Find difference in cut, specific to subjects 2 and 3
            ETcut = TE(end-Ecutdiff:end,:);
            targetexc=target{i,o};% Pull out matching target trial
            targetcut = targetexc(end-Ecutdiff:end,:)+WaveformAmp(i);
            torquetarget{i}(k,1:length(targetcut)) = targetcut*N_per_volt;
            
            Gcut = Gravitycutint{i,o};% Pull out matching gravity cut 
            Gcut = round(Gcut);
            Gcutdiff = round(Gcut(2)-Gcut(1));
            GTcut = TE(1:Gcutdiff,:);
            Day2GravTorqueAvg = mean (GTcut);
            torquecut{i}(k,1:length(ETcut)) = (Day2GravTorqueAvg - ETcut)*N_per_volt;
            
        else
            o = day2order(i, k);% Get order of trials for each subject
            TE = torque{i,o};% Pull out specific trial
            targetex = target{i,o};% Pull out matching target trial
            Ecut = Exercisecutint{i,o}*200;% Get cut interval for that trial
            Ecut = round(Ecut);
            ETcut = TE(Ecut(1):Ecut(2),:);
            targetcut = targetex(Ecut(1):Ecut(2),:)+WaveformAmp(i);% Cut target and zero using waveform amp
            torquetarget{i}(k,1:length(targetcut)) = targetcut*N_per_volt;% Convert to Nm

            Gcut = Gravitycutint{i,o}*200;% Pull out matching gravity cut 
            Gcut = round(Gcut);
            GTcut = TE(Gcut(1):Gcut(2),:);
            Day2GravTorqueAvg = mean (GTcut);
            torquecut{i}(k,1:length(ETcut)) = (Day2GravTorqueAvg - ETcut)*N_per_volt; % Zero using gravity measure and convert to Nm           
            
        end
    end
end
%% EMG pre-process
visualize = 0; % Visualize individual results 

for i = 1:11 % Loop through all 11 subjects
EMG = RVLEMGcut{i};% Pull out one subjects data
T = CutTarget{i};%Pull of subjects target data

for o = 1:6
    Ns(o) = sum(isfinite(EMG(o,:)));
end

N = min(Ns);

EMG = EMG(:, 1:N);
T = T(:, 1:N);

k = 1:N;

fs = 200;
dt = 1/fs;
N = sum(k);
t = 0:dt:((N-1)*dt);

EMG = EMG(:,k);
T = T(:,k);
%% EMG fourier analysis

for c = 2:6 % Loop through frequencies 0.5-2.5
[Pxx1,F1] = pwelch(EMG(c,:)-mean(EMG(c,:),'omitnan'),[],[],[],fs);% Perform welch analysis for EMG data
%[Pxx1T,F1T] = pwelch(T(c,:)-mean(T(c,:),'omitnan'),[],[],[],fs);% Perform welch analysis for torque target data

[Pmax(i,c), Index] = max(Pxx1); % Find max EMG power

Fourierfreq(i,c) = F1(Index); %Get fournier frequency corresponding to peak EMG power

F1Interp = [0:0.1:2.5];% Create spaced frequency for interpolation
[Pxx1Interp]=interp1(F1,Pxx1,F1Interp);% Perform interp on power data 
[ind]=find(F1Interp==frequencies(c));% Find index where waveform frequency is equal to interpolated frequency 
PowerWaveform(i,c) = Pxx1Interp(ind);% Find the corresponding power to waveform frequency

if visualize
figure(i) % Plot resulting data 
subplot(2,3,c);
plot(F1(Index-10:Index+10),Pxx1(Index-10:Index+10)); hold on
plot(frequencies(c),PowerWaveform(i,c),'ro');hold on
plot(F1(Index), Pxx1(Index),'bo');hold on
xline(frequencies(c),'r',LineWidth=2);hold on
xline(Fourierfreq(i,c),'b',LineWidth=1);hold on
legend('Power','Max Waveform Power','Max Fourier Power','Waveform Frequency','Fourier Frequency');
xlabel('Frequency (Hz)');ylabel('EMG Power (W)');
title([num2str(round(frequencies(c),1)),' Hz']);
sgtitle(['EMG Power Subject ',num2str(i)]);
end
end

% Sort EMG data to match torque order
[FreqSort,I]=sort(frequencies');% Sort frequency order 
PmaxSort(i,:)=Pmax(i,I);% Sort max power
PowerWaveformSort(i,:)=PowerWaveform(i,I);%Sort waveform frequency power
Fourierfreq(i,:)=Fourierfreq(i,I);% Sort fourier frequency power
Tsort = T(I,:);% Sort target data to match torque data frequency trial order


%% Process Torque data

% Pull out subjects data
Torque = torquecut{i};
Target = torquetarget{i};

Torque(Torque==0)=nan; % Remove unneeded zero points to become nans

for o = 1:6 % Loop through each frequency to get sample size for each trial
    NsT(o) = sum(isfinite(Torque(o,:)));
end

NT = min(NsT);%Detemine lowest trial sample size

% Cut data based on lowest sample sized 
Torque = Torque(:, 1:NT);
Target = Target(:, 1:NT);

k = 1:NT;

fsT = 200;
dtT = 1/fsT;
NT = sum(k);
tT = 0:dtT:((NT-1)*dtT);% Detemine time interval 

Torque = Torque(:,k);
Target = Target(:,k);
%% Torque fourier analysis

for c = 2:6 % Loop through frequencies 0.5-2.5
[Pxx1Torque,F1Torque] = pwelch(Torque(c,:)-mean(Torque(c,:),'omitnan'),[],[],[],fs);% Perform welch analysis for Torque data
[Pxx1T,F1T] = pwelch(Target(c,:)-mean(Target(c,:),'omitnan'),[],[],[],fs);% Perform welch analysis for torque target data

[PmaxTorque(i,c), Index] = max(Pxx1Torque); % Find max Torque power
PmaxTorqueNorm(i,c) = PmaxTorque(i,c)/WaveformAmp(i);% Normalize torque power by individual waveform amp

[PmaxT(i,c), ~] = max(Pxx1T); % Find max target power
PmaxTNorm(i,c) = PmaxT(i,c)/WaveformAmp(i);% Normalize target power by individual waveform amp

FourierfreqTorque(i,c) = F1Torque(Index); %Get fournier frequency corresponding to peak Torque power

F1InterpTorque = [0:0.1:2.5];% Create spaced frequency for interpolation
[Pxx1TorqueInterp]=interp1(F1Torque,Pxx1Torque,F1InterpTorque);% Perform interp on power data 
[indT]=find(F1InterpTorque==FreqSort(c));% Find index where waveform frequency is equal to interpolated frequency 
PowerWaveformTorque(i,c) = Pxx1TorqueInterp(indT);% Find the corresponding power to waveform frequency

PowerWaveformTorqueNorm(i,c) = PowerWaveformTorque(i,c)/WaveformAmp(i);% Normalize torque power by individual waveform amp

if visualize
figure(i+11) % Plot resulting data 
subplot(2,3,c);
plot(F1Torque(Index-10:Index+10),Pxx1Torque(Index-10:Index+10)); hold on
plot(FreqSort(c),PowerWaveformTorque(i,c),'ro');hold on
plot(F1Torque(Index), Pxx1Torque(Index),'bo');hold on
xline(FreqSort(c),'r',LineWidth=2);hold on
xline(FourierfreqTorque(i,c),'b',LineWidth=1);hold on
legend('Power','Max Waveform Power','Max Fourier Power','Waveform Frequency','Fourier Frequency');
xlabel('Frequency (Hz)');ylabel('Torque Power (W)');
title([num2str(round(FreqSort(c),1)),' Hz']);
sgtitle(['Torque Power Subject ',num2str(i)]);
end
end

end

save('FourierSummary','subnames','WaveformAmp','Pmax','PmaxTorqueNorm','PmaxTNorm','PmaxTorque','PmaxT','FreqSort','FourierfreqTorque','Fourierfreq','PowerWaveform','PowerWaveformTorqueNorm','PowerWaveformTorque');



