clear all; close all; clc
%% Load data 
load('FourierSummary.mat');
%% Plot Summary figures
figure()
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
color = [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:11
    plot(FreqSort,Pmax(i,:),symbols(i), 'color','b','markersize',7); hold on
end
errorbar(FreqSort,mean(Pmax,'omitnan'),std(Pmax,'omitnan'),'b.',MarkerSize=20,LineWidth=1.5);hold on
for i = 1:11
    plot(FreqSort,PowerWaveform(i,:),symbols(i), 'color','r','markersize',7); hold on
end
errorbar(FreqSort,mean(PowerWaveform,'omitnan'),std(PowerWaveform,'omitnan'),'r.',MarkerSize=25,LineWidth=1.5);hold on
xlabel('Waveform Frequency (Hz)');
ylabel('Peak EMG Power (W)');
title('Peak EMG Power vs Waveform Frequency');
legend('1','2','3','4','5','6','7','8','9','10','11','Fourier Power mean');

figure()
plot([0:0.1:3],[0:0.1:3],'k-',LineWidth=1.5);hold on
for i = 1:11
    plot(FreqSort,Fourierfreq(i,:),symbols(i), 'color', color(i,:),'markersize',5); hold on
end
errorbar(FreqSort,mean(Fourierfreq,'omitnan'),std(Fourierfreq,'omitnan'),'b.',MarkerSize=15);hold on
legend ('Frequency','Unity Line');
xlabel('Waveform Frequency (Hz)');
ylabel('EMG Fourier Analysis Frequencies (Hz)');
title('EMG Frequency Sanity Check');legend('Unity Line','1','2','3','4','5','6','7','8','9','10','11','mean');
xlim([0 3]);ylim([0 3])

figure()
for i = 1:11
    plot(FreqSort,PmaxTorqueNorm(i,:),symbols(i), 'color','b','markersize',7); hold on
end
errorbar(FreqSort,mean(PmaxTorqueNorm,'omitnan'),std(PmaxTorqueNorm,'omitnan'),'b.',MarkerSize=20,LineWidth=1.5);hold on
for i = 1:11
    plot(FreqSort,PowerWaveformTorqueNorm(i,:),symbols(i), 'color','r','markersize',7); hold on
end
errorbar(FreqSort,mean(PowerWaveformTorqueNorm,'omitnan'),std(PowerWaveformTorqueNorm,'omitnan'),'r.',MarkerSize=25,LineWidth=1.5);hold on
xlabel('Waveform Frequency (Hz)');
ylabel('Normalized Peak Torque Power (W/Nm)');
title('Peak Torque Power vs Waveform Frequency');
legend('1','2','3','4','5','6','7','8','9','10','11','Fourier Power mean');

figure()
plot([0:0.1:3],[0:0.1:3],'k-',LineWidth=1.5);hold on
for i = 1:11
    plot(FreqSort,FourierfreqTorque(i,:),symbols(i), 'color', color(i,:),'markersize',5); hold on
end
errorbar(FreqSort,mean(FourierfreqTorque,'omitnan'),std(FourierfreqTorque,'omitnan'),'b.',MarkerSize=15);hold on
legend ('Frequency','Unity Line');
xlabel('Waveform Frequency (Hz)');
ylabel('Torque Fourier Analysis Frequencies (Hz)');
title('Torque Frequency Sanity Check');legend('Unity Line','1','2','3','4','5','6','7','8','9','10','11','mean');
xlim([0 3]);ylim([0 3])

figure()
for i = 1:11
    plot(FreqSort,PmaxTNorm(i,:),symbols(i), 'color','b','markersize',7); hold on
end
errorbar(FreqSort,mean(PmaxTNorm,'omitnan'),std(PmaxTNorm,'omitnan'),'b.',MarkerSize=20,LineWidth=1.5);hold on
xlabel('Waveform Frequency (Hz)');
ylabel('Normalized Peak Waveform Target Power (W/Nm)');
title('Peak Waveform Target Power vs Waveform Frequency');
legend('1','2','3','4','5','6','7','8','9','10','11','Fourier Power mean');

%%
EPrel = Pmax(:,2:6) ./ mean(Pmax,2);
TPrel = PmaxTorqueNorm(:,2:6) ./ mean(PmaxTorqueNorm,2);

if ishandle(10), close(10); end; figure(10);
for i = 1:11
    
    subplot(131); 
    plot(FreqSort(2:end),TPrel(i,:),symbols(i), 'color', color(i,:),'markersize',5); hold on
    
    subplot(132); 
    plot(FreqSort(2:end),EPrel(i,:),symbols(i), 'color', color(i,:),'markersize',5); hold on
    
    subplot(133); 
    plot(FreqSort(2:end),(EPrel(i,:)./TPrel(i,:)),symbols(i), 'color', color(i,:),'markersize',5); hold on
end

subplot(131); errorbar(FreqSort(2:end),mean(TPrel,'omitnan'),std(TPrel,'omitnan'),'b.',MarkerSize=20);hold on
subplot(132); errorbar(FreqSort(2:end),mean(EPrel,'omitnan'),std(EPrel,'omitnan'),'b.',MarkerSize=20);hold on
subplot(133); errorbar(FreqSort(2:end),mean((EPrel./TPrel),'omitnan'),std((EPrel./TPrel),'omitnan'),'b.',MarkerSize=20);hold on

% legend('1','2','3','4','5','6','7','8','9','10','11','Fourier Power Ratio mean')

titles = {'Torque', 'EMG', 'EMG/Torque'};
for i = 1:3
    subplot(1,3,i)
    xlabel('Waveform Frequency (Hz)');
    ylabel('Relative power');
    title(titles{i})
end

%% vs metabolic power
load('RespiratorySummary.mat')

Day2MetPowerrel = Day2MetPower(:,2:end-1) ./ mean(Day2MetPower(:,2:end-1),2);


if ishandle(11), close(11); end; figure(11)

plot(EPrel./TPrel, Day2MetPowerrel,'.','markersize',10); hold on

color = get(gca,'colororder');
for i = 1:5
    errorbar(mean(EPrel(:,i)./TPrel(:,i),1), mean(Day2MetPowerrel(:,i),1), std(Day2MetPowerrel(:,i),0,1), '.','markersize',20,'color',color(i,:)); hold on
end

plot([0 4], [0 4],'k-')
xlabel('EMG/Torque power'); ylabel('Relative metabolic power')