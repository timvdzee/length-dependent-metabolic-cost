clear all; close all; clc
% This script takes the previously processed EMG data and analyzes it based 
% on the peaks found in the torque target data. The data corresponding to
% each peak including the max EMG values is saved
%% Day 1 EMG Analysis

% subject names (according to protocol)
subnames = {'Tim', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby','S12'};
N = length(subnames);

% load MVC
load('MVCDay1EMGData.mat')

% some parameters
showfig = true;
angles = [10:10:60];
Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; %Day 1
n = 10; %Downsample factor
Peakcutoff = [2,5,6,8,11,15,20,30,50];
cutoffs = {'2','5','6','8','11','15','20','30','50'};

% Create not a number filled variable for each EMG average 
% Day1RVLEMGPeak = nan(N,length(Day1filenames),10000);
% Day1RVMEMGPeak = nan(N,length(Day1filenames),10000);
% Day1RRFEMGPeak = nan(N,length(Day1filenames),10000);

%Resave variables
resave = true

% Show plots
visualize = 0;

for l = 1:length(cutoffs)
    clear Day1RVLEMGPeak;clear Day2RVLEMGPeak;clear Day1RVMEMGPeak;clear Day2RVMEMGPeak;clear Day1RRFEMGPeak;clear Day2RRFEMGPeak;
%% Day 1 Analysis
load([cutoffs{l},'_RVL_PeakEMGProcessedDay1']);
load([cutoffs{l},'_RVM_PeakEMGProcessedDay1']);
load([cutoffs{l},'_RRF_PeakEMGProcessedDay1']);
for i = 1:N
    if i == 1
        continue
    else

% loop over trials
        for k = 1:length(Day1filenames)

            RVLEMGcut2 = RVLEMGcut{i}(k,:);
            RVMEMGcut2 = RVMEMGcut{i}(k,:);
            RRFEMGcut2 = RRFEMGcut{i}(k,:);
            CutTarget2 = CutTarget{i}(k,:);

            [pks,locs]=findpeaks(CutTarget2);

            for j = 1:[length(pks)-1]
                loc1 = locs(j);loc2=locs(j+1);

                Day1RVLEMGPeak{i}(k,j) = (max(RVLEMGcut2(loc1:loc2))-Day1RVLEMGAvgZero(i,k))/MVCDay1RVLEMGAvg(i,1)*100;

                Day1RVMEMGPeak{i}(k,j) = (max(RVMEMGcut2(loc1:loc2))-Day1RVMEMGAvgZero(i,k))/MVCDay1RVMEMGAvg(i,1)*100;
            
                Day1RRFEMGPeak{i}(k,j) = (max(RRFEMGcut2(loc1:loc2))-Day1RRFEMGAvgZero(i,k))/MVCDay1RRFEMGAvg(i,1)*100;
            
                if visualize
                alpha = 0.02
                figure(i+[12*l]);subplot(2,3,k);plot(RVLEMGcut2(loc1:loc2),'color',[0,0,1,alpha]);hold on 
                plot(Day1RVLEMGPeak{i}(k,j),'b.','MarkerSize',10); hold on
                sgtitle((sprintf('Subject %d: Cutoff %d Hz ',subnames{i},cutoffs{i})));
                end
            end
            

        end
    RVL = Day1RVLEMGPeak{i};
    RVL(RVL==0)=nan;
    Day1RVLEMGPeak{i}=RVL;

    RVM = Day1RVMEMGPeak{i};
    RVM(RVM==0)=nan;
    Day1RVMEMGPeak{i}=RVM;

    RRF = Day1RRFEMGPeak{i};
    RRF(RRF==0)=nan;
    Day1RRFEMGPeak{i}=RRF;
    end
end
if resave
save([cutoffs{l},'PeakEMGAnalysisDay1'],'subnames','Day1RVLEMGPeak','Day1RVMEMGPeak','Day1RRFEMGPeak','DSsr','sr','angles');%,'Day1RVMEMGPeak','Day1RRFEMGPeak'
end
%% Day 2 Analysis
% Some parameters
frequencies = [0,1,2,0.5,1.5,2.5,0];
Day2filenames = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv'};
N = length(subnames);
% Load Day 2 MVC
load('MVCDay2EMGData.mat');
load([cutoffs{l},'_RVL_PeakEMGProcessedDay2']);
load([cutoffs{l},'_RVM_PeakEMGProcessedDay2']);
load([cutoffs{l},'_RRF_PeakEMGProcessedDay2']);

for i = 1:N
    if i == 1
        continue
    else

        for k = 1:length(Day2filenames)

            RVLEMGcut2 = RVLEMGcut{i}(k,:);
            RVMEMGcut2 = RVMEMGcut{i}(k,:);
            RRFEMGcut2 = RRFEMGcut{i}(k,:);
            CutTarget2 = CutTarget{i}(k,:);

            [pks,locs]=findpeaks(CutTarget2);

            for j = 1:[length(pks)-1]
                loc1 = locs(j);loc2=locs(j+1);

                Day2RVLEMGPeak{i}(k,j) = (max(RVLEMGcut2(loc1:loc2))-Day2RVLEMGAvgZero(i,k))/MVCDay2RVLEMGAvg(i,1)*100;

                Day2RVMEMGPeak{i}(k,j) = (max(RVMEMGcut2(loc1:loc2))-Day2RVMEMGAvgZero(i,k))/MVCDay2RVMEMGAvg(i,1)*100;

                Day2RRFEMGPeak{i}(k,j) = (max(RRFEMGcut2(loc1:loc2))-Day2RRFEMGAvgZero(i,k))/MVCDay2RRFEMGAvg(i,1)*100;
            
                if visualize
                figure(100+[i+[12*l]]);subplot(2,3,k);plot(RVLEMGcut2(loc1:loc2),'color',[0,0,1,alpha]);hold on 
                plot(Day2RVLEMGPeak{i}(k,j),'b.','MarkerSize',10); hold on
                sgtitle((sprintf('Subject %d: Cutoff %d Hz ',subnames{i},cutoffs{i})));
                end
            end
            

        end


    RVL2 = Day2RVLEMGPeak{i};
    RVL2(RVL2==0)=nan;
    Day2RVLEMGPeak{i}=RVL2;

    RVM2 = Day2RVMEMGPeak{i};
    RVM2(RVM2==0)=nan;
    Day2RVMEMGPeak{i}=RVM2;

    RRF2 = Day2RRFEMGPeak{i};
    RRF2(RRF2==0)=nan;
    Day2RRFEMGPeak{i}=RRF2;
    end
end


if resave
save([cutoffs{l},'PeakEMGAnalysisDay2'],'subnames','Day2RVLEMGPeak','Day2RVMEMGPeak','Day2RRFEMGPeak','DSsr','sr','frequencies');%,'Day1RVMEMGPeak','Day1RRFEMGPeak'
end
end