clear all; close all; clc
% This script process each subjects EMG data by cutting it and filtering it
% using the cutoffs of 2,5,6,8,11,15,20,30,50 and is finished by being
% downsampled and saved.
%% Day 1 EMG Analysis

% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);

% load MVC
load('MVCDay1EMGData.mat')

% some parameters
showfig = true;
angles = [10:10:60];
Day1filenamesEMG = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; %Day 1
n = 10; %Downsample factor
Peakcutoff = 100; %[2,5,6,8,11,15,20,30,50];
cutoffs = {'100'}; %{'2','5','6','8','11','15','20','30','50'};%
sr = 2000;
DSsr = 200;

%Resave variables
resave = true

for j = 1:length(cutoffs)
    for i = 1:length(subnames)
            RVLEMGcut{i} = nan(6,71937);
            RVMEMGcut{i} = nan(6,71937);
            RRFEMGcut{i} = nan(6,71937);
            CutTarget{i}= nan(6,71937);
    end


% Day 1 Process
load('TorqueProcDay1.mat');
for i = 1:N
       if i == 2
        % extract variables
        data = csvread('Koen_June21_submax.csv');
        t = data(:,1);
        Traw = data(:,2);
        TargetRaw = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,Peakcutoff(j) / (fs*.5),'low');
        [bt,at] = butter(2,5/ (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        filterTarget = filtfilt(bt,at,TargetRaw);

        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        Target = downsample(filterTarget,n);

        %Cut EMG data

        Ecut10 = Exercisecutint{i,6};
        CutTarget10 = Target(Ecut10(1):Ecut10(2),:);
        CT=CutTarget10-min(CutTarget10);
        CutTarget{i}(1,1:length(CT)) = CT;

        Gcut10 = Gravitycutint{i,6};
        RVLEMGcutgrav = EMG_RVL(Gcut10(1):Gcut10(2),:);
        Day1RVLEMGAvgZero(i,1) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut10(1):Ecut10(2),:);
        RVLEMGcut{i}(1,1:length(RVLC)) = RVLC;

        
        Ecut20 = Exercisecutint{i,2};
        CutTarget20 = Target(Ecut20(1):Ecut20(2),:);
        CT=CutTarget20-min(CutTarget20);
        CutTarget{i}(2,1:length(CT)) = CT;

        Gcut20 = Gravitycutint{i,2};
        RVLEMGcutgrav = EMG_RVL(Gcut20(1):Gcut20(2),:);
        Day1RVLEMGAvgZero(i,2) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut20(1):Ecut20(2),:);
        RVLEMGcut{i}(2,1:length(RVLC)) = RVLC;


        Ecut30 = Exercisecutint{i,5};
        CutTarget30 = Target(Ecut30(1):Ecut30(2),:);
        CT=CutTarget30-min(CutTarget30);
        CutTarget{i}(3,1:length(CT)) = CT;

        Gcut30 = Gravitycutint{i,5};
        RVLEMGcutgrav = EMG_RVL(Gcut30(1):Gcut30(2),:);
        Day1RVLEMGAvgZero(i,3) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut30(1):Ecut30(2),:);
        RVLEMGcut{i}(3,1:length(RVLC)) = RVLC;


        Ecut40 = Exercisecutint{i,4};
        CutTarget40 = Target(Ecut40(1):Ecut40(2),:);
        CT=CutTarget40-min(CutTarget40);
        CutTarget{i}(4,1:length(CT)) = CT;

        Gcut40 = Gravitycutint{i,4};
        RVLEMGcutgrav = EMG_RVL(Gcut40(1):Gcut40(2),:);
        Day1RVLEMGAvgZero(i,4) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut40(1):Ecut40(2),:);
        RVLEMGcut{i}(4,1:length(RVLC)) = RVLC;


        Ecut50 = Exercisecutint{i,1};
        CutTarget50 = Target(Ecut50(1):Ecut50(2),:);
        CT=CutTarget50-min(CutTarget50);
        CutTarget{i}(5,1:length(CT)) = CT;

        Gcut50 = Gravitycutint{i,1};
        RVLEMGcutgrav = EMG_RVL(Gcut50(1):Gcut50(2),:);
        Day1RVLEMGAvgZero(i,5) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut50(1):Ecut50(2),:);
        RVLEMGcut{i}(5,1:length(RVLC)) = RVLC;


        Ecut60 = Exercisecutint{i,3};
        CutTarget60 = Target(Ecut60(1):Ecut60(2),:);
        CT=CutTarget60-min(CutTarget60);
        CutTarget{i}(6,1:length(CT)) = CT;

        Gcut60 = Gravitycutint{i,3};
        RVLEMGcutgrav = EMG_RVL(Gcut60(1):Gcut60(2),:);
        Day1RVLEMGAvgZero(i,6) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut60(1):Ecut60(2),:);
        RVLEMGcut{i}(6,1:length(RVLC)) = RVLC;

        RVMEMGcutgrav = EMG_RVM(Gcut10(1):Gcut10(2),:);
        Day1RVMEMGAvgZero(i,1) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut10(1):Ecut10(2),:);
        RVMEMGcut{i}(1,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut20(1):Gcut20(2),:);
        Day1RVMEMGAvgZero(i,2) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut20(1):Ecut20(2),:);
        RVMEMGcut{i}(2,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut30(1):Gcut30(2),:);
        Day1RVMEMGAvgZero(i,3) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut30(1):Ecut30(2),:);
        RVMEMGcut{i}(3,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut40(1):Gcut40(2),:);
        Day1RVMEMGAvgZero(i,4) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut40(1):Ecut40(2),:);
        RVMEMGcut{i}(4,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut50(1):Gcut50(2),:);
        Day1RVMEMGAvgZero(i,5) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut50(1):Ecut50(2),:);
        RVMEMGcut{i}(5,1:length(RVMC)) = RVMC;

        RVMEMGcutgrav = EMG_RVM(Gcut60(1):Gcut60(2),:);
        Day1RVMEMGAvgZero(i,6) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut60(1):Ecut60(2),:);
        RVMEMGcut{i}(6,1:length(RVMC)) = RVMC;

        RRFEMGcutgrav = EMG_RRF(Gcut10(1):Gcut10(2),:);
        Day1RRFEMGAvgZero(i,1) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut10(1):Ecut10(2),:);
        RRFEMGcut{i}(1,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut20(1):Gcut20(2),:);
        Day1RRFEMGAvgZero(i,2) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut20(1):Ecut20(2),:);
        RRFEMGcut{i}(2,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut30(1):Gcut30(2),:);
        Day1RRFEMGAvgZero(i,3) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut30(1):Ecut30(2),:);
        RRFEMGcut{i}(3,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut40(1):Gcut40(2),:);
        Day1RRFEMGAvgZero(i,4) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut40(1):Ecut40(2),:);
        RRFEMGcut{i}(4,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut50(1):Gcut50(2),:);
        Day1RRFEMGAvgZero(i,5) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut50(1):Ecut50(2),:);
        RRFEMGcut{i}(5,1:length(RRFC)) = RRFC;

        RRFEMGcutgrav = EMG_RRF(Gcut60(1):Gcut60(2),:);
        Day1RRFEMGAvgZero(i,6) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut60(1):Ecut60(2),:);
        RRFEMGcut{i}(6,1:length(RRFC)) = RRFC;
        
    else

        % loop over trials
        for k = 1:length(Day1filenamesEMG)
            data = csvread([subnames{i},Day1filenamesEMG{k}]);

            % extract variables
            t = data(:,1);
            Traw = data(:,2);
            TargetRaw = data(:,3);
            EMGraw = data(:,4:6);    
            fs = 1 / mean(diff(t));

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,Peakcutoff(j) / (fs*.5),'low');
            [bt,at] = butter(2,5/ (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            filterTarget = filtfilt(bt,at,TargetRaw);
            
            % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);

            EMG_RVM = downsample(FiltEMG_RVM,n);

            EMG_RRF = downsample(FiltEMG_RRF,n);

            Target = downsample(filterTarget,n);

            % Cut data

            %load ([subnames{i},'ProcDay1.mat']);

            Ecut = Exercisecutint{i,k}*200;
            Gcut = Gravitycutint{i,k}*200;


            CT = Target(Ecut(1):Ecut(2),:)-min(Target);
            CutTarget{i}(k,1:length(CT)) = CT;

            RVLEMGcutgrav = EMG_RVL(Gcut(1):Gcut(2),:);
            Day1RVLEMGAvgZero(i,k) = mean(RVLEMGcutgrav);

            RVMEMGcutgrav = EMG_RVM(Gcut(1):Gcut(2),:);
            Day1RVMEMGAvgZero(i,k) = mean(RVMEMGcutgrav);

            RRFEMGcutgrav = EMG_RRF(Gcut(1):Gcut(2),:);
            Day1RRFEMGAvgZero(i,k) = mean(RRFEMGcutgrav);
            
            RVLC = EMG_RVL(Ecut(1):Ecut(2),:);
            RVLEMGcut{i}(k,1:length(RVLC)) = RVLC;

            RVMC = EMG_RVM(Ecut(1):Ecut(2),:);
            RVMEMGcut{i}(k,1:length(RVMC)) = RVMC;

            RRFC = EMG_RRF(Ecut(1):Ecut(2),:);
            RRFEMGcut{i}(k,1:length(RRFC)) = RRFC;


        end
    end
end
if resave
save([cutoffs{j},'_RVL_PeakEMGProcessedDay1'],'subnames','CutTarget','RVLEMGcut','Day1RVLEMGAvgZero','sr','DSsr','angles');
save([cutoffs{j},'_RVM_PeakEMGProcessedDay1'],'subnames','CutTarget','RVMEMGcut','Day1RVMEMGAvgZero','sr','DSsr','angles');%,'Day1RVMEMGPeak','Day1RRFEMGPeak'
save([cutoffs{j},'_RRF_PeakEMGProcessedDay1'],'subnames','CutTarget','RRFEMGcut','Day1RRFEMGAvgZero','sr','DSsr','angles');%,'Day1RVMEMGPeak','Day1RRFEMGPeak'
end
%% Day 2 Analysis
for i = 1:length(subnames)
            RVLEMGcut{i} = nan(6,68401);
            RVMEMGcut{i} = nan(6,68401);
            RRFEMGcut{i} = nan(6,68401);
            CutTarget{i}= nan(6,68401);
 end



% Some parameters
frequencies = [0,1,2,0.5,1.5,2.5];
Day2filenamesEMG = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv'};
N = length(subnames);
% Load Day 2 MVC
load('MVCDay2EMGData.mat');
load('TorqueProcDay2.mat');

for i = 1:N
    if i == 2
        % extract variables
        %load('KoenProcDay2.mat')
        data = csvread('Koen_June21_submax_variable_freq.csv');
        t = data(:,1);
        Traw = data(:,2);
        TargetRaw = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,Peakcutoff(j) / (fs*.5),'low');
        [bt,at] = butter(2,5/ (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        filterTarget = filtfilt(bt,at,TargetRaw);

        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        Target = downsample(filterTarget,n);

        %Cut EMG data

        Ecut0 = Exercisecutint{i,4};
        CutTarget0 = Target(Ecut0(1):Ecut0(2),:);
        CT=CutTarget0-min(CutTarget0);
        CutTarget{i}(1,1:length(CT)) = CT;

        Gcut0 = Gravitycutint{i,4};
        RVLEMGcutgrav = EMG_RVL(Gcut0(1):Gcut0(2),:);
        Day2RVLEMGAvgZero(i,1) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut0(1):Ecut0(2),:);
        RVLEMGcut{i}(1,1:length(RVLC)) = RVLC;

        
        Ecut1 = Exercisecutint{i,1};
        CutTarget1 = Target(Ecut1(1):Ecut1(2),:);
        CT=CutTarget1-min(CutTarget1);
        CutTarget{i}(2,1:length(CT)) = CT;

        Gcut1 = Gravitycutint{i,1};
        RVLEMGcutgrav = EMG_RVL(Gcut1(1):Gcut1(2),:);
        Day2RVLEMGAvgZero(i,2) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut1(1):Ecut1(2),:);
        RVLEMGcut{i}(2,1:length(RVLC)) = RVLC;


        Ecut2 = Exercisecutint{i,2};
        CutTarget2 = Target(Ecut2(1):Ecut2(2),:);
        CT=CutTarget2-min(CutTarget2);
        CutTarget{i}(3,1:length(CT)) = CT;

        Gcut2 = Gravitycutint{i,2};
        RVLEMGcutgrav = EMG_RVL(Gcut2(1):Gcut2(2),:);
        Day2RVLEMGAvgZero(i,3) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut2(1):Ecut2(2),:);
        RVLEMGcut{i}(3,1:length(RVLC)) = RVLC;


        Ecut05 = Exercisecutint{i,3};
        CutTarget05 = Target(Ecut05(1):Ecut05(2),:);
        CT=CutTarget05-min(CutTarget05);
        CutTarget{i}(4,1:length(CT)) = CT;

        Gcut05 = Gravitycutint{i,3};
        RVLEMGcutgrav = EMG_RVL(Gcut05(1):Gcut05(2),:);
        Day2RVLEMGAvgZero(i,4) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut05(1):Ecut05(2),:);
        RVLEMGcut{i}(4,1:length(RVLC)) = RVLC;


        Ecut15 = Exercisecutint{i,6};
        CutTarget15 = Target(Ecut15(1):Ecut15(2),:);
        CT=CutTarget15-min(CutTarget15);
        CutTarget{i}(5,1:length(CT)) = CT;

        Gcut15 = Gravitycutint{i,6};
        RVLEMGcutgrav = EMG_RVL(Gcut15(1):Gcut15(2),:);
        Day2RVLEMGAvgZero(i,5) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut15(1):Ecut15(2),:);
        RVLEMGcut{i}(5,1:length(RVLC)) = RVLC;


        Ecut25 = Exercisecutint{i,5};
        CutTarget25 = Target(Ecut25(1):Ecut25(2),:);
        CT=CutTarget25-min(CutTarget25);
        CutTarget{i}(6,1:length(CT)) = CT;

        Gcut25 = Gravitycutint{i,5};
        RVLEMGcutgrav = EMG_RVL(Gcut25(1):Gcut25(2),:);
        Day2RVLEMGAvgZero(i,6) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut25(1):Ecut25(2),:);
        RVLEMGcut{i}(6,1:length(RVLC)) = RVLC;

        RVMEMGcutgrav = EMG_RVM(Gcut0(1):Gcut0(2),:);
        Day2RVMEMGAvgZero(i,1) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut0(1):Ecut0(2),:);
        RVMEMGcut{i}(1,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut1(1):Gcut1(2),:);
        Day2RVMEMGAvgZero(i,2) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut1(1):Ecut1(2),:);
        RVMEMGcut{i}(2,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut2(1):Gcut2(2),:);
        Day2RVMEMGAvgZero(i,3) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut2(1):Ecut2(2),:);
        RVMEMGcut{i}(3,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut05(1):Gcut05(2),:);
        Day2RVMEMGAvgZero(i,4) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut05(1):Ecut05(2),:);
        RVMEMGcut{i}(4,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut15(1):Gcut15(2),:);
        Day2RVMEMGAvgZero(i,5) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut15(1):Ecut15(2),:);
        RVMEMGcut{i}(5,1:length(RVMC)) = RVMC;

        RVMEMGcutgrav = EMG_RVM(Gcut25(1):Gcut25(2),:);
        Day2RVMEMGAvgZero(i,6) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut25(1):Ecut25(2),:);
        RVMEMGcut{i}(6,1:length(RVMC)) = RVMC;

        RRFEMGcutgrav = EMG_RRF(Gcut0(1):Gcut0(2),:);
        Day2RRFEMGAvgZero(i,1) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut0(1):Ecut0(2),:);
        RRFEMGcut{i}(1,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut1(1):Gcut1(2),:);
        Day2RRFEMGAvgZero(i,2) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut1(1):Ecut1(2),:);
        RRFEMGcut{i}(2,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut2(1):Gcut2(2),:);
        Day2RRFEMGAvgZero(i,3) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut2(1):Ecut2(2),:);
        RRFEMGcut{i}(3,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut05(1):Gcut05(2),:);
        Day2RRFEMGAvgZero(i,4) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut05(1):Ecut05(2),:);
        RRFEMGcut{i}(4,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut15(1):Gcut15(2),:);
        Day2RRFEMGAvgZero(i,5) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut15(1):Ecut15(2),:);
        RRFEMGcut{i}(5,1:length(RRFC)) = RRFC;

        RRFEMGcutgrav = EMG_RRF(Gcut25(1):Gcut25(2),:);
        Day2RRFEMGAvgZero(i,6) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut25(1):Ecut25(2),:);
        RRFEMGcut{i}(6,1:length(RRFC)) = RRFC;


    elseif i == 3
        % extract variables
        %load('BillyProcDay2.mat')
        data = csvread('Billy_June27_variable_frequency.csv');
        t = data(:,1);
        Traw = data(:,2);
        TargetRaw = data(:,3);
        EMGraw = data(:,4:6);
        fs = 1 / mean(diff(t));

        %  band-pass filter EMG
        fc1 = [30 500];
        [b,a] = butter(2,fc1 / (fs*.5));
        [b2,a2] = butter(2,Peakcutoff(j) / (fs*.5),'low');
        [bt,at] = butter(2,5/ (fs*.5),'low');

        FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
        FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
        FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
        filterTarget = filtfilt(bt,at,TargetRaw);

        % Downsample EMG data
        EMG_RVL = downsample(FiltEMG_RVL,n);
        EMG_RVM = downsample(FiltEMG_RVM,n);
        EMG_RRF = downsample(FiltEMG_RRF,n);
        Target = downsample(filterTarget,n);

        %Cut EMG data

        Ecut0 = Exercisecutint{i,1};
        CutTarget0 = Target(Ecut0(1):Ecut0(2),:);
        CT=CutTarget0-min(CutTarget0);
        CutTarget{i}(1,1:length(CT)) = CT;

        Gcut0 = Gravitycutint{i,1};
        RVLEMGcutgrav = EMG_RVL(Gcut0(1):Gcut0(2),:);
        Day2RVLEMGAvgZero(i,1) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut0(1):Ecut0(2),:);
        RVLEMGcut{i}(1,1:length(RVLC)) = RVLC;

        
        Ecut1 = Exercisecutint{i,3};
        CutTarget1 = Target(Ecut1(1):Ecut1(2),:);
        CT=CutTarget1-min(CutTarget1);
        CutTarget{i}(2,1:length(CT)) = CT;

        Gcut1 = Gravitycutint{i,3};
        RVLEMGcutgrav = EMG_RVL(Gcut1(1):Gcut1(2),:);
        Day2RVLEMGAvgZero(i,2) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut1(1):Ecut1(2),:);
        RVLEMGcut{i}(2,1:length(RVLC)) = RVLC;


        Ecut2 = Exercisecutint{i,7};
        CutTarget2 = Target(Ecut2(1):Ecut2(2),:);
        CT=CutTarget2-min(CutTarget2);
        CutTarget{i}(3,1:length(CT)) = CT;

        Gcut2 = Gravitycutint{i,7};
        RVLEMGcutgrav = EMG_RVL(Gcut2(1):Gcut2(2),:);
        Day2RVLEMGAvgZero(i,3) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut2(1):Ecut2(2),:);
        RVLEMGcut{i}(3,1:length(RVLC)) = RVLC;


        Ecut05 = Exercisecutint{i,6};
        CutTarget05 = Target(Ecut05(1):Ecut05(2),:);
        CT=CutTarget05-min(CutTarget05);
        CutTarget{i}(4,1:length(CT)) = CT;

        Gcut05 = Gravitycutint{i,6};
        RVLEMGcutgrav = EMG_RVL(Gcut05(1):Gcut05(2),:);
        Day2RVLEMGAvgZero(i,4) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut05(1):Ecut05(2),:);
        RVLEMGcut{i}(4,1:length(RVLC)) = RVLC;


        Ecut15 = Exercisecutint{i,2};
        CutTarget15 = Target(Ecut15(1):Ecut15(2),:);
        CT=CutTarget15-min(CutTarget15);
        CutTarget{i}(5,1:length(CT)) = CT;

        Gcut15 = Gravitycutint{i,2};
        RVLEMGcutgrav = EMG_RVL(Gcut15(1):Gcut15(2),:);
        Day2RVLEMGAvgZero(i,5) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut15(1):Ecut15(2),:);
        RVLEMGcut{i}(5,1:length(RVLC)) = RVLC;


        Ecut25 = Exercisecutint{i,5};
        CutTarget25 = Target(Ecut25(1):Ecut25(2),:);
        CT=CutTarget25-min(CutTarget25);
        CutTarget{i}(6,1:length(CT)) = CT;

        Gcut25 = Gravitycutint{i,5};
        RVLEMGcutgrav = EMG_RVL(Gcut25(1):Gcut25(2),:);
        Day2RVLEMGAvgZero(i,6) = mean(RVLEMGcutgrav);

        RVLC = EMG_RVL(Ecut25(1):Ecut25(2),:);
        RVLEMGcut{i}(6,1:length(RVLC)) = RVLC;

        RVMEMGcutgrav = EMG_RVM(Gcut0(1):Gcut0(2),:);
        Day2RVMEMGAvgZero(i,1) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut0(1):Ecut0(2),:);
        RVMEMGcut{i}(1,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut1(1):Gcut1(2),:);
        Day2RVMEMGAvgZero(i,2) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut1(1):Ecut1(2),:);
        RVMEMGcut{i}(2,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut2(1):Gcut2(2),:);
        Day2RVMEMGAvgZero(i,3) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut2(1):Ecut2(2),:);
        RVMEMGcut{i}(3,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut05(1):Gcut05(2),:);
        Day2RVMEMGAvgZero(i,4) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut05(1):Ecut05(2),:);
        RVMEMGcut{i}(4,1:length(RVMC)) = RVMC;


        RVMEMGcutgrav = EMG_RVM(Gcut15(1):Gcut15(2),:);
        Day2RVMEMGAvgZero(i,5) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut15(1):Ecut15(2),:);
        RVMEMGcut{i}(5,1:length(RVMC)) = RVMC;

        RVMEMGcutgrav = EMG_RVM(Gcut25(1):Gcut25(2),:);
        Day2RVMEMGAvgZero(i,6) = mean(RVMEMGcutgrav);

        RVMC = EMG_RVM(Ecut25(1):Ecut25(2),:);
        RVMEMGcut{i}(6,1:length(RVMC)) = RVMC;

        RRFEMGcutgrav = EMG_RRF(Gcut0(1):Gcut0(2),:);
        Day2RRFEMGAvgZero(i,1) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut0(1):Ecut0(2),:);
        RRFEMGcut{i}(1,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut1(1):Gcut1(2),:);
        Day2RRFEMGAvgZero(i,2) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut1(1):Ecut1(2),:);
        RRFEMGcut{i}(2,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut2(1):Gcut2(2),:);
        Day2RRFEMGAvgZero(i,3) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut2(1):Ecut2(2),:);
        RRFEMGcut{i}(3,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut05(1):Gcut05(2),:);
        Day2RRFEMGAvgZero(i,4) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut05(1):Ecut05(2),:);
        RRFEMGcut{i}(4,1:length(RRFC)) = RRFC;


        RRFEMGcutgrav = EMG_RRF(Gcut15(1):Gcut15(2),:);
        Day2RRFEMGAvgZero(i,5) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut15(1):Ecut15(2),:);
        RRFEMGcut{i}(5,1:length(RRFC)) = RRFC;

        RRFEMGcutgrav = EMG_RRF(Gcut25(1):Gcut25(2),:);
        Day2RRFEMGAvgZero(i,6) = mean(RRFEMGcutgrav);

        RRFC = EMG_RRF(Ecut25(1):Ecut25(2),:);
        RRFEMGcut{i}(6,1:length(RRFC)) = RRFC;
    else

        % loop over trials
        for k = 1:length(Day2filenamesEMG)
            data = csvread([subnames{i},Day2filenamesEMG{k}]);

            % extract variables
            t = data(:,1);
            Traw = data(:,2);
            TargetRaw = data(:,3);
            EMGraw = data(:,4:6);    
            fs = 1 / mean(diff(t));

            %  band-pass filter EMG
            fc1 = [30 500];
            [b,a] = butter(2,fc1 / (fs*.5));
            [b2,a2] = butter(2,Peakcutoff(j) / (fs*.5),'low');
            [bt,at] = butter(2,5/ (fs*.5),'low');

            FiltEMG_RVL = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,1)))));
            FiltEMG_RVM = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,2)))));
            FiltEMG_RRF = filtfilt(b2,a2,abs(hilbert(filtfilt(b,a, EMGraw(:,3)))));
            filterTarget = filtfilt(bt,at,TargetRaw);
    
             % Downsample EMG data
            EMG_RVL = downsample(FiltEMG_RVL,n);

            EMG_RVM = downsample(FiltEMG_RVM,n);

            EMG_RRF = downsample(FiltEMG_RRF,n);

            Target = downsample(filterTarget,n);

            % Cut data

            %load ([subnames{i},'ProcDay2.mat']);

            Ecut = Exercisecutint{i,k}*200;
            Gcut = Gravitycutint{i,k}*200;


            CT = Target(Ecut(1):Ecut(2),:)-min(Target);
            CutTarget{i}(k,1:length(CT)) = CT;

            RVLEMGcutgrav = EMG_RVL(Gcut(1):Gcut(2),:);
            Day2RVLEMGAvgZero(i,k) = mean(RVLEMGcutgrav);

            RVMEMGcutgrav = EMG_RVM(Gcut(1):Gcut(2),:);
            Day2RVMEMGAvgZero(i,k) = mean(RVMEMGcutgrav);

            RRFEMGcutgrav = EMG_RRF(Gcut(1):Gcut(2),:);
            Day2RRFEMGAvgZero(i,k) = mean(RRFEMGcutgrav);
            
            RVLC = EMG_RVL(Ecut(1):Ecut(2),:);
            RVLEMGcut{i}(k,1:length(RVLC)) = RVLC;

            RVMC = EMG_RVM(Ecut(1):Ecut(2),:);
            RVMEMGcut{i}(k,1:length(RVMC)) = RVMC;

            RRFC = EMG_RRF(Ecut(1):Ecut(2),:);
            RRFEMGcut{i}(k,1:length(RRFC)) = RRFC;
        end
    end
end
if resave
    save([cutoffs{j},'_RVL_PeakEMGProcessedDay2'],'subnames','CutTarget','RVLEMGcut','Day2RVLEMGAvgZero','sr','DSsr','frequencies');
    save([cutoffs{j},'_RVM_PeakEMGProcessedDay2'],'subnames','CutTarget','RVMEMGcut','Day2RVMEMGAvgZero','sr','DSsr','frequencies');
    save([cutoffs{j},'_RRF_PeakEMGProcessedDay2'],'subnames','CutTarget','RRFEMGcut','Day2RRFEMGAvgZero','sr','DSsr','frequencies');
end

end