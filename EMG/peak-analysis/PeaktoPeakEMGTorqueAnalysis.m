clear all; close all; clc
% This script is to look at the torque and EMG based on the peaks found in
% the torque target data. EMG data is selected and normalized. Torque data
% is cut and normalized.
% The data is normalized 0-100 where 0 is the inital torque target peak and 100 is
% preceding torque target peak.
%% Load data
% cd('C:\Users\Tim\OneDrive - University of Calgary\ultrasound\Billy pilot\Data\Variable Frequency and Knee Angle Experiment\Torque and EMG\EMG_peak_analysis')
load('20_RVL_PeakEMGProcessedDay2.mat'); % Load EMG data for day 2
load('TorqueProcDay2.mat'); % Load torque data for day 2

N_per_volt = 69.71; % Conversion V to Nm 
frequencies = [0, 0.5, 1, 1.5, 2, 2.5];% Frequency order 
dispfreq = {'0', '0.5', '1', '1.5', '2', '2.5'};
WaveformAmp = ([0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163]*0.5); % Subjects waveform ampltude
% Subjects day 2 trial order
day2order = [1,4,2,5,3,6;4,3,1,6,2,5;1,6,3,2,7,5;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6];

visualize = 1; % Plot figures for individual subjects

%% Cut torque data
for i = 1:11
    for k = 1:length(frequencies)
        if (i == 2)||(i == 3)
            o = day2order(i, k);% Get order of trials for each subject
            TE = torque{i,o};% Pull out specific trial
            Ecut = Exercisecutint{i,o};% Get cut interval for that trial
            Ecut = round(Ecut);
            Ecutdiff = Ecut(2)-Ecut(1); % Find difference in cut, specific to subjects 2 and 3
            ETcut = TE(end-Ecutdiff:end,:);
            targetexc=target{i,o};% Pull out matching target trial
            targetcut = targetexc(end-Ecutdiff:end,:)+WaveformAmp(i);
            torquetarget{i}(k,1:length(targetcut)) = targetcut*N_per_volt;
            
            Gcut = Gravitycutint{i,o};% Pull out matching gravity cut 
            Gcut = round(Gcut);
            Gcutdiff = round(Gcut(2)-Gcut(1));
            GTcut = TE(1:Gcutdiff,:);
            Day2GravTorqueAvg = mean (GTcut);
            torquecut{i}(k,1:length(ETcut)) = (Day2GravTorqueAvg - ETcut)*N_per_volt;
            
        else
            o = day2order(i, k);% Get order of trials for each subject
            TE = torque{i,o};% Pull out specific trial
            targetex = target{i,o};% Pull out matching target trial
            Ecut = Exercisecutint{i,o}*200;% Get cut interval for that trial
            Ecut = round(Ecut);
            ETcut = TE(Ecut(1):Ecut(2),:);
            targetcut = targetex(Ecut(1):Ecut(2),:)+WaveformAmp(i);% Cut target and zero using waveform amp
            torquetarget{i}(k,1:length(targetcut)) = targetcut*N_per_volt;% Convert to Nm

            Gcut = Gravitycutint{i,o}*200;% Pull out matching gravity cut 
            Gcut = round(Gcut);
            GTcut = TE(Gcut(1):Gcut(2),:);
            Day2GravTorqueAvg = mean (GTcut);
            torquecut{i}(k,1:length(ETcut)) = (Day2GravTorqueAvg - ETcut)*N_per_volt; % Zero using gravity measure and convert to Nm           
            
        end
    end
end
%% pre-process EMG data

for sub = 2:11
% Pull out subjects data
EMG = RVLEMGcut{sub};
T = CutTarget{sub};

for i = 1:6 % Loop through each frequency to get sample size for each trial
    Ns(i) = sum(isfinite(EMG(i,:)));
end

N = min(Ns);%Detemine lowest trial sample size

% Cut data based on lowest sample sized 
EMG = EMG(:, 1:N);
T = T(:, 1:N);

fs = 200;
dt = 1/fs;
N = sum(k);
t = 0:dt:((N-1)*dt);% Detemine time interval

%% Process Torque data

% Pull out subjects data
Torque = torquecut{sub};
Target = torquetarget{sub};
Torque(Torque==0.00)=nan; % Remove unneeded zero points to become nans

for i = 1:6 % Loop through each frequency to get sample size for each trial
    NsT(i) = sum(isfinite(Torque(i,:)));
end

NT = min(NsT);%Detemine lowest trial sample size

% Cut data based on lowest sample sized 
Torque = Torque(:, 1:NT);
Target = Target(:, 1:NT);

fsT = 200;
dtT = 1/fsT;
NT = sum(k);
tT = 0:dtT:((NT-1)*dtT);% Detemine time interval 

%% EMG time-series analysis
close all

for c = 2:6 % Loop through frequencies 0.5-2.5
    if (sub == 1)||(sub == 6) % Subject 1 and 6's data is newer and formatted so target isn't flipped
    else % Other subjects target needs to be flipped
     T(c,:)=abs(T(c,:)-max(T(c,:)));
    end

    [p,l] = findpeaks(T(c,:));% Find the torque peaks based on the target data

    freq(c) = 1/ (mean(diff(l)) * dt);% Determine frequency of the peak data
    
    disp(['Freq = ', num2str(round(freq(c),1))])

    subplot(2,3,c)
    
    clear m
    EMGi = nan(length(l),101);% Create empty variable to save data to
    for i = 1:(length(l)-1) % Loop through each of the found peaks 

        % Interpret data to normalize torque data to be between 0-100
        EMGi(i,:)= interp1(l(i):l(i+1), EMG(c,l(i):l(i+1)), linspace(l(i),l(i+1),101)); 

        [m(i),j] = max(EMG(c,l(i):l(i+1)));% Find max 
        
if visualize
        figure(sub);subplot(2,3,c);plot(EMGi(i,:),'Color',[0,0,1,0.1]);hold on %change alpha 
        xlabel('Normalized Time');ylabel('EMG Signal (% MVC)');
        sgtitle(['EMG Peak Subject ',num2str(sub)]);title([num2str(round(freq(c),1)), ' Hz'])
end
        
    end
    EMGcyclemean{sub}(c,:) = mean(EMGi,'omitnan');% Create mean of normalized data
    
    M(sub,c) = median(m);%Find mean of peak data
    if visualize
    subplot(2,3,c);plot(EMGcyclemean{sub}(c,:),'k',LineWidth=2);ylim([0 0.6]);xlim([0 100]);
    end

end

%% torque time-series analysis
[FreqSort,I]=sort(freq');% Sort target frequency order 
Tsort = T(I,:);% Sort target data to match torque data frequency trial order

for c = 2:6 % Loop through frequencies 0.5-2.5 

    [pT,lT] = findpeaks(Tsort(c,:));% Find the torque peaks based on the target data

    freqT(c) = 1/ (mean(diff(lT)) * dtT);% Determine frequency of the peak data
    
    disp(['Freq = ', num2str(round(frequencies(c),1))])

    subplot(2,3,c)
    
    clear mT
    Torquei = nan(length(lT),101);% Create empty variable to save data to
    for i = 1:(length(lT)-1) % Loop through each of the found peaks 
        
        % Interpret data to normalize torque data to be between 0-100
        Torquei(i,:)= interp1(lT(i):lT(i+1), Torque(c,lT(i):lT(i+1)), linspace(lT(i),lT(i+1),101)); 

        [mT(i),jT] = max(Torque(c,lT(i):lT(i+1)));% Find max 
        
if visualize
        figure(sub+12);subplot(2,3,c);plot(Torquei(i,:),'Color',[0,0,1,0.1]);hold on %change alpha 
        xlabel('Normalized Time');ylabel('Torque (Nm)');
        xlim([0 100]);ylim([0 60]);
        sgtitle(['Torque Peak Subject ',num2str(sub)]);
         title([num2str(round(freqT(c),1)), ' Hz'])
end
        
    end
    Torquecyclemean{sub}(c,:) = mean(Torquei,'omitnan');% Create mean of normalized data
    
    MTorque(sub,c) = median(mT);%Find mean of peak data
    if visualize
    subplot(2,3,c);plot(Torquecyclemean{sub}(c,:),'k',LineWidth=2);
    end

end
end
%% Plot Summary figure
figure(100);color = [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:11
    for c = 2:6
    figure(100);subplot(2,3,c);plot(EMGcyclemean{i}(c,:),Color=color(i,:));hold on
     title([num2str(round(freq(c),1)), ' Hz']);
     meanEMG{c}(i,:) = EMGcyclemean{i}(c,:);
     xlabel('Normalized Time');ylabel('EMG Signal (% MVC)');
     xlim([0 100]);ylim([0 0.36]);
    end
end
for c = 2:6
    figure(100)
    subplot(2,3,c);plot(mean(meanEMG{c},'omitnan'),Color='k',LineWidth=1.5);hold on
end
sgtitle('Summary of EMG Peak Analysis');legend('1','2','3','4','5','6','7','8','9','10','11','Mean')


figure(101);color = [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:11
    for c = 2:6
    figure(101);subplot(2,3,c);plot(Torquecyclemean{i}(c,:),Color=color(i,:));hold on
     title([num2str(round(frequencies(c),1)), ' Hz']);
     meanTorque{c}(i,:) = Torquecyclemean{i}(c,:);
     xlabel('Normalized Time');ylabel('Torque (Nm)');
     ylim([0 50]);xlim([0 100])
    end
end
for c = 2:6
    figure(101)
    subplot(2,3,c);plot(mean(meanTorque{c},'omitnan'),Color='k',LineWidth=1.5);hold on
end
sgtitle('Summary of Torque Peak Analysis');
legend('1','2','3','4','5','6','7','8','9','10','11','Mean')




