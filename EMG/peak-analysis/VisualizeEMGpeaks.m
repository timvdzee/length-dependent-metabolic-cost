clear all; close all; clc
% cd('C:\Users\Tim\OneDrive - University of Calgary\ultrasound\Billy pilot\Data\Variable Frequency and Knee Angle Experiment\Torque and EMG\EMG_peak_analysis')
load('50_RVL_PeakEMGProcessedDay2.mat')

%% pre-process
S = 10;
EMG = RVLEMGcut{S};
T = CutTarget{S};

for i = 1:6
    Ns(i) = sum(isfinite(EMG(i,:)));
end

N = min(Ns);

EMG = EMG(:, 1:N);
T = T(:, 1:N);

k = 1:N;
x = round([.25 .75] * N);

id = (k>x(1)) & (k<x(2));

fs = 200;
dt = 1/fs;
N = sum(id);
t = 0:dt:((N-1)*dt);

EMG = EMG(:,id);
T = T(:,id);

%% plot data
% plot(t,EMG(5,id));
close all

for c = 2:6
    subplot(2,3,c);
    plot(EMG(c,:));
end

%% time-series analysis
close all

for c = 2:6

    [p,l] = findpeaks(T(c,:));

    freq(c) = 1/ (mean(diff(l)) * dt);
    
    disp(['Freq = ', num2str(round(freq(c),1))])

    subplot(2,3,c)
    
    clear m
    figure(10)
    EMGi = nan(length(l),101);
    for i = 1:(length(l)-1)
        title([num2str(round(freq(c),1)), ' Hz'])
%         plot(EMG(c,l(i):l(i+1)),'color', [0 0 0 .1]); hold on
        
        EMGi(i,:)= interp1(l(i):l(i+1), EMG(c,l(i):l(i+1)), linspace(l(i),l(i+1),101)); 

        [m(i),j] = max(EMG(c,l(i):l(i+1)));
        
%         plot(j,m(i),'.', 'color', [1 0 0 .1])
        figure(10);subplot(2,3,c);plot(EMGi(i,:));hold on %change alpha 
        xlabel('Normalized Time');ylabel('EMG Signal (% MVC)');
        
    end
    EMGcyclemean(c,:) = mean(EMGi,'omitnan');
    
    M(c) = median(m);

end

%% make plot nice
for i = 1:6
    subplot(2,3,i);
    ylim([0 .2]);   
    plot(EMGcyclemean(i),'LineWidth',2);hold on
end

%% fourier analysis
for c = 2:6
[Pxx1,F1] = pwelch(EMG(c,:)-mean(EMG(c,:),'omitnan'),[],[],[],fs);
[Pxx1T,F1T] = pwelch(T(c,:)-mean(T(c,:),'omitnan'),[],[],[],fs);

figure(2)
% subplot(2,3,c);
plot(F1, Pxx1); hold on

[Pmax(c), i] = max(Pxx1);

[PmaxT(c), ~] = max(Pxx1T);

plot(F1(i), Pxx1(i),'o')
end


%% summary plot
% freq = [0 1 2 .5 1.5 2.5];
figure(3)
subplot(221);
plot(freq, M,'o')

subplot(222);
plot(freq, Pmax,'o')

subplot(223);
plot(freq, PmaxT,'o')

subplot(224);
plot(freq, Pmax./PmaxT,'o')




