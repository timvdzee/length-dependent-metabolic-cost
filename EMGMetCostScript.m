clear; close all; clc
% This script is used to visualize the relationship between EMG data and
% the metabolic cost. 
%%
angles = 10:10:60;%Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
N_per_volt = 69.71; 
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order

% Ten degree MVC for each subject
TendegMVC = [1.721,1.601,1.185,1.941,1.164,0.603,1.664,2.024,2.481,1.167,0.652]';

%Exclusion Variables
day1subj_exc = [9]; %Select which subjects to exclude from day 1 analysis   
day1exc = ones(1,11);
day1exc(day1subj_exc) = 0;

day2subj_exc = [9]; %Select which subjects to exclude from day 2 analysis   
day2exc = ones(1,11);
day2exc(day2subj_exc) = 0;

%% Day 1 EMG
% Load data for EMG and respiratory
load('EMGSummaryAnalysisDay1.mat');%EMG data for day 1
load('ProcessedTorqueDay1.mat');%Torque data for day 1
load('RespiratorySummary.mat');%Respiratory data for both days
load('MVCDay1TorqueData.mat');
load('MVCDay2TorqueData.mat');

N = 11; % Number of subjects

% Exclude subjects
for i = 1:N
    Day1RVLEMGAvgall(i,:) = Day1RVLEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RVMEMGAvgall(i,:) = Day1RVMEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RRFEMGAvgall(i,:) = Day1RRFEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RRFEMGAvg(i,:) = nan;
    end
end  

% Get average EMG signal across right leg muscles
Day1EMGAvg = (Day1RVLEMGAvg + Day1RVMEMGAvg + Day1RRFEMGAvg)/3;
Day1EMGAvgall = (Day1RVLEMGAvgall + Day1RVMEMGAvgall + Day1RRFEMGAvgall)/3;

%% Day 1 Respiratory 

% Exclude subjects
for i = 1:N
    Day1MetPowerall(i,:) = Day1MetPower(i,:);
    if day1exc(i) == 0;
    Day1MetPower(i,:) = nan;
    corfacs(i) = nan;
    end
end
visualize = 0; % Anyfit visualization 
x = [0:1:50]; % Angle intervals of interest

% Fit data accordingly and pull out stats
[OFDay1Fit] = anyfit(Day1EMGAvg,Day1MetPower(:,1:6),'visualize',0,'type', 'indiv','function',@(x)[x]);
pDay1R2 = OFDay1Fit.r2;
pDay1CI = [OFDay1Fit.coefint(1,1),OFDay1Fit.coef(4,1)];
pDay1c = [OFDay1Fit.coef(1,1),OFDay1Fit.offset];
pDay1 = polyval(pDay1c,x);
Day1VO2OS = Day1MetPower(:,1:6)-OFDay1Fit.coef(2:end)+OFDay1Fit.offset;

%% Day 2 EMG
% Load data
load('EMGSummaryAnalysisDay2.mat');%EMG data for day 2
load('ProcessedTorqueDay2.mat');%Torque data for day 2
N = 11; % Number of subjects

% Sort EMG data according to frequency
[B,I] = sort(EMGfrequencies);
sortedDay2RVLEMGAvg = Day2RVLEMGAvg(:,I);
sortedDay2RVMEMGAvg = Day2RVMEMGAvg(:,I);
sortedDay2RRFEMGAvg = Day2RRFEMGAvg(:,I);

% Exclude subjects
for i = 1:N
    sortedDay2RVLEMGAvgall(i,:) = sortedDay2RVLEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RVMEMGAvgall(i,:) = sortedDay2RVMEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RRFEMGAvgall(i,:) = sortedDay2RRFEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RRFEMGAvg(i,:) = nan;
    end
end

% Get average EMG signal across right leg muscles
Day2EMGAvg = (sortedDay2RVLEMGAvg + sortedDay2RVMEMGAvg + sortedDay2RRFEMGAvg)/3;
Day2EMGAvgall = (sortedDay2RVLEMGAvgall + sortedDay2RVMEMGAvgall + sortedDay2RRFEMGAvgall)/3;

%% Day 2 Respiratory 

% Exclude subjects
for i = 1:N
    Day2MetPowerall(i,:) = Day2MetPower(i,:);
    if day2exc(i) == 0;
    Day2MetPower(i,:) = nan;
    end
end

% Fit data accordingly and pull out stats
[OFDay2Fit] = anyfit(Day2EMGAvg,Day2MetPower(:,2:7),'visualize',0,'type', 'indiv','function',@(x)[x]);
pDay2R2 = OFDay2Fit.r2;
pDay2CI = [OFDay2Fit.coefint(1,1),OFDay2Fit.coef(4,1)];
pDay2c = [OFDay2Fit.coef(1,1),OFDay2Fit.offset];
pDay2 = polyval(pDay2c,x);
Day2VO2OS = Day2MetPower(:,2:7)-OFDay2Fit.coef(2:end)+OFDay2Fit.offset;

%% Both days fit
BDEMG = [Day1EMGAvg;Day2EMGAvg];


Day1Resp = Day1MetPower(:,1:6); Day2Resp = Day2MetPower(:,2:7);
BDresp = [Day1Resp;Day2Resp];


[OFBDFit] = anyfit(BDEMG,BDresp,'visualize',0,'type', 'indiv','function',@(x)[x]);
pBDR2 = OFBDFit.r2;
pBDCI = [OFBDFit.coefint(1,1),OFBDFit.coef(4,1)];
pBDc = [OFBDFit.coef(1,1),OFBDFit.offset];
pBD = polyval(pBDc,x);
BDVO2OS = BDresp-OFBDFit.coef(2:end)+OFBDFit.offset;

%% Plot figure
if ishandle(1), close(1); end
figure(1);
color = [get(gca,'colororder'); get(gca,'colororder')];
Day1Lin = plot(x,pDay1,'--'); hold on; set(Day1Lin,'lineWidth',2);set(Day1Lin,'color','r');
Day2Lin = plot(x,pDay2,'--'); hold on; set(Day2Lin,'lineWidth',2);set(Day2Lin,'color','b');
BDLin = plot(x,pBD,'--'); hold on; set(BDLin,'lineWidth',2);set(BDLin,'color','k');

% plot(mean(Day1EMGAvg,'omitnan'), mean(Day1VO2OS,'omitnan'),'r.','linewidth',1.5,'markersize',30); hold on
% plot(mean(Day2EMGAvg,'omitnan'), mean(Day2VO2OS,'omitnan'),'b.','linewidth',1.5,'markersize',30); hold on

for i = 1:N
    h = plot(Day1EMGAvg(i,:), Day1VO2OS(i,:), symbols(i), 'color', 'r','markersize',5); hold on
end 
for i = 1:N
    h = plot(Day2EMGAvg(i,:), Day2VO2OS(i,:), symbols(i), 'color', 'b','markersize',5); hold on
end 

xlabel('Average EMG (% MVC)');ylabel('Net metabolic rate (W/N-m)')
xlim([0 25]);ylim([-2 10]);
legend('Day 1 fit','Day 2 fit','Both days fit','Subject 1','Subject 2','Subject 3','Subject 4','Subject 5','Subject 6','Subject 7','Subject 8','Subject 9','Subject 10','Subject 11','Location','northwest')
title('EMG vs Metabolic Cost');
j = sprintf(' Day 1 R-square = %.2f\n Day 2 R-square = %.2f\n Both days R-square = %.2f\n',pDay1R2,pDay2R2,pBDR2);
text(5,8,j);







