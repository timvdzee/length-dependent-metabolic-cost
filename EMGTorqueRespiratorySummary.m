clear all; close all; clc
% this script visualizes a complete summary of the processed torque,
% respiratory, and EMG data for both days. A comparison between each days EMG and respiratory 
% data for 2Hz at 20deg from each day is plotted for each subject.

%% Run this first: Variables needed to run sections
% Day 1 angles 
angles = 10:10:60;

% Individual symbols for plotting
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];

% Nm per volt conversion 
N_per_volt = 69.71; 

% Freqency order for torque and EMG
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order

% Exclusion Variables
day1subj_exc = [9]; %Select which subjects to exclude from day 1 analysis   
day1exc = ones(1,11);
day1exc(day1subj_exc) = 0;

day2subj_exc = [9]; %Select which subjects to exclude from day 2 analysis   
day2exc = ones(1,11);
day2exc(day2subj_exc) = 0;

NIRSsubj_exc = [6,9]; %Select which subjects to exclude from NIRS analysis 
NIRSexc = ones(1,11);
NIRSexc(NIRSsubj_exc) = 0;

gravityintervalsubj_exc = [9]; %Select which subjects to exclude from gravity interval analysis
gravityintervalexc = ones(1,11);
gravityintervalexc(gravityintervalsubj_exc) = 0;

ultrasoundsubj_exc = [4,9,11]; %Select which subjects to exclude from ultrasound analysis
ultrasoundexc = ones(1,11);
ultrasoundexc(ultrasoundsubj_exc) = 0;

% Normalize torque by their torque target voltage
normalize = false;

% Absolute Deviation of torque
absolute = true;

% Save Figures
savefigures = 0;
%% Figure 1: Variable joint angle, constant frequency (i.e. Day 1)
% create main figure
[Day1RVLEMGAvg, Day1RVMEMGAvg, Day1RRFEMGAvg, Day1MetPower] = figure1(normalize, absolute, symbols, angles,day1exc);
set(gcf,'position', [100 100 800 800])
if savefigures
saveas(gcf,'Figure1.jpg');
end

%% Figure 2: Variable frequency, constant joint angle (i.e. Day 2)
clc
[sortedDay2RVLEMGAvg, sortedDay2RVMEMGAvg, sortedDay2RRFEMGAvg, Day2MetPower] = figure2(normalize, absolute, symbols, frequencies, EMGfrequencies,day2exc);
figure(2);xlim=[0 3];
set(gcf,'position', [100 100 800 800])
if savefigures
saveas(gcf,'Figure2.jpg');
end
%% Figure 3: Sanity Check of 2Hz at 20deg for both days
[Day1EMGAvg, Day2EMGAvg] = figure3(symbols,frequencies,EMGfrequencies);

if savefigures
saveas(figure(3),'Figure3.jpg');
end

%% Figure 4: NIRS analysis 
[NIRS] = figure4(symbols,frequencies,NIRSexc);

if savefigures
saveas(figure(4),'Figure4.jpg');
end

%% Figure 5: MVC Sanity Check
[AllMVCTorqueAvgNm,AllMVCEMGAvg] = figure5(symbols,N_per_volt);

if savefigures
saveas(figure(5),'Figure5.jpg');
end

%% Figure 6 & 7: MVC vs Angle 
[AllMVCEMGAvgnorm,AllMVCRVMEMGAvgnorm,AllMVCRVLEMGAvgnorm,AllMVCRRFEMGAvgnorm,AllMVCTorqueAvgNm] = figure6_7(angles,symbols,N_per_volt);

if savefigures
saveas(figure(6),'Figure6.jpg');
saveas(figure(7),'Figure7.jpg');
end
%% Figure 8: Gravity Intervals
[Day1GravTorqueAvg,Day1GravTorqueDiff] = figure8(angles,symbols,N_per_volt,gravityintervalexc);

if savefigures
saveas(figure(8),'Figure8.jpg');
end
%% Figure 9,10,11:Ultrasound analysis
[pennationchange,thicknesschange,flengthchange] = figure9_10_11(symbols,ultrasoundexc);

if savefigures
saveas(figure(9),'Figure9.jpg');
saveas(figure(10),'Figure10.jpg');
saveas(figure(11),'Figure11.jpg');   
end

%% Frequency by angle grid for conditions
Day1Frequency = [2,2,2,2,2]; % Day 1 frequencies used
Day1Angles = [10,30,40,50,60];% Day 1 angles used 
Day2Frequency = [0,0,0.5,1,1.5,2.5]; % Day 2 frequencies used
Day2Angles = [60,20,20,20,20,20];% Day 2 angles used 
ConstantTrialAngle = [20]; % Angle used on both days
ConstantTrialFrequency = [2]; % Frequency used on both days

% Plot figure
figure();
plot(Day1Angles,Day1Frequency,'b.','markersize',20); hold on
plot(Day2Angles,Day2Frequency,'r.','markersize',20); hold on
plot(ConstantTrialAngle,ConstantTrialFrequency,'g.','markersize',20); hold on
legend('Day 1 Trials','Day 2 Trials','Overlapping Trial');
xlabel('Angle (degrees)');ylabel('Frequency (Hz)');
title('Each Days Testing Conditions');


%% Examine specific subject's torque data
measure = 'torque';
subNumbers = 12;% Subject of interest 
TestingDays = 2;% Testing day of interest
trials = ['all'];% Trials of interest
for i = 1:length(subNumbers)
subNumber = subNumbers(i);
TestingDay = TestingDays(i);
do_InvestigateSubject(subNumber, TestingDay, trials, measure)
end
%% Examine specific subject's respiratory data
measure = 'respiratory';
subName = '1'; % Subject of interest
trials = 'all';% Trials of interest

for TestingDay = 1:2 % Look at both days 
    do_InvestigateSubject(subName, TestingDay, trials, measure);
end



%% Examine specific subject's EMG data
measure = 'EMG'; 
subNumbers = 12; % Subject of interest
TestingDays = 2; % Testing day of interest
trials = 'all'; % Trials of interest
for i = 1:length(subNumbers)
subNumber = subNumbers(i);
TestingDay = TestingDays(i);
do_InvestigateSubject(subNumber, TestingDay, trials, measure);
end

%% Function 1: Day 1 Torque, Respiratory, and EMG Analysis
function[Day1RVLEMGAvg, Day1RVMEMGAvg, Day1RRFEMGAvg, Day1MetPower] = figure1(normalize, absolute, symbols, angles,day1exc)

if ishandle(1), close(1); end

%% Loading
load('EMGSummaryAnalysisDay1.mat');%EMG data for day 1
load('ProcessedTorqueDay1.mat');%Torque data for day 1
load('RespiratorySummary.mat');%Respiratory data for both days

N = 11; % number of subjects
N_per_volt = 69.71; % Nm per volt
fraction_MVC = .5; % fraction of torque target

% Torque target amplitude
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;

% Normalize Torque data
if normalize, Torque_avg = TorqueAvgD1 ./ WaveformAmp(:);    
elseif absolute, Torque_avg = TorqueAvgD1 - WaveformAmp(:);   
else,Torque_avg = TorqueAvgD1;
end

% Start to exclude subjects
for i = 1:N
    Torque_avgall(i,:) = Torque_avg(i,:);
    if day1exc(i) == 0;
    Torque_avg(i,:) = nan;
    end
end

for i = 1:N
    Day1MetPowerall(i,:) = Day1MetPower(i,:);
    if day1exc(i) == 0;
    Day1MetPower(i,:) = nan;
    end
end
for i = 1:N
    Day1RVLEMGAvgall(i,:) = Day1RVLEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RVMEMGAvgall(i,:) = Day1RVMEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RRFEMGAvgall(i,:) = Day1RRFEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RRFEMGAvg(i,:) = nan;
    end
end

%Set target line parameters
if normalize, y = WaveformAmp/WaveformAmp;
else, y = WaveformAmp;
end   

visualize = 0; % Anyfit visualization

% Get average EMG signal across right leg muscles
Day1EMGAvg = (Day1RVLEMGAvg + Day1RVMEMGAvg + Day1RRFEMGAvg)/3;
Day1EMGAvgall = (Day1RVLEMGAvgall + Day1RVMEMGAvgall + Day1RRFEMGAvgall)/3;

% angle used for fitting and R2 analysis
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];

% Fit data accordingly and pull out stats 
%[p1,gofp1] = fit((10:10:60)', mean(VO2a(:,1:6),'omitnan')','poly2'); 
[AFpr] = anyfit(r2angles,Day1MetPower(:,1:6),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
x = [0:1:70]; 
prCI = [AFpr.coefint(1,1),AFpr.coefint(2,1),AFpr.coef(4,1)];
prc = [AFpr.coef(1,1),AFpr.coef(2,1),AFpr.offset];
prR2 = AFpr.r2;
pr = polyval(prc,x);
VO2aOS = Day1MetPower(:,1:6)-AFpr.coef(3:end)+AFpr.offset;
if visualize == 1
figure(1);
hold on;plot(x,pr,'k-','LineWidth',3);
end

%[p2,gofp2] = fit(angles', mean(Day1EMGAvg,'omitnan')','poly2'); 
[AFpEMG] = anyfit(r2angles,Day1EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pEMGR2 = AFpEMG.r2;
pEMGCI = [AFpEMG.coefint(1,1),AFpEMG.coefint(2,1),AFpEMG.coef(4,1)];
pEMGc = [AFpEMG.coef(1,1),AFpEMG.coef(2,1),AFpEMG.offset];
pEMG = polyval(pEMGc,x);
Day1EMGAvgOS = Day1EMGAvg-AFpEMG.coef(3:end)+AFpEMG.offset;

%[pRVL,gofpRVL] = fit(angles', mean(Day1RVLEMGAvg,'omitnan')','poly2');    
[AFpRVL] = anyfit(r2angles,Day1RVLEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRVLR2 = AFpRVL.r2;
pRVLCI = [AFpRVL.coefint(1,1),AFpRVL.coefint(2,1),AFpRVL.coef(4,1)];
pRVLc = [AFpRVL.coef(1,1),AFpRVL.coef(2,1),AFpRVL.offset];
pRVL = polyval(pRVLc,x);
Day1RVLEMGAvgOS = Day1RVLEMGAvg-AFpRVL.coef(3:end)+AFpRVL.offset;

%[pRVM,gofpRVM] = fit(angles', mean(Day1RVMEMGAvg,'omitnan')','poly2'); 
[AFpRVM] = anyfit(r2angles,Day1RVMEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRVMR2 = AFpRVM.r2;
pRVMCI = [AFpRVM.coefint(1,1),AFpRVM.coefint(2,1),AFpRVM.coef(4,1)];
pRVMc = [AFpRVM.coef(1,1),AFpRVM.coef(2,1),AFpRVM.offset];
pRVM = polyval(pRVMc,x);
Day1RVMEMGAvgOS = Day1RVMEMGAvg-AFpRVM.coef(3:end)+AFpRVM.offset;

%[pRRF,gofpRRF] = fit(angles', mean(Day1RRFEMGAvg,'omitnan')','poly2'); 
[AFpRRF] = anyfit(r2angles,Day1RRFEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRRFR2 = AFpRRF.r2;
pRRFCI = [AFpRRF.coefint(1,1),AFpRRF.coefint(2,1),AFpRRF.coef(4,1)];
pRRFc = [AFpRRF.coef(1,1),AFpRRF.coef(2,1),AFpRRF.offset];
pRRF = polyval(pRRFc,x);
Day1RRFEMGAvgOS = Day1RRFEMGAvg-AFpRRF.coef(3:end)+AFpRRF.offset;

%% Plot Upper row: Variable joint angle, constant frequency (i.e. Day 1)
f = figure(1); color = [get(gca,'colororder'); get(gca,'colororder')];
set(f,'name','Variable joint angle, constant frequency (i.e. Day 1)');

% Plot Torque data
subplot(231);

if normalize, ylabel('Normalized Average Torque'); yline(y,'r','LineWidth', 2);hold on
else, ylabel('Average Torque (Nm)'); yline(0,'k-','LineWidth', 2); hold on
end

for i = 1:N, h = plot(angles, Torque_avgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles,mean(Torque_avg,'omitnan'),std(Torque_avg,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Angle (deg)'); title('Knee Torque'); ylim([-8 6]);xlim([0 70]);


% Plot Respiratory data
subplot(232);  %plot(0:60, polyval(p1, 0:60), 'k','linewidth',2); hold on
for i = 1:N, h= plot(angles, Day1MetPowerall(i,1:6), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles, mean(VO2aOS(:,1:6),'omitnan'),std(VO2aOS(:,1:6),'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pr = plot(x,pr,'-'); hold on; set(pr,'lineWidth',2);set(pr,'color','k');
xlabel('Angle (deg)'); ylabel('Net metabolic rate(W/N-m)'); title('Respiratory');  ylim([0 9]);xlim([0 70]);
legend('off'); 
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',prR2,prc(1),prCI(1),prc(2),prCI(2),prc(3),prCI(3));
text(20,7.5,j);
 
% Plot EMG data
subplot(233); %plot(0:60, polyval(p2, 0:60), 'k','linewidth',2); hold on
for i = 1:N, h = plot(angles, Day1EMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles,mean(Day1EMGAvgOS,'omitnan'),std(Day1EMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pEMG = plot(x,pEMG,'-'); hold on; set(pEMG,'lineWidth',2);set(pEMG,'color','k');xlim([0 70]);
ylim([0 30]); xlabel('Angle (deg)'); ylabel('Average EMG (% of MVC)'); title('Electromyography');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pEMGR2,pEMGc(1),pEMGCI(1),pEMGc(2),pEMGCI(2),pEMGc(3),pEMGCI(3));
text(20,25.5,j);

%% Plot Lower row: EMG figure with all muscles
% Vastus Lateralis
subplot(234)
%plot(0:60, polyval(pRVL, 0:60), 'k-','linewidth',2); hold on
for i = 1:N, h = plot(angles, Day1RVLEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles,mean(Day1RVLEMGAvgOS,'omitnan'),std(Day1RVLEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRVL = plot(x,pRVL,'-'); hold on; set(pRVL,'lineWidth',2);set(pRVL,'color','k');xlim([0 70]);
ylim([0 30]); xlabel('Angle (deg)');ylabel('Average EMG (% of MVC)'); title('Right Vastus Lateralis EMG');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRVLR2,pRVLc(1),pRVLCI(1),pRVLc(2),pRVLCI(2),pRVLc(3),pRVLCI(3));
text(20,25.5,j);

% Vastus Medialis
subplot(235); %plot(0:60, polyval(pRVM, 0:60), 'k','linewidth',2); hold on
for i = 1:N, h = plot(angles, Day1RVMEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles,mean(Day1RVMEMGAvgOS,'omitnan'),std(Day1RVMEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRVM = plot(x,pRVM,'-'); hold on; set(pRVM,'lineWidth',2);set(pRVM,'color','k');xlim([0 70]);
ylim([0 30]); xlabel('Angle (deg)');ylabel('Average EMG (% of MVC)');
title('Right Vastus Medialis EMG');legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRVMR2,pRVMc(1),pRVMCI(1),pRVMc(2),pRVMCI(2),pRVMc(3),pRVMCI(3));
text(20,25.5,j);

subplot(236)
%plot(0:60, polyval(pRRF, 0:60), 'k','linewidth',2); hold on
for i = 1:N
    h = plot(angles, Day1RRFEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day1exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(angles,mean(Day1RRFEMGAvgOS,'omitnan'),std(Day1RRFEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRRF = plot(x,pRRF,'-'); hold on; set(pRRF,'lineWidth',2);set(pRRF,'color','k');xlim([0 70]);
ylim([0 30]); legend('1','2','3','4','5','6','7','8','9','10','11','mean','fit','location','best')
xlabel('Angle (deg)');ylabel('Average EMG (% of MVC)'); title('Right Rectus Femoris EMG');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRRFR2,pRRFc(1),pRRFCI(1),pRRFc(2),pRRFCI(2),pRRFc(3),pRRFCI(3));
text(20,25.5,j);
end

%%  Function 2: Day 2 Torque, Respiratory, and EMG Analysis
function[sortedDay2RVLEMGAvg, sortedDay2RVMEMGAvg, sortedDay2RRFEMGAvg, Day2MetPower] = figure2(normalize, absolute, symbols, frequencies, EMGfrequencies,day2exc)
if ishandle(2), close(2); end

%% Loading
load('EMGSummaryAnalysisDay2.mat');%EMG data for day 2
load('ProcessedTorqueDay2.mat');%Torque data for day 2
load('RespiratorySummary.mat');%Respiratory data for both days

N = 11; % number of subjects
N_per_volt = 69.71; % Nm per volt
fraction_MVC = .5; % fraction of torque target

% Torque target amplitude
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;

%Normalize Torque data
if normalize, Torque_avg = TorqueAvgD2 ./ WaveformAmp(:);    
elseif absolute, Torque_avg = TorqueAvgD2 - WaveformAmp(:);   
else,Torque_avg = TorqueAvgD2;
end

% Sort EMG data according to frequency
[B,I] = sort(EMGfrequencies);
sortedDay2RVLEMGAvg = Day2RVLEMGAvg(:,I);
sortedDay2RVMEMGAvg = Day2RVMEMGAvg(:,I);
sortedDay2RRFEMGAvg = Day2RRFEMGAvg(:,I);

% Start to exclude subjects
for i = 1:N
    Torque_avgall(i,:) = Torque_avg(i,:);
    if day2exc(i) == 0;
    Torque_avg(i,:) = nan;
    end
end

for i = 1:N
    Day2MetPowerall(i,:) = Day2MetPower(i,:);
    if day2exc(i) == 0;
    Day2MetPower(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RVLEMGAvgall(i,:) = sortedDay2RVLEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RVMEMGAvgall(i,:) = sortedDay2RVMEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RRFEMGAvgall(i,:) = sortedDay2RRFEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RRFEMGAvg(i,:) = nan;
    end
end

% Get average EMG signal across right leg muscles
Day2EMGAvg = (sortedDay2RVLEMGAvg + sortedDay2RVMEMGAvg + sortedDay2RRFEMGAvg)/3;
Day2EMGAvgall = (sortedDay2RVLEMGAvgall + sortedDay2RVMEMGAvgall + sortedDay2RRFEMGAvgall)/3;

% Frequencies used for fitting and R2 analysis
r2frequencies = [0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5];
% some fits
% idx = isfinite(mean(NIRS(:,2:7)));
% p1 = polyfit(frequencies, mean(VO2f(:,2:7),'omitnan'),2);
% p2 = polyfit(frequencies, mean(Day2EMGAvg,'omitnan'),2);
% pRVL = polyfit(frequencies,mean(sortedDay2RVLEMGAvg,'omitnan'),2);
% pRVM = polyfit(frequencies,mean(sortedDay2RVMEMGAvg,'omitnan'),2);
% pRRF = polyfit(frequencies,mean(sortedDay2RRFEMGAvg,'omitnan'),2);

% Fit data and pull out stats
visualize = 0; % Any fit figures
x = [0:0.1:3]; % Frequency intervals of interest
%[p1,gofp1] = fit(frequencies', mean(VO2f(:,2:7),'omitnan')','poly2'); 
[AFpr] = anyfit(r2frequencies,Day2MetPower(:,2:7),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
prR2 = AFpr.r2;
prCI = [AFpr.coefint(1,1),AFpr.coefint(2,1),AFpr.coef(4,1)];
prc = [AFpr.coef(1,1),AFpr.coef(2,1),AFpr.offset];
pr = polyval(prc,x);
VO2fOS = Day2MetPower(:,2:7)-AFpr.coef(3:end)+AFpr.offset;

%[p2,gofp2] = fit(frequencies', mean(Day2EMGAvg,'omitnan')','poly2'); 
[AFpEMG] = anyfit(r2frequencies,Day2EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pEMGR2 = AFpEMG.r2;
pEMGCI = [AFpEMG.coefint(1,1),AFpEMG.coefint(2,1),AFpEMG.coef(4,1)];
pEMGc = [AFpEMG.coef(1,1),AFpEMG.coef(2,1),AFpEMG.offset];
pEMG = polyval(pEMGc,x);
Day2EMGAvgOS = Day2EMGAvg-AFpEMG.coef(3:end)+AFpEMG.offset;

%[pRVL,gofpRVL] = fit(frequencies', mean(sortedDay2RVLEMGAvg,'omitnan')','poly2'); 
[AFpRVL] = anyfit(r2frequencies,sortedDay2RVLEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRVLR2 = AFpRVL.r2;
pRVLCI = [AFpRVL.coefint(1,1),AFpRVL.coefint(2,1),AFpRVL.coef(4,1)];
pRVLc = [AFpRVL.coef(1,1),AFpRVL.coef(2,1),AFpRVL.offset];
pRVL = polyval(pRVLc,x);
sortedDay2RVLEMGAvgOS = sortedDay2RVLEMGAvg-AFpRVL.coef(3:end)+AFpRVL.offset;

%[pRVM,gofpRVM] = fit(frequencies', mean(sortedDay2RVMEMGAvg,'omitnan')','poly2'); 
[AFpRVM] = anyfit(r2frequencies,sortedDay2RVMEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRVMR2 = AFpRVM.r2;
pRVMCI = [AFpRVM.coefint(1,1),AFpRVM.coefint(2,1),AFpRVM.coef(4,1)];
pRVMc = [AFpRVM.coef(1,1),AFpRVM.coef(2,1),AFpRVM.offset];
pRVM = polyval(pRVMc,x);
sortedDay2RVMEMGAvgOS = sortedDay2RVMEMGAvg-AFpRVM.coef(3:end)+AFpRVM.offset;

%[pRRF,gofpRRF] = fit(frequencies', mean(sortedDay2RRFEMGAvg,'omitnan')','poly2'); 
[AFpRRF] = anyfit(r2frequencies,sortedDay2RRFEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRRFR2 = AFpRRF.r2;
pRRFCI = [AFpRRF.coefint(1,1),AFpRRF.coefint(2,1),AFpRRF.coef(4,1)];
pRRFc = [AFpRRF.coef(1,1),AFpRRF.coef(2,1),AFpRRF.offset];
pRRF = polyval(pRRFc,x);
sortedDay2RRFEMGAvgOS = sortedDay2RRFEMGAvg-AFpRRF.coef(3:end)+AFpRRF.offset;
%% Plot Upper row: Variable frequency, constant joint angle (i.e. Day 2)
f = figure(2); color = [get(gca,'colororder'); get(gca,'colororder')];
set(f,'name','Variable frequency, constant joint angle (i.e. Day 2)');

% Plot Torque data
subplot(231);

if normalize, ylabel('Normalized Average Torque'); yline(y,'r','LineWidth', 2);hold on
else, ylabel('Average Torque (Nm)'); yline(0,'k-','LineWidth', 2); hold on
end

for i = 1:N, h = plot(frequencies, Torque_avgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies,mean(Torque_avg,'omitnan'),std(Torque_avg,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Frequency (Hz)'); title('Knee Torque'); ylim([-8 6]);xlim([0 3]);

% Plot Respiratory data
subplot(232); %plot(0:.1:3, polyval(p1, 0:.1:3), 'k','linewidth',2); hold on
for i = 1:N, h = plot(frequencies, Day2MetPowerall(i,2:7), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies, mean(VO2fOS,'omitnan'),std(VO2fOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pr = plot(x,pr,'-'); hold on; set(pr,'lineWidth',2);set(pr,'color','k');
xlabel('Frequency (Hz)'); ylabel('Net metabolic rate(W/N-m)'); title('Respiratory'); ylim([0 5]);xlim([0 3]);
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',prR2,prc(1),prCI(1),prc(2),prCI(2),prc(3),prCI(3));
text(0.5,4,j);

% Plot EMG data
subplot(233); %plot(0:.1:3, polyval(p2, 0:.1:3), 'k','linewidth',2); hold on
for i = 1:N, h = plot(frequencies, Day2EMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies,mean(Day2EMGAvgOS,'omitnan'),std(Day2EMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pEMG = plot(x,pEMG,'-'); hold on; set(pEMG,'lineWidth',2);set(pEMG,'color','k');xlim([0 3]);
ylim([0 30]); xlabel('Frequency (Hz)'); ylabel('Average EMG (% of MVC)'); title('Electromyography');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pEMGR2,pEMGc(1),pEMGCI(1),pEMGc(2),pEMGCI(2),pEMGc(3),pEMGCI(3));
text(1.5,25.5,j);

%% Plot Lower row: EMG figure with all muscles
% Vastus Lateralis
subplot(234)
%plot(0:.1:3, polyval(pRVL, 0:.1:3), 'k-','linewidth',2); hold on
for i = 1:N, h = plot(frequencies, sortedDay2RVLEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies,mean(sortedDay2RVLEMGAvgOS,'omitnan'),std(sortedDay2RVLEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRVL = plot(x,pRVL,'-'); hold on; set(pRVL,'lineWidth',2);set(pRVL,'color','k');xlim([0 3]);
ylim([0 30]); xlabel('Frequency (Hz)');ylabel('Average EMG (% of MVC)'); title('Right Vastus Lateralis EMG');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRVLR2,pRVLc(1),pRVLCI(1),pRVLc(2),pRVLCI(2),pRVLc(3),pRVLCI(3));
text(1.5,25.5,j);

% Vastus Medialis
subplot(235); %plot(0:.1:3, polyval(pRVM, 0:.1:3), 'k','linewidth',2); hold on
for i = 1:N, h = plot(frequencies, sortedDay2RVMEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies,mean(sortedDay2RVMEMGAvgOS,'omitnan'),std(sortedDay2RVMEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRVM = plot(x,pRVM,'-'); hold on; set(pRVM,'lineWidth',2);set(pRVM,'color','k');xlim([0 3]);
ylim([0 30]); xlabel('Frequency (Hz)');ylabel('Average EMG (% of MVC)');
title('Right Vastus Medialis EMG');legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRVMR2,pRVMc(1),pRVMCI(1),pRVMc(2),pRVMCI(2),pRVMc(3),pRVMCI(3));
text(1.5,25.5,j);

subplot(236)
%plot(0:.1:3, polyval(pRRF, 0:.1:3), 'k','linewidth',2); hold on
for i = 1:N, h = plot(frequencies, sortedDay2RRFEMGAvgall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if day2exc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
errorbar(frequencies,mean(sortedDay2RRFEMGAvgOS,'omitnan'),std(sortedDay2RRFEMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pRRF = plot(x,pRRF,'-'); hold on; set(pRRF,'lineWidth',2);set(pRRF,'color','k');xlim([0 3]);
ylim([0 30]); legend('1','2','3','4','5','6','7','8','9','10','11','mean','fit','location','best')
xlabel('Frequency (Hz)');ylabel('Average EMG (% of MVC)'); title('Right Rectus Femoris EMG');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pRRFR2,pRRFc(1),pRRFCI(1),pRRFc(2),pRRFCI(2),pRRFc(3),pRRFCI(3));
text(1.5,25.5,j);
end
%% Function 3: Sanity Check for EMG of 2Hz at 20deg for both days
function [Day1EMGAvg, Day2EMGAvg] = figure3(symbols,frequencies,EMGfrequencies)
N = 11; % Number of subjects

% Load Variables from both days
load('EMGSummaryAnalysisDay1.mat');%EMG data for day 1
load('ProcessedTorqueDay1.mat');%Torque data for day 1
load('RespiratorySummary.mat');%Respiratory data for both days

load('EMGSummaryAnalysisDay2.mat');%EMG data for day 2
load('ProcessedTorqueDay2.mat');%Torque data for day 2
load('RespiratorySummary.mat');%Respiratory data for both days

N = 11; % number of subjects
N_per_volt = 69.71; % Nm per volt
fraction_MVC = .5; % fraction of torque target

% Torque target amplitude
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;

% Sort EMG data according to frequency 
[B,I] = sort(EMGfrequencies);
sortedDay2RVLEMGAvg = Day2RVLEMGAvg(:,I);
sortedDay2RVMEMGAvg = Day2RVMEMGAvg(:,I);
sortedDay2RRFEMGAvg = Day2RRFEMGAvg(:,I);

% Get average EMG signal across right leg muscles
Day1EMGAvg = (Day1RVLEMGAvg + Day1RVMEMGAvg + Day1RRFEMGAvg)/3;
Day2EMGAvg = (sortedDay2RVLEMGAvg + sortedDay2RVMEMGAvg + sortedDay2RRFEMGAvg)/3;

% Plot figures 
if ishandle(3), close(3); end; figure(3)
color =  [get(gca,'colororder'); get(gca,'colororder')];

for i = 1:3
    subplot(2,3,i+3); plot([0:1:25],[0:1:25],'k','linewidth',1); hold on;  axis equal; axis([0 25 0 25]); 
    xlabel('Day 1 (% MVC)'); ylabel('Day 2 (% MVC)');
end

subplot(231); plot([0:2:30],[0:2:30],'k','linewidth',1); hold on;  axis equal; axis([0 30 0 30]); 
xlabel('Day 1 (N-m)'); ylabel('Day 2 (N-m)'); title('Torque')
for i = 1:N
    subplot(231);  plot(TorqueAvgD1(i,2),TorqueAvgD2(i,5),symbols(i),'markersize',5,'color', color(i,:)); hold on 
end
legend('Unity line','1','2','3','4','5','6','7','8','9','10','11');

subplot(232); plot([0:1:6],[0:1:6],'k','linewidth',1); hold on;  axis equal; axis([0 6 0 6]); 
xlabel('Day 1 (W/N-m)'); ylabel('Day 2 (W/N-m)'); title('Respirometry')
for i = 1:N
    subplot(232);  plot(Day1MetPower(i,2),Day2MetPower(i,6),symbols(i),'markersize',5,'color', color(i,:)); hold on 
end

subplot(233); plot([0:1:25],[0:1:25],'k','linewidth',1); hold on;  axis equal; axis([0 25 0 25]); 
xlabel('Day 1 (% MVC)'); ylabel('Day 2 (% MVC)'); title('Average EMG')
for i = 1:N
    subplot(233);  plot(Day1EMGAvg(i,2),Day2EMGAvg(i,5),symbols(i),'markersize',5,'color', color(i,:)); hold on 
end

for i = 1:N
    subplot(234); plot(Day1RVLEMGAvg(i,2),sortedDay2RVLEMGAvg(i,5),symbols(i),'markersize',5,'color', color(i,:)); hold on; title('VL');
    subplot(235); plot(Day1RVMEMGAvg(i,2),sortedDay2RVMEMGAvg(i,5),symbols(i),'markersize',5,'color', color(i,:)); hold on; title('VM'); 
    subplot(236); plot(Day1RRFEMGAvg(i,2),sortedDay2RRFEMGAvg(i,5),symbols(i),'markersize',5,'color', color(i,:)); hold on; title('RF'); 
end
end

%% Function 4: NIRS Analysis
function [NIRS] = figure4(symbols,frequencies,NIRSexc)
load('RespiratorySummary'); % load respiratory data

% Frequencies used for fitting and R2 analysis
r2frequencies = [0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5];

% Start to exclude subjects
for i = 1:11
    NIRSall(i,:) = NIRS(i,:);
    if NIRSexc(i) == 0;
    NIRS(i,:) = nan;
    end
end
for i = 1:11
    Day2MetPowerall(i,:) = Day2MetPower(i,:);
    if NIRSexc(i) == 0;
    Day2MetPower(i,:) = nan;
    end
end

% Fit data and pull out stats
visualize = 0; % Anyfit visualization 
idx = isfinite(mean(NIRS,'omitnan'));
x = [0:0.1:3]; % Frequency intervals of interest
% pN = polyfit(0:.5:2.5, mean(NIRS(:,idx),'omitnan'),2);
%[pN,gofpN] = fit(frequencies', mean(NIRS(:,idx),'omitnan')','poly2'); 
[AFpN] = anyfit(r2frequencies,NIRS(:,2:7),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pNR2 = AFpN.r2;
pNCI = [AFpN.coefint(1,1),AFpN.coefint(2,1),AFpN.coef(4,1)];
pNc = [AFpN.coef(1,1),AFpN.coef(2,1),AFpN.offset];
pN = polyval(pNc,x);
NIRSOS = NIRS(:,2:7)-AFpN.coef(3:end)+AFpN.offset;

% Plot data
if ishandle(4), close(4); end
figure(4); color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(121);errorbar(frequencies, mean(NIRSOS,'omitnan'),std(NIRSOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
 subplot(121);h = plot(sort(frequencies), NIRSall(i,2:7), symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
 if NIRSexc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
subplot(121);%plot(0:.1:2.5, polyval(pN, 0:.1:2.5), 'k','linewidth',2);
pN = plot(x,pN,'-'); hold on; set(pN,'lineWidth',2);set(pN,'color','k');
xlabel('Frequency (Hz)');ylabel('TSI-rate (% s^{-1}/N-m)'); pNps = abs(pNCI(2)) - abs(pNc(1));
title((sprintf('NIRS Summary')));
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pNR2,pNc(1),pNCI(1),pNc(2),pNCI(2),pNc(3),pNCI(3));
text(0.45,0.07,j);
xlim = ([0 3]);ylim([0 0.08]);
legend('Mean','1','2','3','4','5','6','7','8','9','10','11','Fit');

NIRSavg = (mean(NIRS(:,2:7),'omitnan'));
VO2favg = (mean(Day2MetPower(:,2:7),'omitnan'));

y = NIRS(find(NIRSexc)',2:7)';
x = Day2MetPower(find(NIRSexc)',2:7)';

[curvefit,gof,output] = fit(x(:),y(:),'a*x');
CI = confint(curvefit,0.95); 
pc = coeffvalues(curvefit);
r = corrcoef(x,y);
R2 = gof.rsquare;
% [AFpC] = anyfit(x(:),y(:),'type', 'indiv');
% R2 = AFpC.r2;

figure(4);
for i = 1:11
subplot(122);h = plot(Day2MetPowerall(i,2:7), NIRSall(i,2:7),symbols(i),'markersize',5,'color', color(i,:)); hold on
   if NIRSexc(i) == 0;
     set(h,'color', [0.5, 0.5, 0.5]);
    end
end
h=plot(curvefit,'-'); hold on
set(h,'lineWidth',2);
set(h,'color','k');
xlabel('Net O2 rate (L/min/N-m)');ylabel('Desaturation rate (% s^{-1}/N-m)');
pCps = abs(CI(2)) - abs(pc(1));
title((sprintf('NIRS and Respiratory Comparison\n r = %.2f, R-square = %.2f, a*x: a = %.3f±%.3f',r(1,2),R2,pc,pCps)));
legend('1','2','3','4','5','6','7','8','9','10','11','Regression Slope');
end

%% Function 5: MVC Sanity Check
function [AllMVCTorqueAvgNm,AllMVCEMGAvg] = figure5(symbols,N_per_volt)

% Load torque data 
load('MVCDay1TorqueData.mat');
load('MVCDay2TorqueData.mat');
load('AllMVCTorqueData.mat');

% Convert torque to Nm
MVCDay1TorqueAvg = MVCDay1TorqueAvg*N_per_volt;
MVCDay2TorqueAvg = MVCDay2TorqueAvg*N_per_volt;
AllMVCTorqueAvgNm = AllMVCTorqueAvg*N_per_volt;

N=11; % Number of subjects

% Plot day 1 figures
if ishandle(5), close(5); end
figure(5); color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(121);plot([0:10:230],[0:10:230],'k','linewidth',1); hold on;  axis equal; axis([0 230 0 230]); 
xlabel('Day 1 (N-m)'); ylabel('Day 2 (N-m)'); title('MVC Torque Production')
for i = 1:N
    if (i==3)||(i==6)||(i==10)||(i==11)
    subplot(121);plot(MVCDay1TorqueAvg(i,1),AllMVCTorqueAvgNm(i,1),symbols(i),'markersize',10,'MarkerFaceColor', color(i,:),'MarkerEdgeColor',color(i,:)); hold on 
    else
    subplot(121);plot(MVCDay1TorqueAvg(i,1),MVCDay2TorqueAvg(i,1),symbols(i),'markersize',10,'color', color(i,:)); hold on    
    end
end
title('MVC Torque Comparison')

% Load EMG data
load('MVCDay1EMGData.mat');
load('MVCDay2EMGData.mat');
load('AllMVCEMGData.mat');

% Get average EMG signal across right leg muscles
AllMVCEMGAvg = (AllMVCRVMEMGAvg + AllMVCRVLEMGAvg + AllMVCRRFEMGAvg)/3;
MVCDay1EMGAvg = (MVCDay1RVMEMGAvg + MVCDay1RVLEMGAvg + MVCDay1RRFEMGAvg)/3;
MVCDay2EMGAvg = (MVCDay2RVMEMGAvg + MVCDay2RVLEMGAvg + MVCDay2RRFEMGAvg)/3;

% Plot day 2 figures
figure(5); color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(122);plot([0:1:2],[0:1:2],'k','linewidth',1); hold on;  axis equal; axis([0 1.5 0 1.5]); 
xlabel('Day 1 (N-m)'); ylabel('Day 2 (N-m)'); title('MVC Torque Production')
for i = 1:N
    if (i==3)||(i==6)||(i==10)||(i==11)
    subplot(122);plot(MVCDay1EMGAvg(i,1),AllMVCEMGAvg(i,1),symbols(i),'markersize',10,'MarkerFaceColor', color(i,:),'MarkerEdgeColor',color(i,:)); hold on 
    else
    subplot(122);plot(MVCDay1EMGAvg(i,1),MVCDay2EMGAvg(i,1),symbols(i),'markersize',10,'color', color(i,:)); hold on    
    end
end
legend('Unity line','1','2','3','4','5','6','7','8','9','10','11');
title('MVC EMG Comparison')
end

%% Function 6: MVC vs Angle 
function [AllMVCEMGAvgnorm,AllMVCRVMEMGAvgnorm,AllMVCRVLEMGAvgnorm,AllMVCRRFEMGAvgnorm,AllMVCTorqueAvgNm] = figure6_7(angles,symbols,N_per_volt)

% Torque
% Load Torque 
load('AllMVCTorqueData.mat');
% Conver to Nm
AllMVCTorqueAvgNm = AllMVCTorqueAvg*N_per_volt;

% Fit data and pull out stats
visualize = 0; % Any fit figures
x = [0:1:70]; % Angle intervals of interest

% Angles used for fitting and R2 analysis
r2angles = [nan(1,6);10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];

%pTMVC= polyfit(angles,mean(AllMVCTorqueAvgNm,'omitnan'),2);
%[pTMVC,gofpTMVC] = fit(angles', mean(AllMVCTorqueAvgNm,'omitnan')','poly2'); 
[AFpTMVC] = anyfit(r2angles,AllMVCTorqueAvgNm,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pTMVCR2 = AFpTMVC.r2;
pTMVCCI = [AFpTMVC.coefint(1,1),AFpTMVC.coefint(2,1),AFpTMVC.coef(4,1)];
pTMVCc = [AFpTMVC.coef(1,1),AFpTMVC.coef(2,1),AFpTMVC.offset];
pTMVC = polyval(pTMVCc,x);
AllMVCTorqueAvgNmOS = AllMVCTorqueAvgNm-AFpTMVC.coef(3:end)+AFpTMVC.offset;

% Plot figures 
if ishandle(6), close(6); end
figure(6); color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(121);%plot(angles, polyval(pTMVC, angles), 'k','linewidth',2); hold on
errorbar(angles,mean(AllMVCTorqueAvgNmOS,'omitnan'),std(AllMVCTorqueAvgNmOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:11
subplot(121);plot (angles, AllMVCTorqueAvgNm(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
end
pTMVC = plot(x,pTMVC,'-'); hold on; set(pTMVC,'lineWidth',2);set(pTMVC,'color','k');
legend('Mean','1','2','3','4','5','6','7','8','9','10','11','fit');xlim = ([0 70]);
xlabel('Angle (degrees)');ylabel('Knee Torque (Nm)');title('Maximal Voluntary Contraction');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pTMVCR2,pTMVCc(1),pTMVCCI(1),pTMVCc(2),pTMVCCI(2),pTMVCc(3),pTMVCCI(3));
text(15,270,j);

%EMG
% Load EMG data
load('AllMVCEMGData.mat')
% Get average EMG signal across right leg muscles
AllMVCEMGAvg = (AllMVCRVMEMGAvg + AllMVCRVLEMGAvg + AllMVCRRFEMGAvg)/3;
% pEMGMVC= polyfit(angles,mean(AllMVCEMGAvg,'omitnan'),1);

% Normalize EMG data
for i = 1:11
AllMVCEMGAvgmean = mean(AllMVCEMGAvg(i,1:6));
AllMVCEMGAvgnorm(i,1:6) = AllMVCEMGAvg(i,1:6)/AllMVCEMGAvgmean;
AllMVCRVMEMGAvgmean = mean(AllMVCRVMEMGAvg(i,1:6));
AllMVCRVMEMGAvgnorm(i,1:6) = AllMVCRVMEMGAvg(i,1:6)/AllMVCRVMEMGAvgmean;
AllMVCRVLEMGAvgmean = mean(AllMVCRVLEMGAvg(i,1:6));
AllMVCRVLEMGAvgnorm(i,1:6) = AllMVCRVLEMGAvg(i,1:6)/AllMVCRVLEMGAvgmean;
AllMVCRRFEMGAvgmean = mean(AllMVCRRFEMGAvg(i,1:6));
AllMVCRRFEMGAvgnorm(i,1:6) = AllMVCRRFEMGAvg(i,1:6)/AllMVCRRFEMGAvgmean;
end

% Plot figures 
subplot(122);xlim = ([0 70]);
errorbar(angles,mean(AllMVCEMGAvgnorm,'omitnan'),std(AllMVCEMGAvg,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:11
subplot(122);plot (angles, AllMVCEMGAvgnorm(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
end
yline(1,'r','linewidth',2); hold on
legend('Mean','1','2','3','4','5','6','7','8','9','10','11');
xlabel('Angle (degrees)');
ylabel('Normalized EMG Signal');
title('Maximal Voluntary Contraction');

if ishandle(7), close(7); end
figure(7);
% pRVLMVC= polyfit(angles,mean(AllMVCRVLEMGAvg,'omitnan'),1);
% pRVMMVC= polyfit(angles,mean(AllMVCRVMEMGAvg,'omitnan'),1);
% pRRFMVC= polyfit(angles,mean(AllMVCRRFEMGAvg,'omitnan'),1);


subplot(131);xlim = ([0 70]);
errorbar(angles,mean(AllMVCRVLEMGAvgnorm,'omitnan'),std(AllMVCRVLEMGAvgnorm,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:11
subplot(131);plot (angles, AllMVCRVLEMGAvgnorm(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
end
yline(1,'r','linewidth',2); hold on
xlabel('Angle (degrees)');
ylabel('Normalized EMG Signal');
title('Right Vastus Lateralis Maximal Voluntary Contraction');

subplot(132);xlim = ([0 70]);
errorbar(angles,mean(AllMVCRVMEMGAvgnorm,'omitnan'),std(AllMVCRVMEMGAvgnorm,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:11
subplot(132);plot (angles, AllMVCRVMEMGAvgnorm(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
end
yline(1,'r','linewidth',2); hold on
xlabel('Angle (degrees)');
ylabel('Normalized EMG Signal');
title('Right Vastus Medialis Maximal Voluntary Contraction');

subplot(133);xlim = ([0 70]);
errorbar(angles,mean(AllMVCRRFEMGAvgnorm,'omitnan'),std(AllMVCRRFEMGAvgnorm,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:11
subplot(133);plot (angles, AllMVCRRFEMGAvgnorm(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
end
yline(1,'r','linewidth',2); hold on
legend('Mean','1','2','3','4','5','6','7','8','9','10','11');
xlabel('Angle (degrees)');
ylabel('Normalized EMG Signal');
title('Right Rectus Femoris Maximal Voluntary Contraction');
end

%% Function 7: Gravity Intervals
function [Day1GravTorqueAvg,Day1GravTorqueDiff] = figure8(angles,symbols,N_per_volt,gravityintervalexc)

load('Day1Gravity.mat');% Load gravity data
Day1GravTorqueAvg = Day1GravTorqueAvg*N_per_volt;% Convert to Nm 

% Exclude subjects
for i = 1:11
    Day1GravTorqueAvgall(i,:) = Day1GravTorqueAvg(i,:);
    if gravityintervalexc(i) == 0;
    Day1GravTorqueAvg(i,:) = nan;
    end
end

N = 11;% Number of subjects

% Fit data and pull out stats

% angle used for fitting and R2 analysis
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];     

% Get mean of gravity
meanDay1GravTorqueAvg = (mean(Day1GravTorqueAvg, 'omitnan'));
y = meanDay1GravTorqueAvg';% Y for fitting
x = angles';% X for fitting
visualize = 0;% Anyfit visualization

% Use curve fitting to get fit 
[AFcurvefit] = anyfit(r2angles,Day1GravTorqueAvg,'visualize',0,'type', 'indiv');
curvefitR2 = AFcurvefit.r2;
% curvefitCI = [AFcurvefit.coefint(1,1),AFcurvefit.coefint(2,1),AFcurvefit.coef(4,1)];
% curvefitc = [AFcurvefit.coef(1,1),AFcurvefit.coef(2,1),AFcurvefit.offset];

% Individual curve fit for each subject
if ishandle(8), close(8); end
figure(8);
color =  [get(gca,'colororder'); get(gca,'colororder')];
[curvefit1,gofcurvefit1] = fit(x,y,'(m*9.81*0.35)*cosd((x-b))');
curvefitCI = confint(curvefit1,0.95); 
curvefitc = coeffvalues(curvefit1);

% Plot figures
subplot(121);h=plot(curvefit1,'-',x,y,'.'); hold on
set(h,'lineWidth',2);
set(h,'color',color(1,:));
h(1).MarkerSize = 20;
for i = 1:N
    y = Day1GravTorqueAvgall(i,1:6)';
    curvefit = fit(x,y,'(m*9.81*0.35)*cosd((x-b))');
    Yfit(i,:) = curvefit(x);
    subplot(121);h=plot(curvefit,'-',x,y,symbols(i)); hold on
    set(h,'lineWidth',1);
        if gravityintervalexc(i) == 0;
            set(h,'color', [0.5, 0.5, 0.5]);
        else
            set(h,'color', color(i,:));
        end
    h(1).MarkerSize = 5;
end
legend('mean','mean fit','1','1 Fit','2','2 Fit','3','3 Fit','4','4 Fit','5','5 Fit','6','6 Fit','7','7 Fit','8','8 Fit','9','9 Fit','10','10 Fit','11','11 Fit');
xlabel('Angle (degrees)');ylabel('Average Gravity Torque (Nm)');title('Gravity Torque');
curvefitps1 = abs(curvefitCI(2)) - abs(curvefitc(1));curvefitps2 = abs(curvefitCI(4)) - abs(curvefitc(2));
j = sprintf(' R-square = %.2f\n (m*9.81*0.35)*cosd((x-b))\n b = %.3f±%.3f\n m = %.3f±%.3f',curvefitR2,curvefitc(1),curvefitps1,curvefitc(2),curvefitps2);
text(20,115,j);

% Difference between actual and expected gravity results
color =  [get(gca,'colororder'); get(gca,'colororder')];
Day1GravTorqueDiff = Day1GravTorqueAvg - Yfit ;% Get difference between expected and actual gravity 

% Exclude subjects
for i = 1:N
    Day1GravTorqueDiffall(i,:) = Day1GravTorqueDiff(i,:);
    if gravityintervalexc(i) == 0;
    Day1GravTorqueDiff(i,:) = nan;
    end
end

% PLot figures
subplot(122);errorbar(angles,mean(Day1GravTorqueDiff,'omitnan'),std(Day1GravTorqueDiff,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i=1:N
      subplot(122);h = plot (angles, Day1GravTorqueDiffall(i,1:6),symbols(i),'markersize',5,'color', color(i,:),'linestyle','-'); hold on
            if gravityintervalexc(i) == 0;
            set(h,'color', [0.5, 0.5, 0.5]);
            end
end
legend('Mean','1','2','3','4','5','6','7','8','9','10','11');
xlabel('Angle (degrees)');
ylabel('Average Gravity Torque (Nm)');
title('Difference in Gravity Torque');
end

%% Function 8: Ultrasound Analysis
function [pennationchange,thicknesschange,flengthchange] = figure9_10_11(symbols,ultrasoundexc)

% Percent of waveform target for each subject
PercentofTarget = (0:10:100); % Percent of waveform target for each subject
r2PercentofTarget = [0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100];

% Subject numbers
subnames = {'S1', 'S2', 'S3', 'S4', 'S5', 'S6','S7','S8','S9','S10','S11'};
subnumber = {'1','2','3','4','5','6','7','8','9','10','11'};

N = length(subnames); 

% Create Nan variables to fill
pennationangles = nan(11,11);
pennationchange = nan(11,11);
thickness = nan(11,11);
thicknesschange = nan(11,11);
flength = nan(11,11);
flengthchange = nan(11,11);
visualize = 0; % anyfit visualizatiob

% Loop through each subject
for i = 1:N
   
    % Get manual estimates of all
    sheetname = subnames{i};
    alpha(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'B2:B12');
    supapo(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'C2:C12');
    pennationangles(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'E2:E12');
    pennationchange(i,:) = pennationangles(i,1:11) - pennationangles(i,1);
    thickness(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'F2:F12');
    thicknesschange(i,:) = thickness(i,1:11) - thickness(i,1);
    flength(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'G2:G12');
    flengthchange(i,:) = flength(i,1:11) - flength(i,1);
    conversion(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'H2');
    
end

% Exclude subjects
for i = 1:11
pennationanglesall(i,:) = pennationangles(i,:);
pennationchangeall(i,:) = pennationchange(i,:);
thicknessall(i,:) = thickness(i,:);
thicknesschangeall(i,:) = thicknesschange(i,:);
flengthall(i,:) = flength(i,:);
flengthchangeall(i,:) = flengthchange(i,:);
alphaall(i,:) = alpha(i,:);
supapoall(i,:) = supapo(i,:);
if ultrasoundexc(i) == 0
    pennationangles(i,:) = nan;
    pennationchange(i,:)= nan;
    thickness(i,:)= nan;
    thicknesschange(i,:)= nan;
    flength(i,:)= nan;
    flengthchange(i,:)= nan;
    alpha(i,:) = nan;
    supapo(i,:) = nan;
end
end

% Fit change in ultrasound data and pull out stats
x = [0:1:100]; % Percent of target intervals for fitting
%pPen= polyfit(PercentofTarget,mean(pennationchange,'omitnan'),2);
%[pPen,gofpPen] = fit(PercentofTarget', mean(pennationchange,'omitnan')','poly2'); 
[AFpPen] = anyfit(r2PercentofTarget,pennationchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pPenR2 = AFpPen.r2;
pPenCI = [AFpPen.coefint(1,1),AFpPen.coefint(2,1),AFpPen.coef(4,1)];
pPenc = [AFpPen.coef(1,1),AFpPen.coef(2,1),AFpPen.offset];
pPen = polyval(pPenc,x);
pennationchangeOS = pennationchange-AFpPen.coef(3:end)+AFpPen.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pThick,gofpThick] = fit(PercentofTarget', mean(thicknesschange,'omitnan')','poly1'); 
[AFpThick] = anyfit(r2PercentofTarget,thicknesschange,'visualize',0,'type', 'indiv');
pThickR2 = AFpThick.r2;
pThickCI = [AFpThick.coefint(1,1),AFpThick.coef(4,1)];
pThickc = [AFpThick.coef(1,1),AFpThick.offset];
pThick = polyval(pThickc,x);
thicknesschangeOS = thicknesschange-AFpThick.coef(2:end)+AFpThick.offset;

%pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
%[pFas,gofpFas] = fit(PercentofTarget', mean(flengthchange,'omitnan')','poly2'); 
[AFpFas] = anyfit(r2PercentofTarget,flengthchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pFasR2 = AFpFas.r2;
pFasCI = [AFpFas.coefint(1,1),AFpFas.coefint(2,1),AFpFas.coef(4,1)];
pFasc = [AFpFas.coef(1,1),AFpFas.coef(2,1),AFpFas.offset];
pFas = polyval(pFasc,x);
flengthchangeOS = flengthchange-AFpFas.coef(3:end)+AFpFas.offset;

% Plot figure for change in manual estimates
if ishandle(9), close(9); end
figure(9);
color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(231);%plot(PercentofTarget, polyval(pThick, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(thicknesschangeOS,'omitnan'),std(thicknesschangeOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, thicknesschangeall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pThick = plot(x,pThick,'-'); hold on; set(pThick,'lineWidth',2);set(pThick,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Thickness Change \Delta (cm)');title('Thickness Change');ylim([-0.6 0.4]);
legend('off');
j = sprintf(' R-square = %.2f\n p1*x + p2\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n',pThickR2,pThickc(1),pThickCI(1),pThickc(2),pThickCI(2));
text(20,0.5,j); ylim([-0.8 0.8]);


subplot(232);%plot(PercentofTarget, polyval(pPen, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(pennationchangeOS,'omitnan'),std(pennationchangeOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, pennationchangeall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pPen = plot(x,pPen,'-'); hold on; set(pPen,'lineWidth',2);set(pPen,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Pennation Change \Delta \Phi (deg)');title('Pennation Angle Change')
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pPenR2,pPenc(1),pPenCI(1),pPenc(2),pPenCI(2),pPenc(3),pPenCI(3));
text(20,8,j);ylim([-4 10]);

subplot(233);%plot(PercentofTarget, polyval(pFas, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(flengthchangeOS,'omitnan'),std(flengthchangeOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, flengthchangeall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pFas = plot(x,pFas,'-'); hold on; set(pFas,'lineWidth',2);set(pFas,'color','k');
legend('off');
xlabel('Knee Torque (% of waveform target)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('Fascicle Length Change');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pFasR2,pFasc(1),pFasCI(1),pFasc(2),pFasCI(2),pFasc(3),pFasCI(3));
text(50,7,j);ylim([-10 10]);

% pPenA= polyfit(PercentofTarget,mean(pennationangles,'omitnan'),2);
% pThickA= polyfit(PercentofTarget,mean(thickness,'omitnan'),1);
% pFasA= polyfit(PercentofTarget,mean(flength,'omitnan'),2);

% Fit absolute manual data and pull out stats
%[pPenA,gofpPenA] = fit(PercentofTarget', mean(pennationangles,'omitnan')','poly2'); 
[AFpPenA] = anyfit(r2PercentofTarget,pennationangles,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pPenAR2 = AFpPenA.r2;
pPenACI = [AFpPenA.coefint(1,1),AFpPenA.coefint(2,1),AFpPenA.coef(4,1)];
pPenAc = [AFpPenA.coef(1,1),AFpPenA.coef(2,1),AFpPenA.offset];
pPenA = polyval(pPenAc,x);
pennationanglesOS = pennationangles-AFpPenA.coef(3:end)+AFpPenA.offset;

%[pThickA,gofpThickA] = fit(PercentofTarget', mean(thickness,'omitnan')','poly1'); 
[AFpThickA] = anyfit(r2PercentofTarget,thickness,'visualize',0,'type', 'indiv');
pThickAR2 = AFpThickA.r2;
pThickACI = [AFpThickA.coefint(1,1),AFpThickA.coef(4,1)];
pThickAc = [AFpThickA.coef(1,1),AFpThickA.offset];
pThickA = polyval(pThickAc,x);
thicknessOS = thickness-AFpThickA.coef(2:end)+AFpThickA.offset;

%[pFasA,gofpFasA] = fit(PercentofTarget', mean(flength,'omitnan')','poly2'); 
[AFpFasA] = anyfit(r2PercentofTarget,flength,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pFasAR2 = AFpFasA.r2;
pFasACI = [AFpFasA.coefint(1,1),AFpFasA.coefint(2,1),AFpFasA.coef(4,1)];
pFasAc = [AFpFasA.coef(1,1),AFpFasA.coef(2,1),AFpFasA.offset];
pFasA = polyval(pFasAc,x);
flengthOS = flength-AFpFasA.coef(3:end)+AFpFasA.offset;

% Plot manual absolute figures
figure(9);
color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(234);%plot(PercentofTarget, polyval(pThickA, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(thicknessOS,'omitnan'),std(thicknessOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, thicknessall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pThickA = plot(x,pThickA,'-'); hold on; set(pThickA,'lineWidth',2);set(pThickA,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Thickness (cm)');title('Absolute Thickness')
legend('off');
j = sprintf(' R-square = %.2f\n p1*x + p2\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n',pThickAR2,pThickAc(1),pThickACI(1),pThickAc(2),pThickACI(2));
text(20,4.25,j);ylim([0 5]);

subplot(235);%plot(PercentofTarget, polyval(pPenA, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(pennationanglesOS,'omitnan'),std(pennationanglesOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, pennationanglesall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pPenA = plot(x,pPenA,'-'); hold on; set(pPenA,'lineWidth',2);set(pPenA,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Pennation Angle \Phi (deg)');title('Absolute Pennation Angle')
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pPenAR2,pPenAc(1),pPenACI(1),pPenAc(2),pPenACI(2),pPenAc(3),pPenACI(3));
text(10,35,j);ylim([0 40]);

subplot(236);%plot(PercentofTarget, polyval(pFasA, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(flengthOS,'omitnan'),std(flengthOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, flengthall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pFasA = plot(x,pFasA,'-'); hold on; set(pFasA,'lineWidth',2);set(pFasA,'color','k');
legend('mean','1','2','3','4','5','6','7','8','9','10','11','fit');
xlabel('Knee Torque (% of waveform target)');ylabel('Fascicle Length Lfas (cm)');title('Absolute Fascicle Length');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pFasAR2,pFasAc(1),pFasACI(1),pFasAc(2),pFasACI(2),pFasAc(3),pFasACI(3));
text(40,14,j);ylim([0 16]);

% Tim Track Estimates
load('TimTrackUltrasoundEstimates.mat'); % Load timtrack estimates
I = [11,[1:1:10]];

% Loop through each subject and pull out data
for i = 1:N
    TimTrackPennationAnglesabsunsorted = TimTrackPennationAngles(i,[1:10,14]);
    TimTrackPennationAnglesabs(i,:) = TimTrackPennationAnglesabsunsorted(I);
    TimTrackPennationAngleschange(i,:) = TimTrackPennationAnglesabs(i,:) - TimTrackPennationAnglesabs(i,1);
    TimTrackThicknessabsunsorted = TimTrackThickness(i,[1:10,14])./conversion(i,:);
    TimTrackThicknessabs(i,:) = TimTrackThicknessabsunsorted(I);
    TimTrackThicknesschange(i,:) = TimTrackThicknessabs(i,:) - TimTrackThicknessabs(i,1);
    TimTrackFlengthabsunsorted = TimTrackFlength(i,[1:10,14])./conversion(i,:);
    TimTrackFlengthabs(i,:) = TimTrackFlengthabsunsorted(I);
    TimTrackFlengthchange(i,:) = TimTrackFlengthabs(i,:) - TimTrackFlengthabs(i,1);
    TimTrackAlphaabsunsorted = TimTrackAlpha(i,[1:10,14]);
    TimTrackAlphaabs(i,:) = TimTrackAlphaabsunsorted(I);
    TimTrackAlphachange(i,:) = TimTrackAlphaabs(i,:) - TimTrackAlphaabs(i,1);
    TimTrackBetaabsunsorted = TimTrackBeta(i,[1:10,14]);
    TimTrackBetaabs(i,:) = TimTrackBetaabsunsorted(I);
    TimTrackBetachange(i,:) = TimTrackBetaabs(i,:) - TimTrackBetaabs(i,1);
    
end

%Exclude subjects
for i = 1:11
TimTrackPennationAnglesabsall(i,:) = TimTrackPennationAnglesabs(i,:);
TimTrackThicknessabseall(i,:) = TimTrackThicknessabs(i,:);
TimTrackFlengthabsall(i,:) = TimTrackFlengthabs(i,:);
TimTrackAlphaabsall(i,:) = TimTrackAlphaabs(i,:);
TimTrackBetaabsall(i,:) = TimTrackBetaabs(i,:);
TimTrackPennationAnglesall(i,:) = TimTrackPennationAngleschange(i,:);
TimTrackThicknessall(i,:) = TimTrackThicknesschange(i,:);
TimTrackFlengthall(i,:) = TimTrackFlengthchange(i,:);
TimTrackAlphaall(i,:) = TimTrackAlphachange(i,:);
TimTrackBetaall(i,:) = TimTrackBetachange(i,:);
if ultrasoundexc(i) == 0
    TimTrackPennationAnglesabs(i,:)= nan;
    TimTrackThicknessabs(i,:)= nan;
    TimTrackFlengthabs(i,:)= nan;
    TimTrackAlphaabs(i,:)= nan;
    TimTrackBetaabs(i,:)= nan;
    TimTrackPennationAngleschange(i,:)= nan;
    TimTrackThicknesschange(i,:)= nan;
    TimTrackFlengthchange(i,:)= nan;
    TimTrackAlphachange(i,:)= nan;
    TimTrackBetachange(i,:)= nan;
end
end

% Fit absolute and change in timtrack data and pull out stats
%pPen= polyfit(PercentofTarget,mean(pennationchange,'omitnan'),2);
%[pTPen,gofpTPen] = fit(PercentofTarget', mean(TimTrackPennationAngleschange,'omitnan')','poly2'); 
[AFpTPen] = anyfit(r2PercentofTarget,TimTrackPennationAnglesabs,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pPenR2 = AFpTPen.r2;
pTPenCI = [AFpTPen.coefint(1,1),AFpTPen.coefint(2,1),AFpTPen.coef(4,1)];
pTPenc = [AFpTPen.coef(1,1),AFpTPen.coef(2,1),AFpTPen.offset];
pTPen = polyval(pTPenc,x);
TimTrackPennationAnglesabsOS = TimTrackPennationAnglesabs-AFpTPen.coef(3:end)+AFpTPen.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pTThick,gofpTThick] = fit(PercentofTarget', mean(TimTrackThicknesschange,'omitnan')','poly1'); 
[AFpTThick] = anyfit(r2PercentofTarget,TimTrackThicknessabs,'visualize',0,'type', 'indiv');
pTThickR2 = AFpTThick.r2;
pTThickCI = [AFpTThick.coefint(1,1),AFpTThick.coef(4,1)];
pTThickc = [AFpTThick.coef(1,1),AFpTThick.offset];
pTThick = polyval(pTThickc,x);
TimTrackThicknessabsOS = TimTrackThicknessabs-AFpTThick.coef(2:end)+AFpTThick.offset;

%pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
%[pTFas,gofpTFas] = fit(PercentofTarget', mean(TimTrackFlengthchange,'omitnan')','poly2'); 
[AFpTFas] = anyfit(r2PercentofTarget,TimTrackFlengthabs,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pTFasR2 = AFpTFas.r2;
pTFasCI = [AFpTFas.coefint(1,1),AFpTFas.coefint(2,1),AFpTFas.coef(4,1)];
pTFasc = [AFpTFas.coef(1,1),AFpTFas.coef(2,1),AFpTFas.offset];
pTFas = polyval(pTFasc,x);
TimTrackFlengthabsOS = TimTrackFlengthabs-AFpTFas.coef(3:end)+AFpTFas.offset;

[AFpTPenC] = anyfit(r2PercentofTarget,TimTrackPennationAngleschange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pPenR2C = AFpTPenC.r2;
pTPenCIC = [AFpTPenC.coefint(1,1),AFpTPenC.coefint(2,1),AFpTPenC.coef(4,1)];
pTPencC = [AFpTPenC.coef(1,1),AFpTPenC.coef(2,1),AFpTPenC.offset];
pTPenC = polyval(pTPencC,x);
TimTrackPennationAnglesOS = TimTrackPennationAngleschange-AFpTPenC.coef(3:end)+AFpTPenC.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pTThick,gofpTThick] = fit(PercentofTarget', mean(TimTrackThicknesschange,'omitnan')','poly1'); 
[AFpTThickC] = anyfit(r2PercentofTarget,TimTrackThicknesschange,'visualize',0,'type', 'indiv');
pTThickR2C = AFpTThickC.r2;
pTThickCIC = [AFpTThickC.coefint(1,1),AFpTThickC.coef(4,1)];
pTThickcC = [AFpTThickC.coef(1,1),AFpTThickC.offset];
pTThickC = polyval(pTThickcC,x);
TimTrackThicknessOS = TimTrackThicknesschange-AFpTThickC.coef(2:end)+AFpTThickC.offset;

%pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
%[pTFas,gofpTFas] = fit(PercentofTarget', mean(TimTrackFlengthchange,'omitnan')','poly2'); 
[AFpTFasC] = anyfit(r2PercentofTarget,TimTrackFlengthchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pTFasR2C = AFpTFasC.r2;
pTFasCIC = [AFpTFasC.coefint(1,1),AFpTFasC.coefint(2,1),AFpTFasC.coef(4,1)];
pTFascC = [AFpTFasC.coef(1,1),AFpTFasC.coef(2,1),AFpTFasC.offset];
pTFasC = polyval(pTFascC,x);
TimTrackFlengthOS = TimTrackFlengthchange-AFpTFasC.coef(3:end)+AFpTFasC.offset;

% Plot figures for timtrack data
if ishandle(10), close(10); end
figure(10);
color = [get(gca,'colororder'); get(gca,'colororder')];

subplot(231);
errorbar(PercentofTarget,mean(TimTrackThicknessOS,'omitnan'),std(TimTrackThicknessOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackThicknessall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTThickC = plot(x,pTThickC,'-'); hold on; set(pTThickC,'lineWidth',2);set(pTThickC,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Thickness Change \Delta (cm)');title('Tim Track Thickness Change');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x + p2\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n',pTThickR2C,pTThickcC(1),pTThickCIC(1),pTThickcC(2),pTThickCIC(2));
text(30,0.6,j);ylim([-0.8 0.8]);


subplot(232);
errorbar(PercentofTarget,mean(TimTrackPennationAnglesOS,'omitnan'),std(TimTrackPennationAnglesOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackPennationAnglesall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTPenC = plot(x,pTPenC,'-'); hold on; set(pTPenC,'lineWidth',2);set(pTPenC,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Pennation Change \Delta \Phi (deg)');title('Tim Track Pennation Angle Change')
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pPenR2C,pTPencC(1),pTPenCIC(1),pTPencC(2),pTPenCIC(2),pTPencC(3),pTPenCIC(3));
text(10,8,j);ylim([-4 10]);

subplot(233);
errorbar(PercentofTarget,mean(TimTrackFlengthOS,'omitnan'),std(TimTrackFlengthOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackFlengthall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTFasC = plot(x,pTFasC,'-'); hold on; set(pTFasC,'lineWidth',2);set(pTFasC,'color','k');
legend('off');
xlabel('Knee Torque (% of waveform target)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('TimTrack Fascicle Length Change');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pTFasR2C,pTFascC(1),pTFasCIC(1),pTFascC(2),pTFasCIC(2),pTFascC(3),pTFasCIC(3));
text(25,6,j);ylim([-10 10]);


subplot(234);%plot(PercentofTarget, polyval(pThick, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(TimTrackThicknessabsOS,'omitnan'),std(TimTrackThicknessabsOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackThicknessabseall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTThick = plot(x,pTThick,'-'); hold on; set(pTThick,'lineWidth',2);set(pTThick,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Thickness Absolute (cm)');title('Tim Track Thickness Change');
legend('off');
j = sprintf(' R-square = %.2f\n p1*x + p2\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n',pTThickR2,pTThickc(1),pTThickCI(1),pTThickc(2),pTThickCI(2));
text(30,4.25,j);ylim([0 5]);


subplot(235);%plot(PercentofTarget, polyval(pPen, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(TimTrackPennationAnglesabsOS,'omitnan'),std(TimTrackPennationAnglesabsOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackPennationAnglesabsall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTPen = plot(x,pTPen,'-'); hold on; set(pTPen,'lineWidth',2);set(pTPen,'color','k');
xlabel('Knee Torque (% of waveform target)');ylabel('Pennation Absolute \Phi (deg)');title('Tim Track Pennation Angle Change')
legend('off');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pPenR2,pTPenc(1),pTPenCI(1),pTPenc(2),pTPenCI(2),pTPenc(3),pTPenCI(3));
text(20,35,j);ylim([0 40]);

subplot(236);%plot(PercentofTarget, polyval(pFas, PercentofTarget), 'k','linewidth',2); hold on
errorbar(PercentofTarget,mean(TimTrackFlengthabsOS,'omitnan'),std(TimTrackFlengthabsOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
for i = 1:11
    h = plot(PercentofTarget, TimTrackFlengthabsall(i,:), symbols(i), 'color', color(i,:),'markersize',5,'linestyle','-'); hold on    
    if ultrasoundexc(i) == 0
            set(h,'color', [0.5, 0.5, 0.5]);
    end
end
pTFas = plot(x,pTFas,'-'); hold on; set(pTFas,'lineWidth',2);set(pTFas,'color','k');
legend('mean','1','2','3','4','5','6','7','8','9','10','11','fit');
xlabel('Knee Torque (% of waveform target)');ylabel('Fascicle Length Absolute Lfas (cm)');title('TimTrack Fascicle Length Change');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pTFasR2,pTFasc(1),pTFasCI(1),pTFasc(2),pTFasCI(2),pTFasc(3),pTFasCI(3));
text(40,14,j);ylim([0 16]);

% Plot figures comparing manual estimates to TimTrack estimates 
if ishandle(11), close(11); end
figure(11);

subplot(231); plot([0:1:6],[0:1:6],'k','linewidth',1); hold on;  axis equal; axis([0 6 0 6]); 
xlabel('Manual Estimates (\Delta (cm))'); ylabel('TimTrack Estimates (\Delta (cm))'); title('Absolute Thickness')
for i = 1:11
    subplot(231);  plot(thicknessall(i,:),TimTrackThicknessabseall(i,:),symbols(i),'color', color(i,:),'markersize',10); hold on 
end
legend('Unity line','1','2','3','4','5','6','7','8','9','10','11');

subplot(232); plot([0:1:40],[0:1:40],'k','linewidth',1); hold on;  axis equal; axis([0 40 0 40]); 
xlabel('Manual Estimates (\Delta \Phi (deg))'); ylabel('TimTrack Estimates (\Delta \Phi (deg))'); title('Absolute Pennation Angle')
for i = 1:11
    subplot(232);  plot(pennationanglesall(i,:),TimTrackPennationAnglesabsall(i,:),symbols(i),'color', color(i,:),'markersize',10); hold on 
end

subplot(233); plot([0:1:40],[0:1:40],'k','linewidth',1); hold on;  axis equal; axis([0 15 0 15]); 
xlabel('Manual Estimates (\Delta Lfas (cm))'); ylabel('TimTrack Estimates (\Delta Lfas (cm))'); title('Absolute Fascicle Length')
for i = 1:11
    subplot(233);  plot(flengthall(i,:),TimTrackFlengthabsall(i,:),symbols(i),'color', color(i,:),'markersize',10); hold on 
end

subplot(234); plot([-5:1:30],[-5:1:30],'k','linewidth',1); hold on;  axis equal; axis([0 30 0 30]); 
xlabel('Manual Estimates (\Delta \alpha (deg))'); ylabel('TimTrack Estimates (\Delta \alpha (deg))'); title('Absolute Fasiscle Angle')
for i = 1:11
    subplot(234);  plot(alphaall(i,:),TimTrackAlphaabsall(i,:),symbols(i),'color', color(i,:),'markersize',10); hold on 
end

subplot(235); plot([-10:1:5],[-10:1:5],'k','linewidth',1); hold on;  axis equal; axis([-10 5 -10 5]); 
xlabel('Manual Estimates (\Delta \beta (deg))'); ylabel('TimTrack Estimates (\Delta \beta (deg))'); title('Absolute Superficial Aponeurosis')
for i = 1:11
    subplot(235);  plot(supapoall(i,:),TimTrackBetaabsall(i,:),symbols(i),'color', color(i,:),'markersize',10); hold on 
end

end











