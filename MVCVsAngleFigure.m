clear all; close all; clc
% this script visualizes a complete summary of the processed torque,
% respiratory, and EMG data for both days. A comparison between each days EMG and respiratory 
% data for 2Hz at 20deg from each day is plotted for each subject.

%% Run this first: Variables needed to run sections
angles = 10:10:60;%Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
N_per_volt = 69.71; 
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order
TendegMVC = [1.721,1.601,1.185,1.941,1.164,0.603,1.664,2.024,2.481,1.167,0.652]';

%Exclusion Variables
subj_exc = [9]; %Select which subjects to exclude from analysis   
exc = ones(1,11);
exc(subj_exc) = 0;

%Exclusion Day 1 data
subj_excD1 = [9]; %Select which subjects to exclude from analysis   
excD1 = ones(1,11);
excD1(subj_excD1) = 0;


%Normalize torque by their torque target voltage
normalize = true;

%Absolute Deviation of torque
absolute = false;

% Save Figures
savefigures = 0;
%% Gravity vs Angle
N = 11;
% Torque 
load('AllMVCTorqueData.mat');
AllMVCTorqueAvgNm = AllMVCTorqueAvg*N_per_volt;

for i = 1:N
    AllMVCTorqueAvgNmall(i,:) = AllMVCTorqueAvgNm(i,:);
    if exc(i) == 0;
    AllMVCTorqueAvgNm(i,:) = nan;
    end
end

visualize = 0;
xos = [0:1:70];
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];

%pTMVC= polyfit(angles,mean(AllMVCTorqueAvgNm,'omitnan'),2);
%[pTMVC,gofpTMVC] = fit(angles', mean(AllMVCTorqueAvgNm,'omitnan')','poly2'); 
[AFpTMVC] = anyfit(r2angles,AllMVCTorqueAvgNm,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]); 
pTMVCR2 = AFpTMVC.r2;
pTMVCCI = [AFpTMVC.coefint(1,1),AFpTMVC.coefint(2,1),AFpTMVC.coef(4,1)];
pTMVCc = [AFpTMVC.coef(1,1),AFpTMVC.coef(2,1),AFpTMVC.offset];
pTMVC = polyval(pTMVCc,xos);

AllMVCTorqueAvgNmOS = AllMVCTorqueAvgNm-AFpTMVC.coef(3:end)+AFpTMVC.offset;

figure(); color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(121);pTMVCline = plot(xos,pTMVC,'--'); hold on; set(pTMVCline,'lineWidth',2);set(pTMVCline,'color','k');
for i=1:11
subplot(121);h = plot (angles, AllMVCTorqueAvgNmall(i,1:6),symbols(i),'markersize',5,'color', [0.6, 0.6, 0.6],'linestyle','--'); hold on
end
errorbar(angles,mean(AllMVCTorqueAvgNmOS,'omitnan'),std(AllMVCTorqueAvgNmOS,'omitnan'),'.','LineWidth', 1.5,'markersize',30,'color',color(1,:)); hold on
legend('fit','1','2','3','4','5','6','7','8','9','10','11','Mean');xlim = ([0 70]);ylim([0 350]);
xlabel('Knee angle (deg)');ylabel('Knee Torque (Nm)');title('Variable angle maximal voluntary contraction');
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pTMVCR2,pTMVCc(1),pTMVCCI(1),pTMVCc(2),pTMVCCI(2),pTMVCc(3),pTMVCCI(3));
text(15,270,j);

%% Activation Cost vs  Angle
IF = 1./pTMVC;
subplot(122);pAC = plot(xos,(1./pTMVC),'--'); hold on; set(pAC,'lineWidth',2);set(pAC,'color','k');
xlabel('Knee angle (deg)'); ylabel('Variable angle muscle activation');
title('Variable Angle Muscle Activation');ylim([0 0.03]);

%% Application to EMG and Respiratory data
load('EMGSummaryAnalysisDay1.mat');

for i = 1:N
    Day1RVLEMGAvgall(i,:) = Day1RVLEMGAvg(i,:);
    if exc(i) == 0;
    Day1RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RVMEMGAvgall(i,:) = Day1RVMEMGAvg(i,:);
    if exc(i) == 0;
    Day1RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RRFEMGAvgall(i,:) = Day1RRFEMGAvg(i,:);
    if excD1(i) == 0;
    Day1RRFEMGAvg(i,:) = nan;
    end
end  
visualize = 0; 
xos = [0:1:70];

%1/C1X.^2+C2X+C3 Find the Cs from previous inverse fit and write them out (3SigFig) with X as angles and Y as EMG data. "offset" = none
Day1EMGAvg = (Day1RVLEMGAvg + Day1RVMEMGAvg + Day1RRFEMGAvg)/3;
Day1EMGAvgall = (Day1RVLEMGAvgall + Day1RVMEMGAvgall + Day1RRFEMGAvgall)/3;
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];

[AFpEMG] = anyfit(r2angles,Day1EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pEMGR2 = AFpEMG.r2;
pEMGCI = [AFpEMG.coefint(1,1),AFpEMG.coefint(2,1),AFpEMG.coef(4,1)];
pEMGc = [AFpEMG.coef(1,1),AFpEMG.coef(2,1),AFpEMG.offset];
pEMG = polyval(pEMGc,xos);
%Day1EMGAvgOS = Day1EMGAvg-AFpEMG.coef(3:end)+AFpEMG.offset;

[PDpEMG] = anyfit(r2angles,Day1EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[(((-0.013392640350796*x.^2)+(3.114067251080848*x)+58.351403340870130).^-1)]);
PDEMG = IF * PDpEMG.coef(1)+PDpEMG.offset;%Scale original prediciton 'IF' to EMG data
Day1EMGAvgOS = Day1EMGAvg-PDpEMG.coef(2:end)+PDpEMG.offset;

figure(2);
clear xlim; hold off
subplot(121); %plot(0:.1:3, polyval(p2, 0:.1:3), 'k','linewidth',2); hold on
errorbar(angles,mean(Day1EMGAvgOS,'omitnan'),std(Day1EMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pEMGline = plot(xos,PDEMG,'--'); hold on; set(pEMGline,'lineWidth',2);set(pEMGline,'color','k');xlim([5 65]);
ylim([0 20]); xlabel('Angle (deg)'); ylabel('Average EMG (% of MVC)'); title('Variable angle electromyography');
legend('off');

load('RespiratorySummary.mat');
for i = 1:N
    Day1MetPowerall(i,:) = Day1MetPower(i,:);
    if excD1(i) == 0;
    Day1MetPower(i,:) = nan;
    end
end

[AFpr] = anyfit(r2angles,Day1MetPower(:,1:6),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
x = [0:1:70]; 
prCI = [AFpr.coefint(1,1),AFpr.coefint(2,1),AFpr.coef(4,1)];
prc = [AFpr.coef(1,1),AFpr.coef(2,1),AFpr.offset];
prR2 = AFpr.r2;
pr = polyval(prc,x);
% VO2aOS = VO2a(:,1:6)-AFpr.coef(3:end)+AFpr.offset;

% VO2aOSNAN = VO2aOS;
% VO2aOSNAN(1,:) = [];
% VO2aOSNAN(8,:) = [];
% x = r2anglesNAN(:);
% y = VO2aOSNAN(:);
% PDVO2aOS = fit(x,y,'a*(1/((-0.013392640350796*x^2)+(3.114067251080848*x)+58.351403340870130))');
% PDVO2aOSc = coeffvalues(PDVO2aOS);
[PDpDay1MetPower] = anyfit(r2angles,Day1MetPower(:,1:6),'visualize',0,'type', 'indiv','function',@(x)[(((-0.013392640350796*x.^2)+(3.114067251080848*x)+58.351403340870130).^-1)]);
PDDay1MetPower = IF .* PDpDay1MetPower.coef(1)+PDpDay1MetPower.offset; %Scale original prediciton 'IF' to respiratory data
Day1MetPowerOS = Day1MetPower(:,1:6)-PDpDay1MetPower.coef(2:end)+PDpDay1MetPower.offset;



subplot(122); %plot(0:.1:3, polyval(p2, 0:.1:3), 'k','linewidth',2); hold on
errorbar(angles,mean(Day1MetPowerOS(:,1:6),'omitnan'),std(Day1MetPowerOS(:,1:6),'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
pVO2aline = plot(xos,PDDay1MetPower,'--'); hold on; set(pVO2aline,'lineWidth',2);set(pVO2aline,'color','k');xlim([0 70]);
xlabel('Knee angle (deg)'); ylabel('Net metabolic rate(W/N-m)'); title('Variable angle respiratory');  ylim([0 6]);xlim([0 70]);
legend('off'); 