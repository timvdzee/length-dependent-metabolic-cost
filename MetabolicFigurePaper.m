clear Day1MetPowerall; close all; clc
% this script visualizes a complete summary of the processed torque,
% respiratory, and EMG data for both days. A comparison between each days EMG and respiratory 
% data for 2Hz at 20deg from each day is plotted for each subject.

%% Run this first: Variables needed to run sections
angles = 10:10:60;%Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
N_per_volt = 69.71; 
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * N_per_volt;

%Exclusion Variables
day1subj_exc = [9]; %Select which subjects to exclude from day 1 analysis   
day1exc = ones(1,11);
day1exc(day1subj_exc) = 0;

day2subj_exc = [9]; %Select which subjects to exclude from day 2 analysis   
day2exc = ones(1,11);
day2exc(day2subj_exc) = 0;

NIRSsubj_exc = [9]; %Select which subjects to exclude from NIRS analysis 
NIRSexc = ones(1,11);
NIRSexc(NIRSsubj_exc) = 0;

gravityintervalsubj_exc = [9]; %Select which subjects to exclude from gravity interval analysis
gravityintervalexc = ones(1,12);
gravityintervalexc(gravityintervalsubj_exc) = 0;

ultrasoundsubj_exc = [4,9,11]; %Select which subjects to exclude from ultrasound analysis
ultrasoundexc = ones(1,11);
ultrasoundexc(ultrasoundsubj_exc) = 0;

%Normalize torque by their torque target voltage
normalize = true;

%Absolute Deviation of torque
absolute = false;

% Save Figures
savefigures = 0;
%% Loading
load('RespiratorySummary.mat');%Respiratory data for both days
N = 11;
%% Day 1
for i = 1:N
    Day1MetPowerall(i,:) = Day1MetPower(i,:);
    if day1exc(i) == 0;
    Day1MetPower(i,:) = nan;
    WaveformAmp(i) = nan;
    end
end
visualize = 0;
torqueavg = mean(WaveformAmp,'omitnan');
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];
[AFpr] = anyfit(r2angles,Day1MetPower(:,1:6),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
x = [0:1:70]; 
prCI = [AFpr.coefint(1,1),AFpr.coefint(2,1),AFpr.coef(4,1)];
prc = [AFpr.coef(1,1),AFpr.coef(2,1),AFpr.offset];
prR2 = AFpr.r2;
pr = polyval(prc,x);
D1MetPowerOS = Day1MetPower(:,1:6)-AFpr.coef(3:end)+AFpr.offset;

figure(1);
color = [get(gca,'colororder'); get(gca,'colororder')];
subplot(121);  %plot(0:60, polyval(p1, 0:60), 'k','linewidth',2); hold on
pr = plot(x,pr,'--'); hold on; set(pr,'lineWidth',2);set(pr,'color','k');
errorbar(angles, mean(D1MetPowerOS(:,1:6),'omitnan'),std(D1MetPowerOS(:,1:6),'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Angle (deg)'); ylabel('Net metabolic rate (W/N-m)'); title('Testing Day 1 Metabolics');  %ylim([0 6]);xlim([0 70]);
legend('off'); 
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',prR2,prc(1),prCI(1),prc(2),prCI(2),prc(3),prCI(3));
text(50,5,j);

%% Day 2
for i = 1:N
    Day2MetPowerall(i,:) = Day2MetPower(i,:);
    if day2exc(i) == 0;
    Day2MetPower(i,:) = nan;
    end
end

for i = 1:N
    NIRSall(i,:) = NIRS(i,:);
    if NIRSexc(i) == 0;
    NIRS(i,:) = nan;
    end
end

r2frequencies = [0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5];

x2 = [0:0.1:3];
%[p1,gofp1] = fit(frequencies', mean(VO2f(:,2:7),'omitnan')','poly2'); 
[AFpr2] = anyfit(r2frequencies,Day2MetPower(:,2:7),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pr2R2 = AFpr2.r2;
pr2CI = [AFpr2.coefint(1,1),AFpr2.coefint(2,1),AFpr2.coef(4,1)];
pr2c = [AFpr2.coef(1,1),AFpr2.coef(2,1),AFpr2.offset];
pr2 = polyval(pr2c,x2);
VO2fOS = Day2MetPower(:,2:7)-AFpr2.coef(3:end)+AFpr2.offset;

[AFpN] = anyfit(r2frequencies,NIRS(:,2:7),'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pNR2 = AFpN.r2;
pNCI = [AFpN.coefint(1,1),AFpN.coefint(2,1),AFpN.coef(4,1)];
pNc = [AFpN.coef(1,1),AFpN.coef(2,1),AFpN.offset];
pN = polyval(pNc,x2);
NIRSOS = NIRS(:,2:7)-AFpN.coef(3:end)+AFpN.offset;

%% Metabolic Work 
Efreq =[0:0.5:3];
workper = 3.9266 %3.2616
C1 = 3.33;
Ew = workper*C1*Efreq; 
EwNorm = Ew/(mean(WaveformAmp,'omitnan'));


%% Metabolic Forcerate 
FR = Day2MetPower(:,2:7) - EwNorm(1:6);
[pFR] = anyfit(r2frequencies,FR,'visualize',0,'type', 'indiv','function',@(x)[x.^2]);
pFRR2 = pFR.r2;
pFRCI = [pFR.coefint(1,1),pFR.coefint(2,1),pFR.coef(4,1)];
C2 = pFR.coef(1,1);
C0 = pFR.offset;
FROS = Day2MetPower(:,2:7)-pFR.coef(2:end)+pFR.offset;


%% Net Metabolic
ENet = (C2*(Efreq.^2))+(EwNorm)+C0;

%% Figure
figure(1);
subplot(122); clear xlim;box on
yyaxis left
%pr2 = plot(x2,pr2,'--'); hold on; set(pr2,'lineWidth',2);set(pr2,'color',color(1,:));
pW = plot(Efreq,(EwNorm+C0),'--'); hold on; set(pW,'lineWidth',1);set(pW,'color','k');
pNet = plot(Efreq,ENet,'--'); hold on; set(pNet,'lineWidth',2);set(pNet,'color','k');
yline(C0,'--','lineWidth',1,'color','k')
errorbar(frequencies, mean(FROS,'omitnan'),std(FROS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Frequency f (Hz)'); ylabel('Net metabolic rate (W/N-m)'); ylim([0 6]);xlim([0 3]);
legend('off');
j = sprintf(' R-square = %.2f',pFRR2);%pr2c(1),pr2CI(1),pr2c(2),pr2CI(2),pr2c(3),pr2CI(3)
text(0.5,5,j);

yyaxis right
pN = plot(x2,pN,'--'); hold on; set(pN,'lineWidth',2);set(pN,'color',color(2,:));
errorbar(frequencies, mean(NIRSOS,'omitnan'),std(NIRSOS,'omitnan'),'s','LineWidth', 1.5,'markersize',10,'color',color(2,:),'MarkerFaceColor', color(2,:),'CapSize',20); hold on
ylabel('TSI-rate (% s^{-1}/N-m)'); 
title((sprintf('Testing Day 2 Metabolics')));
j = sprintf(' R-square = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pNR2,pNc(1),pNCI(1),pNc(2),pNCI(2),pNc(3),pNCI(3));
text(2,0.083,j);
xlim = ([0 3]);ylim([0 0.10]);
