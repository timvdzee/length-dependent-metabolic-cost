function[mp] = do_NIRS(filename, freqs, col, tstart1, lf_duration, tadd)

p = nan(21,2);
data = xlsread(filename);
tsi = data(48:end,col);

fs = repmat(freqs,3,1);
f = fs(1:numel(fs));
         
tstart = nan(1,(length(f)));
tstart(1) = tstart1;

if nargin < 6
    tadd = [1:2 4:6 8:10 12:14 16:18 20:22]*60;
end

tstart(2:end) = tstart(1) + tadd;

%% plot the data and show the start of inflation and deflation
% the linear portion of the decrease in tsi is assumed to be 0-6 s after
% inflation, the end of the linear portion is defined by the variable tend.
% for each condition, this linear data is cut out and stored in lindata
figure(1); color = get(gca,'colororder');

% create time vector
dt = .1; % [s]
N = length(tsi);
t = 0:dt:((N-1)*dt);

% plot all the data
plot(t,tsi,'color', color(1,:)); hold on
title('NIRS data')
xlabel('Time (s)'); ylabel('TSI (%)');

tstop = tstart+10; % stop of inflation (i.e. start of deflation)
tend = tstart + lf_duration; % end of "linear phase"

% lindata = nan(60, length(f));

clear lindata

for i = 1:length(tstart)
    plot([tstart(i) tstart(i)], [min(tsi) max(tsi)], '--','color',color(2,:));  hold on
    plot([tstop(i) tstop(i)], [min(tsi) max(tsi)], '--','color',color(3,:)); 
    plot([tend(i) tend(i)], [min(tsi) max(tsi)], '--','color',color(4,:)); 
    
    lidx = t>tstart(i) & t<tend(i);
    lindata(1:length(tsi(lidx)),i) = tsi(lidx);
end

axis tight

%% plot the linear data
% time vector for linear data
n = length(lindata);
tlin = 0:dt:((n-1)*dt);

figure(2)

lindata(lindata==0) = nan;

for i = 1:size(lindata,2)
        plot(tlin, lindata(:,i),'o'); hold on
end

for i = 1:size(lindata,2)
    idx = isfinite(lindata(:,i));
    p(i,:) = polyfit(tlin(idx), lindata(idx,i)', 1);    
    plot(tlin, polyval(p(i,:), tlin));
end

title('Linear portion of decrease in TSI')

xlabel('Time (s)'); ylabel('TSI (%)');


%% plot the slope vs. waveform frequency
figure(3)

        
frq = sort(unique(f));

for i = 1:length(f)
    c = color(mod(i,3)+1,:);
    plot(f(i),-p(i,1),'o','color',c,'markerfacecolor', c); hold on%  hold on
end

title('Slope vs. waveform frequency')
xlabel('Waveform frequency (Hz)')
ylabel('TSI-rate (% s^{-1})')

xlim([0 3])

%% averages  
for i = 1:length(frq)

    mp(i) = mean(p(f == frq(i),1));

end

plot(frq, -mp,'o','color',[0 0 0],'markerfacecolor', [0 0 0], 'markersize', 8); hold on%
end