# Length-dependent-metabolic-cost

## The Experiment Performed
Muscle force-rate accounts for a big part the increase in metabolic cost with increasing frequency of cyclic force production, as discussed [here.](https://journals.biologists.com/jeb/article/224/9/jeb233965/237823/The-high-energetic-cost-of-rapid-force-development)
But it does not account for the increase in metabolic cost with shorter muscle lengths, as discussed [here.](https://www.biorxiv.org/content/10.1101/2021.02.10.430661v1)

We here attempt to unit the effects of frequency and muscle length on metabolic cost in this new project.
This study includes measurements of torque, EMG, and respirometry data for a grid of frequencies and muscle lengths, during cyclic force production by the knee extensor muscles. Attached below is a figure outlining the grid of angle and frequencies used for each testing day. Day 1 trials are outlined in blue while day 2 trials are marked with red. The 20 degree trial at 2Hz happens on both days so it is marked in green as it overlaps between testing days. 

![AnglebyFrequencyGrid](FrequencybyAngleGrid.jpg)  

This repo contains the required Matlab scripts and functions used to analyze torque, EMG, and respiratory data organized into specific folders. The following flow chart gives a general overview regarding the scripts used to turn the raw data into the finalized processed variables.

![FlowChart](MainFolderFlowChart.jpg)  

## The main folders

### Folder: Torque 
This folder contains the needed scripts, variables, and functions to process and analyze the raw Torque data.

### Folder EMG
This folder contains the needed scripts, variables, and functions to process and analyze the raw EMG data.

### Folder: Respiratory
This folder contains the following: 

* The required scripts, variables, and functions to process and analyze the raw VO2 and respiratory exchange ratio (RER)
* Subfolder VO2 Analysis: Contains the analyzed VO2 data for both testing days and the needed script and variable to create these variables
* Subfolder Analyzed RER: Contains the analyzed RER variables for each subject and the required functions to create these variables

## EMGTorqueRespiratorySummary script
To start, run the 'EMGTorqueRespiratorySummary' script. This script does not require raw data to run and plots the following 10 graphs that summarize the final processed torque, respiratory, EMG and ultrasound data. Each subject in the following figures is represented by different symbols which can be seen in each figure's legend. 

### Figure 1: Variable joint angle, constant frequency (i.e. Day 1)
![Day1SummaryFigure](Figure1.jpg)  

Figure 1 plots the summary torque, respiratory, and EMG data from day 1 of testing where joint angle changed but movement frequency was constant at 2Hz.

### Figure 2: Variable frequency, constant joint angle (i.e. Day 2)
![Day2SummaryFigure](Figure2.jpg)

Figure 2 plots the summary torque, respiratory, and EMG data from day 2 of testing where movement frequency changed but joint angle was constant at 20 degrees. 

### Figure 3: Sanity Check of 2Hz at 20deg for both days
![SanityCheckSummaryFigure](Figure3.jpg)

Figure 3 performs a sanity check where each subjects' 2Hz at 20deg trials, which was completed once on each day, is compared.  

### Figure 4: NIRS analysis 

![NIRSSummaryFigure](Figure4.jpg)

Figure 4 plots a summary of the Near-Infrared Spectroscopy (NIRS) data collected on day 2. Panel one plots the NIRS data collected while panel two compares the NIRS results vs the corresponding VO2 results for each subject.

### Figure 5: MVC Sanity Check

![MVCSanityCheckFigure](Figure5.jpg)

Figure 5 performs a sanity check by comparing each subjects' day 1 and day 2 maximum voluntary contraction (MVC) torque and EMG data. If a subject's symbol is filled-in within the legend then their MVC comparison was done at 10 degrees on both days. If their symbol is not filled in then their MVC comparison was done at 20 degrees on both days.  

### Figure 6: MVC vs Angle 

![TorqueEMGvsAngleSummaryFigure](Figure6.jpg)

Figure 6 summarizes each subject's MVC torque and average EMG data compared to a change in angle. The EMG data is averaged across the three right leg muscles measured which include the Vastus Lateralis, Vastus Medialis, and Rectus Femoris. Each angles MVC EMG signal is normalized to the mean of each angles EMG signal. 

### Figure 7: MVC vs Angle 

![EMGvsAngleSummaryFigure](Figure7.jpg)

Figure 7 summarizes each subject's MVC EMG data for the three right leg muscles measured which include the Vastus Lateralis, Vastus Medialis, and Rectus Femoris. Each angles MVC EMG signal is normalized to the mean of each angles EMG signal. 
 

### Figure 8: Gravity Intervals

![GravityIntervalsSummaryFigure](Figure8.jpg)

Figure 8 summarizes the gravity torque data compared to a change in joint angle. Panel one looks at the overall data while panel two examines the difference between the fit data (theoretical/ideal data) for each subject compared to their actual measurement. 

### Figure 9: Ultrasound analysis

![DeltaUltrasoundAnalysisFigure](Figure9.jpg)

Figure 9 summarizes the change in thickness (panel one), pennation angle (panel two), and fascicle length (panel three) during torque production compared to resting measurements derived from manual ultrasound measurements. Torque production is seen as a percent of each subject's specific torque waveform target. 

### Figure 10: Ultrasound analysis 

![AbsoluteUltrasoundAnalysisFigure](Figure10.jpg)

Figure 10 summarizes an absolute measure of thickness (panel one), pennation angle (panel two), and fascicle length (panel three) during torque production derived from manual ultrasound measurements. Torque production is seen as a percent of each subject's specific torque waveform target. 

### Figure 11: TimTrack Ultrasound analysis 

![TimTrackUltrasoundAnalysisFigure](Figure11.jpg)

Figure 11 summarizes an absolute measure of thickness (panel one), pennation angle (panel two), and fascicle length (panel three) during torque production derived from the TimTrack's function ultrasound measurements. Torque production is seen as a percent of each subject's specific torque waveform target. 

### Figure 12: TimTrack and Manual Ultrasound Estimate Comparisons 

![TimTrackManualEstComparisonFigure](Figure12.jpg)

Figure 12 compares the ultrasound estimates determined by manual methods and the function TimTrack. These comparisons can be seen for absolute measures of thickness (panel one), pennation angle (panel two), and fascicle length (panel three). Torque production is seen as a percent of each subject's specific torque waveform target. 

## Further investigation
All other scripts will require raw data to be added to the path to allow it to function. To begin start with the respective analysis scripts for torque, respiratory, and EMG data. 
After analysis is complete move on to each data sets respective 'visualize' or 'process' scripts to get a fully processed data set.  