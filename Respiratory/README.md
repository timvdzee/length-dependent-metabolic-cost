# Length-dependent-metabolic-cost: Respiratory Folder

The following readme is used as a guide through the respiratory folder which includes the relevant scripts and functions to analyze and process the raw VO2 and Respiratory Exchange Ratio (RER) data. Below is a flow chart to visualize this process.

![RespiratoryFlowChart](RespiratoryFlowChart.jpg)  

## Analysis of Respiratory data
The VO2 raw data begins as an '.xlsx' format with three files per day for each subject (One exercise file and two resting files). The raw data is analyzed and processed using the script 'RespiratorySummaryScript.m' which first loads the data files names and recorded trial intervals using the function 'getRespSubjectSpecificParameters.m'. The data is then analyzed using the function 'do_respirometry.m' and 'do_rer.m' which returns both the gross and the net values for VO2 and RER respectively where net is seen as (exercise VO2 or RER) - (average resting VO2 or RER between the two resting recordings). VO2 and RER are only analyzed for the final 3 minutes of exercise to ensure steady state is reached. The analyzed VO2 data is then processed by being converted to metabolic power in joules. The processed data is looked at for both days and saved in the variables 'RespiratorySummary.mat'. The processed metabolic power data is also compared to the recorded NIRS data. All the processed respiratory data is then visualized using the script 'RespiratoryVisualization.m'. The processed metabolic power data is further summarized visually using the 'EMGTorqueRespiratorySummary.m' script. 



