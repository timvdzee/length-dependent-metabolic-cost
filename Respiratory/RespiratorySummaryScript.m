clear all; close all; clc
% This script is used to summarized the respiratory data. This is done by analyzing and processing each subjects data by cutting
% respiratory files then averaging and normalizing all subjects respiratory data for both day. 
%% Day 1 VO2 Analysis
% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan','Emily','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);% Length for looping

N_per_volt = 69.71;% Nm per volt ratio
% Waveform amplitude of torque target used
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * N_per_volt;

TestingDay = 1;
%% First Analyze S2 Day 1/Day 2 and S3 Day 2 data as formatting is weird 
%% Gross O2 consumption
% Create empty variables to fill
VO2_grossunsort = nan(7,2,3);
rer_grossunsort = nan(7,2,3);

% Order for first three subjects
exp_freqs = [0 1.5 2.5 0.0001 0.5 2 1; flip([0 1.5 2.5 0.0001 0.5 2 1]); 0.0001 1.5 1 0 2.5 0.5 2];
exp_angles = [flip([10 30 40 60 20 50]); flip([10 30 40 60 20 50]); 50 20 60 40 30 10];

tpoints(:,:,1) = [7:8:47, nan; 8:8:60];
tpoints(:,:,2) = [2:8:42, nan; 7:8:60];
tpoints(:,:,3) = [2:8:42, nan; 2:8:50];

% filenames for subjects with weird formatting
filenames = {'S1_0_20130909_1816.xlsx', 'S1_variable_frequency_20_degrees.xlsx'; 
             'S2_Jun21_variable_freq.xlsx',  'S2_Jun21_variable_angle.xlsx';
             'S3_variable_angle_June26.xlsx', 'S3_variable_frequency_June26.xlsx'};

for j = 1:2
    for p = 1:3
        data = xlsread(filenames{p,j});
        time = data(:,1); vo2 = data(:,2);
        time = data(:,1); rer = data(:,7);
        
        figure(1); 
        plot(time, vo2); hold on

        for i = 1:(length(tpoints))
            
            if isfinite(tpoints(j,i,p))
                idx = time > tpoints(j,i,p) & time < (tpoints(j,i,p)+3);

                plot(time(idx), vo2(idx), '.')
                
              
                VO2_grossunsort(i,j,p) = trapz(time(idx), vo2(idx)) / 3;
            end
        end
        figure(2); 
        plot(time, rer); hold on

        for i = 1:(length(tpoints))
            
            if isfinite(tpoints(j,i,p))
                idx = time > tpoints(j,i,p) & time < (tpoints(j,i,p)+3);

                plot(time(idx), rer(idx), '.')
                
              
                rer_grossunsort(i,j,p) = trapz(time(idx), rer(idx)) / 3;
            end
        end
    end
end

%% plot gross
figure(1);
plot(flip([10 30 40 60 20 50]), VO2_grossunsort(1:6,1,1),'o'); hold on
plot(flip([10 30 40 60 20 50]), VO2_grossunsort(1:6,1,2),'o')
plot(flip([10 30 40 60 20 50]), VO2_grossunsort(1:6,1,3),'o')

figure(2);
plot(flip([10 30 40 60 20 50]), rer_grossunsort(1:6,1,1),'o'); hold on
plot(flip([10 30 40 60 20 50]), rer_grossunsort(1:6,1,2),'o')
plot(flip([10 30 40 60 20 50]), rer_grossunsort(1:6,1,3),'o')

%% Determine resting
% load resting files
filenames_rest = {'S1_0_20130909_1816.xlsx', 'S1_variable_frequency_20_degrees.xlsx'; 
             'S2_Jun21_rest.xlsx',  'S2_Jun21_variable_angle.xlsx';
             'S3_rest_June26.xlsx', 'S3_variable_frequency_June26.xlsx'};
tpoints_rest = [2 3; 2 2; 2 59];
for p = 1:3
    for j = 1:2
        data = xlsread(filenames_rest{p,j});
        time = data(:,1); vo2 = data(:,2);
        idx = time > tpoints_rest(p,j) & time < (tpoints_rest(p,j)+3);
        VO2_rest(p,j) = trapz(time(idx), vo2(idx)) / 3;
        
        time = data(:,1); rer = data(:,7);
        idx = time > tpoints_rest(p,j) & time < (tpoints_rest(p,j)+3);
        rer_rest(p,j) = trapz(time(idx), rer(idx)) / 3;
    end
end

% take the lowest
VO2_minrest = min(VO2_rest,[],2);
rer_minrest = min(rer_rest,[],2);
%% Net O2 consumption
VO2_netunsort = nan(7,2,3);
VO2_sort = nan(size(VO2_netunsort));

rer_netunsort = nan(7,2,3);
rer_sort = nan(size(rer_netunsort));
figure('name','Calcium cost: length/angle- and rate/frequency-dependence'); color = get(gca,'colororder');

for p = 1:3
    VO2_netunsort(:,:,p) = VO2_grossunsort(:,:,p) - VO2_minrest(p);
    
    for j = 1:2
        if j == 1, [~,i] =  sort(exp_angles(p,:));
        elseif j == 2, [~,i] =  sort(exp_freqs(p,:));
        end
        
        VO2_sort(1:length(i),j,p) = VO2_netunsort(i,j,p);
    end
     for j = 1:2
        if j == 1, [~,i] =  sort(exp_angles(p,:));
        elseif j == 2, [~,i] =  sort(exp_freqs(p,:));
        end
        
        VO2_grossort(1:length(i),j,p) = VO2_grossunsort(i,j,p);
    end
    subplot(321);
    plot(10:10:60, VO2_sort(1:end-1,1,p),'.','color',color(p,:),'markersize',10); hold on
    
    subplot(322);
    plot([0, 0:.5:2.5], VO2_sort(:,2,p),'.','color',color(p,:),'markersize',10); hold on

end
for p = 1:3
    rer_netunsort(:,:,p) = rer_grossunsort(:,:,p) - rer_minrest(p);
    
    for j = 1:2
        if j == 1, [~,i] =  sort(exp_angles(p,:));
        elseif j == 2, [~,i] =  sort(exp_freqs(p,:));
        end
        
        rer_sort(1:length(i),j,p) = rer_netunsort(i,j,p);
    end
     for j = 1:2
        if j == 1, [~,i] =  sort(exp_angles(p,:));
        elseif j == 2, [~,i] =  sort(exp_freqs(p,:));
        end
        
        rer_grossort(1:length(i),j,p) = rer_grossunsort(i,j,p);
    end
    subplot(321);
    plot(10:10:60, rer_sort(1:end-1,1,p),'.','color',color(p,:),'markersize',10); hold on
    
    subplot(322);
    plot([0, 0:.5:2.5], rer_sort(:,2,p),'.','color',color(p,:),'markersize',10); hold on

end
    %% Loop through all other Subject Analysis for Day 1
    % Create empty variables to fill
    Day1VO2Gross = nan(N,7);
    Day1VO2 = nan(N,7);
    Day1RERGross = nan(N,7);
    Day1RER = nan(N,7);

    % Loop through each subject
   for i = 1:N
    if i == 2 % Add in previously analyzed subject 2 day 1 data 
        Day1VO2Gross(i,:) = VO2_grossort(:,1,i)';
        Day1VO2(i,:) = VO2_sort(:,1,i)';
        Day1RERGross(i,:) = rer_grossort(:,1,i)';
        Day1RER(i,:) = rer_sort(:,1,i)';
    else 
        
    subName = subnames{i};  % Get subject name    
    sOut = getRespSubjectSpecificParameters(subName, TestingDay); % Use function to get subject parameters
    [VO2_gross, VO2_net] = do_respirometry(sOut.filename,sOut.filename_rest,sOut.filename_rest_end, sOut.tps, sOut.angles);   
    [~,j] = sort(sOut.angles);% Sort according to their angle order
    if i == 1 % Subject 1 doesn't have restless trial
    Day1VO2Gross(i,1:6) = VO2_gross(j);
    Day1VO2(i,1:6) = VO2_net(j);
    else % All others have restless trial 
    Day1VO2Gross(i,:) = VO2_gross(j);
    Day1VO2(i,:) = VO2_net(j);
    end
    
    % Perform RER analysis
    [rer_gross, rer_net] = do_rer(sOut.filename,sOut.filename_rest,sOut.filename_rest_end, sOut.tps, sOut.angles);   
    [~,l] = sort(sOut.angles); % Sort according to angle order
    if i ==1 % Subject 1 doesn't have restless trial
    Day1RERGross(i,1:6) = rer_gross(l);
    Day1RER(i,1:6) = rer_net(l);
    else % All others have restless trial 
    Day1RERGross(i,:) = rer_gross(l);
    Day1RER(i,:) = rer_net(l);
    end
    % make nice
    axis([0 70 0 max(VO2_net)])
    xlabel('Angle (deg)')
    ylabel('Average net O2 rate (L/min)')
    title('Energy cost vs. joint angle',sprintf('%s',subName))
    end
    end 

%% Loop through Subject Analysis for Day 2
% Create empty variables to fill
 Day2VO2Gross = nan(N,7);
 Day2VO2 = nan(N,7);
 Day2RERGross = nan(N,7);
Day2RER = nan(N,7);

TestingDay = 2;
   for i = 1:N
    if i == 2% Add in previously analyzed subject 2 day 2 data 
        Day2VO2Gross(i,:) = VO2_grossort(:,2,i)';
        Day2VO2(i,:) = VO2_sort(:,2,i)';
        Day2RERGross(i,:) = rer_grossort(:,2,i)';
        Day2RER(i,:) = rer_sort(:,2,i)';
    elseif i == 3% Add in previously analyzed subject 3 day 2 data 
        Day2VO2Gross(i,:) = VO2_grossort(:,2,i)';
        Day2VO2(i,:) = VO2_sort(:,2,i)';
        Day2RERGross(i,:) = rer_grossort(:,2,i)';
        Day2RER(i,:) = rer_sort(:,2,i)';
    else 
        
    subName = subnames{i}; % Get subject name         
    sOut = getRespSubjectSpecificParameters(subName, TestingDay); % Use function to get subject parameters
    [VO2_gross, VO2_net] = do_respirometry(sOut.filename,sOut.filename_rest,sOut.filename_rest_end, sOut.tps, sOut.frequencies);   
    sOut.frequencies(sOut.frequencies == 0) = 0.001;% Sort 0-20deg condition 
    sOut.frequencies(sOut.frequencies == 60) = 0;% Sort 0-60deg condition
    [~,j] = sort(sOut.frequencies); % Sort by frequency order
    Day2VO2Gross(i,:) = VO2_gross(j);
    Day2VO2(i,:) = VO2_net(j);
    
    [rer_gross, rer_net] = do_rer(sOut.filename,sOut.filename_rest,sOut.filename_rest_end, sOut.tps, sOut.frequencies);   
    sOut.frequencies(sOut.frequencies == 0) = 0.001;% Sort 0-20deg condition 
    sOut.frequencies(sOut.frequencies == 60) = 0;% Sort 0-60deg condition
    [~,l] = sort(sOut.frequencies);% Sort by frequency order
    Day2RERGross(i,:) = rer_gross(l);
    Day2RER(i,:) = rer_net(l);
    % make nice
    axis([0 3 0 .5])
    xlabel('Frequency (Hz)')
    ylabel('Average net O2 rate (L/min)')
    title('Energy cost vs. Frequency',sprintf('%s',subName))

    end
   end

%% Process Respiratory data

%Get Conditions
Day1angles = [10:10:60, nan];
Day2frequencies  = [0, 0.5, 1, 1.5, 2, 2.5, 60];
Day2frequencies(Day2frequencies == 0) = 0.001;
Day2frequencies(Day2frequencies == 60) = 0;

% Create empty variables to fill
Day1MetPower = nan(N,7);
Day2MetPower = nan(N,7);
NIRS = nan(N,7);

J_per_O2 = 20000; % Joules per O2 conversion factor

for i = 1:N % Loop through each subject
    % Convert and normalize by waveform amplitude
        Day1MetPower(i,:) = Day1VO2(i,:)  / WaveformAmp(i) * J_per_O2 / 60;
        Day2MetPower(i,:) = Day2VO2(i,:) / WaveformAmp(i) * J_per_O2 / 60;
    if exist([subnames{i},'NIRS.mat'])% Load NIRS data if subject has it
        load([subnames{i},'NIRS.mat'])
        
        NIRS(i,2) = -p(1) / WaveformAmp(i);% Normalize by waveform amplitude
        NIRS(i,3:end) = -p(2:end) / WaveformAmp(i);
    end
end

% Save data
save('RespiratorySummary','NIRS','Day1MetPower','Day2MetPower','Day1RER','Day2RER','Day1VO2','Day2VO2','subnames','Day1angles','Day2frequencies','WaveformAmp');


%% RER Function
   % This function is used to determine the respiratory exchange ratio for
   % subjects. Inputs are filenames for exercise and rest, time points, and
   % order of trials. RER both net and gross are retured from these
function[rer_gross, rer_net] = do_rer(filename,filename_rest, filename_rest_end, tps, indep_var)

%% load the data
% first rest measurement
rest_data = xlsread(filename_rest);
rest_time = rest_data(:,1); rest_rer = rest_data(:,7);

% exercise measurement
exercise_data = xlsread(filename);
exercise_time = exercise_data(:,1) + max(rest_time); exercise_rer = exercise_data(:,7);

% second rest measurement
rest_data_end = xlsread(filename_rest_end);
rest_time_end = rest_data_end(:,1)+max(exercise_time); restrer_end = rest_data_end(:,7);

figure(1); color = get(gca,'colororder'); subplot(121);

% plot the raw data
plot([rest_time; exercise_time; rest_time_end], [rest_rer; exercise_rer; restrer_end]); hold on

% plot time intervals
for i = 1:length(tps)
    plot([tps(i) tps(i)], [0 1.5], 'k--')
end

% make nice
xlabel('Time (min)');
ylabel('Respiratory Exchange Ratio');
axis([0 max(rest_time_end) 0 1.5])
title('Respirometry time series')

%% determine average resting rate
% first rest measurement
idx = isfinite(rest_time);
rer_rest = trapz(rest_time(idx), rest_rer(idx)) / (max(rest_time(idx)) - min(rest_time(idx)));

% second rest measurement
idx = isfinite(rest_time_end);
rer_rest_end = trapz(rest_time_end(idx), restrer_end(idx)) / (max(rest_time_end(idx)) - min(rest_time_end(idx)));

% plot the resting rate
hold on
plot([0 max(rest_time(idx))], [rer_rest rer_rest], '-','color',color(2,:), 'linewidth',2)
plot([min(rest_time_end(idx)) max(rest_time_end(idx))], [rer_rest_end rer_rest_end], '-','color',color(2,:), 'linewidth',2)

% fit the resting rate (linear)
p = polyfit([mean(rest_time(idx)) mean(rest_time_end(idx))], [rer_rest rer_rest_end], 1);
plot([0 max(rest_time_end)], polyval(p, [0 max(rest_time_end)]), '--','color',color(2,:))

%% resample the data & compute moving average
% new time vector
ti = linspace(0, max(exercise_time), 200);

% new vo2 vector
idx = isfinite(exercise_time);
reri = interp1(exercise_time(idx), exercise_rer(idx), ti);

% plot resample data
plot(ti, reri, 'k')

% moving average
rerma = movmean(reri, 5);

% plot moving average data
plot(ti, rerma, 'color',color(3,:), 'linewidth',2)

%% plot average exercise rates (gross and net)
rer_gross = nan(1, (length(tps)-1));
rer_net = nan(1, (length(tps)-1));

for i = 1:(length(tps)-1)
    
    idx = exercise_time > (tps(i)+2) & exercise_time < (tps(i)+5);
    
    % gross O2 rate
    rer_gross(i) = trapz(exercise_time(idx), exercise_rer(idx)) / (max(exercise_time(idx)) - min(exercise_time(idx)));
    plot([(tps(i)+2) (tps(i)+5)], [rer_gross(i) rer_gross(i)], '-', 'color', color(4,:), 'linewidth', 3)
        
    % net O2 rate
    rer_net(i) = rer_gross(i) - polyval(p, mean(exercise_time(idx))); 
   
end

%% summary: plot average rates vs. knee angle
subplot(122);
plot(indep_var, rer_net, 'o', 'markerfacecolor', color(1,:)); hold on

% fit quadratic
idx = isfinite(indep_var);
p2 = polyfit(indep_var(idx), rer_net(idx), 2);
plot(0:70, polyval(p2, 0:70), 'linewidth', 2);
end