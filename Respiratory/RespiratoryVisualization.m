clear all; close all; clc
% this script is used visualize the processed data by graphing the summary respiratory data
% for each day in addition to the NIRS data taken on day 2.
% As well the relationship between each subject's NIRS and respiratory data
% is plotted 
%% Load data and Variables
addpath(genpath(cd)); % Load path

% Subject names in order according to protocol
subnames = {'S1','Koen','Billy','Rudi','Jordan','Emily','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);% Length for looping
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>']; % Individual symbols for plotting

N_per_volt = 69.71; % Nm per volt ratio
% Waveform amplitude of torque target used
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * N_per_volt;

% color = colors(round(linspace(1,length(colors), N)), :);

load('RespiratorySummary.mat');% Load respiratory data
%% plot summary figures
close all
figure;
color = [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:size(Day1MetPower,1)
    subplot(131); plot(sort(Day1angles), Day1MetPower(i,:),symbols(i),'color',color(i,:),'markersize',8); hold on
    subplot(132); plot(sort(Day2frequencies), Day2MetPower(i,:), symbols(i),'color',color(i,:),'markersize',8); hold on
    subplot(133); plot(sort(Day2frequencies), NIRS(i,:), symbols(i),'color',color(i,:),'markersize',8); hold on
end

subplot(131); errorbar(sort(Day1angles), mean(Day1MetPower,'omitnan'),std(Day1MetPower,1,'omitnan'),'o','color',[.5 .5 .5],'linewidth',1.5,'markerfacecolor',[.5 .5 .5])
subplot(132); errorbar(sort(Day2frequencies), mean(Day2MetPower,'omitnan'),std(Day2MetPower,1,'omitnan'),'o','color',[.5 .5 .5],'linewidth',1.5,'markerfacecolor',[.5 .5 .5])
subplot(133); errorbar(sort(Day2frequencies), mean(NIRS,'omitnan'),std(NIRS,1,'omitnan'),'o','color',[.5 .5 .5],'linewidth',1.5,'markerfacecolor',[.5 .5 .5])

legend('1','Koen','Billy','Rudi','Jordan','Emily','Brian','Austin','Nick','Elizabeth','Gaby','Mean','location','northwest')

% make nice
subplot(131); box off; grid on
xlabel('Angle (deg)')
ylabel('Net metabolic rate(W/N-m)');
title('Constant frequency, variable angle')

subplot(132); box off; grid on
xlabel('Frequency (Hz)')
ylabel('Net metabolic rate (W/N-m)');
title('Constant angle, variable frequency')

subplot(133); box off; grid on
xlabel('Frequency (Hz)')
ylabel('TSI-rate (% s^{-1}/N-m)')
title('Constant angle, variable frequency')

% fit data and plot it 
pa = polyfit(10:10:60, mean(Day1MetPower(:,1:6),'omitnan'),2);
subplot(131); plot(0:60, polyval(pa, 0:60), 'k--','linewidth',2);

idx = isfinite(mean(NIRS,'omitnan'));

sfreq = sort(Day2frequencies);
pf = polyfit(sfreq(idx), mean(Day2MetPower(:,idx),'omitnan'),2);
subplot(132); plot(0:.1:3, polyval(pf, 0:.1:3), 'k--','linewidth',2)

pN = polyfit(0:.5:2.5, mean(NIRS(:,idx),'omitnan'),2);
subplot(133); plot(0:.1:3, polyval(pN, 0:.1:3), 'k--','linewidth',2)

%% NIRS vs. respirometry

figure
plot(Day2MetPower(1:N,:)', NIRS(1:N,:)','.','markersize',15)

xlabel('Net O2 rate (L/min/N-m)')
ylabel('Desaturation rate (% s^{-1}/N-m)')

%% statistics respirometry
X = repmat(sfreq(idx),11,1);
Y = Day2MetPower(:,idx);
S = repmat((1:size(X,1))',1,size(X,2));
M = [(Y(1:numel(Y)))' (X(1:numel(X)))' (S(1:numel(S)))'];
varnames = {'Cost' 'Freq' 'Subject'};
modelString = 'Cost ~ Freq^2 + (1|Subject)';
T = array2table(M);
T.Properties.VariableNames = varnames;

LME = fitlme(T,modelString);

%% statistics NIRS
X = repmat(sfreq(idx),11,1);
Y = NIRS(:,idx);
S = repmat((1:size(X,1))',1,size(X,2));
M = [(Y(1:numel(Y)))' (X(1:numel(X)))' (S(1:numel(S)))'];
varnames = {'Cost' 'Freq' 'Subject'};
modelString = 'Cost ~ Freq^2 + (1|Subject)';
T = array2table(M);
T.Properties.VariableNames = varnames;

LME = fitlme(T,modelString);

%% work estimate from previous
load('tendonfit.mat');% Load previous work estimate data

xtor = 0:50; % Torque variable
xFSEE = xtor(:)  ./ (r/100);% Series elastic element force
fit_ten = cten_exp(1)*exp(cten_exp(2).*xFSEE) - cten_exp(1);% Tendon force
Wfit_ten = cumtrapz(fit_ten/100, xFSEE);% Work done by tendon

% Plot outcome
figure;
plot(fit_ten, xFSEE,'linewidth',2); hold on
axis([0 3 0 300])

for i= 1:10
    dL(i) = fit_ten(xtor == round(WaveformAmp(i)));
    W(i)  = Wfit_ten(xtor == round(WaveformAmp(i)));
end

Wcost = mean(W) .* sfreq * 4 / mean(WaveformAmp);% Work cost

figure(1)
subplot(132);
plot(sfreq, mean(Day2MetPower(:,2)) + Wcost,'r--')

%% Process and plot RER data 
% Normalize RER
normalize = false

% plot
figure();
color = [get(gca,'colororder'); get(gca,'colororder')];
for i = 1:size(Day1RER,1)
    subplot(121); plot(sort(Day1angles), Day1RER(i,:),symbols(i),'color',color(i,:),'markersize',8,'linestyle','-'); hold on
    subplot(122); plot(sort(Day2frequencies), Day2RER(i,:), symbols(i),'color',color(i,:),'markersize',8,'linestyle','-'); hold on
end

subplot(121); errorbar(sort(Day1angles), mean(Day1RER,'omitnan'),std(Day1RER,1,'omitnan'),'o','color',[.5 .5 .5],'linewidth',1.5,'markerfacecolor',[.5 .5 .5])
subplot(122); errorbar(sort(Day2frequencies), mean(Day2RER,'omitnan'),std(Day2RER,1,'omitnan'),'o','color',[.5 .5 .5],'linewidth',1.5,'markerfacecolor',[.5 .5 .5])

legend('S1','Koen','Billy','Rudi','Jordan','Emily','Brian','Austin','Nick','Elizabeth','Gaby','Mean','location','northwest')

% make nice
subplot(121); box off; grid on
xlabel('Angle (deg)')
if normalize 
ylabel('Normalized Respiratory Exchange Ratio (1/N-m)');
else
ylabel('Respiratory Exchange Ratio');  
end
title('Constant frequency, variable angle')

subplot(122); box off; grid on
xlabel('Frequency (Hz)')
if normalize 
ylabel('Normalized Respiratory Exchange Ratio (1/N-m)');
else
ylabel('Respiratory Exchange Ratio'); 
end
title('Constant angle, variable frequency')

% fit
pa = polyfit(10:10:60, mean(Day1RER(:,1:6),'omitnan'),1);
subplot(121); plot(0:60, polyval(pa, 0:60), 'k--','linewidth',2);

sfreq = sort(Day2frequencies);
pf = polyfit(sfreq, mean(Day2RER(:,:),'omitnan'),1);
subplot(122); plot(0:.1:3, polyval(pf, 0:.1:3), 'k--','linewidth',2)
