 % This function is used to determine the VO2 for
   % subjects. Inputs are filenames for exercise and rest, time points, and
   % order of trials (indep_var). VO2 both net and gross are retured from these
function[VO2_gross, VO2_net] = do_respirometry(filename,filename_rest, filename_rest_end, tps, indep_var)

%% load the data
% first rest measurement
rest_data = readmatrix(filename_rest);
rest_time = rest_data(:,1); rest_vo2 = rest_data(:,2);

% exercise measurement
exercise_data = readmatrix(filename);
exercise_time = exercise_data(:,1) + max(rest_time); exercise_vo2 = exercise_data(:,2);

% second rest measurement
rest_data_end = readmatrix(filename_rest_end);
rest_time_end = rest_data_end(:,1)+max(exercise_time); restvo2_end = rest_data_end(:,2);

figure(1); color = get(gca,'colororder'); subplot(121);

% plot the raw data
plot([rest_time; exercise_time; rest_time_end], [rest_vo2; exercise_vo2; restvo2_end]); hold on

% plot time intervals
for i = 1:length(tps)
    plot([tps(i) tps(i)], [0 .7], 'k--')
end

% make nice
xlabel('Time (min)');
ylabel('O2 rate (L/min)');
axis([0 max(rest_time_end) 0 .7])
title('Respirometry time series')

%% determine average resting rate
% first rest measurement
idx = isfinite(rest_time);
VO2_rest = trapz(rest_time(idx), rest_vo2(idx)) / (max(rest_time(idx)) - min(rest_time(idx)));

% second rest measurement
idx = isfinite(rest_time_end);
VO2_rest_end = trapz(rest_time_end(idx), restvo2_end(idx)) / (max(rest_time_end(idx)) - min(rest_time_end(idx)));

% plot the resting rate
hold on
plot([0 max(rest_time(idx))], [VO2_rest VO2_rest], '-','color',color(2,:), 'linewidth',2)
plot([min(rest_time_end(idx)) max(rest_time_end(idx))], [VO2_rest_end VO2_rest_end], '-','color',color(2,:), 'linewidth',2)

% fit the resting rate (linear)
p = polyfit([mean(rest_time(idx)) mean(rest_time_end(idx))], [VO2_rest VO2_rest_end], 1);
plot([0 max(rest_time_end)], polyval(p, [0 max(rest_time_end)]), '--','color',color(2,:))

%% resample the data & compute moving average
% new time vector
ti = linspace(0, max(exercise_time), 200);

% new vo2 vector
idx = isfinite(exercise_time);
vo2i = interp1(exercise_time(idx), exercise_vo2(idx), ti);

% plot resample data
plot(ti, vo2i, 'k')

% moving average
vo2ma = movmean(vo2i, 5);

% plot moving average data
plot(ti, vo2ma, 'color',color(3,:), 'linewidth',2)

%% plot average exercise rates (gross and net)
VO2_gross = nan(1, (length(tps)-1));
VO2_net = nan(1, (length(tps)-1));

for i = 1:(length(tps)-1)
    
    idx = exercise_time > (tps(i)+2) & exercise_time < (tps(i)+5);
    
    % gross O2 rate
    VO2_gross(i) = trapz(exercise_time(idx), exercise_vo2(idx)) / (max(exercise_time(idx)) - min(exercise_time(idx)));
    plot([(tps(i)+2) (tps(i)+5)], [VO2_gross(i) VO2_gross(i)], '-', 'color', color(4,:), 'linewidth', 3)
        
    % net O2 rate
    VO2_net(i) = VO2_gross(i) - polyval(p, mean(exercise_time(idx))); 
   
end

%% summary: plot average rates vs. knee angle
subplot(122);
plot(indep_var, VO2_net, 'o', 'markerfacecolor', color(1,:)); hold on

% fit quadratic
idx = isfinite(indep_var);
p2 = polyfit(indep_var(idx), VO2_net(idx), 2);
plot(0:70, polyval(p2, 0:70), 'linewidth', 2)
