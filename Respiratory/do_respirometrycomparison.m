% This function is used to compare respiratory data from each testing day
% for each subject. All that is needed is an input of subject number of interest,
% testing day, and which subject in loop it is (i). Output is visualization
% comparison of each testing day with the VO2 data (net and gross) and testing condition order. 
function[VO2_gross, VO2_net,order] = do_respirometrycomparison(subName, TestingDay,i)
sOut = getRespSubjectSpecificParameters(subName, TestingDay);
if TestingDay == 1, order = sOut.angles;
elseif TestingDay == 2, order = sOut.frequencies; 
end
%% load the data
% first rest measurement
rest_data = readmatrix(sOut.filename_rest);
rest_time = rest_data(:,1); rest_vo2 = rest_data(:,2);

% exercise measurement
exercise_data = readmatrix(sOut.filename);
exercise_time = exercise_data(:,1) + max(rest_time); exercise_vo2 = exercise_data(:,2);

% second rest measurement
rest_data_end = readmatrix(sOut.filename_rest_end);
rest_time_end = rest_data_end(:,1)+max(exercise_time); restvo2_end = rest_data_end(:,2);

o = i+45;
figure(o); color = get(gca,'colororder'); 
if TestingDay == 1, subplot(121);
elseif TestingDay == 2, subplot(122); 
end

% plot the raw data
plot([rest_time; exercise_time; rest_time_end], [rest_vo2; exercise_vo2; restvo2_end]); hold on

% plot time intervals
for k = 1:length(sOut.tps)
    plot([sOut.tps(k) sOut.tps(k)], [0 0.85], 'k--')
end

% make nice
xlabel('Time (min)');
ylabel('O2 rate (L/min)');
axis([0 max(rest_time_end) 0 0.85])
Z = subName;
title(sprintf('Respirometry time series for Day %d',TestingDay), sprintf('%s',Z))

%% determine average resting rate
% first rest measurement
idx = isfinite(rest_time);
VO2_rest = trapz(rest_time(idx), rest_vo2(idx)) / (max(rest_time(idx)) - min(rest_time(idx)));

% second rest measurement
idx = isfinite(rest_time_end);
VO2_rest_end = trapz(rest_time_end(idx), restvo2_end(idx)) / (max(rest_time_end(idx)) - min(rest_time_end(idx)));

% plot the resting rate
hold on
plot([0 max(rest_time(idx))], [VO2_rest VO2_rest], '-','color',color(2,:), 'linewidth',2)
plot([min(rest_time_end(idx)) max(rest_time_end(idx))], [VO2_rest_end VO2_rest_end], '-','color',color(2,:), 'linewidth',2)

% fit the resting rate (linear)
p = polyfit([mean(rest_time(idx)) mean(rest_time_end(idx))], [VO2_rest VO2_rest_end], 1);
plot([0 max(rest_time_end)], polyval(p, [0 max(rest_time_end)]), '--','color',color(2,:))

%% resample the data & compute moving average
% new time vector
ti = linspace(0, max(exercise_time), 200);

% new vo2 vector
idx = isfinite(exercise_time);
vo2i = interp1(exercise_time(idx), exercise_vo2(idx), ti);

% plot resample data
plot(ti, vo2i, 'k')

% moving average
vo2ma = movmean(vo2i, 5);

% plot moving average data
plot(ti, vo2ma, 'color',color(3,:), 'linewidth',2)

%% plot average exercise rates (gross and net)
VO2_gross = nan(1, (length(sOut.tps)-1));
VO2_net = nan(1, (length(sOut.tps)-1));

for k = 1:(length(sOut.tps)-1)
    
    idx = exercise_time > (sOut.tps(k)+2) & exercise_time < (sOut.tps(k)+5);
    
    % gross O2 rate
    VO2_gross(k) = trapz(exercise_time(idx), exercise_vo2(idx)) / (max(exercise_time(idx)) - min(exercise_time(idx)));
    plot([(sOut.tps(k)+2) (sOut.tps(k)+5)], [VO2_gross(k) VO2_gross(k)], '-', 'color', color(4,:), 'linewidth', 3)
        
    % net O2 rate
    VO2_net(k) = VO2_gross(k) - polyval(p, mean(exercise_time(idx))); 
   
end