% This function is used to pull out each subject's individual testing
% parameters such as filenames, time points, and angle/frequency trial
% order. Input is the subject number of interest and which testing day
function sOut = getRespSubjectSpecificParameters(subName, TestingDay)

sOut = struct;
switch subName
    case {'Austin', '8'}
        if TestingDay == 1
            sOut.filename = 'S8_20210810_DiffAngles.xlsx';
            sOut.filename_rest = 'S8_20210810_Rest1.xlsx';
            sOut.filename_rest_end = 'S8_20210810_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [nan 20 50 60 40 30 10];
        elseif TestingDay == 2
            sOut.filename = 'S8_20210827_DiffFreq.xlsx';
            sOut.filename_rest = 'S8_20210827_Rest1.xlsx';
            sOut.filename_rest_end = 'S8_20210827_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [2 0.5 2.5 0 1.5 60 1];
        end
    case {'Billy', '3'}
        if TestingDay == 1
            sOut.filename = 'S3_20210805_DiffAngles.xlsx';
            sOut.filename_rest = 'S3_20210805_Rest1.xlsx';
            sOut.filename_rest_end = 'S3_20210805_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [40 10 50 60 20 30 nan];
        elseif TestingDay == 2
        end
        case {'Brian', '7'}
        if TestingDay == 1
            sOut.filename = 'S7_20210728_DiffAngles.xlsx';
            sOut.filename_rest = 'S7_20210728_Rest1.xlsx';
            sOut.filename_rest_end = 'S7_20210728_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [40 50 20 nan 10 60 30];
        elseif TestingDay == 2
            sOut.filename = 'S7_20210729_DiffFreq.xlsx';
            sOut.filename_rest = 'S7_20210729_Rest1.xlsx';
            sOut.filename_rest_end = 'S7_20210729_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [2 2.5 0.5 60 0 1 1.5];
        end
        case {'Eliz', '10'}
        if TestingDay == 1
            sOut.filename = 'S10_20210812_DiffAngles.xlsx';
            sOut.filename_rest = 'S10_20210812_Rest1.xlsx';
            sOut.filename_rest_end = 'S10_20210812_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [nan 30 10 50 40 20 60];
        elseif TestingDay == 2
            sOut.filename = 'S10_20210818_DiffFreq.xlsx';
            sOut.filename_rest = 'S10_20210818_Rest1.xlsx';
            sOut.filename_rest_end = 'S10_20210818_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [60 0 0.5 2.5 1.5 1 2];
        end
        case {'Emily', '6'}
        if TestingDay == 1
            sOut.filename = 'S6_20210713_DifferentAngles.xlsx';
            sOut.filename_rest = 'S6_20210713_Rest1.xlsx';
            sOut.filename_rest_end = 'S6_20210713_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [60 nan 10 20 40 30 50];
        elseif TestingDay == 2
            sOut.filename = 'S6_20220330_DiffFreq.xlsx';
            sOut.filename_rest = 'S6_20220330_REST1.xlsx';
            sOut.filename_rest_end = 'S6_20220330_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [1.5 0 2.5 1 2 60 0.5];
        end
        case {'Gaby', '11'}
        if TestingDay == 1
            sOut.filename = 'S11_20210819_DiffAngles.xlsx';
            sOut.filename_rest = 'S11_20210819_Rest1.xlsx';
            sOut.filename_rest_end = 'S11_20210819_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [50 30 40 20 nan 60 10];
        elseif TestingDay == 2
            sOut.filename = 'S11_20210826_DiffFreq.xlsx';
            sOut.filename_rest = 'S11_20210826_Rest1.xlsx';
            sOut.filename_rest_end = 'S11_20210826_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [0 2 1 2.5 60 1.5 0.5];
        end
        case {'Jordan', '5'}
        if TestingDay == 1
            sOut.filename = 'S5_20210709_DifferentAngles.xlsx';
            sOut.filename_rest = 'S5_20210709_Rest1.xlsx';
            sOut.filename_rest_end = 'S5_20210709_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 23:8:63]]; % time intervals
            sOut.angles = [20 50 10 30 nan 60 40];
        elseif TestingDay == 2
            sOut.filename = 'S5_20210831_DiffFreq.xlsx';
            sOut.filename_rest = 'S5_20210831_Rest1.xlsx';
            sOut.filename_rest_end = 'S5_20210831_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [2.5 60 1.5 0.5 0 2 1];
        end
        case {'Nick', '9'}
        if TestingDay == 1
            sOut.filename = 'S9_20210811_DiffAngles.xlsx';
            sOut.filename_rest = 'S9_20210811_Rest1.xlsx';
            sOut.filename_rest_end = 'S9_20210811_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16, 24, 32, 40, 48, 56]]; % time intervals
            sOut.angles = [10 50 nan 20 60 30 40];
        elseif TestingDay == 2
            sOut.filename = 'S9_20210812_DifferentFrequencies.xlsx';
            sOut.filename_rest = 'S9_20210812_Rest1.xlsx';
            sOut.filename_rest_end = 'S9_20210812_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [1 2.5 0 2 60 0.5 1.5];
        end
        case {'Rudi', '4'}
        if TestingDay == 1
            sOut.filename = 'S4_0_20210706_Differentangles.xlsx';
            sOut.filename_rest = 'S4_0_20210706_Rest.xlsx';
            sOut.filename_rest_end = 'S4_0_20210706_Rest.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.angles = [30 60 nan 10 50 40 20];
        elseif TestingDay == 2
            sOut.filename = 'S4_20210722_DifferentFreq.xlsx';
            sOut.filename_rest = 'S4_20210722_Rest1.xlsx';
            sOut.filename_rest_end = 'S4_20210722_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [0.5 2 60 2.5 1 0 1.5];
        end
        case {'S1','1'}
        if TestingDay == 1
            sOut.filename = 'S1_20220309_DiffAngles.xlsx';
            sOut.filename_rest = 'S1_20220309_Rest1.xlsx';
            sOut.filename_rest_end = 'S1_20220309_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:48]]; % time intervals
            sOut.angles = [10 40 30 60 20 50];
        elseif TestingDay == 2
            sOut.filename = 'S1_20220311_DiffFreq.xlsx';
            sOut.filename_rest = 'S1_20220311_Rest1.xlsx';
            sOut.filename_rest_end = 'S1_20220311_Rest2.xlsx';
            sOut.tps = [5, 5+ [8, 16:8:56]]; % time intervals
            sOut.frequencies = [60 1.5 2.5 0 0.5 2 1];
        end
end
