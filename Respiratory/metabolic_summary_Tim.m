clear all; close all; clc
N_per_volt = 69.71;
corfacs = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163,0.430] * N_per_volt;

load('RespiratorySummary.mat');%Respiratory data for both days
    
Pmeta = VO2a .* corfacs(:);
Pmetf = VO2f .* corfacs(:);

angles = [10:10:60, nan];frequencies  = [0, 0.5, 1, 1.5, 2, 2.5, 60];
frequencies(frequencies == 0) = 0.001;
frequencies(frequencies == 60) = 0;

% From ultrasound
Work_per_contraction = 4; % [J]
Wcoef = 3.3333333;

%% Cost vs. angle
Wcost = Work_per_contraction * Wcoef * 2;

subplot(121);
errorbar(angles, mean(Pmeta), std(Pmeta),'o'); hold on
plot(angles, Wcost * ones(size(angles)))
ylim([0 120]); xlabel('Frequency (Hz)'); ylabel('Metabolic rate (W)')
title('Cost vs. angle')

%% Cost vs. frequency
Wcost = Work_per_contraction * Wcoef * frequencies;

subplot(122);
errorbar(sort(frequencies), mean(Pmetf), std(Pmetf),'o'); hold on
plot(frequencies, Wcost)
ylim([0 120]); xlabel('Frequency (Hz)'); ylabel('Metabolic rate (W)')
title('Cost vs. frequency')
