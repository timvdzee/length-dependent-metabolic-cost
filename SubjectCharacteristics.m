clear; close all; clc
% This script stores each subject characteristics and saves them
%% Load subject data
weightsubj = [nan,73,80,77.8,79.5,52.5,73.5,77,87,57.5,56];%Subject weight in kgs
heightsubj = [177,190.5,180.5,182,173,155,178,190.2,194.5,169,168.5];%Subject height in cm
leglengthsubj = [nan,99.5,96.52,100,101.6,83.8,96.5,99,107.5,90.5,92];%Subject leg length in cm
agesubj = [27,33,22,26,22,21,30,23,23,26,34]; % Subject age

save('SubjectCharacteristics','weightsubj','heightsubj','leglengthsubj','agesubj');

