clear all; close all; clc
%% Day 1 MVC EMG Analysis Parameters
% subject names (according to protocol)
subnames = {'S1','S2','S3','S4','S5','S6','S7','S8','S9','S10','S11'};

% Nm per volt
N_per_volt = 69.71; 

% Create not a number filled variable for each MVC EMG average 
N = length(subnames);
load('AllMVCTorqueData','AllMVCTorqueAvg');
FDcut = nan(11,2);%Force development 
FRcut = nan(11,2);%Force Relaxation
Gcut = nan(11,2);%Gravity cut intervals

FDTimeMed= nan(11,10215);
FDTorqueNmMed = nan(11,10215);
FDTorqueNormMed = nan(11,10215);
FDTimeMedRaw= nan(11,10215);
FDTorqueNmMedRaw = nan(11,10215);
FDTorqueNormMedRaw = nan(11,10215);

FRTimeMed= nan(11,8001);
FRTorqueNmMed = nan(11,8001);
FRTorqueNormMed = nan(11,8001);
FRTimeMedRaw= nan(11,8001);
FRTorqueNmMedRaw = nan(11,8001);
FRTorqueNormMedRaw = nan(11,8001);

%Filtering Cutoff
cutoff = 5; % lower this (lowest = 0.5)

%load previous intervals
 load('AllMVCTorqueTime','FDcut','FRcut','Gcut');
 FDcut = round(FDcut);
 FRcut = round(FRcut);
 Gcut = round(Gcut);

% want to determine new intervals??
redo = 0;
show = 0;
%% Loop through each subject and select cut intervals for filtered data
for i = 1:N
    
    if (i==1) % Tim doesn't have MVC all days
        FDNormTime{i,:}=nan;
        FDTorqueNm{i,:}=nan;
        FRNormTime{i,:}=nan;
        FRTorqueNm{i,:}=nan;
        FDTorqueNorm{i,:}=nan;
        FRTorqueNorm{i,:}=nan;
        
        
%        load('Subject1AllMVC');
%        order = [10 60 20 90 30 100 80 40 50 70];
%        [sortedorder,I] = sort(order);
%        sortedTmvc = -(Tmvc(:,I));
%        AllMVCTorqueAvg(1,1:6) = sortedTmvc(1:6);
       
    else 
        x = csvread([subnames{i},'_AllMVC.csv']);           
   
    
        T = x(:,2); 
        EMG = x(:,4);
        
        data = T;
        samprate = 2000;
        dt = 1/samprate;
        U = length(data);
        t = 0:dt:((U-1)*dt);

        %Filter the Torque data
        torque = lowpassSimple(data,samprate,cutoff);
        EMGf = lowpassSimple(abs(EMG),samprate,cutoff);
            
            if redo
                figure(); 
                plot(torque,'k'); hold on
                Z = subnames{i};
                title('Torque MVC',sprintf('%s',Z));
                [xi,~] = ginput(2);
                xlim([xi(1) xi(2)])
                %[Gcut(i,:),~] = ginput(2);
                %[FDcut(i,:),~] = ginput(2); 
                [FRcut(i,:),~] = ginput(2);
                
            end
            FDcut1 = FDcut(i,:);
            FRcut1 = FRcut(i,:);
            Gcut1 = Gcut(i,:);
            GravityTorqueCut = torque(Gcut1(1,1):Gcut1(1,2),:);
            GravityMVC = mean(GravityTorqueCut);
            FDTorqueCut{i,:} = GravityMVC-(torque(FDcut1(1,1):FDcut1(1,2),:));
            MaxFD(i,:) = max(FDTorqueCut{i,:});
            FRTorqueCut{i,:} = GravityMVC-(torque(FRcut1(1,1):FRcut1(1,2),:));
            
            % EMG
            FDEMGCut{i,:} = EMGf(FDcut1(1,1):FDcut1(1,2));
            FREMGCut{i,:} = EMGf(FRcut1(1,1):FRcut1(1,2));
            
            
            MaxFR(i,:) = max(FRTorqueCut{i,:});
            FDTimeCut{i,:} = t(:,FDcut1(1,1):FDcut1(1,2));
            FRTimeCut{i,:} = t(:,FRcut1(1,1):FRcut1(1,2),:);
            FDTime = FDTimeCut{i,:};
            FDDiff = FDTime(length(FDTime)) - FDTime(1);
            FDZeroTime = linspace(0,FDDiff,length(FDTime));
            FRTime = FRTimeCut{i,:};
            FRDiff = FRTime(end) - FRTime(1);
            FRZeroTime = linspace(0,FRDiff,length(FRTime));
            
            FRTorque = round(FRTorqueCut{i,:},2);
            halfMaxFR=round((MaxFR(i,:)/2),2);
            halfFR = find(FRTorque==halfMaxFR);
            zeroingFR = FRZeroTime(halfFR(1));
            FRNormTime{i,:} = FRZeroTime - zeroingFR;
            
            FDTorque = round(FDTorqueCut{i,:},2);
            halfMaxFD=round((MaxFD(i,:)/2),2);
            halfFD = find(FDTorque==halfMaxFD);
            zeroingFD = FDZeroTime(halfFD(1));
            FDNormTime{i,:} = FDZeroTime - zeroingFD;
            
            FDTorqueNm{i,:} = (FDTorqueCut{i,:})*N_per_volt;
            FRTorqueNm{i,:} = (FRTorqueCut{i,:})*N_per_volt;
            FDTorqueNorm{i,:} = ((FDTorqueCut{i,:})/MaxFD(i,:))*100;
            FRTorqueNorm{i,:}= ((FRTorqueCut{i,:})/MaxFR(i,:))*100;
            
            FDTime1 = FDNormTime{i,:};
            FDtp0 = find(FDTime1==0);
            FDT1 = FDtp0 - 4214;FDT2 = FDtp0+6000;  
            FDTimeMed(i,:) = FDTime1(1,FDT1:FDT2);
            
            FDTorqueNm1 = FDTorqueNm{i,:}; 
            FDTorqueNmMed(i,:) = FDTorqueNm1(FDT1:FDT2,1);
            
            FRTime1 = FRNormTime{i,:};
            FRtp0 = find(FRTime1==0);
            FRT1 = FRtp0 - 4000;FRT2 = FRtp0+4000;  
            FRTimeMed(i,:) = FRTime1(1,FRT1:FRT2);
            
            FRTorqueNm1 = FRTorqueNm{i,:}; 
            FRTorqueNmMed(i,:) = FRTorqueNm1(FRT1:FRT2,1);
            
             
    end
end

medianTFD = median(FDTorqueNmMed,'omitnan');
medianTFR = median(FRTorqueNmMed,'omitnan');
MedMaxFD = max(medianTFD);
MedMaxFR = max(medianTFR);
FDTorqueNormMed= (medianTFD/MedMaxFD)*100;
FRTorqueNormMed= (medianTFR/MedMaxFR)*100;

if show %Plot Torque and Cut   
           
            figure(34); 
            subplot(221);
            color = [get(gca,'colororder'); get(gca,'colororder')];
            for i = 1:N
            plot(FDNormTime{i,:},FDTorqueNm{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FDTimeMed,'omitnan'),medianTFD,'color', 'k','linewidth',2.5); hold on
            title('MVC Torque Development vs Time');
            xlabel('Time');ylabel('Torque (Nm)');xlim([-1 3]) 
            
            
            subplot(222);
            for i = 1:N
            plot(FRNormTime{i,:},FRTorqueNm{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FRTimeMed,'omitnan'),medianTFR,'color', 'k','linewidth',2.5); hold on
            title('MVC Torque Relaxation vs Time');
            xlabel('Time');ylabel('Torque (Nm)');xlim([-2 2])
            
            subplot(223);
            for i = 1:N
            plot(FDNormTime{i,:},FDTorqueNorm{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FDTimeMed,'omitnan'),FDTorqueNormMed,'color', 'k','linewidth',2.5); hold on
            title('Normalized MVC Torque Development vs Time');
            xlabel('Time');ylabel('Normalized Torque (% MVC)');xlim([-1 3]) 
            
            
            subplot(224);
            for i = 1:N
            plot(FRNormTime{i,:},FRTorqueNorm{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FRTimeMed,'omitnan'),FRTorqueNormMed,'color', 'k','linewidth',2.5); hold on
            title('Normalized MVC Torque Relaxation vs Time');
            xlabel('Time');ylabel('Normalized Torque (% MVC)');xlim([-2 2])
            legend('1','2','3','4','5','6','7','8','9','10','11','median')
            
    end 

           
if redo
save('AllMVCTorqueTime','subnames','FDcut','FRcut','Gcut','FDTorqueNm','FRTorqueNm')
end

%% Loop through each subject and select cut intervals for raw data
for i = 1:N
    
    if (i==1) % Tim doesn't have MVC all days
        FDNormTimeRaw{i,:}=nan;
        FDTorqueNmRaw{i,:}=nan;
        FRNormTimeRaw{i,:}=nan;
        FRTorqueNmRaw{i,:}=nan;
        FDTorqueNormRaw{i,:}=nan;
        FRTorqueNormRaw{i,:}=nan;
        
        
%        load('Subject1AllMVC');
%        order = [10 60 20 90 30 100 80 40 50 70];
%        [sortedorder,I] = sort(order);
%        sortedTmvc = -(Tmvc(:,I));
%        AllMVCTorqueAvg(1,1:6) = sortedTmvc(1:6);
       
    else 
        x = csvread([subnames{i},'_AllMVC.csv']);           
   
    
        T = x(:,2); 
        data = T;
        samprate = 2000;
        dt = 1/samprate;
        U = length(data);
        t = 0:dt:((U-1)*dt);

        %Filter the Torque data
        torque = data;

            FDcut1 = FDcut(i,:);
            FRcut1 = FRcut(i,:);
            Gcut1 = Gcut(i,:);
            GravityTorqueCut = torque(Gcut1(1,1):Gcut1(1,2),:);
            GravityMVCRaw = mean(GravityTorqueCut);
            FDTorqueCutRaw{i,:} = GravityMVCRaw-(torque(FDcut1(1,1):FDcut1(1,2),:));
            MaxFDRaw(i,:) = max(FDTorqueCutRaw{i,:});
            FRTorqueCutRaw{i,:} = GravityMVCRaw-(torque(FRcut1(1,1):FRcut1(1,2),:));
            MaxFRRaw(i,:) = max(FRTorqueCutRaw{i,:});
            FDTimeCutRaw{i,:} = t(:,FDcut1(1,1):FDcut1(1,2));
            FRTimeCutRaw{i,:} = t(:,FRcut1(1,1):FRcut1(1,2),:);
            FDTimeRaw = FDTimeCutRaw{i,:};
            FDDiffRaw = FDTimeRaw(length(FDTimeRaw)) - FDTimeRaw(1);
            FDZeroTimeRaw = linspace(0,FDDiffRaw,length(FDTimeRaw));
            FRTimeRaw = FRTimeCutRaw{i,:};
            FRDiffRaw = FRTimeRaw(end) - FRTimeRaw(1);
            FRZeroTimeRaw = linspace(0,FRDiffRaw,length(FRTimeRaw));
            
            if (i==3)||(i==7)
            FRTorqueRaw = round(FRTorqueCutRaw{i,:},1);
            halfMaxFRRaw=round((MaxFRRaw(i,:)/2),1);
            else
            FRTorqueRaw = round(FRTorqueCutRaw{i,:},2);
            halfMaxFRRaw=round((MaxFRRaw(i,:)/2),2);
            end
            halfFRRaw = find(FRTorqueRaw==halfMaxFRRaw);
            zeroingFRRaw = FRZeroTimeRaw(halfFRRaw(1));
            FRNormTimeRaw{i,:} = FRZeroTimeRaw - zeroingFRRaw;
            
            if (i==3)||(i==7)
            FDTorqueRaw = round(FDTorqueCutRaw{i,:},1);
            halfMaxFDRaw=round((MaxFDRaw(i,:)/2),1);
            else
            FDTorqueRaw = round(FDTorqueCutRaw{i,:},2);
            halfMaxFDRaw=round((MaxFDRaw(i,:)/2),2);
            end
            halfFDRaw = find(FDTorqueRaw==halfMaxFDRaw);
            zeroingFDRaw = FDZeroTimeRaw(halfFDRaw(1));
            FDNormTimeRaw{i,:} = FDZeroTimeRaw - zeroingFDRaw;
            
            FDTorqueNmRaw{i,:} = (FDTorqueCutRaw{i,:})*N_per_volt;
            FRTorqueNmRaw{i,:} = (FRTorqueCutRaw{i,:})*N_per_volt;
            FDTorqueNormRaw{i,:} = ((FDTorqueCutRaw{i,:})/MaxFDRaw(i,:))*100;
            FRTorqueNormRaw{i,:}= ((FRTorqueCutRaw{i,:})/MaxFRRaw(i,:))*100;
            
            FDTime1Raw = FDNormTimeRaw{i,:};
            FDtp0Raw = find(FDTime1Raw==0);
            FDT1Raw = FDtp0Raw - 4214;FDT2Raw = FDtp0Raw+6000;  
            FDTimeMedRaw(i,:) = FDTime1Raw(1,FDT1Raw:FDT2Raw);
            
            FDTorqueNm1Raw = FDTorqueNmRaw{i,:}; 
            FDTorqueNmMedRaw(i,:) = FDTorqueNm1Raw(FDT1Raw:FDT2Raw,1);
            
            FRTime1Raw = FRNormTimeRaw{i,:};
            FRtp0Raw = find(FRTime1Raw==0);
            FRT1Raw = FRtp0Raw -4000;FRT2Raw = FRtp0Raw+4000;  
            FRTimeMedRaw(i,:) = FRTime1Raw(1,FRT1Raw:FRT2Raw);
            
            FRTorqueNm1Raw = FRTorqueNmRaw{i,:}; 
            FRTorqueNmMedRaw(i,:) = FRTorqueNm1Raw(FRT1Raw:FRT2Raw,1);
            
             
    end
end

medianTFDRaw = median(FDTorqueNmMedRaw,'omitnan');
medianTFRRaw = median(FRTorqueNmMedRaw,'omitnan');
MedMaxFDRaw = max(medianTFDRaw);
MedMaxFRRaw = max(medianTFRRaw);
FDTorqueNormMedRaw= (medianTFDRaw/MedMaxFDRaw)*100;
FRTorqueNormMedRaw= (medianTFRRaw/MedMaxFRRaw)*100;

if show %Plot Torque and Cut   
           
            figure(35); 
            subplot(221);
            color = [get(gca,'colororder'); get(gca,'colororder')];
            for i = 1:N
            plot(FDNormTimeRaw{i,:},FDTorqueNmRaw{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FDTimeMedRaw,'omitnan'),medianTFDRaw,'color', 'k','linewidth',2.5); hold on
            title('Raw MVC Torque Development vs Time');
            xlabel('Time');ylabel('Torque (Nm)');xlim([-1.5 3]) 
            
            
            subplot(222);
            for i = 1:N
            plot(FRNormTimeRaw{i,:},FRTorqueNmRaw{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FRTimeMedRaw,'omitnan'),medianTFRRaw,'color', 'k','linewidth',2.5); hold on
            title('Raw MVC Torque Relaxation vs Time');
            xlabel('Time');ylabel('Torque (Nm)');xlim([-2 2])
            
            subplot(223);
            for i = 1:N
            plot(FDNormTimeRaw{i,:},FDTorqueNormRaw{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FDTimeMedRaw,'omitnan'),FDTorqueNormMedRaw,'color', 'k','linewidth',2.5); hold on
            title('Raw Normalized MVC Torque Development vs Time');
            xlabel('Time');ylabel('Normalized Torque (% MVC)');xlim([-1.5 3]) 
            
            
            subplot(224);
            for i = 1:N
            plot(FRNormTimeRaw{i,:},FRTorqueNormRaw{i,:},'color', color(i,:),'linewidth',1); hold on
            end
            plot(median(FRTimeMedRaw,'omitnan'),FRTorqueNormMedRaw,'color', 'k','linewidth',2.5); hold on
            title('Raw Normalized MVC Torque Relaxation vs Time');
            xlabel('Time');ylabel('Normalized Torque (% MVC)');xlim([-2 2])
            legend('1','2','3','4','5','6','7','8','9','10','11','median')
            
end 

