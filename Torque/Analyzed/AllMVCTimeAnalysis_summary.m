clear all; close all; clc

load('AllMVCTorqueTime.mat')
dt = 1/2000;

figure(1)
% color = get(gca,'colorder');

Tcut = nan(2e4, 11, 2);
Ecut = nan(2e4, 11, 2);
for k = 1:2
    if k == 1
        Torquesback = FDTorqueNm; 
        EMGsback = FDEMGCut;
    else, Torquesback = FRTorqueNm;
          EMGsback = FREMGCut;
    end
    
for i = 2:11
    T = Torquesback{i,:}; 
    E = EMGsback{i,:};
    t = 0:dt:(length(T)-1)*dt;
    
    Tmax = max(T);
    Emax = max(E);
    Emin = min(E);
    
    E = E - Emin;
    
    if k == 1, j = find(T > .5*Tmax,1);
    else,      j = find(T < .5*Tmax,1);
    end
    
    Tcut(1:length(T(j-2000:end)),i,k) = T(j-2000:end)/Tmax;
    Ecut(1:length(E(j-2000:end)),i,k) = E(j-2000:end)/Emax;
    
    tcut = 0:dt:(length(Tcut)-1)*dt;
    
    figure(k);
    subplot(221); plot(t, T); hold on; %plot(j, T(j),'o');
    subplot(222); plot(tcut, Tcut(:,i,k)); hold on
    
    subplot(223); plot(t, E); hold on; %plot(j, T(j),'o');
    subplot(224); plot(tcut, Ecut(:,i,k)); hold on
    
    
%     FRTorqueNmMed(i,:) = FRTorqueNm1(FRT1:FRT2,1);
              
end
end

%% median
if ishandle(3), close(3); end

tmax = 3;
imax = tmax/dt;
titles = {'Force development', 'Force relaxation'};

Tmedrel = nan(imax, 2);

for k = 1:2
    figure(3); 
    subplot(1,2,k);
    
    Tmed = median(Tcut(1:imax,:,k),2,'omitnan');
    
    Tmedrel(:,k) = Tmed / max(Tmed);
    
    t = 0:dt:((imax-1)*dt);
    plot(t, Tmedrel(:,k),'linewidth',2); hold on

    axis([0 3 0 1]); title(titles{k}); xlabel('Time (s)'); ylabel('Torque (%max)')

end

% compare to low-pass filter
tstar1 = .75;
tstar2 = .9;

F1o_act_opt = 1 - exp(-(t-tstar1)/.4);
F1o_deact_opt = exp(-(t-tstar2)/.1);

subplot(121); plot(t, F1o_act_opt/max(F1o_act_opt),'k--');
subplot(122); plot(t, F1o_deact_opt, 'k--');

legend('Empirical data (N=10)', 'First-order approximation', 'location','best')

% medianTFD = median(FDTorqueNmMed,'omitnan');

%% median
tmax = 3;
imax = tmax/dt;
titles = {'Force development', 'Force relaxation'};

Tmedrel = nan(imax, 2);
Emedrel = nan(imax, 2);

for k = 1:2
    figure(3); 
    subplot(1,2,k);
    
    Emed = median(Ecut(1:imax,:,k),2,'omitnan');
    
    Emedrel(:,k) = Emed / max(Emed);
    
    t = 0:dt:((imax-1)*dt);
    plot(t, 1.1*Emedrel(:,k),'linewidth',2); hold on

    axis([0 3 0 1.2]); title(titles{k}); xlabel('Time (s)'); ylabel('Torque (%max)')

end

% compare to low-pass filter
tstar1 = .75;
tstar2 = .9;

F1o_act_opt = 1 - exp(-(t-tstar1)/.4);
F1o_deact_opt = exp(-(t-tstar2)/.1);

subplot(121); plot(t, F1o_act_opt/max(F1o_act_opt),'k--');
subplot(122); plot(t, F1o_deact_opt, 'k--');

legend('Empirical data (N=10)', 'First-order approximation', 'EMG','location','best')

% medianTFD = median(FDTorqueNmMed,'omitnan');

%% quick experiment
F1o_act_opt = 1 - exp(-(t-tstar1)/.4);
F1o_deact_opt = exp(-(t-tstar2)/.1);

[t1, X1] = ode113(@diffunc1, [0 1], 0);
[t2, X2] = ode113(@diffunc2, [0 1], [0; 0]);

if ishandle(100), close(100); end
figure(100);
plot(t1, X1); hold on
plot(t2, X2,'--');

%% quick experiment 2
dt = .001;
t = 0:dt:.3;

X(1) = 0;
for i = 1:(length(t)-1)
    X(i+1) = X(i) + ((1-X(i))/.1)*dt;
end

X21(1) = 0;
for i = 1:(length(t)-1)
    if ~mod(i,10), U = 1;
    else, U = 0;
    end
    X21(i+1) = X21(i) + ((U-X21(i))/.01)*dt;
end

if ishandle(101), close(101); end
figure(101);
plot(t, X); hold on
plot(t, X21);

function[dXdt] = diffunc1(t, X)
dXdt(1,1) = (1-X(1))/.1;
end

function[dXdt] = diffunc2(t, X)

dXdt(1,1) = (1-X(1))/.1;
dXdt(2,1) = (X(1)-X(2))/.05;
end









