clear all; close all; clc
% this script is used to determine each subject's Torque MVC data across all angle by first
% cutting the data and then averaging it.
% this script outputs one .mat files called 'AllMVCTorqueGData.mat' and 'AllMVCTorqueData.mat', which
% contains each subject's MVC torque data and their respective cut intervals
% where the MVC occurs.
%% Day 1 MVC EMG Analysis Parameters
% subject names (according to protocol)
subnames = {'S1','S2','S3','S4','S5','S6','S7','S8','S9','S10','S11'};

% Nm per volt
N_per_volt = 69.71; 

%Downsample factor
n = 10;

% Create not a number filled variable for each MVC EMG average 
N = length(subnames);
AllMVCTorqueAvg = nan(N,6);
Ecut = nan(11,2,6);
Gcut = nan(11,2,6);
%Filtering Cutoff
cutoff = 100; 

% load previous intervals
 load('AllMVCTorqueData.mat','Ecut','Gcut');
 Ecut = round(Ecut);
 Gcut = round(Gcut);

% want to determine new intervals??
redo = 0;
show = 1;
%% Loop through each subject and select cut intervals for day 1
for i = 1:N
    
    if (i==1) % Subject 1 has all MVC from previous study
       load('Subject1AllMVC'); % Load previous data
       order = [10 60 20 90 30 100 80 40 50 70]; % Subject 1 trial order
       [sortedorder,I] = sort(order);% Sort data according to order
       sortedTmvc = -(Tmvc(:,I));
       AllMVCTorqueAvg(1,1:6) = sortedTmvc(1:6);% Add sorted data into variable
       
    else 
        x = csvread([subnames{i},'_AllMVC.csv']); % Load CSV data          

        data = x(:,2); % Pull out raw torque data
        samprate = 2000;% sample rate
        dt = 1/samprate;
        U = length(data);%length of data samples
        t = 0:dt:((U-1)*dt);% time vector

        %Filter the Torque data
        torque = lowpassSimple(data,samprate,cutoff);
        
            
            if redo
                % Cut MVC data according to angle order 
                figure(); 
                plot(torque,'k'); hold on
                Z = subnames{i};
                title('Torque MVC',sprintf('%s',Z));
                for k = 1:6
                [Ecut(i,:,k),~] = ginput(2); % Cut exercise section of MVC
                [Gcut(i,:,k),~] = ginput(2); % Cut gravity section of MVC
                end
            end
            
            %Determine Max MVC EMG
            for j = 1:6
            Ecut1 = Ecut(i,:,j); % Get exercise cut interval
            Gcut1 = Gcut(i,:,j); % Get gravity cut interval
            ExcTorquecut = torque(Ecut1(1,1):Ecut1(1,2),:);% Cut exercise torque 
            [ExerciseMVC,loc1] = min(ExcTorquecut); % Find peak of MVC data
            GravityTorqueCut = torque(Gcut1(1,1):Gcut1(1,2),:);% Cut gravity torque 
            GravityMVC = mean(GravityTorqueCut); % Get mean of gravity data
            AllMVCTorqueAvg(i,j) = (GravityMVC - ExerciseMVC);% Zero torque data according to gravity
            
            if show %Plot Torque and Cut section   
            torqueNm = torque*N_per_volt;% Convert to Nm
            ExerciseMVCNm = ExerciseMVC*N_per_volt;% Convert to Nm
            figure(i); 
            plot(torqueNm,'k'); hold on
            plot(loc1+Ecut1(1,1), ExerciseMVCNm,'ko'); hold on
            Z = subnames{i};
                title('MVC Torque Cut Interval Comparison',sprintf('%s',Z));
            xlabel('Sample Rate');ylabel('Torque (Nm)');
            end
            end

end          
end
if redo % Save data
save('AllMVCTorqueData','subnames','AllMVCTorqueAvg','Ecut','Gcut')
end