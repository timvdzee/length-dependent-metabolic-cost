clear all; close all; clc
% this script is used to analyze each subject's raw torque data by filtering, downsampling, and determining cut intervals.   
% this script outputs two .mat files per subject called 'subjectnameProcDay1.mat' and 'subjectnameProcDay2.mat', 
% which contains the analyzed torque, time variable, filtering cutoff variable used,
% the downsampling factor used, each respective day's filename order, 
% the cut intervals where a gravity measurment takes place, the cut intervals where exercise happens, 
% and the analyzed time
%% Parameters
% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);

%Day 1 filenames sorted order 
Day1filenames = {'_10deg_2Hz.csv','_20deg_2Hz.csv','_30deg_2Hz.csv','_40deg_2Hz.csv','_50deg_2Hz.csv','_60deg_2Hz.csv'}; 

%Day 2 filenames sorted order
Day2filenames = {'_20deg_0Hz.csv','_20deg_1Hz.csv','_20deg_2Hz(2).csv','_20deg_05Hz.csv','_20deg_15Hz.csv','_20deg_25Hz.csv','_60deg_0Hz.csv'};

%Filtering Cutoff
cutoff = 100; 

%Downsample factor
n = 10; 
%% Analyze Day 1 data for each subject
    % Create cells to place data
    torque=cell(11,length(Day1filenames));
    target=cell(11,length(Day1filenames));
    tnew=cell(11,length(Day1filenames));

for i = 1:N % Loop through all subjects
    if (i==2) % Subject 2 has weird data formatting
        x = load('Koen_June21_submax.csv');% Load CSV file
        KoenAngles = [50 20 60 40 30 10];  % Subject 2 angles trial order 

        data = x(:,2); % Pull out raw torque data
        Targetraw = x(:,3); % Raw torque target data
        samprate = 2000;% sample rate
        dt = 1/samprate;
        U = length(data);%length of data samples
        t = 0:dt:((U-1)*dt);% time vector

        %Filter the Torque data
        newdata = lowpassSimple(data,samprate,cutoff);
        filttarget = lowpassSimple(Targetraw,samprate,cutoff);

        % Downsample the Torque and time data
        koentorque = downsample(newdata,n);
        koentnew = downsample(t,n);
        koentarget = downsample(filttarget, n);

        % Cut each exercise section
        for j = 1:6
            figure
            plot(koentorque)
            [xi,~] = ginput(2);% Zoom in first for better cut
            xlim([xi(1) xi(2)])
            [Exercisecutint{i,j},~] = ginput(2);% Select exercise intervals
            xlim([0 max(t)])

        end

        % Cut each gravity section
        for j = 1:6
            figure
            plot(koentorque)
            [xi,~] = ginput(2);% Zoom in first for better cut
            xlim([xi(1) xi(2)])
             [Gravitycutint{i,j},~] = ginput(2);% Select gravity intervals
            xlim([0 max(t)])

        end

        koentnew = koentnew';
        for k = 1:6
            Ecut = Exercisecutint{i,k};% Get exercise cut intervals
            Gcut = Gravitycutint{i,k};% Get gravity cut intervals

            % Cut Subject 2 data into individual trials
            torque{2,k}=koentorque(Gcut(1):Ecut(2),:);
            tnew{2,k}=koentnew(Gcut(1):Ecut(2),:);
            target{2,k}=koentarget(Gcut(1):Ecut(2),:);
        end

    else %All other subjects have the same day 1 data formatting
        for k = 1:length(Day1filenames); 
            
            x = load([subnames{i},Day1filenames{k}]);% Load CSV file
            data = x(:,2); % Pull out torque data
            Targetraw = x(:,3); % Raw torque target data
            samprate = 2000;% sample rate
            dt = 1/samprate;
            U = length(data);%length of data samples
            t{i,k} = 0:dt:((U-1)*dt);% time vector

            % Filter the Torque data
            newdata = lowpassSimple(data,samprate,cutoff);
            filttarget = lowpassSimple(Targetraw,samprate,cutoff);
            % Downsample the Torque and time data
            torque{i,k} = downsample(newdata,n);
            tnew{i,k} = downsample(t{i,k},n);
            target{i,k} = downsample(filttarget, n)


            % Plot both raw and analyzed torque data
            figure
            plot(t{i,k}, T); hold on
            plot(t{i,k}, newdata);hold on
            plot(tnew{i,k}, torque{i,k});

            %Cut analyzed torque data into Gravity and Exercise sections
            [Gravitycutint{i,k},~]=ginput(2);% Select gravity cut intervals
            [Exercisecutint{i,k},~]=ginput(2);% Select exercise cut intervals
            
        end
 end
end
% Save variables
save('TorqueProcDay1','subnames','torque','cutoff','n','Day1filenames','Gravitycutint','Exercisecutint','tnew','target','KoenAngles')
 
%% Analyze Day 2 data for each subject
% Clear variables
clear 'torque'
clear 'tnew'
clear 'target'
for i = 1:N % Loop through all subjects 
    if (i==2)||(i==3) % Subjects 2, and 3 have weird data formatting
        
        if i == 2
        x = load('Koen_June21_submax_variable_freq.csv'); % Load CSV file
        KoenFrequency = [1 2 0.5 0 2.5 1.5 60]; % Subject day 2 frequency trial order
        elseif i == 3
        x = load('Billy_June27_variable_frequency.csv');% Load CSV file
        BillyFrequency = [0 1.5 1 60 2.5 0.5 2]; % Subject day 3 frequency trial order
        end

        data = x(:,2); % Pull out raw torque data
        Targetraw = x(:,3); % Raw torque target data
        samprate = 2000;% sample rate
        dt = 1/samprate;
        U = length(data);%length of data samples
        t = 0:dt:((U-1)*dt);% time vector

        %Filter the Torque data
        newdata = lowpassSimple(data,samprate,cutoff);
        filttarget = lowpassSimple(Targetraw,samprate,cutoff);

        % Downsample the Torque and time data
         if (i==2)||(i==3)
        torqueuncut = downsample(newdata,n);
        tnewuncut = downsample(t,n);
        targetuncut = downsample(filttarget, n)
         end
        
        % Cut each exercise section
        for j = 1:7
                figure
                plot(torqueuncut)
                [xi,~] = ginput(2);% Zoom in first for better cut
                xlim([xi(1) xi(2)])
                [Exercisecutint{i,j},~] = ginput(2);% Select exercise intervals
                xlim([0 max(t)])

        end

        % Cut each gravity section
        for j = 1:7
                figure
                plot(torqueuncut)
                [xi,~] = ginput(2);% Zoom in first for better cut
                xlim([xi(1) xi(2)])
                [Gravitycutint{i,j},~] = ginput(2);% Select gravity intervals
                xlim([0 max(t)])

        end

        tnewuncut = tnewuncut';
        for k = 1:7
            Ecut = Exercisecutint{i,k};
            Gcut = Gravitycutint{i,k};

            % Cut Subject data into individual trials
            torque{i,k}=torqueuncut(Gcut(1):Ecut(2),:);
            tnew{i,k}=tnewuncut(Gcut(1):Ecut(2),:);
            target{i,k}=targetuncut(Gcut(1):Ecut(2),:);
        end

    else %All other subjects have the same day 2 data formatting
    torque=cell(1,length(Day2filenames));
    target=cell(1,length(Day2filenames));
    tnew=cell(1,length(Day2filenames));
    
        for k = 1:length(Day2filenames);    
            x = load([subnames{i},Day2filenames{k}]);

            data = x(:,2); % Pull out torque data
            Targetraw = x(:,3); % Raw torque target data
            samprate = 2000;% sample rate
            dt = 1/samprate;
            U = length(data);%length of data samples
            t{i,k} = 0:dt:((U-1)*dt);% time vector

            % Filter the Torque data
            newdata = lowpassSimple(data,samprate,cutoff);
            filttarget = lowpassSimple(Targetraw,samprate,cutoff);
            
            % Downsample the Torque and time data
            torque{i,k} = downsample(newdata,n);
            tnew{i,k} = downsample(t,n);
            target{i,k} = downsample(filttarget, n)

            % Plot both raw and analyzed torque data
            figure
            plot(t, T); hold on
            plot(t, newdata);
            plot(tnew{i,k}, torque{i,k}, '.')

            %Cut analyzed torque data into Gravity and Exercise sections
            [Gravitycutint{i,k},~]=ginput(2);
            [Exercisecutint{i,k},~]=ginput(2);
    
        end
        
          end
end
% Save variables
save('TorqueProcDay2','subnames','torque','cutoff','n','Day2filenames','Gravitycutint','Exercisecutint','tnew','target','KoenFrequency','BillyFrequency')

 
 
