clear all; close all; clc
% this script is used to determine each subject's EMG MVC data by first
% cutting the data and then averaging it.
% this script outputs two .mat files called 'MVCDay1EMGData.mat' and 'MVCDay2EMGData.mat', which
% contains each subject's MVC EMG data and their respective cut intervals
% where the MVC occurs for each day.
%% Day 1 MVC EMG Analysis Parameters
% subject names (according to protocol)
subnames = {'S1','Koen','Billy','Rudi','Jordan','S6','Brian','Austin','Nick','Eliz','Gaby'};

% Nm per volt
N_per_volt = 69.71; 

%Downsample factor
n = 10;

% Create not a number filled variable for each MVC EMG average 
N = length(subnames);
MVCDay1TorqueAvg = nan(N,1);

%Filtering Cutoff
cutoff = 100; 

% load previous intervals
load('MVCDay1TorqueData.mat','Ecut1','Gcut1');
Ecut1 = round(Ecut1);
Gcut1 = round(Gcut1);

% want to determine new intervals and show individual figures??
redo = 0;
show = 1;
%% Loop through each subject and select cut intervals for day 1
for i = 1:N
    
    if i == 1 % Subject 1 has a unique trial formatting
        x = csvread('S1_20deg_MVC.csv');% Load CSV file
    elseif i == 2 % Subject 2 has unique trial formatting
        x = csvread('Koen_June21_MVC.csv');
    elseif (i == 6)||(i==10)||(i==11)% Subjects 6,10,and 11 have different MVC trial used         
        x = csvread([subnames{i},'_10deg_MVC.csv']);
    else % All other subjects use the same filenames
        x = csvread([subnames{i},'_AllMVC.csv']);           
    end
    
        T = x(:,2); % Time data
        data = T;% Raw torque data
        samprate = 2000;% sample rate
        dt = 1/samprate;
        U = length(data);%length of data samples
        t = 0:dt:((U-1)*dt);% time vector

        %Filter the Torque data
        torque = lowpassSimple(data,samprate,cutoff);
        
           
            if redo
                % Plot individual figure and select gravity and exercise
                % cut intervals
                figure(); 
                plot(torque,'k'); hold on
                Z = subnames{i};
                title('Torque MVC',sprintf('%s',Z));
                [Ecut1(i,:),~] = ginput(2); % Pick exercise intervals
                [Gcut1(i,:),~] = ginput(2); % Pick gravity intervals 
            end
            
            %Determine Max MVC EMG
            ExcTorquecut = torque(Ecut1(i,1):Ecut1(i,2),:);% Cut exercise portion of MVC data
            [ExerciseMVC,loc1] = min(ExcTorquecut); % Find peak of torque
            GravityTorqueCut = torque(Gcut1(i,1):Gcut1(i,2),:);% Cut gravity portion of MVC data
            GravityMVC = mean(GravityTorqueCut);% Get mean of gravity cut
            MVCDay1TorqueAvg(i,:) = (GravityMVC - ExerciseMVC);% Zero MVC to gravity
            

           if show %Plot Torque and Cut section   
            torqueNm = torque*N_per_volt;% Convert to Nm
            ExerciseMVCNm = ExerciseMVC*N_per_volt;% Convert to Nm
            figure(); 
            plot(torqueNm,'k'); hold on
            plot(loc1+Ecut1(i,1), ExerciseMVCNm,'ro','MarkerSize',15); hold on
            Z = subnames{i};
                title('MVC Torque Cut Interval Comparison Day 1',sprintf('%s',Z));
                legend('Raw Torque Data','MVC Torque');
                xlabel('Sample Rate');ylabel('Torque (Nm)');
           end
end          

if redo % Save data
save('MVCDay1TorqueData','subnames','MVCDay1TorqueAvg','Ecut1','Gcut1')
end
%% Day 2 MVC EMG Analysis
% Create not a number filled variable for each MVC EMG average 
MVCDay2TorqueAvg = nan(N,1);


% load previous intervals
load('MVCDay2TorqueData.mat','Ecut2','Gcut2');
Ecut2 = round(Ecut2);
Gcut2 = round(Gcut2);
%% Loop through each subject and select cut intervals for day 1
for i = 1:N
    
    if i == 1 % Subject 1 has a unique trial formatting
        x = csvread('S1_20deg_MVC(2).csv');
    elseif i == 2 % Subject 2 has unique trial formatting
        x = csvread('Koen_June21_MVC.csv');
    elseif i == 3 % Subject 3 has unique trial formatting
        x = csvread('Billy_June26.csv');
    elseif (i==10)||(i==11) % Subjects 6,10,and 11 have different MVC trial used               
        x = csvread([subnames{i},'_AllMVC.csv']);
    else % All other subjects use the same filenames
        x = csvread([subnames{i},'_20deg_MVC.csv']);
    end

        T = x(:,2); % Time data
        data = T;% Raw torque data
        samprate = 2000;% sample rate
        dt = 1/samprate;
        U = length(data);%length of data samples
        t = 0:dt:((U-1)*dt);% time vector


        %Filter the Torque data
        torque2 = lowpassSimple(data,samprate,cutoff);
        
            
            if redo
                % Plot individual figure and select gravity and exercise
                % cut intervals
                figure(); 
                plot(torque2,'k'); hold on
                Z = subnames{i};
                title('Torque MVC',sprintf('%s',Z));
                [Ecut2(i,:),~] = ginput(2); % Pick exercise intervals
                [Gcut2(i,:),~] = ginput(2); % Pick gravity intervals
            end
            
            %Determine Max MVC EMG
            ExcTorquecut = torque2(Ecut2(i,1):Ecut2(i,2),:);% Cut exercise portion of MVC data
            [ExerciseMVC,loc1] = min(ExcTorquecut);% Find peak of torque
            GravityTorqueCut = torque2(Gcut2(i,1):Gcut2(i,2),:);% Cut gravity portion of MVC data
            GravityMVC = mean(GravityTorqueCut);% Get mean of gravity cut
            MVCDay2TorqueAvg(i,:) = (GravityMVC - ExerciseMVC);% Zero MVC to gravity
            

           if show %Plot Torque and Cut section 
            torque2Nm = torque2*N_per_volt;% Convert to Nm
            ExerciseMVCNm = ExerciseMVC*N_per_volt;% Convert to Nm
            figure(); 
            plot(torque2Nm,'k'); hold on
            plot(loc1+Ecut2(i,1), ExerciseMVCNm,'ro','MarkerSize',15); hold on
            Z = subnames{i};
                title('MVC Torque Cut Interval Comparison Day 2',sprintf('%s',Z));
                legend('Raw Torque Data','MVC Torque');
                xlabel('Sample Rate');ylabel('Torque (Nm)');
           end
end          

if redo % Save data
save('MVCDay2TorqueData','subnames','MVCDay2TorqueAvg','Ecut2','Gcut2')
end