clear all; close all; clc
% This script is used to process the specific gravity data for each of the
% different angle conditions taken on day 1. The data is cut into gravity
% sections, averaged, and saved accordingly. As well individual outlier can
% be selected to further analyze their data.
%% Parameters
% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'Emily','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
%Nm per volt 
N_per_volt = 69.71; 

%Fraction of maximal voluntary contraction used
fraction_MVC = .5; 

%Each Subjects torque target (25% of 10 degree MVC)
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;

%Angle order
angles = [10:10:60];

%% Day 1 Torque Gravity per Angle processing
load ('TorqueProcDay1.mat'); % Load cut intervals 
for i = 1:N 
    
    if i == 2 %Subject 2 has weird data formatting
        
        torquegrav = torque{i};% Get torque trial of interest
        T10G = torquegrav;
        Gcut10 = Gravitycutint{i,6};% Get gravity cut intervals
        GT10cut = T10G(Gcut10(1):Gcut10(2),:);% Cut torque into gravity section
        Day1GravTorqueAvg(i,1) = mean (GT10cut);% Average gravity cut section
       
        T20G = torquegrav;
        Gcut20 = Gravitycutint{i,2};
        GT20cut = T20G(Gcut20(1):Gcut20(2),:);
        Day1GravTorqueAvg(i,2) = mean (GT20cut);
        
        T30G = torquegrav;
        Gcut30 = Gravitycutint{i,5};
        GT30cut = T30G(Gcut30(1):Gcut30(2),:);
        Day1GravTorqueAvg(i,3) = mean (GT30cut);

        T40G = torquegrav;
        Gcut40 = Gravitycutint{i,4};
        GT40cut = T40G(Gcut40(1):Gcut40(2),:);
        Day1GravTorqueAvg(i,4) = mean (GT40cut);
        
        T50G = torquegrav;
        Gcut50 = Gravitycutint{i,1};
        GT50cut = T50G(Gcut50(1):Gcut50(2),:);
        Day1GravTorqueAvg(i,5) = mean (GT50cut);

        T60G = torquegrav;
        Gcut60 = Gravitycutint{i,3};
        GT60cut = T60G(Gcut60(1):Gcut60(2),:);
        Day1GravTorqueAvg(i,6) = mean (GT60cut);      

    else %All other subjects have the same day 1 formatting
   

        T10G = torque{i,1};% Get torque of interest
        Gcut10 = Gravitycutint{i,1}*200;% Get gravity cut intervals
        GT10cut = T10G(Gcut10(1):Gcut10(2),:);% Cut torque into gravity section
        Day1GravTorqueAvg(i,1) = mean (GT10cut);% Average gravity cut section
        
        T20G = torque{i,2};
        Gcut20 = Gravitycutint{i,2}*200;
        GT20cut = T20G(Gcut20(1):Gcut20(2),:);
        Day1GravTorqueAvg(i,2) = mean (GT20cut);
        
        T30G = torque{i,3};
        Gcut30 = Gravitycutint{i,3}*200;
        GT30cut = T30G(Gcut30(1):Gcut30(2),:);
        Day1GravTorqueAvg(i,3) = mean (GT30cut);
        
        T40G = torque{i,4};
        Gcut40 = Gravitycutint{i,4}*200;
        GT40cut = T40G(Gcut40(1):Gcut40(2),:);
        Day1GravTorqueAvg(i,4) = mean (GT40cut);
        
        T50G = torque{i,5};
        Gcut50 = Gravitycutint{i,5}*200;
        GT50cut = T50G(Gcut50(1):Gcut50(2),:);
        Day1GravTorqueAvg(i,5) = mean (GT50cut);
        
        T60G = torque{i,6};
        Gcut60 = Gravitycutint{i,6}*200;
        GT60cut = T60G(Gcut60(1):Gcut60(2),:);
        Day1GravTorqueAvg(i,6) = mean (GT60cut);
end
end 

% Save data
save('Day1Gravity','Day1GravTorqueAvg','subnames','angles')
%% Examine outlier further

subname = 'Nick'; % Select outlier to examine further and see their data visually

load ('TorqueProcDay1.mat'); % Load cut intervals
for i = 1:length(angles) % Loop through all angles
        TG = torque{9,i};% Get trial of interest
        Gcut = Gravitycutint{9,i}*200;% Get gravity cut intervals
        GTcut = TG(Gcut(1):Gcut(2),:);% Cut data
        Day1GravTorqueAvg = mean (GTcut);% Average

        % Plot figure
        figure();
        plot(TG); hold on
        plot(GTcut); hold on
        yline(Day1GravTorqueAvg,'r','LineWidth', 2);
        legend('EMG','Gravity Average')
        xlabel('Sample Number');ylabel('EMG Signal (mV)');
        title(sprintf('%d',angles(i)));
        
        
end