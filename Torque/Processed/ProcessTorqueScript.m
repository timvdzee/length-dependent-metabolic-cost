clear all; close all; clc
% this script is used to process each subjects previously analyzed data by cutting, averaging,
% and determining the maxima/minima of their data with the option to furthur normalize 
% by each Subjects torque target which is 25% of their 10 degree MVC. 
% this script also visualizes the processed torque data by graphing each
% subject's processed torque compared to their torque target and a summary
% graph for each day plotting the average torque of all subjects compared to the expected torque. 
% this script outputs two .mat files called 'ProcessedTorqueDay1.mat' and 'ProcessedTorqueDay2.mat', which
% contains each subjects torque target, the Nm per volt and each subject's
% average, max, and min torque for each day. 

%% Parameters
% subject names (according to protocol)
subnames = {'S1', 'Koen', 'Billy', 'Rudi', 'Jordan', 'S6','Brian','Austin','Nick','Eliz','Gaby'};
N = length(subnames);
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
%Nm per volt 
N_per_volt = 69.71; 

%Fraction of maximal voluntary contraction used
fraction_MVC = .5; 

%Each Subjects torque target (25% of 10 degree MVC)
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;  

%Angle order
angles = [10:10:60];
 
% Show individual Figures
showindfig = 1;
%% Day 1 Torque processing per subject
for i = 1:N % Loop through all subjects
    subNumber = i; % Subject of interest in loop 
    TestingDay = 1; % Testing day
    plotgraph = 0; % Plot individual subject figures 
    trials = 'all'; % Look at all trials 
    [Day1NetTorqueAvg,Tmin,Tmax,Tmdot] = do_TorqueSubjectIndAnalysis(subNumber, TestingDay,trials,plotgraph);

     % Convert data to Nm
     TorqueAvgD1 (i,:) = Day1NetTorqueAvg * N_per_volt;
     TorqueMinimaD1 (i,:) = Tmin * N_per_volt;
     TorqueMaximaD1(i,:) = Tmax * N_per_volt;
     TorquerateD1(i,:) = Tmdot* N_per_volt;

       if showindfig
        %Plot figure
        figure
        plot (angles, TorqueAvgD1 (i,:),'o'); hold on
        y = WaveformAmp(:,i);
        yline(y,'r','LineWidth', 2);
        legend('Average Torque','Expected Torque')
        xlabel('Angle (degrees)');
        ylabel('Average Torque (Nm)');
        Z = subnames{i};
        title('Variable Angle, Constant Frequency',sprintf('%s',Z));
       end
   
end 
%% Day 1 Average Torque Summary Processing 
    
    %Plot Results
    figure
    color = [get(gca,'colororder'); get(gca,'colororder')];
    errorbar(angles,mean(TorqueAvgD1),std(TorqueAvgD1),'ko','LineWidth', 2,'markersize',18); hold on
    for i = 1:N, plot(angles, TorqueAvgD1(i,:), symbols(i), 'color', color(i,:),'markersize',10,'linestyle','-'); hold on
    end
    AvgExpected = mean(WaveformAmp);
    yline(AvgExpected,'r','LineWidth', 2);

    legend('Mean Torque','1','2','3','4','5','6','7','8','9','10','11','Expected Torque')
    xlabel('Angle (degrees)');ylabel('Average Torque (Nm)');
    title('Summary Variable Angle, Constant Frequency');

% Save data
save('ProcessedTorqueDay1','subnames','angles','WaveformAmp','N_per_volt','TorqueAvgD1','TorqueMinimaD1','TorqueMaximaD1', 'TorquerateD1')
%% Day 2 Torque processing per subject
%Frequency order
frequencies = [0,0.5,1,1.5,2,2.5]; % Day 2 frequency order
for i = 1:N
    subNumber = i;% Subject of interest in loop 
    TestingDay = 2;% Testing day
    plotgraph = 0;% Plot individual figures
    trials = 'all';% Look at all trials
    [Day2NetTorqueAvg,Tmin,Tmax,Tmdot] = do_TorqueSubjectIndAnalysis(subNumber, TestingDay,trials,plotgraph);
     
     % Convert data to Nm
     TorqueAvgD2 (i,:) = Day2NetTorqueAvg * N_per_volt;
     TorqueMinimaD2 (i,:) = Tmin * N_per_volt;
     TorqueMaximaD2(i,:) = Tmax * N_per_volt;
     TorquerateD2(i,:) = Tmdot* N_per_volt;
     
        %Plot Figure
        if showindfig
        figure
        plot (frequencies,TorqueAvgD2(i,:),'o'); hold on
        y = WaveformAmp(:,i);
        yline(y,'r','LineWidth', 2);
        legend('Average Torque','Expected Torque');
        xlabel('Frequency (Hz)');
        ylabel('Average Torque (Nm)');
        Z = subnames{i};
        title('Constant Angle, Variable Frequency',sprintf('%s',Z));
        end
    end

%% Day 2 Average Torque Summary Processing
 %Plot results
figure
    color = [get(gca,'colororder'); get(gca,'colororder')];
    errorbar(frequencies,mean(TorqueAvgD2),std(TorqueAvgD2),'ko','LineWidth', 2,'markersize',18); hold on
    for i = 1:N, plot(frequencies, TorqueAvgD2(i,:), symbols(i), 'color', color(i,:),'markersize',10,'linestyle','-'); hold on
    end
    AvgExpected = mean(WaveformAmp);
    yline(AvgExpected,'r','LineWidth', 2);    
    legend('Mean Torque','1','2','3','4','5','6','7','8','9','10','11','Expected Torque')
    xlabel('Frequency (Hz)');ylabel('Average Torque (Nm)');
    title('Summary Constant Angle, Variable Frequency');

% Save data    
save('ProcessedTorqueDay2','subnames','frequencies','WaveformAmp','N_per_volt','TorqueAvgD2','TorqueMinimaD2','TorqueMaximaD2', 'TorquerateD2');
