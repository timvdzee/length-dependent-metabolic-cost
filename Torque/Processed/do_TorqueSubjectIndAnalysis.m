% This function is used to analyze subjects torque data for both testing days. 
% Inputs include subject number of interest, testing day of interest, which
% trials you want to look at, and if you want to visualize the results.
% Outputs include average net torque (NetTorqueAvg), torque minima (Tmin), 
% torque maxima (Tmax), and torque rate (Tmdot) for selected subject. 

function [NetTorqueAvg,Tmin,Tmax,Tmdot] = do_TorqueSubjectIndAnalysis(subNumber, TestingDay,trials,plotgraph);
i = subNumber;
N_per_volt = 69.71; %Nm per volt 
angles = [10:10:60]; % Day 1 angles
frequencies = [0, 0.5, 1, 1.5, 2, 2.5];% Day 2 frequencies
dispfreq = {'0', '0.5', '1', '1.5', '2', '2.5'};
WaveformAmp = ([0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163]*0.5); %Each Subjects torque target
day1order = [1:1:6;6,2,5,4,1,3;1:1:6;1:1:6;1:1:6;1:1:6;1:1:6;1:1:6;1:1:6;1:1:6;1:1:6];% Subjects trial order for day 1
day2order = [1,4,2,5,3,6;4,3,1,6,2,5;1,6,3,2,7,5;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6;1,4,2,5,3,6];% Subjects trial order for day 2
if trials == 'all' % Pull out trials of interest
    if TestingDay == 1
    trials = [10:10:60];
    elseif TestingDay == 2 
        trials = [0, 0.5, 1, 1.5, 2, 2.5];
    end
else
end


if TestingDay == 1

  load ('TorqueProcDay1.mat');  % Load cut intervals and torque data     
   for j = 1:length(trials) % Loop through trials we want analyzed       
    for k = 1:length(angles) % Loop through angles
        if angles(k) == trials(j)% Only analyze data if trials we want analyzed match angles 
        
            if (subNumber == 2) % Subject 2 has weird data formatting
            o = day1order(subNumber, k);% Get trial being analyzed
            TE = torque{i,o};% Select torque trial of interest
            Ecut = Exercisecutint{i,o};% Get exercise cut intervals
            Ecut = round(Ecut);
            Ecutdiff = Ecut(2)-Ecut(1);% Get sample difference between exercise cut intervals
            ETcut = TE(end-Ecutdiff:end,:);% Cut torque exercise data
            targetexc=target{i,o};% Select target trial of interest
            targetcut = targetexc(end-Ecutdiff:end,:)+WaveformAmp(subNumber);% Cut target data
            Day1ExcTorqueAvg = mean (ETcut);% Get mean of cut exercise data
            
            Gcut = Gravitycutint{i,o};% Get gravity cut intervals
            Gcut = round(Gcut);
            Gcutdiff = Gcut(2)-Gcut(1);% Get sample difference between gravity cut intervals
            GTcut = TE(1:Gcutdiff,:);% Cut gravity data
            Day1GravTorqueAvg = mean (GTcut);% Get mean of cut gravity data
            
            % findpeaks
            T = Day1GravTorqueAvg-ETcut; 
            maxPKS = findpeaks(T);
            minPKS = findpeaks(-T);
            
            thresh = WaveformAmp(subNumber);

            maxPKS = maxPKS(maxPKS>thresh); % only consider larger than 10
            minPKS = minPKS(minPKS>-thresh); % only consider smaller than 10

            Tmin(k) = -mean(minPKS);
            Tmax(k) = mean(maxPKS);
            
            % determine torque rate
            fs = 200;
            lower = 0.5;
            upper = 1.5;
            Tdot = grad5(T(:),1/fs);        
            idx = Tdot>0 & (T > WaveformAmp(subNumber)*lower) & (T < WaveformAmp(subNumber)*upper);

            Tdoti = nan(size(Tdot));
            Ti = nan(size(T));

            Ti(idx) = T(idx);
            Tdoti(idx) = Tdot(idx);

            Tmdot(k) = mean(Tdoti,'omitnan');

        else
            TE = torque{i,k}; % Select torque trial of interest
            targetex = target{i,k}; % Select target trial of interest
            Ecut = (Exercisecutint{i,k}*200);% Get exercise cut interval
            Ecut = round(Ecut);
            ETcut = (TE(Ecut(1):Ecut(2),:));% Cut exercise data
            targetcut = targetex(Ecut(1):Ecut(2),:)+WaveformAmp(subNumber); % Cut target and zero
            Day1ExcTorqueAvg = mean (ETcut);% Get mean of cut exercise section

            Gcut = (Gravitycutint{i,k}*200);% Get gravity cut interval
            Gcut = round(Gcut);
            GTcut = (TE(Gcut(1):Gcut(2),:));% Cut gravity data
            Day1GravTorqueAvg = mean (GTcut);% Get mean of cut gravity section
            
            % findpeaks
            T = Day1GravTorqueAvg-ETcut; 
            maxPKS = findpeaks(T);
            minPKS = findpeaks(-T);
            
            thresh = WaveformAmp(subNumber);

            maxPKS = maxPKS(maxPKS>thresh); % only consider larger than 10
            minPKS = minPKS(minPKS>-thresh); % only consider smaller than 10

            % amplitude
            Tmin(k) = -mean(minPKS);
            Tmax(k) = mean(maxPKS);
            
            % determine torque rate
            fs = 200;
            lower = 0.5;
            upper = 1.5;
            Tdot = grad5(T(:),1/fs);        
            idx = Tdot>0 & (T > WaveformAmp(subNumber)*lower) & (T < WaveformAmp(subNumber)*upper);

            Tdoti = nan(size(Tdot));
            Ti = nan(size(T));

            Ti(idx) = T(idx);
            Tdoti(idx) = Tdot(idx);

            Tmdot(k) = mean(Tdoti,'omitnan');
            end

            % Zero torque according to gravity
            Day1NetTorqueCut = (Day1GravTorqueAvg - ETcut); 
            NetTorqueAvg(k) =  (Day1GravTorqueAvg-Day1ExcTorqueAvg);
        
        %Plot figure
        if plotgraph == 'yes'
        H = 70+k;
        if ishandle(H), close(H); end
        figure(H);
        sgtitle((sprintf('Subject %d: %d Degrees-2Hz',subNumber,angles(k))));
        
        subplot(121);
        plot(TE*N_per_volt); hold on
        plot(GTcut*N_per_volt); hold on
        yline(Day1GravTorqueAvg*N_per_volt,'r','LineWidth', 2);
        if (subNumber == 1)||(subNumber == 2)
            xline(Ecut(1),'b','LineWidth', 2);xline(Ecut(2),'b','LineWidth', 2);
            xline(Gcut(1),'g','LineWidth', 2);xline(Gcut(2),'g','LineWidth', 2);
            
        end
        legend('Raw Torque Data','Gravity Cut Section','Gravity Average');
        xlabel('Sample Number');ylabel('Knee Torque (Nm)');
        title('Gravity Measure');
        
        subplot(122);
        plot(targetcut*N_per_volt); hold on
        plot(Day1NetTorqueCut*N_per_volt); hold on
        yline(WaveformAmp(subNumber)*N_per_volt,'b','LineWidth', 2);
        yline(NetTorqueAvg(k)*N_per_volt,'r','LineWidth', 2);
        ylabel('Knee Torque (Nm)');xlabel('Sample Number');
        legend('Waveform Target','Net Cut Torque Data','Average Waveform Target','Average Torque Production');
        title('Torque Production');
        end
        else
            continue
        end
    end
end 
% NetTorqueAvg(:,:) = NetTorqueAvg(:,:);        

elseif TestingDay == 2    
         load ('TorqueProcDay2.mat');% Load day 2 cut intervals
    for j = 1:length(trials)    % Loop through trials we want analyzed    
    for k = 1:length(frequencies) % Loop through day 2 frequencies 
        if frequencies(k) == trials(j) % Only analyze trials we want 

        if (subNumber == 2)||(subNumber == 3) % Subject 2 and 3 have weird data formatting
            o = day2order(subNumber, k); % Get trial of interest order
            TE = torque{i,o}; % Get torque for trial of interest
            Ecut = Exercisecutint{i,o};% Get exercise cut intervals
            Ecut = round(Ecut);
            Ecutdiff = round(Ecut(2)-Ecut(1));% Get sample difference between exercise cut intervals
            ETcut = TE(end-Ecutdiff:end,:);% Cut exercise section
            targetexc=target{i,o};% Get target for trial of interest
            targetcut = targetexc(end-Ecutdiff:end,:)+WaveformAmp(subNumber); % Cut and zero target
            Day2ExcTorqueAvg = mean (ETcut); % Get mean of exercise section
            
            Gcut = Gravitycutint{i,o};% Get gravity cut intervals
            Gcut = round(Gcut);
            Gcutdiff = round(Gcut(2)-Gcut(1));% Get sample difference between gravity cut intervals
            GTcut = TE(1:Gcutdiff,:);% Cut gravity section
            Day2GravTorqueAvg = mean (GTcut);% Get mean of gravity section
            
            % findpeaks
            T = Day2GravTorqueAvg-ETcut;  
            maxPKS = findpeaks(T);
            minPKS = findpeaks(-T);
            
            thresh = WaveformAmp(subNumber);

            maxPKS = maxPKS(maxPKS>thresh); % only consider larger than 10
            minPKS = minPKS(minPKS>-thresh); % only consider smaller than 10

            % amplitude
            Tmin(k) = -mean(minPKS);
            Tmax(k) = mean(maxPKS);
            
            % determine torque rate
            fs = 200;
            lower = 0.5;
            upper = 1.5;
            Tdot = grad5(T(:),1/fs);        
            idx = Tdot>0 & (T > WaveformAmp(subNumber)*lower) & (T < WaveformAmp(subNumber)*upper);

            Tdoti = nan(size(Tdot));
            Ti = nan(size(T));

            Ti(idx) = T(idx);
            Tdoti(idx) = Tdot(idx);

            Tmdot(k) = mean(Tdoti,'omitnan');
        else % all other subjects have same data formatting
            o = day2order(subNumber, k);% Get trial of interest order
            TE = torque{i,o};% Get torque for trial of interest
            targetex = target{i,o};% Get target for trial of interest
            Ecut = Exercisecutint{i,o}*200;% Get exercise cut intervals
            Ecut = round(Ecut);
            ETcut = TE(Ecut(1):Ecut(2),:);% Cut exercise section
            targetcut = targetex(Ecut(1):Ecut(2),:)+WaveformAmp(subNumber);% Cut exercise section and zero
            Day2ExcTorqueAvg = mean (ETcut);% Get mean of cut section

            Gcut = Gravitycutint{i,o}*200;% Get gravity cut intervals
            Gcut = round(Gcut);
            GTcut = TE(Gcut(1):Gcut(2),:);
            Day2GravTorqueAvg = mean (GTcut);
            
            % findpeaks
            T = Day2GravTorqueAvg-ETcut; 
            maxPKS = findpeaks(T);
            minPKS = findpeaks(-T);
            
            thresh = WaveformAmp(subNumber);

            maxPKS = maxPKS(maxPKS>thresh); % only consider larger than threshold
            minPKS = minPKS(minPKS>-thresh); % only consider smaller than threshold

            % amplitude
            Tmin(k) = -mean(minPKS);
            Tmax(k) = mean(maxPKS);
            
            % determine torque rate
            fs = 200;
            lower = 0.5;
            upper = 1.5;
            Tdot = grad5(T(:),1/fs);        
            idx = Tdot>0 & (T > WaveformAmp(subNumber)*lower) & (T < WaveformAmp(subNumber)*upper);

            Tdoti = nan(size(Tdot));
            Ti = nan(size(T));

            Ti(idx) = T(idx);
            Tdoti(idx) = Tdot(idx);

            Tmdot(k) = mean(Tdoti,'omitnan');
        end

        % Zero torque according to gravity measure
        Day2NetTorqueCut = Day2GravTorqueAvg - ETcut;
        NetTorqueAvg(k) =  (Day2GravTorqueAvg-Day2ExcTorqueAvg);

        %Plot figure
        if plotgraph == 'yes'
        H = 70+k;
        if ishandle(H), close(H); end
        figure(H);
        sgtitle((sprintf('Subject %d: %s Hz-20 Degrees',subNumber,dispfreq{k})));
        
        subplot(121);
        plot(TE*N_per_volt); hold on
        plot(GTcut*N_per_volt); hold on
        yline(Day2GravTorqueAvg*N_per_volt,'r','LineWidth', 2);
        if (subNumber == 1)||(subNumber == 2)||(subNumber == 3)
            xline(Gcut(1),'b','LineWidth', 2);xline(Ecut(2),'b','LineWidth', 2);
        end
        legend('Raw Torque Data','Gravity Cut Section','Gravity Average');
        xlabel('Sample Number');ylabel('Knee Torque (Nm)');
        title('Gravity Measure');
        
        subplot(122);
        if k > 1
        plot(targetcut*N_per_volt); hold on
        end
        plot(Day2NetTorqueCut*N_per_volt,'color',[0.8500 0.3250 0.0980]); hold on
        yline(WaveformAmp(subNumber)*N_per_volt,'b','LineWidth', 2);
        yline(NetTorqueAvg(k)*N_per_volt,'r','LineWidth', 2);
        ylabel('Knee Torque (Nm)');xlabel('Sample Number');
        if k == 1
        legend('Net Cut Torque Data','Average Waveform Target','Average Torque Production');
        else
        legend('Waveform Target','Net Cut Torque Data','Average Waveform Target','Average Torque Production');
        end
        title('Torque Production');
        end
        else
            continue
    end  
end
end
end
end

 
