# Length-dependent-metabolic-cost: Torque Folder

The following readme is used as a guide through the torque folder which includes the relevant scripts and functions to analyze and process the raw torque data. Below is a flow chart to visualize this process.

![TorqueFlowChart](TorqueFlowChart.jpg)  

## Analysis of main torque data
Raw data starts out as a .csv file that contains the recorded raw time, torque, waveform target, and EMG data. Torque and the waveform target used can be found in the second and third recording channels respectively. The 'AnalyzeTorqueScript.m' script analyzes the raw torque data by filtering it using a cutoff of 100 and then downsampling it by a factor of 10 to create the resulting Matlab variable, there are two variables per subject (one for each testing day). Next the analyzed data is processed using the 'ProcessTorqueScript.m' script. This is done by using the function 'do_TorqueSubjectIndAnalysis.m' which cuts the exercise and gravity intervals for each subject. Next the average of the Net torque for each subject is found where net is seen as the (exercise torque) - (gravitational torque). This data is then visualized within the main processing script to compare subjects generated torque to their waveform target. This visualization issue done using several figures: one for each subject that contains only their data and one for a summary of all the subjects plotted together. The processed torque data is then saved into two variables, one for each day (ProcessedTorqueDay1.mat and ProcessedTorqueDay2.mat). The processed torque data is summarized and visualized using the function 'EMGTorqueRespiratorySummary.m'. 

## Analysis of Torque MVC data
The raw MVC data is analyzed in two ways:

* The first method uses the script 'TorqueMVCAnalysis.m' which analyzes the MVC torque data that will be used to normalize each subjects respective torque data from the main trials. The MVC data is analyzed in the same way as outlined above and is saved as two Matlab variables, one for each day (MVCDay1TorqueData.mat and MVCDay2TorqueData). 
* The second method uses the script 'AllMVCTorqueAnalysis.m' script to analyze each subject's MVC trial in which they performed MVCs against different angles. This MVC data is analyzed in the same way as outlined above and is saved as AllMVCTorqueData.mat. 

## Processed Gravity data
Once the torque data is analyzed in addition to its main processing, the gravity torque is also processed in a separate script 'ProcessGravityIntervals.m'. This is done to look at how the torque's gravity measurement changes with angle for each subject. This data is processed by first being cut into its gravity intervals and then averaged within that interval. The data is saved as 'Day1Gravity.mat'.
