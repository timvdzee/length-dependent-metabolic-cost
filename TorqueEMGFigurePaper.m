clear all; close all; clc
% this script visualizes a complete summary of the processed torque,
% respiratory, and EMG data for both days. A comparison between each days EMG and respiratory 
% data for 2Hz at 20deg from each day is plotted for each subject.

%% Run this first: Variables needed to run sections
angles = 10:10:60;%Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
N_per_volt = 69.71; 
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order
TendegMVC = [1.721,1.601,1.185,1.941,1.164,0.603,1.664,2.024,2.481,1.167,0.652]';
fraction_MVC = 0.5;
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * fraction_MVC * N_per_volt;  


%Exclusion Variables
day1subj_exc = [9]; %Select which subjects to exclude from day 1 analysis   
day1exc = ones(1,11);
day1exc(day1subj_exc) = 0;

day2subj_exc = [9]; %Select which subjects to exclude from day 2 analysis   
day2exc = ones(1,11);
day2exc(day2subj_exc) = 0;

NIRSsubj_exc = [9]; %Select which subjects to exclude from NIRS analysis 
NIRSexc = ones(1,11);
NIRSexc(NIRSsubj_exc) = 0;

gravityintervalsubj_exc = [9]; %Select which subjects to exclude from gravity interval analysis
gravityintervalexc = ones(1,11);
gravityintervalexc(gravityintervalsubj_exc) = 0;

ultrasoundsubj_exc = [4,9,11]; %Select which subjects to exclude from ultrasound analysis
ultrasoundexc = ones(1,11);
ultrasoundexc(ultrasoundsubj_exc) = 0;

%Normalize torque by their torque target voltage
normalize = true;

%Absolute Deviation of torque
absolute = false;

% Save Figures
savefigures = 0;

%% Day 1
% Loading
load('EMGSummaryAnalysisDay1.mat');%EMG data for day 1
load('ProcessedTorqueDay1.mat');%Torque data for day 1
load('RespiratorySummary.mat');%Respiratory data for both days
load('MVCDay1TorqueData.mat');
load('MVCDay2TorqueData.mat');
N = 11;

%Plot Torque data
% Torque_avg = TorqueAvgD1 ./ corfacs(:);    
% elseif absolute, Torque_avg = TorqueAvgD1 - corfacs(:);   
% else,Torque_avg = TorqueAvgD1;
% end
Torque_avgD1 = (TorqueAvgD1./(TendegMVC*N_per_volt))*100; 
Torque_minD1 = (TorqueMinimaD1./(TendegMVC*N_per_volt))*100;
Torque_maxD1 = (TorqueMaximaD1./(TendegMVC*N_per_volt))*100;
TorquerateD1norm = ( TorquerateD1./(TendegMVC*N_per_volt))*100;
% Start to exclude subjects
for i = 1:N
    Torque_avgall(i,:) = Torque_avgD1(i,:);
    TorqueMinall(i,:) = Torque_minD1(i,:);
    TorqueMaxall(i,:) = Torque_maxD1(i,:);
    TorquerateD1all(i,:) = TorquerateD1norm(i,:);
    if day1exc(i) == 0;
    Torque_avgD1(i,:) = nan;
    Torque_minD1(i,:) = nan;
    Torque_maxD1(i,:) = nan;
    TorquerateD1norm(i,:) = nan;
    end
end


for i = 1:N
    Day1RVLEMGAvgall(i,:) = Day1RVLEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RVMEMGAvgall(i,:) = Day1RVMEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    Day1RRFEMGAvgall(i,:) = Day1RRFEMGAvg(i,:);
    if day1exc(i) == 0;
    Day1RRFEMGAvg(i,:) = nan;
    end
end
%Set target line parameters
% if normalize, y = corfacs/corfacs;
% else, y = corfacs;
% end   
WaveformAmp = (WaveformAmp(:)./(TendegMVC*N_per_volt))*100;
y = mean(WaveformAmp);
visualize = 0;
x = [0:1:70]; 

Day1EMGAvg = (Day1RVLEMGAvg + Day1RVMEMGAvg + Day1RRFEMGAvg)/3;
Day1EMGAvgall = (Day1RVLEMGAvgall + Day1RVMEMGAvgall + Day1RRFEMGAvgall)/3;
r2angles = [10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60;10:10:60];
% some fits

%[p2,gofp2] = fit(angles', mean(Day1EMGAvg,'omitnan')','poly2'); 
[AFpEMG] = anyfit(r2angles,Day1EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pEMGR2 = AFpEMG.r2;
pEMGCI = [AFpEMG.coefint(1,1),AFpEMG.coefint(2,1),AFpEMG.coef(4,1)];
pEMGc = [AFpEMG.coef(1,1),AFpEMG.coef(2,1),AFpEMG.offset];
pEMG = polyval(pEMGc,x);
Day1EMGAvgOS = Day1EMGAvg-AFpEMG.coef(3:end)+AFpEMG.offset;

%% Upper row: Variable joint angle, constant frequency (i.e. Day 1)
f = figure(1); color = [get(gca,'colororder'); get(gca,'colororder')];

% Plot Torque data
subplot(231);box on
% if normalize, ylabel('Normalized Average Torque'); yline(y,'r','LineWidth', 2);hold on
% else, ylabel('Average Torque (Nm)'); yline(0,'k-','LineWidth', 2); hold on
% end
yline(y,'k-','LineWidth', 2);hold on
yline(25,'k--','LineWidth', 1);hold on
errorbar(angles,mean(Torque_avgD1,'omitnan'),std(Torque_avgD1,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
errorbar(angles,mean(Torque_minD1,'omitnan'),std(Torque_minD1,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(2,:)); hold on
errorbar(angles,mean(Torque_maxD1,'omitnan'),std(Torque_maxD1,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(3,:)); hold on
xlabel('Knee Angle (deg)'); ylabel('Knee Torque (% MVC)');title('Day 1 Knee Torque'); ylim([0 35]);xlim([5 65]);set(gca,'Xtick',0:10:70)
% cf = gca;cf.XAxis.MinorTick = 'on';cf.YAxis.MinorTickValues = 5:10:65;set(gca,'Xtick',5:10:65) 


% Plot Torque Rate data
subplot(232);box on
s = mean(mean(TorquerateD1norm,'omitnan'));
yline(s,'k-','LineWidth', 2);hold on
errorbar(angles, mean(TorquerateD1norm,'omitnan'),std(TorquerateD1norm,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Knee Angle (deg)'); ylabel('Peak Torque Rate (%MVC s^-^1)'); title('Day 1 Knee torque rate');  ylim([0 300]);xlim([5 65]);
 legend('off');set(gca,'Xtick',0:10:70);%set(gca,'XTickLabel', {'0','','10','','20','','30','','40','','50','','60',''});
% cf = gca;cf.XAxis.MinorTick = 'on';YAxis.MinorTickValues = 5:5:65; 

% Plot EMG data 
hold off;subplot(233); %plot(0:60, polyval(p2, 0:60), 'k','linewidth',2); hold on
pEMG = plot(x,pEMG,'--'); hold on; set(pEMG,'lineWidth',2);set(pEMG,'color','k');
errorbar(angles,mean(Day1EMGAvgOS,'omitnan'),std(Day1EMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
xlim([5 65]);ylim([0 20]); xlabel('Knee Angle (deg)'); ylabel('Average EMG (% MVC)'); title('Day 1 Electromyography');
legend('off');
j = sprintf(' R^2 = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pEMGR2,pEMGc(1),pEMGCI(1),pEMGc(2),pEMGCI(2),pEMGc(3),pEMGCI(3));
text(46,17,j);set(gca,'Xtick',0:10:70);

%% Day 2 plotting 

% Loading
load('EMGSummaryAnalysisDay2.mat');%EMG data for day 2
load('ProcessedTorqueDay2.mat');%Torque data for day 2
load('RespiratorySummary.mat');%Respiratory data for both days
N = 11;
%Plot Torque data
% if normalize, Torque_avg = TorqueAvgD2 ./ corfacs(:);    
% elseif absolute, Torque_avg = TorqueAvgD2 - corfacs(:);   
% else,Torque_avg = TorqueAvgD2;
% end
Torque_avgD2 = (TorqueAvgD2./(TendegMVC*N_per_volt))*100; 
Torque_minD2 = (TorqueMinimaD2./(TendegMVC*N_per_volt))*100;
Torque_maxD2 = (TorqueMaximaD2./(TendegMVC*N_per_volt))*100;
TorquerateD2norm = ( TorquerateD2./(TendegMVC*N_per_volt))*100;
%
[B,I] = sort(EMGfrequencies);
sortedDay2RVLEMGAvg = Day2RVLEMGAvg(:,I);
sortedDay2RVMEMGAvg = Day2RVMEMGAvg(:,I);
sortedDay2RRFEMGAvg = Day2RRFEMGAvg(:,I);

% Start to exclude subjects
for i = 1:N
    Torque_avgall(i,:) = Torque_avgD2(i,:);
    TorqueMinall(i,:) = Torque_minD2(i,:);
    TorqueMaxall(i,:) = Torque_maxD2(i,:);
    TorquerateD2all(i,:) = TorquerateD2norm(i,:);
    if day2exc(i) == 0;
    Torque_avgD2(i,:) = nan;
    Torque_minD2(i,:) = nan;
    Torque_maxD2(i,:) = nan;
    TorquerateD2norm(i,:) = nan;
    end
end

% for i = 1:N
%     VO2fall(i,:) = VO2f(i,:);
%     if day2exc(i) == 0;
%     VO2f(i,:) = nan;
%     end
% end
for i = 1:N
    sortedDay2RVLEMGAvgall(i,:) = sortedDay2RVLEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVLEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RVMEMGAvgall(i,:) = sortedDay2RVMEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RVMEMGAvg(i,:) = nan;
    end
end
for i = 1:N
    sortedDay2RRFEMGAvgall(i,:) = sortedDay2RRFEMGAvg(i,:);
    if day2exc(i) == 0;
    sortedDay2RRFEMGAvg(i,:) = nan;
    end
end

Day2EMGAvg = (sortedDay2RVLEMGAvg + sortedDay2RVMEMGAvg + sortedDay2RRFEMGAvg)/3;
Day2EMGAvgall = (sortedDay2RVLEMGAvgall + sortedDay2RVMEMGAvgall + sortedDay2RRFEMGAvgall)/3;
r2frequencies = [0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5;0:0.5:2.5];

visualize = 0;
x2 = [0:0.1:3];

%[p2,gofp2] = fit(frequencies', mean(Day2EMGAvg,'omitnan')','poly2'); 
[AFpEMG] = anyfit(r2frequencies,Day2EMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pEMGR2 = AFpEMG.r2;
pEMGCI = [AFpEMG.coefint(1,1),AFpEMG.coefint(2,1),AFpEMG.coef(4,1)];
pEMGc = [AFpEMG.coef(1,1),AFpEMG.coef(2,1),AFpEMG.offset];
pEMG = polyval(pEMGc,x2);
Day2EMGAvgOS = Day2EMGAvg-AFpEMG.coef(3:end)+AFpEMG.offset;

%[pRVL,gofpRVL] = fit(frequencies', mean(sortedDay2RVLEMGAvg,'omitnan')','poly2'); 
[AFpRVL] = anyfit(r2frequencies,sortedDay2RVLEMGAvg,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pRVLR2 = AFpRVL.r2;
pRVLCI = [AFpRVL.coefint(1,1),AFpRVL.coefint(2,1),AFpRVL.coef(4,1)];
pRVLc = [AFpRVL.coef(1,1),AFpRVL.coef(2,1),AFpRVL.offset];
pRVL = polyval(pRVLc,x2);
sortedDay2RVLEMGAvgOS = sortedDay2RVLEMGAvg-AFpRVL.coef(3:end)+AFpRVL.offset;
%% Lower row: Variable frequency, constant joint angle (i.e. Day 2)
f = figure(1); color = [get(gca,'colororder'); get(gca,'colororder')];

% Plot Torque data
subplot(234);box on
% if normalize, ylabel('Normalized Average Torque'); yline(y,'r','LineWidth', 2);hold on
% else, ylabel('Average Torque (Nm)'); yline(0,'k-','LineWidth', 2); hold on
% end
yline(y,'k-','LineWidth', 2);hold on
yline(25,'k--','LineWidth', 1);hold on
errorbar(frequencies(2:6),mean(Torque_avgD2(:,2:6),'omitnan'),std(Torque_avgD2(:,2:6),'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
errorbar(frequencies(2:6),mean(Torque_minD2(:,2:6),'omitnan'),std(Torque_minD2(:,2:6),'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(2,:)); hold on
errorbar(frequencies(2:6),mean(Torque_maxD2(:,2:6),'omitnan'),std(Torque_maxD2(:,2:6),'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(3,:)); hold on
errorbar(frequencies(1),mean(Torque_avgD2(:,1),'omitnan'),std(Torque_maxD2(:,1),'omitnan'),'s','LineWidth', 1.5,'markersize',7,'color',[0, 0.5, 0]); hold on
xlabel('Frequency f (Hz)'); ylabel('Knee Torque (% MVC)');title('Day 2 Knee Torque'); ylim([0 35]);xlim([0 3]);set(gca,'Xtick',0:0.5:3);

% Plot Torque Rate  data
subplot(235);box on
plot([0:0.15:3],[0:10:200],'k','linewidth',1); hold on;
errorbar(frequencies(2:6), mean(TorquerateD2norm(:,2:6),'omitnan'),std(TorquerateD2norm(:,2:6),'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
xlabel('Frequency f (Hz)'); ylabel('Peak Torque Rate (% MVC s^-^1)'); title('Day 2 Knee torque rate'); ylim([0 300]);xlim([0 3]);
legend('off');set(gca,'Xtick',0:0.5:3);

% Plot EMG data
subplot(236); %plot(0:.1:3, polyval(p2, 0:.1:3), 'k','linewidth',2); hold on
pEMG = plot(x2,pEMG,'--'); hold on; set(pEMG,'lineWidth',2);set(pEMG,'color','k');xlim([0 3]);
errorbar(frequencies,mean(Day2EMGAvgOS,'omitnan'),std(Day2EMGAvgOS,'omitnan'),'.','linewidth',1.5,'markersize',20,'color',color(1,:)); hold on
ylim([0 20]); xlabel('Frequency f (Hz)'); ylabel('Average EMG (% MVC)'); title('Day 2 Electromyography');
legend('off');
j = sprintf(' R^2 = %.2f\n p1*x^2 + p2*x + p3\n p1 = %.3f±%.3f\n p2 = %.3f±%.3f\n p3 = %.3f±%.3f',pEMGR2,pEMGc(1),pEMGCI(1),pEMGc(2),pEMGCI(2),pEMGc(3),pEMGCI(3));
text(2,17,j);set(gca,'Xtick',0:0.5:3);

set(gcf,'position', [100 100 800 800])
