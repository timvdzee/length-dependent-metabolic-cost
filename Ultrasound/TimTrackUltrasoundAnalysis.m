clear all; close all; clc
% This script is used to analyze each subjects data in TimTrack. Data is
% analyzed in Timtrack using either the Hough or Frangi method and saved
%% Loop through Subjects
redo = 0; % Redo cutting of image parameters
MethodUsed = 1; %1 = hough, 0=Frangi
load('parms.mat');% Load parameters for TimTrack
if redo == 0;
load('TimTrackUltrasoundEstimates.mat','ROI');% Load ROI 
end
parms.cut_image = true; % cut image according to timtrack

% Subject numbers according to testing protocol
subnames = {'Subject 1','Subject 2','Subject 3','Subject 4','Subject 5','Subject 6','Subject 7','Subject 8','Subject 9','Subject 10','Subject 11'};

% Create empty variables to fill
TimTrackThicknessHough = nan(11,14);
TimTrackFlengthHough = nan(11,14);
TimTrackPennationAnglesHough = nan(11,14);

% load('TimTrackUltrasoundEstimates.mat');

% Loop through each subject and analyze ultrasound data in TimTrack
for i = 1:length(subnames) % Loop through each subject

    % Get path
    cd '/Users/billyherspiegel/Documents/Force-Rate/length-dependent-metabolic-cost/Ultrasound';
    folder  = subnames{i}'; % Select folder corresponding to subject
    cd([folder]);% Put folder in path
    files = dir('*jpg');% Select ultrasound image format as .jpg

    if MethodUsed % Select method to use
       parms.apo.method = 'Hough';
    else
        if i == 4 % Subject 4 ultrasound at weird location where Hough doesn't work
            parms.apo.method = 'Hough';
        else
       parms.apo.method = 'Frangi';
        end
    end

    % Select best cut sections and range parameter for each subject 
    if i == 1 % Subject 1 has different best cut sections parameter 
%         parms.apo.method = 'Frangi';
        parms.apo.super.cut = [0.1 0.3];
    else
%         parms.apo.method = 'Hough';
        parms.apo.super.cut = [0.05 0.4];
    end

    if i == 11 % Subject 11 has a different best range parameter 
        parms.fas.range = [3 80];
    else
        parms.fas.range = [8 80];
    end

    % Load ROI for each subject if not redoing ROI cut
    if redo == 0;
    if exist('ROI','var');
        parms.ROI = ROI(:,:,i);
        parms.cut_image = false;
    end
    end

    % Enter into TimTrack for analysis
    [features, parms] = do_TimTrack(files, parms,MethodUsed);

    % Add data into variables 
    if i == 4 % Subject 4 ultrasound at weird location
        if MethodUsed
            TimTrackThicknessHough(i,[1:10,12:14]) = vertcat(features.thickness)';
            TimTrackFlengthHough(i,[1:10,12:14]) = vertcat(features.faslen)';
            TimTrackPennationAnglesHough(i,[1:10,12:14]) = vertcat(features.phi)';
            TimTrackAlphaHough(i,[1:10,12:14]) = vertcat(features.alpha)'; 
            TimTrackBetaHough(i,[1:10,12:14]) = vertcat(features.betha)'; 
        else
            TimTrackThicknessFrangi(i,[1:10,12:14]) = vertcat(features.thickness)';
            TimTrackFlengthFrangi(i,[1:10,12:14]) = vertcat(features.faslen)';
            TimTrackPennationAnglesFrangi(i,[1:10,12:14]) = vertcat(features.phi)';
            TimTrackAlphaFrangi(i,[1:10,12:14]) = vertcat(features.alpha)'; 
            TimTrackBetaFrangi(i,[1:10,12:14]) = vertcat(features.betha)'; 
        end
    else
        if MethodUsed
            TimTrackThicknessHough(i,:) = vertcat(features.thickness);
            TimTrackFlengthHough(i,:) = vertcat(features.faslen);
            TimTrackPennationAnglesHough(i,:) = vertcat(features.phi);
            TimTrackAlphaHough(i,:) = vertcat(features.alpha); 
            TimTrackBetaHough(i,:) = vertcat(features.betha); 
        else
            TimTrackThicknessFrangi(i,:) = vertcat(features.thickness);
            TimTrackFlengthFrangi(i,:) = vertcat(features.faslen);
            TimTrackPennationAnglesFrangi(i,:) = vertcat(features.phi);
            TimTrackAlphaFrangi(i,:) = vertcat(features.alpha); 
            TimTrackBetaFrangi(i,:) = vertcat(features.betha); 
        end
    end
    ROI(:,:,i) = parms.ROI;
end

% Select path to save data into
cd '/Users/billyherspiegel/Documents/Force-Rate/length-dependent-metabolic-cost/Ultrasound';
if MethodUsed
    save('TimTrackUltrasoundEstimatesHough','subnames','TimTrackThicknessHough','TimTrackFlengthHough','TimTrackPennationAnglesHough','TimTrackAlphaHough','TimTrackBetaHough','ROI');
else
    save('TimTrackUltrasoundEstimatesFrangi','subnames','TimTrackThicknessFrangi','TimTrackFlengthFrangi','TimTrackPennationAnglesFrangi','TimTrackAlphaFrangi','TimTrackBetaFrangi','ROI');
end