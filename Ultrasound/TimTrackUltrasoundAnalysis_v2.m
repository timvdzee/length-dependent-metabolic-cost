clear all; close all; clc

% parameters
load('TimTrackUltrasoundEstimates.mat','ROI');
load('parms.mat')


subjectnum = {'Subject 1','Subject 2','Subject 3','Subject 4','Subject 5','Subject 6','Subject 7','Subject 8','Subject 9','Subject 10','Subject 11'};

cd('C:\Users\Tim\Documents\GitHub\length-dependent-metabolic-cost\Ultrasound')

i = 1;

folder  = subjectnum{i}';
cd([folder]);
files = dir('*jpg');

% aponeurosis method
parms.apo.method = 'Frangi';
parms.apo.super.cut = [0.05 0.4];
parms.fas.range = [5 80];

parms.ROI = ROI(:,:,i);
parms.cut_image = false;


%% run TimTrack
[features, parms] = do_TimTrack(files, parms);


%%
if MethodUsed
   parms.apo.method = 'Hough';
else
    if i == 4
        parms.apo.method = 'Hough';
    else
   parms.apo.method = 'Frangi';
    end
end

if i == 1
%         parms.apo.method = 'Frangi';
    parms.apo.super.cut = [0.1 0.3];
else
%         parms.apo.method = 'Hough';
    parms.apo.super.cut = [0.05 0.4];
end
if i == 11
    parms.fas.range = [3 80];
else
    parms.fas.range = [8 80];
end
if redo == 0;
if exist('ROI','var');
    parms.ROI = ROI(:,:,i);
    parms.cut_image = false;
end
end
[features, parms] = do_TimTrack(files, parms,MethodUsed);