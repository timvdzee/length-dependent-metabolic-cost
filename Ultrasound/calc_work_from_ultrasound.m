clear all; close all; clc

% Torque target amplitude
N_per_volt = 69.71; % Nm per volt
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163] * N_per_volt;

load('TimTrackUltrasoundEstimatesFrangi.mat','TimTrackFlengthFrangi','TimTrackAlphaFrangi');

% included subject ids
% remove S4: incorrect probe placement
% remove S9: problem with task performance, excluded from entire study
sid = [1:3, 5:8, 10, 11];
subnames = {'S1', 'S2', 'S3', 'S4', 'S5', 'S6','S7','S8','S9','S10','S11'};

% pix to cm conversion factors
conversion = [139   139   139   138   139   154   150   154   139   154   174];

L_cm = -TimTrackFlengthFrangi(sid,[14,1:10]) ./ conversion(sid)';

Trel = 0:.1:1; % steps of 10 percent

figure; 
for i = 1:size(L_cm,1)
    plot(L_cm(i,:), (Trel * WaveformAmp(i) /.05)); 
    hold on
    
    W(i) = trapz(L_cm(i,:)/100, (Trel * WaveformAmp(i) /.05));
end

xlabel('Fascicle length (cm)')
ylabel('Force (N)')


%%

% leg length
L = [100; 99.5; 96.52; 100; 101.6; 83.8; 96.5; 99; 107.5; 90.5; 92; 99.5]/100;

% relative fascicle length and torque values
Lrel = -TimTrackFlengthFrangi(sid,[14,1:10]) ./ conversion(sid)' ./ L(sid);
Trel = 0:.1:1; % steps of 10 percent

% process fascicle length
Lrel_do = Lrel - mean(Lrel,2); % individual offset
Labs_do = (Lrel_do - mean(Lrel_do(:,1))) * mean(L); % relative to 0 and back to units (m)
SEE = Labs_do .* cosd(TimTrackAlphaFrangi(sid,[14,1:10]));

%% Figure 1: data
if ishandle(1), close(1); end; figure(1)
for i = 1:size(Lrel,1)
    
    figure(1); subplot(2,5,i)
    plot(SEE(i,:), Trel, '.-');
    title(subnames{sid(i)})
    xlabel('Length change (cm)'); ylabel('Force (0-1)')
    grid on
end

%% Figure 2: fit relative data
if ishandle(2), close(2); end; figure(2)

% fit
C = fminsearch(@(c) costfunc(c,SEE, Trel),   [1 1]);

xg_cm = 0:.01:10;
ypred = C(1) * exp(C(2) .* xg_cm) - C(1);
    
% plot individual data
plot(SEE, Trel, '.-'); hold on

% plot fit and mean data
plot(xg_cm(ypred<1), ypred(ypred<1),'r--','linewidth',2)
errorbar(mean(SEE), Trel, [],[], std(SEE), std(SEE),'ko','linewidth',2); hold on
xlabel('Length change (cm)'); ylabel('Force (0-1)')
ylim([0 1]); title('Normalized torque vs. length'); grid on 

%% Figure 3: convert to absolute and calc work done
if ishandle(3), close(3); end; figure(3)

Tamp = mean(WaveformAmp(sid));
xg_m = xg_cm/100; % cm -> m

% torque: relative -> absolute
% when ypred = 1, torque equals Tamp
ypred_Nm = ypred * Tamp;

% moment arm (SEE)
r = .05; % (m)

% ratio of average aponeurosis force to tendon force
corfac = 1;

% calculate force
ypred_N = ypred_Nm ./ r * corfac;

% calculate work
Wfit = cumtrapz(xg_m(ypred<1), ypred_N(ypred<1));

subplot(131);
errorbar(mean(SEE), Trel * Tamp, [],[], std(SEE), std(SEE),'ko','linewidth',1); hold on
plot(xg_cm(ypred<1), ypred_Nm(ypred<1))
xlabel('Length change (cm)'); ylabel('Torque (N-m)'); title('Torque vs. length')

subplot(132);
errorbar(mean(SEE)/100, Trel * Tamp ./ r * corfac, [],[], std(SEE)/100, std(SEE)/100,'ko','linewidth',1); hold on
plot(xg_m(ypred<1), ypred_N(ypred<1))
xlabel('Length change (m)'); ylabel('Force (N)'); title('Force vs. length')

subplot(133);
plot(xg_m(ypred<1), Wfit(ypred<1))
xlabel('Length change (m)'); ylabel('Work (J)'); title('Work vs. length')


%% Fitting function
function[cost] = costfunc(c, L, T)

% average across subjects
L = mean(L);

% prediction
ypred = c(1) * exp(c(2) .* L) - c(1);

% cost
cost = sum((ypred(:)-T(:)).^2);

end
