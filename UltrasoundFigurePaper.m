clear all; close all; clc
%% Run this first: Variables needed to run sections
angles = 10:10:60;%Day 1 angles
symbols = ['+','*','o','s','d','p','^','v','x','h','<','>'];
N_per_volt = 69.71; 
EMGfrequencies = [0,1,2,0.5,1.5,2.5];%EMG Data Frequency Order 
frequencies = [0,0.5,1,1.5,2,2.5];%Torque Data Frequency Order
TendegMVC = [1.721,1.601,1.185,1.941,1.164,0.603,1.664,2.024,2.481,1.167,0.652]';
WaveformAmp = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163]';
for i = 1:11
    diff = WaveformAmp(i)/10;
    torquevalues(i,:) = [0:diff:WaveformAmp(i)];
end
torquevalues = torquevalues*N_per_volt;

%Exclusion Variables
subj_exc = [9,12]; %Select which subjects to exclude from analysis   
exc = ones(1,12);
exc(subj_exc) = 0;

%Exclusion Day 1 data
subj_excD1 = [1,9]; %Select which subjects to exclude from analysis   
excD1 = ones(1,12);
excD1(subj_excD1) = 0;

ultrasoundsubj_exc = [4,9,11]; %Select which subjects to exclude from ultrasound analysis
ultrasoundexc = ones(1,12);
ultrasoundexc(ultrasoundsubj_exc) = 0;

%Normalize torque by their torque target voltage
normalize = true;

%Absolute Deviation of torque
absolute = false;

% Save Figures
savefigures = 0;

%% 

PercentofTarget = (0:10:100);
r2PercentofTarget = [0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;0:10:100;];
subnames = {'S1', 'S2', 'S3', 'S4', 'S5', 'S6','S7','S8','S9','S10','S11'};
subnumber = {'1','2','3','4','5','6','7','8','9','10','11'};
N = length(subnames);
pennationangles = nan(11,11);
pennationchange = nan(11,11);
thickness = nan(11,11);
thicknesschange = nan(11,11);
flength = nan(11,11);
flengthchange = nan(11,11);
visualize = 0;

reprocess = 0;

if reprocess
% Loop through each subject
for i = 1:N
   
    sheetname = subnames{i};
    alpha(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'B2:B12');
    supapo(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'C2:C12');
    pennationangles(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'E2:E12');
    pennationchange(i,:) = pennationangles(i,1:11) - pennationangles(i,1);
    thickness(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'F2:F12');
    thicknesschange(i,:) = thickness(i,1:11) - thickness(i,1);
    flength(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'G2:G12');
    flengthchange(i,:) = flength(i,1:11) - flength(i,1);
    conversion(i,:) = xlsread('UltrasoundManualEstimations.xlsx', sheetname, 'H2');
    
end
for i = 1:11
pennationanglesall(i,:) = pennationangles(i,:);
pennationchangeall(i,:) = pennationchange(i,:);
thicknessall(i,:) = thickness(i,:);
thicknesschangeall(i,:) = thicknesschange(i,:);
flengthall(i,:) = flength(i,:);
flengthchangeall(i,:) = flengthchange(i,:);
alphaall(i,:) = alpha(i,:);
supapoall(i,:) = supapo(i,:);
torquevaluesall(i,:)=torquevalues(i,:)
if ultrasoundexc(i) == 0
    pennationangles(i,:) = nan;
    pennationchange(i,:)= nan;
    thickness(i,:)= nan;
    thicknesschange(i,:)= nan;
    flength(i,:)= nan;
    flengthchange(i,:)= nan;
    alpha(i,:) = nan;
    supapo(i,:) = nan;
    WaveformAmp(i,:) = nan;
    torquevalues(i,:)=nan;
end
end

torquemean = mean(WaveformAmp,'omitnan')*N_per_volt;
torquemeandiff  = torquemean/10;
torqueint = [0:torquemeandiff:torquemean];



% Plot Summary Figure
x = [0:1:100];
%pPen= polyfit(PercentofTarget,mean(pennationchange,'omitnan'),2);
%[pPen,gofpPen] = fit(PercentofTarget', mean(pennationchange,'omitnan')','poly2'); 
% [AFpPen] = anyfit(r2PercentofTarget,pennationchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% pPenR2 = AFpPen.r2;
% pPenCI = [AFpPen.coefint(1,1),AFpPen.coefint(2,1),AFpPen.coef(4,1)];
% pPenc = [AFpPen.coef(1,1),AFpPen.coef(2,1),AFpPen.offset];
% pPen = polyval(pPenc,x);
% pennationchangeOS = pennationchange-AFpPen.coef(3:end)+AFpPen.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pThick,gofpThick] = fit(PercentofTarget', mean(thicknesschange,'omitnan')','poly1'); 
% [AFpThick] = anyfit(r2PercentofTarget,thicknesschange,'visualize',0,'type', 'indiv');
% pThickR2 = AFpThick.r2;
% pThickCI = [AFpThick.coefint(1,1),AFpThick.coef(4,1)];
% pThickc = [AFpThick.coef(1,1),AFpThick.offset];
% pThick = polyval(pThickc,x);
% thicknesschangeOS = thicknesschange-AFpThick.coef(2:end)+AFpThick.offset;

% Tim Track Estimates Hough
load('TimTrackUltrasoundEstimatesHough.mat');
I = [11,[1:1:10]];
% Loop through each subject
for i = 1:N
    TimTrackPennationAnglesabsunsortedHough = TimTrackPennationAnglesHough(i,[1:10,14]);
    TimTrackPennationAnglesabsHough(i,:) = TimTrackPennationAnglesabsunsortedHough(I);
    TimTrackPennationAngleschangeHough(i,:) = TimTrackPennationAnglesabsHough(i,:) - TimTrackPennationAnglesabsHough(i,1);
    TimTrackThicknessabsunsortedHough = TimTrackThicknessHough(i,[1:10,14])./conversion(i,:);
    TimTrackThicknessabsHough(i,:) = TimTrackThicknessabsunsortedHough(I);
    TimTrackThicknesschangeHough(i,:) = TimTrackThicknessabsHough(i,:) - TimTrackThicknessabsHough(i,1);
    TimTrackFlengthabsunsortedHough = TimTrackFlengthHough(i,[1:10,14])./conversion(i,:);
    TimTrackFlengthabsHough(i,:) = TimTrackFlengthabsunsortedHough(I);
    TimTrackFlengthchangeHough(i,:) = TimTrackFlengthabsHough(i,:) - TimTrackFlengthabsHough(i,1);
end

%Exclude subjects
% Loop through each subject
for i = 1:N
    TimTrackPennationAnglesabsunsortedHough = TimTrackPennationAnglesHough(i,[1:10,14]);
    TimTrackPennationAnglesabsHough(i,:) = TimTrackPennationAnglesabsunsortedHough(I);
    TimTrackPennationAngleschangeHough(i,:) = TimTrackPennationAnglesabsHough(i,:) - TimTrackPennationAnglesabsHough(i,1);
    TimTrackThicknessabsunsortedHough = TimTrackThicknessHough(i,[1:10,14])./conversion(i,:);
    TimTrackThicknessabsHough(i,:) = TimTrackThicknessabsunsortedHough(I);
    TimTrackThicknesschangeHough(i,:) = TimTrackThicknessabsHough(i,:) - TimTrackThicknessabsHough(i,1);
    TimTrackFlengthabsunsortedHough = TimTrackFlengthHough(i,[1:10,14])./conversion(i,:);
    TimTrackFlengthabsHough(i,:) = TimTrackFlengthabsunsortedHough(I);
    TimTrackFlengthchangeHough(i,:) = TimTrackFlengthabsHough(i,:) - TimTrackFlengthabsHough(i,1);
    TimTrackAlphaabsunsortedHough = TimTrackAlphaHough(i,[1:10,14]);
    TimTrackAlphaabsHough(i,:) = TimTrackAlphaabsunsortedHough(I);
    TimTrackAlphachangeHough(i,:) = TimTrackAlphaabsHough(i,:) - TimTrackAlphaabsHough(i,1);
    TimTrackBetaabsunsortedHough = TimTrackBetaHough(i,[1:10,14]);
    TimTrackBetaabsHough(i,:) = TimTrackBetaabsunsortedHough(I);
    TimTrackBetachangeHough(i,:) = TimTrackBetaabsHough(i,:) - TimTrackBetaabsHough(i,1);
    
end

%Exclude subjects
for i = 1:11
TimTrackPennationAnglesabsallHough(i,:) = TimTrackPennationAnglesabsHough(i,:);
TimTrackThicknessabseallHough(i,:) = TimTrackThicknessabsHough(i,:);
TimTrackFlengthabsallHough(i,:) = TimTrackFlengthabsHough(i,:);
TimTrackAlphaabsallHough(i,:) = TimTrackAlphaabsHough(i,:);
TimTrackBetaabsallHough(i,:) = TimTrackBetaabsHough(i,:);
TimTrackPennationAnglesallHough(i,:) = TimTrackPennationAngleschangeHough(i,:);
TimTrackThicknessallHough(i,:) = TimTrackThicknesschangeHough(i,:);
TimTrackFlengthallHough(i,:) = TimTrackFlengthchangeHough(i,:);
TimTrackAlphaallHough(i,:) = TimTrackAlphachangeHough(i,:);
TimTrackBetaallHough(i,:) = TimTrackBetachangeHough(i,:);
if ultrasoundexc(i) == 0
    TimTrackPennationAnglesabsHough(i,:)= nan;
    TimTrackThicknessabsHough(i,:)= nan;
    TimTrackFlengthabsHough(i,:)= nan;
    TimTrackAlphaabsHough(i,:)= nan;
    TimTrackBetaabsHough(i,:)= nan;
    TimTrackPennationAngleschangeHough(i,:)= nan;
    TimTrackThicknesschangeHough(i,:)= nan;
    TimTrackFlengthchangeHough(i,:)= nan;
    TimTrackAlphachangeHough(i,:)= nan;
    TimTrackBetachangeHough(i,:)= nan;
end
end

return
% Tim Track Estimates Frangi
load('TimTrackUltrasoundEstimatesFrangi.mat');
I = [11,[1:1:10]];
% Loop through each subject
for i = 1:N
    TimTrackPennationAnglesabsunsortedFrangi = TimTrackPennationAnglesFrangi(i,[1:10,14]);
    TimTrackPennationAnglesabsFrangi(i,:) = TimTrackPennationAnglesabsunsortedFrangi(I);
    TimTrackPennationAngleschangeFrangi(i,:) = TimTrackPennationAnglesabsFrangi(i,:) - TimTrackPennationAnglesabsFrangi(i,1);
    TimTrackThicknessabsunsortedFrangi = TimTrackThicknessFrangi(i,[1:10,14])./conversion(i,:);
    TimTrackThicknessabsFrangi(i,:) = TimTrackThicknessabsunsortedFrangi(I);
    TimTrackThicknesschangeFrangi(i,:) = TimTrackThicknessabsFrangi(i,:) - TimTrackThicknessabsFrangi(i,1);
    TimTrackFlengthabsunsortedFrangi = TimTrackFlengthFrangi(i,[1:10,14])./conversion(i,:);
    TimTrackFlengthabsFrangi(i,:) = TimTrackFlengthabsunsortedFrangi(I);
    TimTrackFlengthchangeFrangi(i,:) = TimTrackFlengthabsFrangi(i,:) - TimTrackFlengthabsFrangi(i,1);
end

%Exclude subjects
% Loop through each subject
for i = 1:N
    TimTrackPennationAnglesabsunsortedFrangi = TimTrackPennationAnglesFrangi(i,[1:10,14]);
    TimTrackPennationAnglesabsFrangi(i,:) = TimTrackPennationAnglesabsunsortedFrangi(I);
    TimTrackPennationAngleschangeFrangi(i,:) = TimTrackPennationAnglesabsFrangi(i,:) - TimTrackPennationAnglesabsFrangi(i,1);
    TimTrackThicknessabsunsortedFrangi = TimTrackThicknessFrangi(i,[1:10,14])./conversion(i,:);
    TimTrackThicknessabsFrangi(i,:) = TimTrackThicknessabsunsortedFrangi(I);
    TimTrackThicknesschangeFrangi(i,:) = TimTrackThicknessabsFrangi(i,:) - TimTrackThicknessabsFrangi(i,1);
    TimTrackFlengthabsunsortedFrangi = TimTrackFlengthFrangi(i,[1:10,14])./conversion(i,:);
    TimTrackFlengthabsFrangi(i,:) = TimTrackFlengthabsunsortedFrangi(I);
    TimTrackFlengthchangeFrangi(i,:) = TimTrackFlengthabsFrangi(i,:) - TimTrackFlengthabsFrangi(i,1);
    TimTrackAlphaabsunsortedFrangi = TimTrackAlphaFrangi(i,[1:10,14]);
    TimTrackAlphaabsFrangi(i,:) = TimTrackAlphaabsunsortedFrangi(I);
    TimTrackAlphachangeFrangi(i,:) = TimTrackAlphaabsFrangi(i,:) - TimTrackAlphaabsFrangi(i,1);
    TimTrackBetaabsunsortedFrangi = TimTrackBetaFrangi(i,[1:10,14]);
    TimTrackBetaabsFrangi(i,:) = TimTrackBetaabsunsortedFrangi(I);
    TimTrackBetachangeFrangi(i,:) = TimTrackBetaabsFrangi(i,:) - TimTrackBetaabsFrangi(i,1);
    
end

%Exclude subjects
for i = 1:11
TimTrackPennationAnglesabsallFrangi(i,:) = TimTrackPennationAnglesabsFrangi(i,:);
TimTrackThicknessabseallFrangi(i,:) = TimTrackThicknessabsFrangi(i,:);
TimTrackFlengthabsallFrangi(i,:) = TimTrackFlengthabsFrangi(i,:);
TimTrackAlphaabsallFrangi(i,:) = TimTrackAlphaabsFrangi(i,:);
TimTrackBetaabsallFrangi(i,:) = TimTrackBetaabsFrangi(i,:);
TimTrackPennationAnglesallFrangi(i,:) = TimTrackPennationAngleschangeFrangi(i,:);
TimTrackThicknessallFrangi(i,:) = TimTrackThicknesschangeFrangi(i,:);
TimTrackFlengthallFrangi(i,:) = TimTrackFlengthchangeFrangi(i,:);
TimTrackAlphaallFrangi(i,:) = TimTrackAlphachangeFrangi(i,:);
TimTrackBetaallFrangi(i,:) = TimTrackBetachangeFrangi(i,:);
if ultrasoundexc(i) == 0
    TimTrackPennationAnglesabsFrangi(i,:)= nan;
    TimTrackThicknessabsFrangi(i,:)= nan;
    TimTrackFlengthabsFrangi(i,:)= nan;
    TimTrackAlphaabsFrangi(i,:)= nan;
    TimTrackBetaabsFrangi(i,:)= nan;
    TimTrackPennationAngleschangeFrangi(i,:)= nan;
    TimTrackThicknesschangeFrangi(i,:)= nan;
    TimTrackFlengthchangeFrangi(i,:)= nan;
    TimTrackAlphachangeFrangi(i,:)= nan;
    TimTrackBetachangeFrangi(i,:)= nan;
end
end
 save('UltrasoundSummary','subnames','torquevalues','conversion','flengthchange','TimTrackFlengthchangeHough','TimTrackFlengthchangeFrangi','alpha','TimTrackAlphaabsHough','TimTrackAlphaabsFrangi','WaveformAmp')
else
    load('UltrasoundSummary.mat');
end

torquemean = mean(WaveformAmp,'omitnan')*N_per_volt;
torquemeandiff  = torquemean/10;
torqueint = [0:torquemeandiff:torquemean];
x = [0:1:100];

% Plot Summary Figure

%pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
%[pFas,gofpFas] = fit(PercentofTarget', mean(flengthchange,'omitnan')','poly2'); 

%pPen= polyfit(PercentofTarget,mean(pennationchange,'omitnan'),2);
%[pTPen,gofpTPen] = fit(PercentofTarget', mean(TimTrackPennationAngleschange,'omitnan')','poly2'); 
% [AFpTPen] = anyfit(r2PercentofTarget,TimTrackPennationAnglesabs,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% pPenR2 = AFpTPen.r2;
% pTPenCI = [AFpTPen.coefint(1,1),AFpTPen.coefint(2,1),AFpTPen.coef(4,1)];
% pTPenc = [AFpTPen.coef(1,1),AFpTPen.coef(2,1),AFpTPen.offset];
% pTPen = polyval(pTPenc,x);
% TimTrackPennationAnglesabsOS = TimTrackPennationAnglesabs-AFpTPen.coef(3:end)+AFpTPen.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pTThick,gofpTThick] = fit(PercentofTarget', mean(TimTrackThicknesschange,'omitnan')','poly1'); 
% [AFpTThick] = anyfit(r2PercentofTarget,TimTrackThicknessabs,'visualize',0,'type', 'indiv');
% pTThickR2 = AFpTThick.r2;
% pTThickCI = [AFpTThick.coefint(1,1),AFpTThick.coef(4,1)];
% pTThickc = [AFpTThick.coef(1,1),AFpTThick.offset];
% pTThick = polyval(pTThickc,x);
% TimTrackThicknessabsOS = TimTrackThicknessabs-AFpTThick.coef(2:end)+AFpTThick.offset;

%pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
%[pTFas,gofpTFas] = fit(PercentofTarget', mean(TimTrackFlengthchange,'omitnan')','poly2'); 
% [AFpTFas] = anyfit(r2PercentofTarget,TimTrackFlengthabs,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% pTFasR2 = AFpTFas.r2;
% pTFasCI = [AFpTFas.coefint(1,1),AFpTFas.coefint(2,1),AFpTFas.coef(4,1)];
% pTFasc = [AFpTFas.coef(1,1),AFpTFas.coef(2,1),AFpTFas.offset];
% pTFas = polyval(pTFasc,x);
% TimTrackFlengthabsOS = TimTrackFlengthabs-AFpTFas.coef(3:end)+AFpTFas.offset;

% [AFpTPenC] = anyfit(torquevalues,TimTrackPennationAngleschangeHough,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% pPenR2C = AFpTPenC.r2;
% pTPenCIC = [AFpTPenC.coefint(1,1),AFpTPenC.coefint(2,1),AFpTPenC.coef(4,1)];
% pTPencC = [AFpTPenC.coef(1,1),AFpTPenC.coef(2,1),AFpTPenC.offset];
% pTPenC = polyval(pTPencC,x);
% TimTrackPennationAnglesOS = TimTrackPennationAngleschangeHough-AFpTPenC.coef(3:end)+AFpTPenC.offset;

%pThick= polyfit(PercentofTarget,mean(thicknesschange,'omitnan'),1);
%[pTThick,gofpTThick] = fit(PercentofTarget', mean(TimTrackThicknesschange,'omitnan')','poly1'); 
% [AFpTThickC] = anyfit(r2PercentofTarget,TimTrackThicknesschange,'visualize',0,'type', 'indiv');
% pTThickR2C = AFpTThickC.r2;
% pTThickCIC = [AFpTThickC.coefint(1,1),AFpTThickC.coef(4,1)];
% pTThickcC = [AFpTThickC.coef(1,1),AFpTThickC.offset];
% pTThickC = polyval(pTThickcC,x);
% TimTrackThicknessOS = TimTrackThicknesschange-AFpTThickC.coef(2:end)+AFpTThickC.offset;

% pFas= polyfit(PercentofTarget,mean(flengthchange,'omitnan'),2);
% [pTFas,gofpTFas] = fit(PercentofTarget', mean(TimTrackFlengthchange,'omitnan')','poly2'); 
% [AFpTFasC] = anyfit(torquevalues,TimTrackFlengthchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% pTFasR2C = AFpTFasC.r2;
% pTFasCIC = [AFpTFasC.coefint(1,1),AFpTFasC.coefint(2,1),AFpTFasC.coef(4,1)];
% pTFascC = [AFpTFasC.coef(1,1),AFpTFasC.coef(2,1),AFpTFasC.offset];
% pTFasC = polyval(pTFascC,x);
% TimTrackFlengthOS = TimTrackFlengthchange-AFpTFasC.coef(3:end)+AFpTFasC.offset;

[AFpFas] = anyfit(torquevalues,flengthchange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
pFasR2 = AFpFas.r2;
pFasCI = [AFpFas.coefint(1,1),AFpFas.coefint(2,1),AFpFas.coef(4,1)];
pFasc = [AFpFas.coef(1,1),AFpFas.coef(2,1),AFpFas.offset];
pFas = polyval(pFasc,x);
flengthchangeOS = flengthchange-AFpFas.coef(3:end)+AFpFas.offset;

manTTmean = (TimTrackFlengthchangeHough + flengthchange)/2;
[AFpmanTTmean] = anyfit(torquevalues,manTTmean,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
AFpmanTTmeanR2 = AFpmanTTmean.r2;
AFpmanTTmeanCI = [AFpmanTTmean.coefint(1,1),AFpmanTTmean.coefint(2,1),AFpmanTTmean.coef(4,1)];
AFpmanTTmeanc = [AFpmanTTmean.coef(1,1),AFpmanTTmean.coef(2,1),AFpmanTTmean.offset];
pmanTTmean = polyval(AFpmanTTmeanc,x);
manTTmeanOS = manTTmean-AFpmanTTmean.coef(3:end)+AFpmanTTmean.offset;

xtor = [0:1:30];
cfas_exp = [2.1974   -0.0687];
faslenTim = cfas_exp(1)*exp(cfas_exp(2).*xtor) - cfas_exp(1);

TimTrackFlengthchangeHoughR = rmmissing(TimTrackFlengthchangeHough);
TimTrackFlengthchangeFrangiR = rmmissing(TimTrackFlengthchangeFrangi);
torquevaluesR = rmmissing(torquevalues);
% X = mean(torquevaluesR);
% Y = mean(TimTrackFlengthchangeR);


for i = 1:N
    faslenInd(i,:) = cfas_exp(1)*exp(cfas_exp(2).*torquevalues(i,:))- cfas_exp(1);
    dTTFL(i,:) = mean(TimTrackFlengthchangeHough(i,:)-faslenInd(i,:));
end
TTFLoffset = mean(dTTFL,'omitnan');
TimTrackFlengthOSHou = TimTrackFlengthchangeHough-dTTFL+TTFLoffset;
pred = cfas_exp(1)*exp(cfas_exp(2).*torqueint) - cfas_exp(1);
Y = rmmissing(TimTrackFlengthOSHou);
predY = rmmissing(faslenInd);
SSE = sum((Y-predY).^2);
SST = sum((Y - mean(Y(:))).^2);%discuss with Art, can also take the mean for each subject 
TTHFitR2 = 1-SSE/SST

for i = 1:N
    faslenInd(i,:) = cfas_exp(1)*exp(cfas_exp(2).*torquevalues(i,:))- cfas_exp(1);
    dTTFL(i,:) = mean(TimTrackFlengthchangeFrangi(i,:)-faslenInd(i,:));
end
TTFLoffset = mean(dTTFL,'omitnan');
TimTrackFlengthOSFran = TimTrackFlengthchangeFrangi-dTTFL+TTFLoffset;
pred = cfas_exp(1)*exp(cfas_exp(2).*torqueint) - cfas_exp(1);
Y = rmmissing(TimTrackFlengthOSFran);
predY = rmmissing(faslenInd);
SSE = sum((Y-predY).^2);
SST = sum((Y - mean(Y(:))).^2);%discuss with Art, can also take the mean for each subject 
TTFFitR2 = 1-SSE/SST

[AFpTimTrackFLChange] = anyfit(torquevalues, TimTrackFlengthchangeFrangi,'visualize',0,'type', 'indiv','function',@(x)[exp(cfas_exp(2)*x)]);
AFpTimTrackFLChangeR2 = AFpTimTrackFLChange.r2;
AFpTimTrackFLChangeCI = [AFpTimTrackFLChange.coefint(1,1),AFpTimTrackFLChange.coefint(2,1),AFpTimTrackFLChange.coef(4,1)];
AFpTimTrackFLChangec = [AFpTimTrackFLChange.coef(1,1),AFpTimTrackFLChange.offset]
pTTFL = AFpTimTrackFLChangec(1)*exp(cfas_exp(2).*xtor) - AFpTimTrackFLChangec(1);
TimTrackFlengthOS = TimTrackFlengthchangeFrangi-AFpTimTrackFLChange.coef(2:end)+AFpTimTrackFLChange.offset;

% ft = @(b,x)b(1)*exp(b(2)*x)-b(1);% 
% tbl = table(torquevaluesR(:), TimTrackFlengthchangeHoughR(:));
% TTFLexp = fitnlm(tbl,ft,cfas_exp);
% TTFLexpc= TTFLexp.Coefficients{:, 'Estimate'}
% faslen = TTFLexpc(1)*exp(TTFLexpc(2).*xtor)-TTFLexpc(1);
% TTFitR2 = TTFLexp.Rsquared.Ordinary;


% [ManFit] = anyfit(torquevalues,flengthchangeOS,'visualize',0,'type', 'indiv','function',...
%     @(x)[exp(x)]);
% ManAF  = flengthchangeOS - ManFit.coef(2:end);
% whichstats = {'rsquare','adjrsquare'};
% Manstats = regstats(ManFit.coef(2:end),ManAF,whichstats);
% ManFitR2 = ManFit.r2;
% [TTFit] = anyfit(torquevalues,TimTrackFlengthOS,'visualize',0,'type', 'indiv','function',@(x)[2.1974*exp((-0.0687).*x) - 2.1974]);
% TTFitR2 = TTFit.r2;
% [AvgFit] = anyfit(torquevalues,manTTmeanOS,'visualize',0,'type', 'indiv','function',@(x)[2.1974*exp((-0.0687).*x) - 2.1974]);
% AvgFitR2 = AvgFit.r2;


if ishandle(9), close(9); end
figure(9);
color = [get(gca,'colororder'); get(gca,'colororder')];

subplot(131);box on
pFas = plot(xtor,faslenTim,'--'); hold on; set(pFas,'lineWidth',2);set(pFas,'color','k');
errorbar(torqueint,mean(flengthchangeOS,'omitnan'),std(flengthchangeOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('Knee Torque (N-m)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('Manual Fascicle Length Change');
% j = sprintf('R-square = %.2f',ManFitR2);
% text(20,1,j);ylim([-3.5 1.5]);

subplot(132);box on
pTFasC = plot(xtor,faslenTim,'--'); hold on; set(pTFasC,'lineWidth',2);set(pTFasC,'color','k');
errorbar(torqueint,mean(TimTrackFlengthOSHou,'omitnan'),std(TimTrackFlengthOSHou,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('Knee Torque (N-m)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('TimTrack Fascicle Length Change Hough');
j = sprintf('R-square = %.2f',TTHFitR2);
text(20,1,j);ylim([-3.5 1.5]);

subplot(133);box on
pTFasC = plot(xtor,pTTFL,'--'); hold on; set(pTFasC,'lineWidth',2);set(pTFasC,'color','k');
errorbar(torqueint,mean(TimTrackFlengthOS,'omitnan'),std(TimTrackFlengthOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('Knee Torque (N-m)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('TimTrack Fascicle Length Change Frangi');
j = sprintf('R-square = %.2f',AFpTimTrackFLChangeR2);
text(20,1,j);ylim([-3.5 1.5]);

% subplot(133);box on
% pmanTTmean = plot(xtor,faslenTim,'--'); hold on; set(pmanTTmean,'lineWidth',2);set(pmanTTmean,'color','k');
% errorbar(torqueint,mean(manTTmeanOS,'omitnan'),std(manTTmeanOS,'omitnan'),'.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
% legend('off');
% xlabel('Knee Torque (N-m)');ylabel('Fascicle Length Change \Delta Lfas (cm)');title('Averaged Fascicle Length Change');
% j = sprintf('R-square = %.2f',AvgFitR2);
% text(20,1,j);ylim([-3.5 1.5]);

%% SEE length 
FSEEAvg = (torqueint/0.05);%torque/momentarm (5cm)
FSEE = (torquevalues/0.05);
xFSEE = [0:1:600];
avgalpha = (alpha+TimTrackAlphaabsFrangi)/2;

for i = 1:11
    SEEChange(i,:) = -((flengthchange(i,:)).*(cosd(alpha(i,:))));
    TimTrackSEEChangeFrangi(i,:)=-((TimTrackFlengthchangeFrangi(i,:).*cosd(TimTrackAlphaabsFrangi(i,:))));
    TimTrackSEEChangeHough(i,:) = -((TimTrackFlengthchangeHough(i,:).*cosd(TimTrackAlphaabsHough(i,:))));
end


[AFpSEEChange] = anyfit(FSEE,SEEChange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
AFpSEEChangeR2 = AFpSEEChange.r2;
AFpSEEChangeCI = [AFpSEEChange.coefint(1,1),AFpSEEChange.coefint(2,1),AFpSEEChange.coef(4,1)];
AFpSEEChangec = [AFpSEEChange.coef(1,1),AFpSEEChange.coef(2,1),AFpSEEChange.offset];
pSEEChange = polyval(AFpSEEChangec,x);
SEEChangeOS = SEEChange-AFpSEEChange.coef(3:end)+AFpSEEChange.offset;


% [AFpTimTrackSEEChange] = anyfit(FSEE,TimTrackSEEChange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% AFpTimTrackSEEChangeR2 = AFpTimTrackSEEChange.r2;
% AFpTimTrackSEEChangeCI = [AFpTimTrackSEEChange.coefint(1,1),AFpTimTrackSEEChange.coefint(2,1),AFpTimTrackSEEChange.coef(4,1)];
% AFpTimTrackSEEChangec = [AFpTimTrackSEEChange.coef(1,1),AFpTimTrackSEEChange.coef(2,1),AFpTimTrackSEEChange.offset];
% pTimTrackSEEChange = polyval(AFpTimTrackSEEChangec,x);
% TimTrackSEEChangeOS = TimTrackSEEChange-AFpTimTrackSEEChange.coef(3:end)+AFpTimTrackSEEChange.offset;


% [AFpMeanSEEChange] = anyfit(FSEE,MeanSEEChange,'visualize',0,'type', 'indiv','function',@(x)[x.^2,x]);
% AFpMeanSEEChangeR2 = AFpMeanSEEChange.r2;
% AFpMeanSEEChangeCI = [AFpMeanSEEChange.coefint(1,1),AFpMeanSEEChange.coefint(2,1),AFpMeanSEEChange.coef(4,1)];
% AFpMeanSEEChangec = [AFpMeanSEEChange.coef(1,1),AFpMeanSEEChange.coef(2,1),AFpMeanSEEChange.offset];
% pMeanSEEChange = polyval(AFpMeanSEEChangec,x);
% MeanSEEChangeOS = MeanSEEChange-AFpMeanSEEChange.coef(3:end)+AFpMeanSEEChange.offset;

% [ManFitSEE] = anyfit(FSEE,SEEChangeOS,'visualize',0,'type', 'indiv','function',@(x)[(-2.2880)*exp((-0.0036).*x) - (-2.2880)]);
% ManFitSEER2 = ManFitSEE.r2;
% [TTFitSEE] = anyfit(FSEE,TimTrackSEEChangeOS,'visualize',0,'type', 'indiv','function',@(x)[(-2.2880)*exp((-0.0036).*x) - (-2.2880)]);
% TTFitSEER2 = TTFitSEE.r2;
% [AvgFitSEE] = anyfit(FSEE,MeanSEEChangeOS,'visualize',0,'type', 'indiv','function',@(x)[(-2.2880)*exp((-0.0036).*x) - (-2.2880)]);
% AvgFitSEER2 = AvgFitSEE.r2;   

cten_exp =[ -2.2880,-0.0036];
SEElenTim = cten_exp(1)*exp(cten_exp(2).*xFSEE) - cten_exp(1);

TimTrackSEEchangeR = rmmissing(TimTrackSEEChangeFrangi);
FSEER = torquevaluesR/0.05;
% X = mean(FSEER);
% Y = mean(TimTrackSEEchangeR);

for i = 1:N
    SEElenInd(i,:) = cten_exp(1)*exp(cten_exp(2).*FSEE(i,:))- cten_exp(1);
    dTTSEE(i,:) = mean(TimTrackSEEChangeFrangi(i,:)-SEElenInd(i,:));
end
TTSEEoffset = mean(dTTSEE,'omitnan');
TimTrackSEEChangeOS = TimTrackSEEChangeFrangi-dTTSEE+TTSEEoffset;
pred = cten_exp(1)*exp(cten_exp(2).*FSEEAvg) - cten_exp(1);
Y = rmmissing(TimTrackSEEChangeOS);
predY = rmmissing(SEElenInd);
SSE = sum((Y-predY).^2);
SST = sum((Y - mean(Y(:))).^2);
TTHFitR2 = 1-SSE/SST

[AFpTimTrackSEEChange] = anyfit(FSEE,TimTrackSEEChangeFrangi,'visualize',0,'type', 'indiv','function',@(x)[exp(cten_exp(2)*x)]);
AFpTimTrackSEEChangeR2 = AFpTimTrackSEEChange.r2;
AFpTimTrackSEEChangeCI = [AFpTimTrackSEEChange.coefint(1,1),AFpTimTrackSEEChange.coefint(2,1),AFpTimTrackSEEChange.coef(4,1)];
AFpTimTrackSEEChangec = [AFpTimTrackSEEChange.coef(1,1),AFpTimTrackSEEChange.offset]
pTTSEE = AFpTimTrackSEEChangec(1)*exp(cten_exp(2).*xFSEE) - AFpTimTrackSEEChangec(1);
TimTrackSEEChangeOS = TimTrackSEEChangeFrangi-AFpTimTrackSEEChange.coef(2:end)+AFpTimTrackSEEChange.offset;


% ft = @(b,x)b(1)*exp(b(2).*x)-b(1);
% tbl = table(FSEER(:), TimTrackSEEchangeR(:));
% SEElenexp = fitnlm(tbl,ft,cten_exp);
% SEElenexpc= SEElenexp.Coefficients{:, 'Estimate'}


if ishandle(10), close(10); end
figure(10);
color = [get(gca,'colororder'); get(gca,'colororder')];

subplot(131);box on
pSEE = plot(SEElenTim,xFSEE,'--'); hold on; set(pSEE,'lineWidth',2);set(pSEE,'color','k');
errorbar(mean(SEEChangeOS,'omitnan'),FSEEAvg,std(SEEChangeOS,'omitnan'),'horizontal','.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('SEE length change \Delta LSEE (cm)');ylabel('SEE force FSEE (N)');title('Manual Series elastic element (SEE)');
% j = sprintf('R-square = %.2f',ManFitSEER2);
%text(-1.5,500,j);
xlim([-2 3.5]);

subplot(132);box on
pSEE = plot(SEElenTim,xFSEE,'--'); hold on; set(pSEE,'lineWidth',2);set(pSEE,'color','k');
errorbar(mean(TimTrackSEEChangeHough,'omitnan'),FSEEAvg,std(TimTrackSEEChangeHough,'omitnan'),'horizontal','.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('SEE length change \Delta LSEE (cm)');ylabel('SEE force FSEE (N)');title('Tim Track Series elastic element (SEE) Hough');
j = sprintf('R-square = %.2f',TTHFitR2);
text(-1.5,500,j);
xlim([-2 3.5]);

subplot(133);box on
pSEE = plot(pTTSEE,xFSEE,'--'); hold on; set(pSEE,'lineWidth',2);set(pSEE,'color','k');
errorbar(mean(TimTrackSEEChangeOS,'omitnan'),FSEEAvg,std(TimTrackSEEChangeOS,'omitnan'),'horizontal','.','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on
legend('off');
xlabel('SEE length change \Delta LSEE (cm)');ylabel('SEE force FSEE (N)');title('Tim Track Series elastic element (SEE) Frangi');
j = sprintf('R-square = %.2f',AFpTimTrackSEEChangeR2);
text(-1.5,500,j);
xlim([-2 3.5]);

%% Work per contraction
torquework = 24.23;
FSEEWork = round(torquework/0.05);
WorkCont = cumtrapz((pTTSEE*0.01),xFSEE);
place = find(xFSEE==FSEEWork)
workper = WorkCont(place) 

if ishandle(11), close(11); end
figure(11);box on
plot(xFSEE,WorkCont,'-','LineWidth', 1.5,'markersize',20,'color',color(1,:)); hold on;
xlabel('SEE force FSEE (N)');ylabel('Fasicle Work (J)');title('Manual Mechanical Work');
xline(FSEEWork,'--','color','k','LineWidth', 1.5);yline(workper,'--','color','k','LineWidth', 1.5);

