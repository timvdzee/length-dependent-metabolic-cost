clear all; 

load('AllMVCTorqueData.mat')

%% torque - angle
phis = 10:10:60;
T = AllMVCTorqueAvg * 69.71;

if ishandle(1), close(1); end; figure(1)
plot(phis, T'); hold on
plot(phis, mean(T),'k-','linewidth',2)

ylabel('Torque (N-m)'); xlabel('Knee angle (deg)')

%% force - passive length
r = .05;
F = T / r;
L0 = .05 + (phis/180*pi) * r;

if ishandle(2), close(2); end; figure(2)
plot(L0, F'); hold on
plot(L0, mean(F),'k-','linewidth',2)

ylabel('Force (N)'); xlabel('Passive length (m)')

%% force - active length
% lump everything together because we also do that for SEE stress-strain
% curve

k = 1e-6;
L = L0 - mean(F) .* k;

if ishandle(3), close(3); end; figure(3)
plot(L, mean(F))

%% relative
if ishandle(4), close(4); end; figure(4)
plot(L, mean(F)/max(mean(F)),'o'); hold on

errorbar(L, mean(F)/max(mean(F)), std(F)/max(mean(F)),'.')


f = @(c,x) c(1)*(x/c(2)).^2 - 2*c(1)*(x/c(2)) + c(1) + 1;

c = [-1/0.56^2 .105];

plot(L, f(c, L))


