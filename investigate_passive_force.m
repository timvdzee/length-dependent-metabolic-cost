cd('C:\Users\timvd\Documents\length-dependent-metabolic-cost\Torque\Processed')

load('Day1Gravity.mat')

Torque = Day1GravTorqueAvg *  69.71;

if ishandle(1), close(1); end
figure(1)
plot(angles, Torque); hold on

% estimate Fg
Fg10 = Torque(:,1);

Fg0 = Fg10 ./ cosd(10);

plot(zeros(11,1), Fg0,'o')

% predict other torque
Fg_pred = repmat(Fg0,1,6) .* cosd(repmat(angles,11,1));

for i = 1:11
    if ishandle(i), close(i); end
    figure(i)
    subplot(121)
    plot(angles, Fg_pred(i,:)); hold on
    plot(angles, Torque(i,:))
    
    Tpassive(i,:) = (Torque(i,:)- Fg_pred(i,:));
    
    subplot(122);
    plot(angles, Tpassive(i,:))
end


Fpassive = Tpassive/.05;
figure(20)
errorbar(angles, mean(Fpassive), std(Fpassive),'o')
