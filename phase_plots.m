clear all; close all; clc


cd('C:\Users\timvd\Documents\length-dependent-metabolic-cost\EMG\summary-data')

fcs = [5, 10];

for fc = 1:2
    
load(['meanpeakemg_fc', num2str(fcs(fc)),'.mat'])

sid = [1:8, 10:11];
corfacs = [0.430,0.400,0.296,0.485,0.291,0.150,0.416,0.506,0.620,0.292,0.163];

f = .5:.5:2.5;
T = 1./f;

for i = 1:5
    
    t = linspace(0, T(i), 101);
    
    figure(1)
    subplot(2,5,i); plot(t, mean(squeeze(meanalltorque(:,i+1,sid)) ./ corfacs(sid) * 25, 2)); hold on; axis([0 T(i) 0 25])
    title(['Freq = ', num2str(f(i)),' Hz']); ylabel('Torque (%MVC)'); box off
    
    [Tmax(i,fc), j] = max(mean(squeeze(meanalltorque(:,i+1,sid)),2));
    [Emax(i,fc), k] = max(mean(meanallrvlemg(:,i+1,sid),3));
    
%     Tmax2(:,i,fc) = max(squeeze(meanalltorque(:,i+1,sid)));
%     Emax2(:,i,fc) = max(squeeze(meanallrvlemg(:,i+1,sid)));
    
    subplot(2,5,i+5); plot(t, mean(meanallrvlemg(:,i+1,sid), 3)*100); hold on; axis([0 T(i) 0 25])
    xlabel('Time (s)'); ylabel('EMG (%MVC)'); box off
    
    if fc == 1
        subplot(2,5,i)
        xline(t(j),'k--'); 
        
        subplot(2,5,i+5)
        xline(t(j),'k--'); xline(t(k),'k--')
    end
    
end

% figure(2)
% subplot(131); plot(f, Tmax,'o-');
% subplot(132); plot(f, Emax,'o-');
% subplot(133); plot(f, Emax./Tmax,'o-');

end